package cn.com.do1;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.common.framebase.dqdp.IBaseVO;
import cn.com.do1.common.util.reflation.BeanHelper;
import cn.com.do1.common.util.reflation.ClassTypeUtil;
import cn.com.do1.component.merchant.merchant.model.TbCzMerchantPO;
import cn.com.do1.component.merchant.merchant.vo.TbCzMerchantVO;
import cn.com.do1.component.systemmgr.person.model.TbDqdpPersonPO;
import cn.com.do1.component.systemmgr.user.model.BaseUserVO;

/**
 * @author LX
 * @date 2015年7月19日
 * @desc
 */
public class PoTest {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, InvocationTargetException, Exception,
            BaseException {



        TbCzMerchantPO po = new TbCzMerchantPO();
        TbCzMerchantVO vo = new TbCzMerchantVO();
        vo.setDiscount("111111.111");

        BeanHelper.copyFormatProperties(po, vo, false);

        System.out.println("po  " + po);
        System.out.println("vo  " + vo);

    }

    public List<String> _getTableNames() {
        return null;
    }
}
