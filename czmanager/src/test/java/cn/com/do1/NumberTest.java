package cn.com.do1;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;

public class NumberTest {

    public static void main(String[] args) {


        DecimalFormat fm = new DecimalFormat(".00");
        // System.out.println(fm.format(100.312312));

        System.out.println(new SimpleDateFormat("yyyy-MM-dd 00:00:00").format(new Date()));
        System.out.println(new SimpleDateFormat("yyyy-MM-dd 23:59:59").format(new Date()));

    }

}
