package cn.com.do1;

import java.io.IOException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.entity.StringEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.com.do1.common.util.security.SecurityUtil;



/* @RunWith(SpringJUnit4ClassRunner.class) */
/* @ContextConfiguration(locations = {"classpath:applicationContext.xml"}) */
public class ClientTest {


    public static String url = "http://localhost:8080/czmanager/merchant/clientPay.action";

    public static void bodyisnull() throws HttpException, IOException, Exception {

        HttpClient httpClient = new HttpClient();
        JSONArray ar = new JSONArray();
        JSONObject tmp = new JSONObject();
        tmp.put("moblie", "111111111");
        tmp.put("type", "20M");
        ar.add(tmp);
        
        JSONObject tmp1 = new JSONObject();
        tmp1.put("moblie", "11111111111");
        tmp1.put("type", "2110M");
        ar.add(tmp1);

        JSONObject json = new JSONObject();
        json.put("userAccount", "111111");
        json.put("userPassword", SecurityUtil.getMD5String("111111"));
        json.put("payPassword", SecurityUtil.getMD5String("111111"));
        json.put("mobile", ar);
        PostMethod post = new PostMethod(url + "?data=" + json.toString());
        httpClient.executeMethod(post);

        System.out.println(post.getResponseBodyAsString());
    }

    public static void main(String[] args) throws HttpException, IOException, Exception {
        bodyisnull();
    }
}
