<%@page import="cn.com.do1.dqdp.core.ConfigMgr"%>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="../../common/dqdp_common.jsp"%>
<%
    response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			//    if(request.getProtocol().split("\\/").equals("http")){
			//        response.sendRedirect("https://"+request.getRequestURI());
			//    };
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<link href="${baseURL}/themes/${style}/css/login.css" rel="stylesheet"
		type="text/css" />
	<link href="${baseURL}/themes/${style}/css/bass.css" rel="stylesheet"
		type="text/css" />
	<link
		href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css"
		rel="stylesheet" type="text/css" />
	<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
	<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
	<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
	<script
		src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jquery-ui-1.8.custom.min.js"></script>
	<!--
   按钮函数 -->
</head>
<body>
	<div id="register_dialog" class="easyui-dialog">
		<form id="id_form_login" action="${baseURL}/merchant/merchantAction!addMerchant.action" method="post" autocomplete="off">
	 		<input type="hidden" name="userIds" id="userIds" value=""/>
     		<input type="hidden" name="orgId" value="<%=ConfigMgr.get("systemmgr", "org_merchant", "deaf8bfe-c403-4b83-84c0-ca1e8db80a05")%>"/>
     		<input type="hidden" name="roleIds" id="roleIds" value="<%=ConfigMgr.get("systemmgr", "merchantRole", "78e033cb-b9e8-48a6-9a8e-5890069bc529")%>"/>
     		<input type="hidden" name="isInternal" value="1"/>
			<!-- <div class="h130 overflow" style="height: 100px;"></div> -->
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">登陆名称：</li>
				<li class="fleft w300"><input type="text" name="j_username"
					style="width: 200px;" id="j_username" class="w280 p6"
					  valid='{"must":true,"tip":"登陆名称"}' /></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">登陆密码：</li>
				<li class="fleft w300"><input type="password" name="j_password"
					style="width: 200px;" id="j_password" class="w280 p6"
					valid="{must:true,tip:'登陆密码'}"   /></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">确定登陆密码：</li>
				<li class="fleft w300"><input type="password" name="j_confirmPassword"
					style="width: 200px;" id="j_confirmPassword" class="w280 p6"
					valid="{must:true,tip:'确定登陆密码'}"   /></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">交易密码：</li>
				<li class="fleft w300"><input type="password" name="j_payPassword"
					style="width: 200px;" id="j_payPassword" class="w280 p6"
					valid="{must:true,tip:'交易密码'}" /></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">确定交易密码：</li>
				<li class="fleft w300"><input type="password" name="j_confirmPayPassword"
					style="width: 200px;" id="j_confirmPayPassword" class="w280 p6"
					valid="{must:true,tip:'确定交易密码'}"   /></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">联系手机：</li>
				<li class="fleft w300"><input type="text" name="j_mobile"
					style="width: 200px;" id="j_mobile" class="w280 p6"
					valid="{must:true,tip:'联系手机'}"   /></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">联系QQ：</li>
				<li class="fleft w300"><input type="text" name="j_qq"
					style="width: 200px;" id=""j_qq"" class="w280 p6"
					valid="{must:true,tip:'联系QQ'}"  /></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">验证码：</li>
				<li class="fleft w300"><input type="text" name="j_code"
					id="j_code" class="w150 p6" valid="{must:true,tip:'验证码'}" /> <img
					onclick="changeCode();" id="img_code"
					src="${baseURL}/PictureCheckCode" height="26" width="80"
					style="margin-bottom: -8px;" /> <a style="cursor: pointer;"
					onclick="changeCode();">换一张</a></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 fleft mr10"></li>
				<li class="fleft w300 overflow"><a href="javascript:doLogin()"
					class="login_btn fleft mr30"></a> <a
					href="${baseURL }/jsp/default/login/login_in.jsp"
					class="reset_btn fleft"></a></li>
			</ul>
		</form>
	</div>

	<div id="result_dialog" class="easyui-dialog" style="display: none;">
		<div class="h130 overflow" style="height: 70px;"></div>
		<ul class="overflow">
	        <li class="fleft w300 overflow">
	             <a href="javascript:;" class="fleft mr30" style="color: #3d79ae;text-decoration: underline;font-size: 18px;" >注册成功！</a>
	             <a href="${baseURL }/jsp/default/login/login_in.jsp" class="fleft" style="color: #3d79ae;text-decoration: underline;font-size: 18px;" >返回登陆页面</a>
	        </li>
        </ul>
	</div>

	
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		$("#register_dialog").dialog({
			title : '代理商注册页面',
			width : 600,
			height : 500,
			closed : false,
			cache : false,
			modal : true,
			resizable : false
		});
	});

	function changeCode() {
		$("#img_code").attr("src",
				"${baseURL}/PictureCheckCode?code=" + Math.random());
	}

	function onKeyUp(event) {
		var e = event ? event : (window.event ? window.event : null);
		if (e.keyCode == 13)
			doLogin();
	}
	function doLogin() {
		var dqdp = new Dqdp();
        if (dqdp.submitCheck("id_form_login")) {
        	if(!checkPassword()){
        		alert('两次输入的密码不一致！');
        		return;
        	}
			if(!checkPayPassword()){
				alert('两次输入的支付密码不一致！');
				return;
        	}
            // 提交数据
            $('#id_form_login').ajaxSubmit({
                dataType:'json',
                success:function (result) {
                    if ("0" == result.code) {
                    	$("#register_dialog").dialog("close");
                		$("#result_dialog").dialog({
                			title : '注册结果',
                			width : 250,
                			height : 250,
                			closed : false,
                			cache : false,
                			modal : true,
                			resizable : false
                		});
                    } else {
                        alert(result.desc);
                    }
                },
                error:function () {
                    alert('新增人员失败，请稍候再试');
                }
            });
        }
	}
	function doLoginTest() {
		$("#j_username").val("");
		$("#j_password").val("");
	}
	
	function checkPassword() {
        return $('#j_password').val() == $('#j_confirmPassword').val();
    }
    function checkPayPassword() {
        return $('#j_payPassword').val() == $('#j_confirmPayPassword').val();
    }

</script>