<%@page import="cn.com.do1.dqdp.core.ConfigMgr"%>
<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="../../common/dqdp_moblie_loginFilter.jsp" %>
<%@include file="../../common/dqdp_common.jsp" %>
<%
    response.setHeader("Pragma", "No-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);
//    if(request.getProtocol().split("\\/").equals("http")){
//        response.sendRedirect("https://"+request.getRequestURI());
//    };
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link href="${baseURL}/themes/${style}/css/login.css" rel="stylesheet" type="text/css"/>
    <link href="${baseURL}/themes/${style}/css/bass.css" rel="stylesheet" type="text/css"/>
    <link href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
	<script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jquery-ui-1.8.custom.min.js"></script>
	<style type="text/css">
	
/* 	.ui-dialog-titlebar-close{ */
/*     	display: none; */
/* 	} */
/* 	#ui-dialog-title-forgetpasswordDialog .ui-dialog-titlebar-close{ */
/* 		display: display; */
/* 	} */
	</style>
    <!--
   按钮函数 -->

</head>
<body>
<script type="text/javascript">
    var lastError = "${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}";
    if (lastError)alert("${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}");
</script>
<form id="id_form_login" action="${baseURL}/j_spring_security_check" method="post" autocomplete="off">
    <input type='hidden' name='_spring_security_remember_me' value="true" />
    <input type='hidden' name='dqdp_csrf_token' value="${sessionScope.dqdp_csrf_token}" />
    <div class="loginDiv" style="height: 500px;">
        <div class="h130 overflow" style="height: 100px;"></div>
        <ul class="overflow lh30 h50">
            <li class="w170 h30 f_black fcolor_3d79ae f20 tr fleft mr10">帐号：</li>
            <li class="fleft w300">
                <input type="text" name="j_username" id="j_username" class="w280 p6" onkeydown="onKeyUp(event)" valid='{"must":true,"tip":"帐号"}' value='${sessionScope['SPRING_SECURITY_LAST_USERNAME']}'/>
            </li>
        </ul>
        <ul class="overflow lh30 h50">
            <li class="w170 h30 f_black fcolor_3d79ae f20 tr fleft mr10">密码：</li>
            <li class="fleft w300">
                <input type="password" name="j_password" id="j_password" class="w280 p6" valid="{must:true,tip:'密码'}" onkeydown="onKeyUp(event)"/>
            </li>
        </ul>
        <ul class="overflow lh30 h50">
            <li class="w170 h30 f_black fcolor_3d79ae f20 tr fleft mr10">验证码：</li>
            <li class="fleft w300">
                <input type="text" name="j_code" id="j_code" class="w150 p6" valid="{must:true,tip:'验证码'}"/>
                 <img onclick="changeImageCode();" id="img_code" src="${baseURL}/PictureCheckCode" height="26" width="80" style="margin-bottom: -8px;" />
                 <a style="cursor: pointer;" onclick="changeImageCode();">换一张</a>
            </li>
        </ul>
        <ul class="overflow">
	        <li class="w170 h30 fleft mr10"></li>
	            <li class="fleft w300 overflow">
	                <a href="javascript:;" class="register fleft mr30" style="color: #3d79ae;text-decoration: underline;" >我要申请代理商</a>
	                <a href="javascript:forgetpassword();" class="fleft" style="color: #3d79ae;text-decoration: underline;" >忘记密码</a>
	            </li>
        </ul>
        <ul class="overflow lh30 h50">
            <li class="w170 h30 fleft mr10"></li>
            <li class="fleft w300 overflow">
                <a href="javascript:doLogin()" class="login_btn fleft mr30"></a>
                <a href="javascript:doLoginTest()" class="reset_btn fleft"></a>
            </li>
        </ul>
    </div>
</form>
<div id="forgetpasswordDialog" title="提示" class="easyui-dialog"  style="display: none;">

	<div class="h130 overflow" style="height: 20px;"></div>
	<ul class="overflow">
        <li class="fleft w300 overflow">
             <div style="color: #3d79ae;text-decoration: underline;font-size: 16px;"><%=ConfigMgr.get("systemmgr", "forgetpasswordTips","忘记密码，请拨打110") %></div>
        </li>
   </ul>

</div> 

<div id="register_dialog" class="easyui-dialog" style="display: none;">
		<form id="id_form_register" action="${baseURL}/merchant/merchantAction!addMerchant.action" method="post" autocomplete="off">
	 		<input type="hidden" name="userIds" id="userIds" value=""/>
     		<input type="hidden" name="orgId" value="<%=ConfigMgr.get("systemmgr", "org_merchant", "deaf8bfe-c403-4b83-84c0-ca1e8db80a05")%>"/>
     		<input type="hidden" name="roleIds" id="roleIds" value="<%=ConfigMgr.get("systemmgr", "merchantRole", "78e033cb-b9e8-48a6-9a8e-5890069bc529")%>"/>
     		<input type="hidden" name="isInternal" value="1"/>
			<!-- <div class="h130 overflow" style="height: 100px;"></div> -->
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">登录账号：</li>
				<li class="fleft w300"><input type="text" name="j_reg_username"
					style="width: 140px;" id="j_reg_username" class="w280 p6"
					  valid="{must:true,tip:'登录账号',fieldType:'pattern',regex:'^\\w{6,20}$'}" /><span  style="color: red;margin-left: 10px;">*6-20位数字或字母组合</span></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">登录密码：</li>
				<li class="fleft w300"><input type="password" name="j_reg_password"
					style="width: 140px;" id="j_reg_password" class="w280 p6"
					valid="{must:true,tip:'登录密码',fieldType:'pattern',regex:'^\\w{6,20}$'}"   /><span  style="color: red;margin-left: 10px;">*6-20位数字或字母组合</span></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">确定登陆密码：</li>
				<li class="fleft w300"><input type="password" name="j_reg_confirmPassword"
					style="width: 140px;" id="j_reg_confirmPassword" class="w280 p6"
					valid="{must:true,tip:'确定登陆密码',fieldType:'pattern',regex:'^\\w{6,20}$'}"   /><span  style="color: red;margin-left: 10px;">*6-20位数字或字母组合</span></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">交易密码：</li>
				<li class="fleft w300"><input type="password" name="j_reg_payPassword"
					style="width: 140px;" id="j_reg_payPassword" class="w280 p6"
					valid="{must:true,tip:'交易密码',fieldType:'pattern',regex:'^\\w{6,20}$'}" /><span  style="color: red;margin-left: 10px;">*6-20位数字或字母组合</span></li></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">确定交易密码：</li>
				<li class="fleft w300"><input type="password" name="j_reg_confirmPayPassword"
					style="width: 140px;" id="j_reg_confirmPayPassword" class="w280 p6"
					valid="{must:true,tip:'确定交易密码',fieldType:'pattern',regex:'^\\w{6,20}$'}"   /><span  style="color: red;margin-left: 10px;">*6-20位数字或字母组合</span></li></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">联系手机：</li>
				<li class="fleft w300"><input type="text" name="j_reg_mobile"
					style="width: 140px;" id="j_reg_mobile" class="w280 p6"
					valid="{must:true, fieldType:'pattern', regex:'^\\d{11}$', tip:'联系手机'}"   /><span  style="color: red;margin-left: 10px;">*11位手机号码</span></li></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">联系QQ：</li>
				<li class="fleft w300"><input type="text" name="j_reg_qq"
					style="width: 140px;" id="j_reg_qq" class="w280 p6"
					valid="{must:true, fieldType:'pattern', regex:'^\\d{6,12}$', tip:'联系QQ'}"  /><span  style="color: red;margin-left: 10px;">*6-12位数字</span></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 f_black fcolor_3d79ae f18 tl fleft mr10">验证码：</li>
				<li class="fleft w300"><input type="text" name="j_code"
					id="j_code" class="w150 p6" valid="{must:true,tip:'验证码'}" /> <img
					onclick="changeRegisterCode();" id="img_codeRegister"
					src="${baseURL}/PictureCheckCode" height="26" width="80"
					style="margin-bottom: -8px;" /> <a style="cursor: pointer;"
					onclick="changeRegisterCode();">换一张</a></li>
			</ul>
			<ul class="overflow lh30 h50">
				<li class="w170 h30 fleft mr10"></li>
				<li class="fleft w300 overflow"><a href="javascript:;"
					class="regieter_btn fleft mr30"></a> <a
					href="javascript:;"
					class="registerReturn_btn fleft"></a></li>
			</ul>
		</form>
	</div>

	<!-- <div id="result_dialog" class="easyui-dialog" style="display: none;">
		<div class="h130 overflow" style="height: 50px;"></div>
		<ul class="overflow">
	        <li class="  w300 overflow">
	        	 <a href="javascript:;" class="  mr30" style="color: #3d79ae; font-size: 18px;margin-left: 60px;color: red;" >注册成功！</a> 
	        </li>
	        <div class="h130 overflow" style="height: 50px;"></div>
	        <li class="  w300 overflow">
	        	 <a href="javascript:registerReturn();" class=" " style="color: #3d79ae;text-decoration: underline;font-size: 18px;margin-left: 50px;" >返回登陆页面</a> 
	        </li>
        </ul>
	</div> -->
</body>
</html>
<script type="text/javascript">
	
	$(document).ready(function(){
		
		//代理商注册
		$(".register").click(function(){
			
			register();
		});
		
		//注册
		$(".regieter_btn").click(function(){
			
			doRegister();
			changeImageCode();
		});
		
		//注册返回
		$(".registerReturn_btn").click(function(){
				
			registerClose();
			changeImageCode();
		});
	});
	
	//****注册start*****//
	
	function resultDialogClose(){
		$('#result_dialog').dialog('close');
	}
	
	function registerClose(){
		$("#register_dialog").dialog("close");
	}
	//****注册end*****//
	
	//忘记密码提示
	function forgetpassword(){
		$('#forgetpasswordDialog').dialog();
	}
	
	//登陆验证码
	function changeImageCode(){
		
		 $("#img_code").attr("src", "${baseURL}/PictureCheckCode?code="+Math.random());
	}
	
	//注册验证码
	function changeRegisterCode() {
		
		$("#img_codeRegister").attr("src","${baseURL}/PictureCheckCode?code=" + Math.random());
	}
	
	function register(){
		changeRegisterCode();//刷新验证码
		$("#register_dialog").dialog({
			title : '代理商注册页面',
			width : 600,
			height : 500,
			closed : false,
			cache : false,
			modal : true,
			resizable : false
		});
		$(".ui-icon-closethick").click(function(){
			changeImageCode();
		});
	}

    function onKeyUp(event) {
        var e = event ? event : (window.event ? window.event : null);
        if (e.keyCode == 13)doLogin();
    }
    function doLogin() {
        var dqdp = new Dqdp();
        if (dqdp.submitCheck("id_form_login"))
            document.getElementById("id_form_login").submit();
    }
    function doLoginTest() {
        $("#j_username").val("");
        $("#j_password").val("");
    }

    var _topDocument = getTopFrame(this).parent.document;
    if (_topDocument != document)
        _topDocument.location.href = "${baseURL}/jsp/default/login/login_in.jsp";
    function getTopFrame($win) {
        var pw = getWindowOpener($win);
        if (undefined != pw)
            $win = getTopFrame(pw);
        return $win;
    }

    function getWindowOpener($win) {
        return $win.opener;
    }

    function doAjaxLogin() {
        $('#id_form_login').ajaxSubmit({
            dataType:'json',
            success:function (result) {
                alert(result.desc);
            },
            error:function(result){
                alert("通讯故障");
            }
        });
    }
    function checkPassword() {
        return $('#j_reg_password').val() == $('#j_reg_confirmPassword').val();
    }
    function checkPayPassword() {
        return $('#j_reg_payPassword').val() == $('#j_reg_confirmPayPassword').val();
    }
    
    function doRegister() {
		var dqdp = new Dqdp();
        if (dqdp.submitCheck("id_form_register")) {
        	if(!checkPassword()){
        		alert('两次输入的密码不一致！');
        		return;
        	}
			if(!checkPayPassword()){
				alert('两次输入的支付密码不一致！');
				return;
        	}
            // 提交数据
            $('#id_form_register').ajaxSubmit({
                dataType:'json',
                success:function (result) {
                    if ("0" == result.code) {
                    	$("#register_dialog").dialog("close");
                    	$("input[type='text']").val("");
                    	$("input[type='password']").val("");
                    	_alert("注册结果","注册成功！");
                    	
                		/* $("#result_dialog").dialog({
                			title : '注册结果',
                			width : 250,
                			height : 250,
                			closed : false,
                			cache : false,
                			modal : true,
                			resizable : false
                		}); */
                    } else {
                        alert(result.desc);
                    }
                },
                error:function () {
                    alert('新增人员失败，请稍候再试');
                }
            });
        }
	}
</script>