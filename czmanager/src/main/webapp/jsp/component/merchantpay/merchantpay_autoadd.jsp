<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value="merchandiseStatus"></jsp:param>
    <jsp:param name="permission" value="userList"></jsp:param>
    <jsp:param name="mustPermission" value="merchandiseAdd"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>新增</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <link href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form action="${baseURL}/merchantpay/merchantpayAction!addTbCzMerchantPayPO.action?userId=${param.userId }" dataSrc='${baseURL}/merchant/merchantAction!viewBaseUserDetailVO.action?personId=${param.personId}' method="post" id="id_form_add">
    <table class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead>
            <tr>
                <th colspan="4">商户充值充值</th>
            </tr>
        </thead>
        <tbody>
            <%-- <input type="hidden" name="tbCzMerchantPayPO.tbMerchatPayUserid" id="userId" value="${param.userId }" /> --%>
            <input type="hidden" name="modelCode"/>
            <tr>
                <td width="120" class="tdBlue">代理商用户：</td>
                <td width="85%">
                	<input readonly="readonly" type="text" name="baseUserVO.userName" id="userName" valid="{must:true,length:50,tip:'代理商用户'}" />
                	
                </td>
            </tr>
             <tr>
                <td width="120" class="tdBlue">代理商余额（单元 元）：</td>
                <td name="merchantVO.money" > 
                </td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">充值金额：</td>
                <td width="85%"><input id="money" class="form120px" name="tbCzMerchantPayPO.tbMerchatPayMoney" type="text" valid="{must:true, fieldType:'pattern', regex:'^\\d+(.?\\d{1,2})?$', tip:'充值金额'}"/>
                	<span  style="color: red;margin-left: 10px;">*小数点2位有效数字</span>
                </td>
            </tr>
            <tr>
	            <td width="120" class="tdBlue">支付密码：</td>
	            <td width="280" colspan="3" ><input class="form120px" name="newPassword" type="password"
	                           valid="{must:true, fieldType:'pattern', regex:'^\\w{6,20}$', tip:'密码凭证'}"/>
	                           <span  style="color: red;margin-left: 10px;">6-11位数字或字母组合，由于充值金额存在风险，请输入正确凭证密码才允许充值！</span>
	                           </td>
        	</tr>
        	<tr height="30"> </tr>
        </tbody>
    </table>
</form>

<div class="toolBar">
    <div align="center">
        <input class="btn4" type="button" onclick="doSave();" value="充值"/>
        <input class="btn4" type="button" onclick="javascript:history.back();" value="返 回"/>
    </div>
</div>

<!--工具栏 end-->
<script type="text/javascript">
    function doSave() {
    	 var dqdp = new Dqdp();
         if (dqdp.submitCheck("id_form_add")) {
        	 var messgae="确定为代理商:" + $("#userName").val()+" 充值：" + $("#money").val()+"元吗？";
         	if(confirm(messgae)){
         		 _doCommonSubmit("id_form_add",null,{ok:function(){history.back();},fail:function(){}});
         	}
         }
    }
    
    /**
     * 用户选择
     */
    function selectUser() {
        window.open('selectUser.jsp', '用户选择', 'top=100,left=100,height=480, width=700, toolbar=no,scrollbars=yes, menubar=no, resizable=yes,location=no, status=no');
    }
    
    function updateUser(userID,userName) {
        $('#userId').val(userID);
        $('#userName').val(userName);
    }


</script>
</body>
</html>
