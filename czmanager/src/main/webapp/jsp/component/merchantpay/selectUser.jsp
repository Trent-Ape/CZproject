<%@page import="cn.com.do1.dqdp.core.ConfigMgr"%>
<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="permission" value="userAdd,userEdit,userDel,userView,userChangePassword,userList,userExport"></jsp:param>
    <jsp:param name="mustPermission" value="userList"></jsp:param>
    <jsp:param name="dict" value="userStatus,personSex,discount"></jsp:param>
</jsp:include>  
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>用户管理</title>

    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script type="text/javascript" src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
    <script type="text/javascript" src="${baseURL}/js/do1/common/common.js"></script>
    <script src="${baseURL}/js/DatePicker/WdatePicker.js"></script>
    <style type="text/css">
    </style>
    <script type="text/javascript">
    </script>
</head>

<body>
<!--头部 end-->

<div>
    <form action="" method="post" id="user_search">
    	<input type="hidden" name="orgId"  value="<%=ConfigMgr.get("systemmgr", "org_merchant", "deaf8bfe-c403-4b83-84c0-ca1e8db80a05")%>"/>
        <div class="searchWrap">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="searchLeft"></td>
                    <td class="searchBg">
                        <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="60" height="30">账号名称：</td>
                                            <td width="140"><input class="form120px" name="searchValue.userName" id="searchUserName" type="text" keySearch="keySearchFunc()"/></td>
                                            <td width="60" height="30">状态：</td>
                                            <td width="140"><select width="100" id="searchStatus" name="searchValue.status" dictType="userStatus" /></td>
                                            <td><input  class="btnQuery" type="button" value="查询" permission="userList" onclick="javascript:listUser(1, currOrgId);"/></td></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="searchRight"></td>
                </tr>
                <tr>
                    <td class="searchButtomLeft"></td>
                    <td class="searchButtom"></td>
                    <td class="searchButtoRight"></td>
                </tr>
            </table>
        </div>
    </form>
    <div class="pageDown" id="topIdPage"></div>
     <div class="operation" id="id_user_toolbar">
        
    </div>
    <div id="user_list"></div>
    <div class="pageDown" id="downIdPage"></div>
    <div class="toolBar">
    <div align="center">
        <input class="btn4" type="button" value="确定选择" id="confirmBtn"/>
    </div>
</div>
</div>

<script type="text/javascript">
    function doSearch() {
        window.location.href = 'orgUserList.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token;
    }

    var currOrgId;

    $(document).ready(function () {
        currOrgId = '<%=ConfigMgr.get("systemmgr", "org_merchant", "deaf8bfe-c403-4b83-84c0-ca1e8db80a05")%>';
        if (!_isNullValue(currOrgId)) {
            listUser(1, currOrgId);
        }
    });

    function keySearchFunc() {
        listUser(1, currOrgId);
    }

    function cleanForm() {
        $("#user_search :input").not(":button,:reset,:submit,:hidden").val("");
    }
    
  	 function ajaxExport(){
  		 
  		 	if($("#id_topIdPage_page").size()!=0){
  		 		$("#user_search").attr("action","${baseURL}/merchant/merchantAction!export.action");
  	  	        $("#user_search").submit();
  		 	} else{
  		 		_alert("提示","没有数据可导出");
  		 	}
  	 }

    function listUser(pageIndex, orgId) {
        // 设置当前机构id
        if (orgId != undefined) {
            currOrgId = orgId;
        }
        $("#id_user_toolbar").css("display", "block");
        $.ajax({
            url:'${baseURL}/merchant/merchantAction!listPersonByOrgId.action',
            type:'post',
            data:{
                'page':pageIndex,
                'orgId':currOrgId,
                'searchValue.userName':$('#searchUserName').val(),
                'searchValue.status':$('#searchStatus').val(),
                'dqdp_csrf_token': dqdp_csrf_token
            },
            dataType:'json',
            success:function (result) {
                if ('0' == result.code) {
                    var userList = new ListTable({
                        checkableColumn:"userId",
                        title:[
                            {width:"5%", name:"userId", showName:"", isCheckColunm:true},
                            {width:"8%", name:"userName", showName:"账户名称"},
                            {width:"8%", name:"personName", showName:"用户名称"},
                            {width:"8%", name:"mobile", showName:"手机号码"},
                            {width:"5%", name:"statusDesc", showName:"状态"},
                            {width:"8%", name:"money", showName:"账户余额"}
                        ],
                        data:result.data.userPager,
                        trStyle:["trWhite"]
                    });
                    userList.createList("user_list");
                    var pager = new Pager({totalPages:result.data.maxPage, currentPage:result.data.currPage, funcName:"listUser"});
                    pager.createPageBar("topIdPage");
                    pager.createPageBar("downIdPage");
                    window.parent.parent._resetFrameHeight(1);
                }
            }
        });
    }
    
    $('#confirmBtn').click(function () {
    	var checkeds = $("[name='ids']:checked");
    	if(checkeds.size()!=1){
    		_alert("提示","请选择一个需要充值的商户");
    		return;
    	}
    	
        window.opener.updateUser(checkeds.val(),checkeds.parent().next().html());
        window.close();
    });


</script>

</body>

</html>
