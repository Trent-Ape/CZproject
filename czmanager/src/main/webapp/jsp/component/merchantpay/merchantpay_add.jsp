<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value="merchandiseStatus"></jsp:param>
    <jsp:param name="permission" value="userList"></jsp:param>
    <jsp:param name="mustPermission" value="merchandiseAdd"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>新增</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <link href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form action="${baseURL}/merchantpay/merchantpayAction!addTbCzMerchantPayPO.action" method="post" id="id_form_add">
    <table class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead>
            <tr>
                <th colspan="4">新增商户平台充值</th>
            </tr>
        </thead>
        <tbody>
            <input type="hidden" name="tbCzMerchantPayPO.tbMerchatPayUserid" id="userId" value="${param.userId }" />
            <input type="hidden" name="modelCode"/>
            <tr>
                <td width="120" class="tdBlue">代理商用户：</td>
                <td width="85%">
                	<input readonly="readonly" type="text" name="baseUserVO.userName" id="userName" valid="{must:true,length:50,tip:'代理商用户'}" />
                	<input class="btn4" type="button" onclick="javascript:selectUser();" permission="userList" value="选择用户"/>
                	
                </td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">充值金额：</td>
                <td width="85%"><input style="width: 500px" class="form120px" name="tbCzMerchantPayPO.tbMerchatPayMoney" type="text" valid="{must:false, fieldType:'pattern', regex:'^\\d+(.?\\d{1,2})?$', tip:'充值金额'}"/></td>
            </tr>
            <tr height="50"></tr>
        </tbody>
    </table>
</form>

<div class="toolBar">
    <div align="center">
        <input class="btn4" type="button" onclick="doSave();" value="保存"/>
        <input class="btn4" type="button" onclick="javascript:history.back();" value="返 回"/>
    </div>
</div>

<!--工具栏 end-->
<script type="text/javascript">
    function doSave() {
        _doCommonSubmit("id_form_add",null,{ok:function(){history.back();},fail:function(){}});
    }
    
    /**
     * 用户选择
     */
    function selectUser() {
        window.open('selectUser.jsp', '用户选择', 'top=100,left=100,height=480, width=700, toolbar=no,scrollbars=yes, menubar=no, resizable=yes,location=no, status=no');
    }
    
    function updateUser(userID,userName) {
        $('#userId').val(userID);
        $('#userName').val(userName);
    }


</script>
</body>
</html>
