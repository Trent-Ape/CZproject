<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value="payStatus"></jsp:param>
    <jsp:param name="permission" value="merchantpayExport,merchantpayDelete"></jsp:param>
    <jsp:param name="mustPermission" value="merchantpayList"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>平台余额充值-列表</title>
    <link type="text/css" href="${baseURL}/themes/do1/jquery-ui/jquery.ui.all.css" rel="stylesheet"/>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <link href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/do1/common/error1.0.js"></script>
    <script src="${baseURL}/js/do1/common/pop_up1.0.js"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
    <script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jquery-ui-1.8.custom.min.js"></script>

</head>

<body>
<!--头部 end-->


<!--公告 end-->
<div class="searchWrap">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="searchLeft"></td>
            <div class="title">
                <h2 class="icon1">商户余额充值管理</h2>
            </div>
            <!--标题 end-->
        </tr>
    </table>
</div>

<!--标签选项卡 end-->
<form action="${baseURL}/merchantpay/merchantpayAction!ajaxSearch.action" method="post" id="id_form_search">
    <div class="searchWrap">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="searchLeft"></td>
                <td class="searchBg">
                    <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>
                                        <td width="100" height="30">代理商账号：</td>
                                        <td width="140"><input class="form120px" keySearch="doSearch('1')" name="searchValue.userName" type="text" value=""/></td>
                                        <!-- <td width="60" height="30">充值状态：</td>
                                        <td width="140"><select class="form120px" name="searchValue.tbMerchatPayStatus"  dictType="payStatus"    /></td> -->
                                        <td><input style="margin-left: 20px;" class="btnQuery" name="input" type="button" value="查询" onclick="doSearch(1)"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="searchRight"></td>
            </tr>
            <tr>
                <td class="searchButtomLeft"></td>
                <td class="searchButtom"></td>
                <td class="searchButtoRight"></td>
            </tr>
        </table>
    </div>
</form>
<!--标题 end-->
<div class="pageDown" id="pageTop"></div>
<!--翻页 end-->

<div class="operation">
	<input class="btnS4" type="button" value="导出" permission="merchantpayExport"  onclick="javascript:ajaxExport();"/>
    <!-- <input class="btnS4" type="button" value="新增余额充值" onclick="javascript:window.location.href='merchantpay_add.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token"/>
    <input class="btnS4" type="button" permission="merchantpayDelete" value="删除余额充值" onclick="javascript:_doDel('id_list_config')"/> -->
</div>

<!--工具栏 end-->
<script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jgxLoader.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        doSearch(1);
//        _showTip("hahaha1");
    });
    function doSearch($pageIndex) {
    	loading();
    	$("#id_form_search").attr("action","${baseURL}/merchantpay/merchantpayAction!ajaxSearch.action");
        $('#id_form_search').ajaxSubmit({
            dataType:'json',
            data:{page:$pageIndex},
            success:function (result) {
                if ("0" == result.code) {
                    var list1 = new ListTable(
                            {
                                checkableColumn:"tbMerchatPayId",
                                title:[
                                    {width:"5%", name:"tbMerchatPayId", showName:"订单号",isCheckColunm:true},
                                    {width:"5%", name:"tbMerchatPayId", showName:"订单号"},
                                    {width:"5%", name:"userName", showName:"充值代理商账户"},
                                    {width:"5%", name:"tbMerchatPayMoney", showName:"充值金额"},
                                    {width:"10%", name:"tbMerchatPayInsert", showName:"充值时间"},
                                    {width:"5%", name:"tbMerchatPayStatusDesc", showName:"充值状态"},
                                    {width:"5%", name:"tbMerchatPayOper", showName:"操作人"}
                                ],
                                data:result.data.pageData,
                                trStyle:["trWhite"]
                            });
                    list1.createList("id_list_config");
                    var pager = new Pager({totalPages:result.data.maxPage, currentPage:result.data.currPage, funcName:"doSearch"});
                    pager.createPageBar("idPage");
                    pager.createPageBar("pageTop");
                } else {
                    $("#tip").error({title:"信息提示层", content:result.desc, button:[
                        {text:"确定", events:"test"},
                        {text:"取消", events:"test"}
                    ]});
                }
            },
            complete:function(){
            	loaded();
            },
            error:function(){
            	loaded();
            }
        });
    }
    
    function ajaxExport(){
    	if($("#id_pageTop_page").size()!=0) {
    		 $("#id_form_search").attr("action","${baseURL}/merchantpay/merchantpayAction!export.action");
 	        $("#id_form_search").submit();
    	}else{
    		_alert("提示","没有数据可导出");
    	}
	       
	 }
    
</script>
<div id="id_list_config">
</div>

<!--表格 end-->

<div class="toolBar" permission="">
    <!--筛选 end-->
    <div class="pageDown" id="idPage">

    </div>
    <!--翻页 end-->
    <div class="operation">
    	<input class="btnS4" type="button" value="导出" permission="merchantpayExport"  onclick="javascript:ajaxExport();"/>
    </div>
    <!--对表格数据的操作 end-->
</div>
<div>&nbsp;
</div>

</body>

</html>
