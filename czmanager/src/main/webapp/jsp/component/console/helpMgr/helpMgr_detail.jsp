<%@page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../../common/dqdp_common.jsp"%>
<jsp:include page="../../../common/dqdp_vars.jsp">
	<jsp:param name="dict" value=""></jsp:param>
	<jsp:param name="permission" value="flowEdit"></jsp:param>
	<jsp:param name="mustPermission" value="flowView"></jsp:param>
</jsp:include>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>代理商流程管理-查看</title>
<link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet"
	type="text/css" />
<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
<script src="${baseURL}/js/do1/common/common.js"></script>
<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
<link
	href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jquery-ui-1.8.custom.min.js"></script>

<script src="${baseURL}/js/DatePicker/WdatePicker.js"></script>
</head>

<body templateId="SQLPager" onload="loadIframe();">
	<div class="searchWrap">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="searchLeft"></td>
				<div class="title">
					<h2 class="icon1">查看代理商流程说明信息</h2>
				</div>
			</tr>
		</table>
	</div>
	<table class="tableCommon mt5" width="100%" border="0" cellspacing="0"
		cellpadding="0" id="id_body"
		datasrc="local:var_8a3c0cf4da7a49218155ad7a8162782d"
		templateId="default" dqdpCheckPoint="view_fields">
		<thead>
			<tr>
				<th>代理商流程说明信息</th>
			</tr>
		</thead>
		<tbody>
			<tr dqdpCheckPoint="view_field_title">
				<td width="100%" class="tdBlue"><iframe id="iframepage" onload="iFrameHeight()" frameborder="0" width="100%" scrolling="no" src="${baseURL}/help.html?<%=System.currentTimeMillis()%>"></iframe></td>
			</tr>
		</tbody>
	</table>
	<script type="text/javascript">
	
		var var_8a3c0cf4da7a49218155ad7a8162782d = _doGetDataSrc(
				"${baseURL}/helpMgr/helpMgrAction!load.action",
				"");
		
		function iFrameHeight() {
			var ifm = document.getElementById("iframepage");
			var subWeb = document.frames ? document.frames["iframepage"].document
					: ifm.contentDocument;
			if (ifm != null && subWeb != null) {
				ifm.height = subWeb.body.scrollHeight;
			}
		}
		function edit(){
			window.location = 'helpMgr_edit.jsp';
		}
		 function loadIframe(){
		    	//取到窗口的高度 
		    	var winH = $(window).height(); 
		    	//取到页面的高度 
		    	var bodyH = $(document).height(); 
		    	if(bodyH > winH){ 
		    	window.parent.document.getElementById("ifm").height = bodyH; 
		    	}else{ 
		    	window.parent.document.getElementById("ifm").height = winH; 
		    	} 
		    }
		 
	</script>

	<div class="toolBar" templateId="default">
		<div align="center">
			<input class="btn4" type="button" permission="flowEdit" onclick="edit()" value="修改" />
		</div>
	</div>

</body>
</html>
