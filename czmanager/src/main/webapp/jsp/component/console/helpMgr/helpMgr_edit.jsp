<%@page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../../common/dqdp_common.jsp"%>
<jsp:include page="../../../common/dqdp_vars.jsp">
	<jsp:param name="dict" value=""></jsp:param>
	<jsp:param name="permission" value=""></jsp:param>
	<jsp:param name="mustPermission" value="flowEdit"></jsp:param>
</jsp:include>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>代理商流程管理-修改</title>
<link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet"
	type="text/css" />
<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
<script src="${baseURL}/js/do1/common/common.js"></script>
<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
<script src="${baseURL}/js/3rd-plug/jquery/ajaxfileupload.js" type="text/javascript"></script>
<link
	href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jquery-ui-1.8.custom.min.js"></script>

<script src="${baseURL}/js/DatePicker/WdatePicker.js"></script>
<script src="${baseURL}/js/3rd-plug/kindeditor/kindeditor.js"></script>
<script src="${baseURL}/js/3rd-plug/kindeditor/lang/zh_CN.js"></script>
</head>

<body templateId="SQLPager" onload="loadIframe();">
	<div class="searchWrap">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="searchLeft"></td>
				<div class="title">
					<h2 class="icon1">代理商流程说明信息</h2>
				</div>
			</tr>
		</table>
	</div>
	<form action="${baseURL}/helpMgr/helpMgrAction!save.action"
		method="post" id="b65209c1d73948afa91d8c9cb3d26377"
		onsubmit="return false;" templateId="default"
		dqdpCheckPoint="add_form">
		<div>
		<table class="tableCommon mt5" width="100%" border="0" cellspacing="0"
			cellpadding="0" id="id_body" dataSrc="${baseURL}/helpMgr/helpMgrAction!load.action"
			>
			<thead>
				<tr>
					<th>编辑代理商流程说明信息</th>
				</tr>
			</thead>
			<tbody>
				<tr dqdpCheckPoint="add_field_title">
					<td width="100%" class="tdBlue">
					   
                            <textarea id="editor_id" name="help" style="width: 100%;height: 400px" ></textarea>
					</td>
				</tr>
			</tbody>
		</table>
		</div>
	</form>
	<script type="text/javascript">
		function func_b65209c1d73948afa91d8c9cb3d26377() {
			editor.sync();
			_doCommonSubmit(
					"b65209c1d73948afa91d8c9cb3d26377",
					"",
					{
						ok : function() {
							window.location.href = "${baseURL}/jsp/component/console/helpMgr/helpMgr_detail.jsp";
						},
						fail : function() {
						}
					});
		}

	 KindEditor.ready(function(K) {
             window.editor = K.create('#editor_id',{
                     uploadJson : '${baseURL}/js/3rd-plug/kindeditor/jsp/upload_json.jsp',
                     fileManagerJson : '${baseURL}/js/3rd-plug/kindeditor/jsp/file_manager_json.jsp',
                     allowFileManager : true
             })
     });

	</script>
	<div class="toolBar" templateId="default">
		<div align="center">
			<input class="btn4" type="button"
				id="05637b1835b84bc7be378a3701dc304a"
				onclick="func_b65209c1d73948afa91d8c9cb3d26377()" value="提交" /> <input
				class="btn4" type="button" id="6313c5d1981f4676b0af9d606944883d"
				onclick="javascript:history.go(-1)" value="返回" />
		</div>
	</div>

</body>
</html>
