<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value=""></jsp:param>
    <jsp:param name="permission" value=""></jsp:param>
    <jsp:param name="mustPermission" value="adminConfig"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>编辑</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<div class="searchWrap">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="searchLeft"></td>
            <div class="title">
                <h2 class="icon1">修改系统配置信息</h2>
            </div>
            <!--标题 end-->
        </tr>
    </table>
</div>

<!-- 版权所有 -->
<form action="${baseURL}/dqdpconfig/config!update.action" method="post" id="id_form_copyright">
    <table dataSrc="${baseURL}/dqdpconfig/config!getConfig.action?ids=ef42885c-7dd5-481a-8d7a-8f3ef5bcc443"
    class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <input type="hidden" name="configVO.configId"/>
            <input type="hidden" name="configVO.componentName"/>
            <input type="hidden" name="configVO.configName"/>
            <tr >
            	<td width="150"  class="tdBlue">网站底部版权所有：</td>
                <td width="50%">
                    <input style="width: 80%;" name="configVO.configValue" valid="{must:true,length:300,tip:'配置值'}" />
                </td>
                <td> 
				    <div align="center">
				        <input class="btn4" type="button" onclick="doSave_Copyright();" value="更新"/>
				    </div>
 				</td>
            </tr>
        </tbody>
    </table>
</form>

<!-- 忘记密码提示 -->
<form action="${baseURL}/dqdpconfig/config!update.action" method="post" id="id_form_password">
    <table dataSrc="${baseURL}/dqdpconfig/config!getConfig.action?ids=4609db44-4c20-457e-90e1-0007e0f29d0e"
    class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <input type="hidden" name="configVO.configId"/>
            <input type="hidden" name="configVO.componentName"/>
            <input type="hidden" name="configVO.configName"/>
            <tr >
            	<td width="150" class="tdBlue">忘记密码提示：</td>
                <td width="50%">
                    <input style="width: 80%;" name="configVO.configValue" valid="{must:true,length:300,tip:'配置值'}" />
                </td>
                <td> 
				    <div align="center">
				        <input class="btn4" type="button" onclick="doSave_Password();" value="更新"/>
				    </div>
 				</td>
            </tr>
        </tbody>
    </table>
</form>

<!-- 广告提示 -->
<form action="${baseURL}/dqdpconfig/config!update.action" method="post" id="id_form_top_advert">
    <table dataSrc="${baseURL}/dqdpconfig/config!getConfig.action?ids=8678bfd1-e628-4134-8834-176ad4237fc8"
    class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <input type="hidden" name="configVO.configId"/>
            <input type="hidden" name="configVO.componentName"/>
            <input type="hidden" name="configVO.configName"/>
            <tr >
            	<td width="150" class="tdBlue">网站头部广告提示：</td>
                <td width="50%">
                    <textarea  rows="5" cols="120" style="width: 80%;" name="configVO.configValue" valid="{must:true,length:300,tip:'配置值'}" rows="" cols=""></textarea>
                </td>
                <td> 
				    <div align="center">
				        <input class="btn4" type="button" onclick="doSave_top_advert();" value="更新"/>
				    </div>
 				</td>
            </tr>
        </tbody>
    </table>
</form>

<!-- mobile -->
<form action="${baseURL}/dqdpconfig/config!update.action" method="post" id="id_form_mobile">
    <table dataSrc="${baseURL}/dqdpconfig/config!getConfig.action?ids=81fe5e39-ed25-4abf-92ef-be2570aec03d"
    class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <input type="hidden" name="configVO.configId"/>
            <input type="hidden" name="configVO.componentName"/>
            <input type="hidden" name="configVO.configName"/>
            <tr >
            	<td width="150" class="tdBlue">网站头部手机号码：</td>
                <td width="50%">
                    <input type="text" style="width: 80%;" name="configVO.configValue" valid="{must:true,length:300,tip:'配置值'}"></textarea>
                </td>
                <td> 
				    <div align="center">
				        <input class="btn4" type="button" onclick="doSave_mobile();" value="更新"/>
				    </div>
 				</td>
            </tr>
        </tbody>
    </table>
</form>

<!-- weixin号码 -->
<form action="${baseURL}/dqdpconfig/config!update.action" method="post" id="id_form_weixin">
    <table dataSrc="${baseURL}/dqdpconfig/config!getConfig.action?ids=50e2fd8c-37d8-4dce-b579-f7e586d33cec"
    class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <input type="hidden" name="configVO.configId"/>
            <input type="hidden" name="configVO.componentName"/>
            <input type="hidden" name="configVO.configName"/>
            <tr >
            	<td width="150" class="tdBlue">网站头部微信号码：</td>
                <td width="50%">
                    <input type="text" style="width: 80%;" name="configVO.configValue" valid="{must:true,length:300,tip:'配置值'}"></textarea>
                </td>
                <td> 
				    <div align="center">
				        <input class="btn4" type="button" onclick="doSave_weixin();" value="更新"/>
				    </div>
 				</td>
            </tr>
        </tbody>
    </table>
</form>

<!-- qq -->
<form action="${baseURL}/dqdpconfig/config!update.action" method="post" id="id_form_qq">
    <table dataSrc="${baseURL}/dqdpconfig/config!getConfig.action?ids=d663459f-440f-4e95-a9e3-c509b661554a"
    class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <input type="hidden" name="configVO.configId"/>
            <input type="hidden" name="configVO.componentName"/>
            <input type="hidden" name="configVO.configName"/>
            <tr >
            	<td width="150" class="tdBlue">网站头部qq号码：</td>
                <td width="50%">
                    <input type="text" style="width: 80%;" name="configVO.configValue" valid="{must:true,length:300,tip:'配置值'}"></textarea>
                </td>
                <td> 
				    <div align="center">
				        <input class="btn4" type="button" onclick="doSave_qq();" value="更新"/>
				    </div>
 				</td>
            </tr>
        </tbody>
    </table>
</form>


<!-- 折扣率 -->
<form action="${baseURL}/dqdpconfig/config!update.action" method="post" id="id_form_discount">
    <table dataSrc="${baseURL}/dqdpconfig/config!getConfig.action?ids=09bfd3a3-4f07-4056-b949-b7cb2e230efe"
    class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <input type="hidden" name="configVO.configId"/>
            <input type="hidden" name="configVO.componentName"/>
            <input type="hidden" name="configVO.configName"/>
            <tr >
            	<td width="150" class="tdBlue">最小折扣率：</td>
                <td width="50%">
                    <input type="text" style="width: 80%;" name="configVO.configValue" valid="{must:true,length:300,tip:'配置值'}"></textarea>
                </td>
                <td> 
				    <div align="center">
				        <input class="btn4" type="button" onclick="doSave_discount();" value="更新"/>
				    </div>
 				</td>
            </tr>
        </tbody>
    </table>
</form>


<!--工具栏 end-->
<script type="text/javascript">
    function doSave_Copyright() {
        if(_doCommonSubmit("id_form_copyright", null, {ok:function(){},fail:function(){}}));
    }
    function doSave_Password() {
        if(_doCommonSubmit("id_form_password", null, {ok:function(){},fail:function(){}}));
    }
    function doSave_top_advert() {
        if(_doCommonSubmit("id_form_top_advert", null, {ok:function(){},fail:function(){}}));
    }
    function doSave_discount() {
        if(_doCommonSubmit("id_form_discount", null, {ok:function(){},fail:function(){}}));
    }
    function doSave_mobile() {
        if(_doCommonSubmit("id_form_mobile", null, {ok:function(){},fail:function(){}}));
    }
    function doSave_weixin() {
        if(_doCommonSubmit("id_form_weixin", null, {ok:function(){},fail:function(){}}));
    }
    function doSave_qq() {
        if(_doCommonSubmit("id_form_qq", null, {ok:function(){},fail:function(){}}));
    }
</script>
</body>
</html>
