<%@ page import="cn.com.do1.common.util.web.WebUtil" %>
<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="permission" value=""></jsp:param>
    <jsp:param name="mustPermission" value="myAdminUserInfoEdit"></jsp:param>
    <jsp:param name="dict" value="userStatus,personSex,discount"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>修改用户</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
    <script src="${baseURL}/js/do1/common/HashMap.js"></script>
    <script src="${baseURL}/jsp/component/systemmgr/js/CheckboxTable.js"></script>
    <style type="text/css">
    </style>
</head>

<body>
<!--公告 end-->
<div class="searchWrap">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="searchLeft"></td>
            <div class="title">
                <h2 class="icon1">修改基本信息</h2>
            </div>
            <!--标题 end-->
        </tr>
    </table>
</div>

<form action="${baseURL}/merchant/myInfoAction!updateAdminBaseUser.action" method="post" id="user_edit">
    <table class="tableCommon mt5" dataSrc="${baseURL}/merchant/myInfoAction!viewAdminBaseUserVO.action?personId=${param.personId}" width="100%" border="0" cellspacing="0" cellpadding="0">
        <input type="hidden" id="userId" name="baseUserVO.userId" value=""/>
        <input type="hidden" name="baseUserVO.personId" value=""/>
        <input type="hidden" name="merchantVO.id" value="" />
        <input type="hidden" name="baseUserVO.status" value="" />
        <tbody>
            <tr>
                <td width="120" class="tdBlue">我的名称：</td>
                <td width="280"><input class="form120px" name="baseUserVO.personName" type="text" valid='{"must":true,"tip":"帐号"}'/>
                <td width="120" class="tdBlue">年龄：</td>
                <td width="280">
                	<input class="form120px" name="baseUserVO.age" type="text" valid="{must:false, fieldType:'pattern', regex:'^\\d{1,3}$', tip:'年龄'}"/>
                	<span  style="color: red;margin-left: 10px;">1-3位数字</span>
                </td>
            </tr>
            <tr>
                
                <td width="120" class="tdBlue">性别：</td>
                <td width="280"><select name="baseUserVO.sex" dictType="personSex" defaultValue="" valid='{"must":false,"tip":"性别"}'/></td>
                <td width="120" class="tdBlue">手机号码：</td>
            	<td width="280" >
            		<input class="form120px" name="merchantVO.mobile" type="text" valid="{must:true, fieldType:'pattern', regex:'^\\d{11}$', tip:'手机号码'}"/>
            		<span  style="color: red;margin-left: 10px;">*11位数字</span>
            	</td>
            </tr>
        
	        <tr>
	            
	            <td width="120" class="tdBlue">QQ：</td>
	            <td width="280" colspan="3"><input class="form120px" name="merchantVO.qq" type="text"
	                           valid="{must:true, fieldType:'pattern', regex:'^\\d{6,12}$', tip:'QQ'}"/><span  style="color: red;margin-left: 10px;">*6-12位数字</span></td>
	        </tr>
        
        </tbody>
    </table>
</form>

<div class="toolBar">
    <div align="center">
        <input class="btn4" id="save" type="button" value="保存"/>
    </div>
</div>

<div class="h130 overflow" style="height: 20px;"></div>
	
<div class="h130 overflow" style="height: 20px;"></div>
<!--公告 end-->
<div class="searchWrap">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="searchLeft"></td>
            <div class="title">
                <h2 class="icon1">修改支付密码</h2>
            </div>
            <!--标题 end-->
        </tr>
    </table>
</div>

<form action="${baseURL}/merchant/myInfoAction!changePayPassword.action" method="post" id="user_edit_paypassword">
    <table class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td width="120" class="tdBlue">原支付密码：</td>
                <td width="85%"><input class="form120px"  name="password" type="password" valid="{must:true, fieldType:'pattern', regex:'^\\w{6,20}$', tip:'原支付密码'}"/>
                <span  style="color: red;margin-left: 10px;">*6-20位数字或字母</span>
                </td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">新支付密码：</td>
                <td width="85%"><input class="form120px" id="newPassword" name="newPassword" type="password" valid="{must:true, fieldType:'pattern', regex:'^\\w{6,20}$', tip:'新支付密码'}"/>
                <span  style="color: red;margin-left: 10px;">*6-20位数字或字母</span></td>
            </tr>
             <tr>
                <td width="120" class="tdBlue">确认新支付密码：</td>
                <td width="85%">
                	<input class="form120px" id="confirmNewPassword" name="confirmNewPassword" type="password" valid="{must:true, fieldType:'pattern', regex:'^\\w{6,20}$', tip:'确认新支付密码'}"/>
                	<span  style="color: red;margin-left: 10px;">*6-20位数字或字母</span>
                </td>
            </tr>
        
        </tbody>
    </table>
</form>

<div class="toolBar">
    <div align="center">
        <input class="btn4" id="save_paypassword" type="button" value="保存"/>
    </div>
</div>

<!--工具栏 end-->
<script type="text/javascript">
    $(document).ready(function () {
    	
    	$('#save').click(function () {
    		
            var dqdp = new Dqdp();
            if (dqdp.submitCheck("user_edit")) {
                // 提交数据
                $('#user_edit').ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if ("0" == result.code) {
                            alert(result.desc);
                            window.location.href=("userAdminView.jsp"+ '?dqdp_csrf_token='+dqdp_csrf_token);
                        } else {
                            alert(result.desc);
                        }
                    },
                    error:function () {
                        alert('修改用户信息失败，可能是由网络原因引起的，请稍候再试');
                    }
                });
            }
        });
    	
    	$('#save_paypassword').click(function () {
    		if(checkPayPassword()){
    			var dqdp = new Dqdp();
                if (dqdp.submitCheck("user_edit_paypassword")) {
                    // 提交数据
                    $('#user_edit_paypassword').ajaxSubmit({
                        dataType:'json',
                        success:function (result) {
                            if ("0" == result.code) {
                                alert(result.desc);
                                window.location.href=("userAdminView.jsp"+ '?dqdp_csrf_token='+dqdp_csrf_token);
                            } else {
                                alert(result.desc);
                            }
                        },
                        error:function () {
                            alert('修改用户信息失败，可能是由网络原因引起的，请稍候再试');
                        }
                    });
                }
            
    		} else{
    			_alert("提示","两次凭证密码不正确")
    		}
    	});
    });
    
    function checkPayPassword() {
        return $('#newPassword').val() == $('#confirmNewPassword').val();
    }

    

</script>

</body>
</html>
