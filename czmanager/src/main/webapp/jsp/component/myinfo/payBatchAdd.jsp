<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value="merchandiseStatus"></jsp:param>
    <jsp:param name="permission" value=""></jsp:param>
    <jsp:param name="mustPermission" value="myUserInfoBatchAdd"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>我要充流量</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <link href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form action="${baseURL}/merchant/myInfoAction!addBatchPay.action" method="post" id="id_form_add">
    <table class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead>
            <tr>
                <th colspan="4">我要批量充流量</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="120" class="tdBlue" height="29px">充流量内容：</td>
                <td width="20%" >
                <textarea rows="10" cols="80"    id="payMoblie"   name="tbCzPayPO.payMoblie" valid="{must:true,tip:'充流量内容'}"></textarea>
                </td>
                <td width="200" style="vertical-align: top;"><font color="red">*非中国移动手机号码无法提供冲流量服务</font><br/><br/><font color="red">*格式:[号码 空格 充值流量M数],所输入的M套餐必须存在，否者不存在套餐的这一条充值不成功,目前可选套餐：<span id="merchantdise"></span> 如：<br/>11111111111 10M<br/>22222222222 20M<br/>33333333333 30M</font>
                <br/><br/><font color="red">*[初始化充值]可以查看 充流量内容最终的充值效果，不会提交充值申请，[提交批量充值]才是提交申请</span></font>
                </td>
            </tr>
            <tr>
                <td width="120" class="tdBlue" height="29px">支付密码：</td>
                <td width="20%"><input height="29px"  style="width: 500px" id="password" type="password" name="newPassword" valid="{must:true,tip:'支付密码',fieldType:'pattern',regex:'^\\w{6,20}$'}" /></td>
            	<td><span  style="color: red;margin-left: 10px;">*6-20位数字或字母组合</span></td>
            </tr>
        </tbody>
    </table>
</form>

<div class="toolBar">
    <div align="center">
    	<input class="btn4" type="button" onclick="doSaveInit();" value="初始化充值"/>
        <input class="btn4" type="button" onclick="doSave();" value="提交批量充值"/>
        <input class="btn4" type="button" onclick="javascript:history.back();" value="取消批量充值"/>
    </div>
</div>

<div id="result" style="display: display;">
	<div>充值结果说明：</div>
</div>
<div id="resultMoney" style="display: none;">
	
</div>

<!--工具栏 end-->
<script type="text/javascript">
function doSave() {
	 var dqdp = new Dqdp();
    if (dqdp.submitCheck("id_form_add")) {
   	//var message="确定为手机:"+$("#payMoblie").val()+" 充值套餐：" +$("#payMerchandiseId").find("option:selected").text();
   	if(confirm($("#resultMoney").html()+" 确定要充值吗？")){
   	 	$('#id_form_add').attr("action","${baseURL}/merchant/myInfoAction!addBatchPay.action");
   	 	$('#id_form_add').ajaxSubmit({
              dataType:'json',
              success:function (result) {
             	 if("0" == result.code){
                      $("#result").html("<div>充值结果说明：</div>" + result.data.result);
                      $("#result").show();
                      $("#resultMoney").html("批量充值总价格："+result.data.sumPayMoney+"元");
                      $("#resultMoney").show();
             	 } else{
             		 _alert("提示",result.desc);
             	 }
                
              },
              error:function () {
                  _alert("提示",'批量充值初始失败，请稍候再试');
              },complete:function(){
              	loadIframe();
              }
          });
   	}
  }
}
    function doSaveInit() {
    	 var dqdp = new Dqdp();
         if (dqdp.submitCheck("id_form_add")) {
	    	//var message="确定为手机:"+$("#payMoblie").val()+" 充值套餐：" +$("#payMerchandiseId").find("option:selected").text();
	    	 $('#id_form_add').attr("action","${baseURL}/merchant/myInfoAction!addInitBatchPay.action");
    		 $('#id_form_add').ajaxSubmit({
                 dataType:'json',
                 success:function (result) {
                	 if("0" == result.code){
                         $("#result").html("<div>充值初始化说明：</div>" + result.data.result);
                         $("#result").show();
                         $("#resultMoney").html("批量充值总价格："+result.data.sumPayMoney+"元");
                         $("#resultMoney").show();
                	 } else{
                		 _alert("提示",result.desc);
                	 }
                   
                 },
                 error:function () {
                     _alert("提示",'批量充值初始失败，请稍候再试');
                 },complete:function(){
                 	loadIframe();
                 }
             });
      	}
    }
    $(document).ready(function(){
    	initPay();
    	
    	$("#payMerchandiseId").change(function(){
    		$("#money").children().hide();
    		$("#money [id='"+this.value+"']").show();
    	});
    });
    
    function loadIframe(){
    	//取到窗口的高度 
    	var winH = $(window).height(); 
    	//取到页面的高度 
    	var bodyH = $(document).height(); 
    	if(bodyH > winH){ 
    	window.parent.document.getElementById("ifm").height=bodyH; 
    	}else{ 
    	window.parent.document.getElementById("ifm").height=winH; 
    	} 
    }
    
    
    function initPay() {
        $.ajax({
            url:"${baseURL}/merchant/myInfoAction!initPay.action",
            type:"post",
            dataType:"json",
            success:function (result) {
                if ("0" == result.code) {
                	var data = result.data.merchandiseList;
                	$("#payMerchandiseId").append("<option value=''>请选择套餐类型</option>");
                	if(data&&data.length > 0){
                		$(data).each(function(){
                			
                			$("#payMerchandiseId").append("<option value="+this.merId+">"+this.merName + "</option>");
                			var priceHtml='<div style="display:none;" id="@merId"><span style="color:#fa5000;font-size:16px;">@merDiscountMoney</span> <span style="text-decoration:line-through;color:#b4b4b4;padding-left: 10px;">@merMoney元</span> </div>';
                			priceHtml =priceHtml.replace("@merId",this.merId);
                			priceHtml =priceHtml.replace("@merDiscountMoney",this.merDiscountMoney);
                			priceHtml =priceHtml.replace("@merMoney",this.merMoney);
                			$("#money").append(priceHtml);
                			$("#merchantdise").append(this.merName+"("+this.merDiscountMoney+"元) ");
                		});
                	}
                } else {
                    _alert("错误提示",result.desc);
                }
            },
            error:function () {
                alert("通讯错误");
            },complete:function(){
            	loadIframe();
            }
        });
    }
</script>
</body>
</html>
