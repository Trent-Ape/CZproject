<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value="merchandiseStatus"></jsp:param>
    <jsp:param name="permission" value=""></jsp:param>
    <jsp:param name="mustPermission" value="myUserInfoAdd"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>我要充流量</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <link href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form action="${baseURL}/merchant/myInfoAction!addPay.action" method="post" id="id_form_add">
    <table class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead>
            <tr>
                <th colspan="4">我要充流量</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="120" class="tdBlue" height="29px">充流量手机：</td>
                <td width="85%" ><input height="29px" style="width: 500px" id="payMoblie" type="text" name="tbCzPayPO.payMoblie" valid="{must:true,tip:'冲流量手机',fieldType:'pattern',regex:'^\\d{11}$'}" /><font color="red">*非中国移动手机号码无法提供冲流量服务</font></td>
            </tr>
            <tr>
                <td width="120" class="tdBlue" height="29px">流量套餐：</td>
                <td width="85%"><select id="payMerchandiseId" name="tbCzPayPO.payMerchandiseId" valid="{must:true,length:100,tip:'流量套餐'}"/></td>
            </tr>
            <tr>
                <td width="120" class="tdBlue" height="29px">套餐价格：</td>
                <td width="85%" id="money" >
                </td>
            </tr>  
            <tr>
                <td width="120" class="tdBlue" height="29px">支付密码：</td>
                <td width="85%"><input height="29px"  style="width: 500px" id="password" type="password" name="newPassword" valid="{must:true,tip:'支付密码',fieldType:'pattern',regex:'^\\w{6,20}$'}" /><span  style="color: red;margin-left: 10px;">*6-20位数字或字母组合</span></td>
            </tr>
        </tbody>
    </table>
</form>

<div class="toolBar">
    <div align="center">
        <input class="btn4" type="button" onclick="doSave();" value="提交充值"/>
        <input class="btn4" type="button" onclick="javascript:history.back();" value="取消充值"/>
        <input class="btn4" type="button" onclick="javascript:window.location.href='payBatchAdd.jsp'" value="进入批量充值"/>
    </div>
</div>

<!--工具栏 end-->
<script type="text/javascript">
    function doSave() {
    	 var dqdp = new Dqdp();
         if (dqdp.submitCheck("id_form_add")) {
        	var message="确定为手机:"+$("#payMoblie").val()+" 充值套餐：" +$("#payMerchandiseId").find("option:selected").text();
         	if(confirm(message)){
         		 _doCommonSubmit("id_form_add",null,{ok:function(){
         	     window.location.href=("pay_list.jsp"+ '?dqdp_csrf_token='+dqdp_csrf_token);},fail:function(){}});
         	}
         }
    }
    $(document).ready(function(){
    	initPay();
    	
    	$("#payMerchandiseId").change(function(){
    		$("#money").children().hide();
    		$("#money [id='"+this.value+"']").show();
    	});
    });
    
    function initPay() {
        $.ajax({
            url:"${baseURL}/merchant/myInfoAction!initPay.action",
            type:"post",
            dataType:"json",
            success:function (result) {
                if ("0" == result.code) {
                	var data = result.data.merchandiseList;
                	$("#payMerchandiseId").append("<option value=''>请选择套餐类型</option>");
                	if(data&&data.length > 0){
                		$(data).each(function(){
                			
                			$("#payMerchandiseId").append("<option value="+this.merId+">"+this.merName + "</option>");
                			var priceHtml='<div style="display:none;" id="@merId"><span style="color:#fa5000;font-size:16px;">@merDiscountMoney</span> <span style="text-decoration:line-through;color:#b4b4b4;padding-left: 10px;">@merMoney元</span> </div>';
                			priceHtml =priceHtml.replace("@merId",this.merId);
                			priceHtml =priceHtml.replace("@merDiscountMoney",this.merDiscountMoney);
                			priceHtml =priceHtml.replace("@merMoney",this.merMoney);
                			$("#money").append(priceHtml);
                		});
                	}
                } else {
                    _alert("错误提示",result.desc);
                }
            },
            error:function () {
                alert("通讯错误");
            }
        });
    }
</script>
</body>
</html>
