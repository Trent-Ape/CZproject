<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value="payStatus"></jsp:param>
    <jsp:param name="permission" value="myPayExport,payUpdateStatusAppeal"></jsp:param>
    <jsp:param name="mustPermission" value="myPayList"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>充值订单-列表</title>
    <link type="text/css" href="${baseURL}/themes/do1/jquery-ui/jquery.ui.all.css" rel="stylesheet"/>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/do1/common/error1.0.js"></script>
    <script src="${baseURL}/js/do1/common/pop_up1.0.js"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
    <script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jquery-ui-1.8.custom.min.js"></script>
    <script src="${baseURL}/js/DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="${baseURL}/js/3rd-plug/jquery/ajaxfileupload.js"></script>

</head>

<body>
<!--头部 end-->


<!--公告 end-->
<div class="searchWrap">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="searchLeft"></td>
            <div class="title">
                <h2 class="icon1">流量充值管理</h2>
            </div>
            <!--标题 end-->
        </tr>
    </table>
</div>

<!--标签选项卡 end-->
<form action="${baseURL}/pay/payAction!ajaxMySearch.action" method="post" id="id_form_search">
    <div class="searchWrap">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="searchLeft"></td>
                <td class="searchBg">
                    <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>
                                        <td width="60" height="30">订单号：</td>
                                        <td width="140"><input class="form120px" keySearch="doSearch('1')" name="searchValue.payId" type="text" value=""/></td>
                                        <td width="60" height="30">充值号码：</td>
                                        <td width="140"><input class="form120px" keySearch="doSearch('1')"  name="searchValue.payMoblie" type="text" value=""/></td>
                                       	<td width="100" height="30">流量套餐：</td>
                                        <td width="100"><select class="form120px" name="searchValue.payMerchandiseId"  id="payMerchandiseId"/></td>
                                   		<td></td>
                                    </tr>
                                    <tr>
                                    	<td width="60" height="30">充值状态：</td>
                                        <td width="140"><select class="form120px" name="searchValue.payStatus"  dictType="payStatus"    /></td>
                                        <td width="60" height="30">充值时间</td>
                                        <td width="300" colspan="3"><input  class="form120px" width="100" name="searchValue.startDate"  id="searchStartDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',startDate:'%y-%M-%d 00:00:00',readOnly:true,alwaysUseStartDate:true});"  type="text" keySearch="keySearchFunc()"/>
                                        							至<input   class="form120px" width="100" id="searchEndDate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true,startDate:'%y-%M-%d 23:59:59'});"  name="searchValue.endDate" keySearch="keySearchFunc()"/></td>
                                        <td><input style="margin-left: 20px;"  class="btnQuery" name="input" type="button" value="查询" onclick="doSearch(1)"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="searchRight"></td>
            </tr>
            <tr>
                <td class="searchButtomLeft"></td>
                <td class="searchButtom"></td>
                <td class="searchButtoRight"></td>
            </tr>
        </table>
    </div>
</form>
<!--标题 end-->
<div class="pageDown" id="pageTop"></div>
<!--翻页 end-->

<div class="operation">
	<input class="btnS4" type="button" value="导出" permission="myPayExport"  onclick="javascript:ajaxExport();"/>
    <input class="btnS4" type="button" value="我要充流量" onclick="javascript:window.location.href='payAdd.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token"/>
</div>

<!--工具栏 end-->
<script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jgxLoader.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        doSearch(1);
        initPay();
//        _showTip("hahaha1");
    });
    function doSearch($pageIndex) {
    	loading();
    	$("#id_form_search").attr("action","${baseURL}/pay/payAction!ajaxMySearch.action");
        $('#id_form_search').ajaxSubmit({
            dataType:'json',
            data:{page:$pageIndex},
            success:function (result) {
                if ("0" == result.code) {
                    var list1 = new ListTable(
                            {
                                checkableColumn:"payId",
                                title:[
                                    {width:"2%", name:"payId", showName:"订单ID",isCheckColunm:true},
                                    {width:"4%", name:"payId", showName:"订单号"},
                                    {width:"4%", name:"payMoblie", showName:"充流量号码"},
                                    {width:"4%", name:"payStatusDesc", showName:"充值状态"},
                                    {width:"4%", name:"payOriginalMoney", showName:"套餐原价"},
                                    {width:"4%", name:"payDiscountMoney", showName:"折扣价格"},
                                    {width:"7%", name:"payStartDate", showName:"充值请求时间"},
                                    {width:"7%", name:"payEndDate", showName:"充值完成时间"},
                                    {width:"18%", name:"operationDesc", permission:"payUpdateStatus", showName:"结果描述", isOperationColumn:true}
                                ],
                                operations:[
          									{name:"查看", permission:"payView", event:function (index, content) {
        										_doSignlView("id_list_config", content.payId);
        									}},
                                            {name:"申诉", permission:"payUpdateStatusAppeal",condition:function (index, content) {
                                                return content.payStatus =="2"||content.payStatus =="7";
                                            },event:function (index, content) {
                                            	//updateStatus(content.payId,"5");
                                            	payUpdateStatusAppeal(content.payId,"5");
                                            }}
                                ],
                                data:result.data.pageData,
                                trStyle:["trWhite"]
                            });
                    list1.createList("id_list_config");
                    var pager = new Pager({totalPages:result.data.maxPage, currentPage:result.data.currPage, funcName:"doSearch"});
                    pager.createPageBar("idPage");
                    pager.createPageBar("pageTop");
                } else {
                    $("#tip").error({title:"信息提示层", content:result.desc, button:[
                        {text:"确定", events:"test"},
                        {text:"取消", events:"test"}
                    ]});
                }
            },
            complete:function(){
            	loaded();
            },
            error:function(){
            	loaded();
            }
        });
    }
    
    function ajaxExport(){
    	if($("#id_pageTop_page").size()!=0) {
    		 $("#id_form_search").attr("action","${baseURL}/pay/payAction!myExport.action");
 	        $("#id_form_search").submit();
    	}else{
    		_alert("提示","没有数据可导出");
    	}
	       
	 }
    
    function payUpdateStatusAppeal(id,status){
    	 
    	$("#payId").val(id);
    	$("#status").val(status);
    	$("#appealInfo").dialog({height:300,
            width:700,
            modal:true});
    	loadIframe();
    }
    
    function loadIframe(){
    	//取到窗口的高度 
    	var winH = $(window).height(); 
    	//取到页面的高度 
    	var bodyH = $(document).height(); 
    	if(bodyH > winH){ 
    	window.parent.document.getElementById("ifm").height=bodyH; 
    	}else{ 
    	window.parent.document.getElementById("ifm").height=winH; 
    	} 
    }
    
    function uploadLogo(){
    	if ($("#upFile").val() == '') return alert("请选择要上传的图片");
        $.ajaxFileUpload({
            url:'${baseURL}/payInfo/payInfoAction!uploadFile.action',//需要链接到服务器地址
            secureuri:false,
            fileElementId:'upFile',                        //文件选择框的id属性
            dataType: 'json',                              //服务器返回的格式，可以是json
            success: function (data) {
                if(data.code=="0"){
                    alert("上传文件成功！");
                    var path = data.data.url;
                    var reFilePath = data.data.reFilePath;
                    $("#reFilePath").val(reFilePath);
                    $("#img").attr("src",path);
                    $("#img").show();
                }else{
                    alert(data.desc);
                }
            },
            error: function () { alert("上传文件失败！"); }
        })
    }
    
    /**
     * 启用或禁用用户
     * @param userId
     * @param disabled
     */
    function updateStatus() {
        $.ajax({
            type:'post',
            url:"${baseURL}/payInfo/payInfoAction!payUpdateStatusAppeal.action",
            data:{
                'id':$("#payId").val(),
                'status':$("#status").val(),
                'payDesc':$("#payDesc").val(),
                'reFilePath':$("#reFilePath").val(),
                'dqdp_csrf_token': dqdp_csrf_token
            },
            dataType:'json',
            success:function (result) {
                if (result.code == '0') {
                    alert("操作成功");
                    window.location.href = 'pay_list.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token;
                } else {
                    alert(result.desc);
                }
            },
            error:function () {
                alert('操作失败，请稍候再试');
            }
        });
    }
    
    function initPay() {
        $.ajax({
        	url:"${baseURL}/pay/payAction!initItemMerchandise.action",
            type:"post",
            dataType:"json",
            success:function (result) {
                if ("0" == result.code) {
                	var data = result.data.merchandiseList;
                	$("#payMerchandiseId").append("<option value=''>请选择</option>");
                	if(data&&data.length > 0){
                		$(data).each(function(){
                			
                			$("#payMerchandiseId").append("<option value="+this.merId+">"+this.merName + "</option>");
                		});
                	}
                } else {
                    _alert("错误提示",result.desc);
                }
            },
            error:function () {
                alert("通讯错误");
            }
        });
    }

    
</script>
<div id="id_list_config" >
</div>

<!--表格 end-->

<div class="toolBar" permission="">
    <!--筛选 end-->
    <div class="pageDown" id="idPage">

    </div>
    <!--翻页 end-->
    <div class="operation">
    	<input class="btnS4" type="button" value="导出" permission="myPayExport"  onclick="javascript:ajaxExport();"/>
    <input class="btnS4" type="button" value="我要充流量" onclick="javascript:window.location.href='payAdd.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token"/>
    </div>
    <!--对表格数据的操作 end-->
</div>
<div>&nbsp;
</div>

<div id="appealInfo" style="display: none;" title="申诉申请">
	<form action="${baseURL}/payInfo/payInfoAction!uploadFile.action" method="post" id="id_form_add">
	<input type="hidden" name="id" id="payId" />
	<input type="hidden" name="status" id="status" />
    <table class="tableCommon mt5"border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td width="200" class="tdBlue" height="29px">申诉截图：</td>
                <td width="200" ><input type="file" name="upFile" id="upFile" class="inputformfile" /></td>
                <td width="200" ><input class="btnS4" type="button" value="上传"  onclick="uploadLogo();"/></td>
            </tr>
             <tr>
                <td width="200" class="tdBlue" height="120px">上传申诉截图预浏览：</td>
                <td width="200" colspan="2"><img id="img" src="" style="width: 100px;height: 100px;display: none;" />
                <input type="hidden" name="reFilePath" id="reFilePath" />
                </td>
            </tr>
            <tr>
                <td width="200" class="tdBlue" height="29px">申诉描述：</td>
                <td width="200" colspan="2"><input height="29px" style="width: 500px;" id="payDesc" type="text" /></td>
            </tr>
        </tbody>
    </table>
</form>
<div class="toolBar">
    <div align="center">
        <input class="btn4" type="button" onclick="updateStatus();" value="提交申诉"/>  
        <input class="btn4" type="button" onclick="javascript:$('#appealInfo').dialog('close');" value="取消申诉"/>
    </div>
</div>
</div>

</body>

</html>
