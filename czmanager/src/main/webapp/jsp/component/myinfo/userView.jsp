<%@ page import="cn.com.do1.common.util.web.WebUtil" %>
<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="permission" value=""></jsp:param>
    <jsp:param name="mustPermission" value="myUserInfoView"></jsp:param>
    <jsp:param name="dict" value="personSex,userStatus"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>查看用户信息</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <link type="text/css" href="${baseURL}/themes/do1/jquery-ui/jquery.ui.all.css" rel="stylesheet"/>
    <link href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
    <script src="${baseURL}/jsp/component/systemmgr/js/DataTable.js"></script>
    <script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jquery-ui-1.8.custom.min.js"></script>
    <style type="text/css">
        #tabs {
            padding-top: 10px;
            padding-bottom: 20px;
            margin: 10px 0px 0px 0px;
        }
    </style>
</head>

<body>
<table class="tableCommon mt5" dataSrc='${baseURL}/merchant/myInfoAction!viewBaseUserDetailVO.action?personId=${param.personId}'
       width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th colspan="4">我的信息</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="120" class="tdBlue">我的称呼：</td>
            <td width="280" name="baseUserVO.personName"></td>
            <td width="120" class="tdBlue">性别：</td>
            <td width="280" name="baseUserVO.sexDesc"></td>
        </tr>
        <tr>
            <td width="120" class="tdBlue">账户名称：</td>
            <td width="280" name="baseUserVO.userName"></td>
            <td width="120" class="tdBlue">状态：</td>
            <td width="280" name="baseUserVO.statusDesc"></td>
        </tr>
        <tr>
            <td width="120" class="tdBlue">年龄：</td>
            <td width="280" name="baseUserVO.age" colspan="3"></td>
        </tr>
        
        
         <tr>
            <td width="120" class="tdBlue">折扣：</td>
            <td width="280" name="merchantVO.discount"></td>
            <td width="120" class="tdBlue">账户余额(单位 元)：</td>
            <td width="280" name="merchantVO.money"></td>
        </tr>
        
        <tr>
            <td width="120" class="tdBlue">手机号码：</td>
            <td width="280" name="merchantVO.mobile"></td>
            <td width="120" class="tdBlue">QQ：</td>
            <td width="280" name="merchantVO.qq"></td>
        </tr>
        
         <tr>
            <td width="120" class="tdBlue">注册时间：</td>
            <td width="280" name="merchantVO.insertDate" colspan="3"></td>
           <!--  <td width="120" class="tdBlue">更新时间：</td>
            <td width="280" name="merchantVO.updateDate"></td> -->
        </tr>
        
        <!-- <tr>
            <td width="120" height="70" class="tdBlue">用户角色：</td>
            <td colspan="3">
                <div id="role_list"></div>
            </td>
        </tr>
        <tr>
            <td width="120" height="70" class="tdBlue">用户权限：</td>
            <td colspan="3">
                <div id="permission_list"></div>
            </td>
        </tr> -->
    </tbody>
</table>

<!--公告 end-->
<div class="searchWrap">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="searchLeft"></td>
            <div class="title">
                <h2 class="icon1">账户余额充值记录</h2>
            </div>
            <!--标题 end-->
        </tr>
    </table>
</div>

<!--标签选项卡 end-->
<form action="merchant/myInfoAction!viewBaseUserDetailVO.action?personId=${currUser.personId}" method="post" id="id_form_search">
    <!-- <div class="searchWrap">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="searchLeft"></td>
                <td class="searchBg">
                    <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="searchRight"></td>
            </tr>
            <tr>
                <td class="searchButtomLeft"></td>
                <td class="searchButtom"></td>
                <td class="searchButtoRight"></td>
            </tr>
        </table>
    </div> -->
</form>
<!--标题 end-->
<div class="pageDown" id="pageTop"></div>
<!--翻页 end-->

<div class="operation">
	<input class="btnS4" type="button" value="导出" permission="merchantpayExport"  onclick="javascript:ajaxExport();"/>
</div>

<!--工具栏 end-->
<script type="text/javascript">

    $(document).ready(function () {
        doSearch(1);
//        _showTip("hahaha1");
    });
    function doSearch($pageIndex) {
    	loading();
    	$("#id_form_search").attr("action","${baseURL}/merchantpay/merchantpayAction!ajaxMySearch.action?personId=${currUser.personId}");
        $('#id_form_search').ajaxSubmit({
            dataType:'json',
            data:{page:$pageIndex},
            success:function (result) {
                if ("0" == result.code) {
                    var list1 = new ListTable(
                            {
                                checkableColumn:"tbMerchatPayId",
                                title:[
                                    {width:"5%", name:"tbMerchatPayId", showName:"订单号"},
                                    {width:"5%", name:"userName", showName:"充值账户名称"},
                                    {width:"5%", name:"tbMerchatPayMoney", showName:"充值金额"},
                                    {width:"10%", name:"tbMerchatPayInsert", showName:"充值时间"},
                                    {width:"5%", name:"tbMerchatPayStatusDesc", showName:"充值状态"}
                                ],
                                data:result.data.pageData,
                                trStyle:["trWhite"]
                            });
                    list1.createList("id_list_config");
                    var pager = new Pager({totalPages:result.data.maxPage, currentPage:result.data.currPage, funcName:"doSearch"});
                    pager.createPageBar("idPage");
                    pager.createPageBar("pageTop");
                } else {
                    $("#tip").error({title:"信息提示层", content:result.desc, button:[
                        {text:"确定", events:"test"},
                        {text:"取消", events:"test"}
                    ]});
                }
            },
            complete:function(){
            	loaded();
            },
            error:function(){
            	loaded();
            }
        });
    }
    
    /**
     * 启用或禁用用户
     * @param userId
     * @param disabled
     */
    function updateStatus(id, status) {
        $.ajax({
            type:'post',
            url:'${baseURL}/merchandise/merchandiseAction!updateStatus.action',
            data:{
                'id':id,
                'status':status,
                'dqdp_csrf_token': dqdp_csrf_token
            },
            dataType:'json',
            success:function (result) {
                if (result.code == '0') {
                    alert("操作成功");
                    window.location.href = 'merchandise_list.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token;
                } else {
                    alert(result.desc);
                }
            },
            error:function () {
                alert('操作失败，请稍候再试');
            }
        });
    }
    
    function ajaxExport(){
    	if($("#id_pageTop_page").size()!=0) {
    		 $("#id_form_search").attr("action","${baseURL}/merchantpay/merchantpayAction!export.action");
 	        $("#id_form_search").submit();
    	}else{
    		_alert("提示","没有数据可导出");
    	}
	       
	 }
    
</script>
<div id="id_list_config" delUrl="${baseURL}/merchantpay/merchantpayAction!batchDeleteTbCzMerchantPayPO.action"
     editUrl="${baseURL}/jsp/component/merchandise/merchandise_edit.jsp" viewUrl="${baseURL}/jsp/component/merchandise/merchandise_view.jsp">
</div>

<!--表格 end-->

<div class="toolBar" permission="">
    <!--筛选 end-->
    <div class="pageDown" id="idPage">

    </div>
    <!--翻页 end-->
    <div class="operation">
    	<input class="btnS4" type="button" value="导出" permission="merchantpayExport"  onclick="javascript:ajaxExport();"/>
        <input class="btnS4" type="button" value="删除记录" permission="merchantpayDelete" onclick="javascript:_doDel('id_list_config')"/>
    </div>
    <!--对表格数据的操作 end-->
</div>
<div>&nbsp;
</div>

<!-- <div class="toolBar">
    <div align="center">
        <input class="btn4" type="button" onclick="javascript:history.back();" value="返 回"/>
    </div>
</div> -->

<!--工具栏 end-->
<script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jgxLoader.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        //listRole();
        //listPermissionByUser();

    });

    function listRole() {
        $.ajax({
            type:'post',
            url:'${baseURL}/role/role!listRoleByPersonId.action',
            dataType:'json',
            data:{'personId':'${param.personId}'},
            success:function (result) {
                if ('0' == result.code) {
                    var roleTable = new DataTable({
                        data:result.data.roleList,
                        colsCount:5,
                        fieldName:"roleName"
                    });
                    roleTable.createTable("role_list");
                } else {
                    alert(result.desc);
                }
            },
            error:function () {
                alert('通讯错误');
            }
        });
    }

    function listPermissionByUser() {
        $.ajax({
            url:"${baseURL}/permissionmgr/permissionmgr!listPermissionByUserId.action",
            type:"post",
            dataType:"json",
            data:{
                "userId":"${param.userId}"
            },
            success:function (result) {
                if ("0" == result.code) {
                    var permissionTable = new DataTable({
                        data:result.data.permissionList,
                        colsCount:5,
                        fieldName:"permissionName"
                    });
                    permissionTable.createTable("permission_list");
                    _resetFrameHeight();
                } else {
                    alert(result.desc);
                }
            },
            error:function () {
                alert("通讯错误");
            }
        });
    }

    function listUser() {
        window.location.href = 'orgUserList.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token;
    }

    function cleanForm() {}

</script>

</body>
</html>
