<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value=""></jsp:param>
    <jsp:param name="permission" value=""></jsp:param>
    <jsp:param name="mustPermission" value="payView"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>流量充值-查看</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form   method="post" id="id_form_add">
    <table class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0"
           dataSrc="${baseURL}/pay/payAction!ajaxView.action?id=${param.id}">
        <thead>
            <tr>
                <th colspan="4">流量充值信息</th>
            </tr>
        </thead>
        <tbody>
            <input type="hidden" name="tbCzMerchandisePO.merId"/>
            <input type="hidden" name="modelCode"/>
            <tr>
                <td width="120" class="tdBlue">订单号：</td>
                <td width="200" name="tbCzPayPO.payId"></td>
                <td width="120" class="tdBlue">代理商：</td>
                <td width="200" name="tbCzPayPO.name"></td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">充值手机：</td>
                <td width="200" name="tbCzPayPO.payMoblie"></td>
                <td width="120" class="tdBlue">充值状态：</td>
                <td width="200" name="tbCzPayPO.payStatusDesc"></td>
            </tr>
             <tr>
                <td width="120" class="tdBlue">套餐名称：</td>
                <td width="200" name="tbCzPayPO.merName"></td>
                <td width="120" class="tdBlue">套餐原价：</td>
                <td width="200" name="tbCzPayPO.payOriginalMoney"></td>
            </tr>
             <tr>
                <td width="120" class="tdBlue">套餐折扣价：</td>
                <td width="200" name="tbCzPayPO.payDiscountMoney"></td>
                <td width="120" class="tdBlue">新增时间：</td>
                <td width="200" name="tbCzPayPO.payStartDate"></td>
            </tr>
             <tr>
                <td width="120" class="tdBlue">完成时间：</td>
                <td width="200" name="tbCzPayPO.payEndDate"></td>
                <td width="120" class="tdBlue">更新时间：</td>
                <td width="200" name="tbCzPayPO.payUpdate"></td>
            </tr>
             <tr>
                <td width="120" class="tdBlue">操作人：</td>
                <td width="200" colspan="3"  name="tbCzPayPO.payOper" ></td>
            </tr>
        </tbody>
    </table>
</form>

<!--公告 end-->
<div class="searchWrap">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="searchLeft"></td>
            <div class="title">
                <h2 class="icon1">流量充值操作记录</h2>
            </div>
            <!--标题 end-->
        </tr>
    </table>
</div>

<!--标签选项卡 end-->
<form action="${baseURL}/merchantpay/merchantpayAction!ajaxSearch.action?personId=${param.personId}" method="post" id="id_form_search">
	<input class="form120px" name="searchValue.payId" type="hidden" value="${param.id }"/>
</form>
<!--标题 end-->
<div class="pageDown" id="pageTop"></div>
<!--翻页 end-->

<div class="operation">
	<!-- <input class="btnS4" type="button" value="导出" permission="merchantpayExport"  onclick="javascript:ajaxExport();"/> -->
</div>

<!--工具栏 end-->
<script type="text/javascript">

    $(document).ready(function () {
        doSearch(1);
//        _showTip("hahaha1");
    });
    function doSearch($pageIndex) {
    	loading();
    	$("#id_form_search").attr("action","${baseURL}/payInfo/payInfoAction!ajaxSearch.action");
        $('#id_form_search').ajaxSubmit({
            dataType:'json',
            data:{page:$pageIndex},
            success:function (result) {
                if ("0" == result.code) {
                    var list1 = new ListTable(
                            {
                                checkableColumn:"id",
                                title:[
                                    {width:"10%", name:"payId", showName:"订单号"},
                                    {width:"5%", name:"statusDesc", showName:"操作状态"},
                                    {width:"20%", name:"info", showName:"操作信息"},
                                    {width:"10%", name:"oper", showName:"操作人"},
                                    {width:"10%", name:"updateDate", showName:"更新时间"},
                                    {width:"10%", name:"operationDesc",showName:"结果描述", isOperationColumn:true}
                                ],
                                data:result.data.pageData,
                                operations:[
                                            {name:"查看操作图片",condition:function (index, content) {
                                                return content.path !="";
                                            },
                                            event:function (index, content) {
                                            	window.open("${baseURL}/" + content.path);
                                            }}
                                        ],
                                trStyle:["trWhite"]
                            });
                    list1.createList("id_list_config");
                    var pager = new Pager({totalPages:result.data.maxPage, currentPage:result.data.currPage, funcName:"doSearch"});
                    pager.createPageBar("idPage");
                    pager.createPageBar("pageTop");
                } else {
                    $("#tip").error({title:"信息提示层", content:result.desc, button:[
                        {text:"确定", events:"test"},
                        {text:"取消", events:"test"}
                    ]});
                }
            },
            complete:function(){
            	loaded();
            },
            error:function(){
            	loaded();
            }
        });
    }
    
    /* function ajaxExport(){
    	if($("#id_pageTop_page").size()!=0) {
    		 $("#id_form_search").attr("action","${baseURL}/merchantpay/merchantpayAction!export.action");
 	        $("#id_form_search").submit();
    	}else{
    		_alert("提示","没有数据可导出");
    	}
	       
	 } */
    
</script>
<div id="id_list_config">
</div>

<!--表格 end-->

<div class="toolBar" permission="">
    <!--筛选 end-->
    <div class="pageDown" id="idPage">

    </div>
    <!--翻页 end-->
    <div class="operation">
    	<!-- <input class="btnS4" type="button" value="导出" permission="merchantpayExport"  onclick="javascript:ajaxExport();"/> -->
    </div>
    <!--对表格数据的操作 end-->
</div>
<div>&nbsp;
</div>

<div class="toolBar">
    <div align="center">
        <input class="btn4" type="button" onclick="javascript:history.back();" value="返 回"/>
    </div>
</div>

<!--工具栏 end-->
<script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jgxLoader.js"></script>
<!--工具栏 end-->
<script type="text/javascript">
</script>
</body>
</html>
