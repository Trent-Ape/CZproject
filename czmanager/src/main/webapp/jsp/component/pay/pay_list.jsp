<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value="payStatus"></jsp:param>
    <jsp:param name="permission" value="payExport,payView,payUpdateStatus,payUpdateStatusCompile,payUpdateStatusFail,payUpdateStatusMoney,payUpdateStatusAppealSuccess,payUpdateStatusAppealFail"></jsp:param>
    <jsp:param name="mustPermission" value="payList"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>充值订单-列表</title>
    <link type="text/css" href="${baseURL}/themes/do1/jquery-ui/jquery.ui.all.css" rel="stylesheet"/>
    <link href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/do1/common/error1.0.js"></script>
    <script src="${baseURL}/js/do1/common/pop_up1.0.js"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
    <script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jquery-ui-1.8.custom.min.js"></script>
    <script src="${baseURL}/js/DatePicker/WdatePicker.js"></script>

</head>

<body>
<!--头部 end-->


<!--公告 end-->
<div class="searchWrap">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="searchLeft"></td>
            <div class="title">
                <h2 class="icon1">流量充值管理</h2>
            </div>
            <!--标题 end-->
        </tr>
    </table>
</div>

<!--标签选项卡 end-->
<form action="${baseURL}/pay/payAction!ajaxSearch.action" method="post" id="id_form_search">
    <div class="searchWrap">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="searchLeft"></td>
                <td class="searchBg">
                    <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>
                                        <td width="100" height="30">订单号：</td>
                                        <td width="140"><input class="form120px" keySearch="doSearch('1')" name="searchValue.payId" type="text" value=""/></td>
                                        <td width="100" height="30">充值流量号码：</td>
                                        <td width="140"><input class="form120px" keySearch="doSearch('1')"  name="searchValue.payMoblie" type="text" value=""/></td>
                                       	<td width="80" height="30">流量套餐：</td>
                                        <td width="100"><select class="form120px" name="searchValue.payMerchandiseId"  id="payMerchandiseId"    /></td>
                                        <td width="80" height="30">充值状态：</td>
                                        <td width="140"><select class="form120px" name="searchValue.payStatus"  dictType="payStatus"    /></td>
                                    </tr>
                                    <tr>
                                        <td width="60" height="30">代理商账号：</td>
                                        <td width="140"><input class="form120px" keySearch="doSearch('1')" name="searchValue.userName" type="text" value=""/></td>
                                        <td width="60" height="30">充值请求时间：</td>
                                        <td width="280" colspan="3">
                                        <input  class="form120px" width="100" name="searchValue.startDate"  id="searchStartDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true,startDate:'%y-%M-%d 00:00:00'});"  type="text" keySearch="keySearchFunc()"/>
                                                                                                                 至<input   class="form120px" width="100" id="searchEndDate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true,startDate:'%y-%M-%d 23:59:59'});"  name="searchValue.endDate" keySearch="keySearchFunc()"/></td>
                                        <td colspan="2"> <input   class="btnQuery" name="input" type="button" value="查询" onclick="doSearch(1)"/></td>
                                    </tr>
                                </table>
                            </td>  
                        </tr>
                    </table>
                </td>
                <td class="searchRight"></td>
            </tr>
            <tr>
                <td class="searchButtomLeft"></td>
                <td class="searchButtom"></td>
                <td class="searchButtoRight"></td>
            </tr>
        </table>
    </div>
</form>
<!--标题 end-->
<div class="pageDown" id="pageTop"></div>
<!--翻页 end-->

<div class="operation">
	<input class="btnS4" type="button" value="导出" permission="payExport"  onclick="javascript:ajaxExport();"/>
   <!--  <input class="btnS4" type="button" value="新增商品" onclick="javascript:window.location.href='merchandise_add.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token"/>
    <input class="btnS4" type="button" value="删除商品" onclick="javascript:_doDel('id_list_config')"/> -->
</div>

<script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jgxLoader.js"></script>
<!--工具栏 end-->
<script type="text/javascript">

    $(document).ready(function () {
    	initPay();
        doSearch(1);
//        _showTip("hahaha1");
    });
    function doSearch($pageIndex) {
    	loading();
    	$("#id_form_search").attr("action","${baseURL}/pay/payAction!ajaxSearch.action");
        $('#id_form_search').ajaxSubmit({
            dataType:'json',
            data:{page:$pageIndex},
            success:function (result) {
                if ("0" == result.code) {
                    var list1 = new ListTable(
                            {
                                checkableColumn:"payId",
                                title:[
                                    {width:"2%", name:"payId", showName:"订单ID",isCheckColunm:true},
                                    {width:"4%", name:"payId", showName:"订单号"},
                                    {width:"5%", name:"userName", showName:"代理商账号"},
                                    {width:"4%", name:"payMoblie", showName:"充流量号码"},
                                    {width:"4%", name:"merName", showName:"套餐名称"},
                                    {width:"4%", name:"payStatusDesc", showName:"充值状态"},
                                    {width:"4%", name:"payOriginalMoney", showName:"套餐原价"},
                                    {width:"4%", name:"payDiscountMoney", showName:"折扣价格"},
                                    {width:"7%", name:"payStartDate", showName:"充值请求时间"},
                                    {width:"7%", name:"payEndDate", showName:"充值完成时间"},
                                    {width:"18%", name:"operationDesc",showName:"结果描述", isOperationColumn:true}
                                ],
                                data:result.data.pageData,
                                operations:[
									{name:"查看", permission:"payView", event:function (index, content) {
										_doSignlView("id_list_config", content.payId);
									}},
                                    {name:"充值完成", permission:"payUpdateStatusCompile",condition:function (index, content) {
                                        return content.payStatus =="1";
                                    },
                                    event:function (index, content) {
                                    	updateStatus(content.payId,"2");
                                    }},
                                    {name:"充值失败", permission:"payUpdateStatusFail",condition:function (index, content) {
                                        return content.payStatus =="1"||content.payStatus =="2";
                                    }
                                    ,event:function (index, content) {
                                    	updateStatus(content.payId,"3");
                                    }},
                                    {name:"退款", permission:"payUpdateStatusMoney",condition:function (index, content) {
                                        return content.payStatus =="3"||content.payStatus =="6";
                                    }
                                    ,event:function (index, content) {
                                    	updateStatus(content.payId,"4");
                                    }},
                                    {name:"申诉成功", permission:"payUpdateStatusAppealSuccess",condition:function (index, content) {
                                        return content.payStatus =="5";
                                    }
                                    ,event:function (index, content) {
                                    	updateStatus(content.payId,"6");
                                    }},
                                    {name:"申诉失败", permission:"payUpdateStatusAppealFail",condition:function (index, content) {
                                        return content.payStatus =="5";
                                    }
                                    ,event:function (index, content) {
                                    	updateStatus(content.payId,"7");
                                    }}
                                ],
                                trStyle:["trWhite"]
                            });
                    list1.createList("id_list_config");
                    var pager = new Pager({totalPages:result.data.maxPage, currentPage:result.data.currPage, funcName:"doSearch"});
                    pager.createPageBar("idPage");
                    pager.createPageBar("pageTop");
                } else {
                    $("#tip").error({title:"信息提示层", content:result.desc, button:[
                        {text:"确定", events:"test"},
                        {text:"取消", events:"test"}
                    ]});
                }
            },
            complete:function(){
            	loaded();
            },
            error:function(){
            	loaded();
            }
        });
    }
    
    /**
     * 启用或禁用用户
     * @param userId
     * @param disabled
     */
    function updateStatus(id, status) {
    	var url="";
    	var oper="";
    	if(status=="2"){
    		url ="${baseURL}/pay/payAction!payUpdateStatusCompile.action";
    		oper="充值完成";
    	} else if(status=="3"){
    		url ="${baseURL}/pay/payAction!payUpdateStatusFail.action";
    		oper="充值失败";
    	} else if(status=="4"){
    		url ="${baseURL}/pay/payAction!payUpdateStatusMoney.action";
    		oper="退款，返还充值金额";
    	} else if(status=="6"){
    		url ="${baseURL}/pay/payAction!payUpdateStatusAppealSuccess.action";
    		oper="申诉成功";
    	} else if(status=="7"){
    		url ="${baseURL}/pay/payAction!payUpdateStatusAppealFail.action";
    		oper="申诉失败";
    	}
    	if(confirm("确定操作：" + oper)){
    		$.ajax({
                type:'post',
                url:url,
                data:{
                    'id':id,
                    'status':status,
                    'dqdp_csrf_token': dqdp_csrf_token
                },
                dataType:'json',
                success:function (result) {
                    if (result.code == '0') {
                        alert("操作成功");
                        window.location.href = 'pay_list.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token;
                    } else {
                        alert(result.desc);
                    }
                },
                error:function () {
                    alert('操作失败，请稍候再试');
                }
            });
    	}
        
    }
    
    function ajaxExport(){
    	if($("#id_pageTop_page").size()!=0) {
    		 $("#id_form_search").attr("action","${baseURL}/pay/payAction!export.action");
 	        $("#id_form_search").submit();
    	}else{
    		_alert("提示","没有数据可导出");
    	}
	 }
    
    function initPay() {
        $.ajax({
            url:"${baseURL}/pay/payAction!initItemMerchandise.action",
            type:"post",
            dataType:"json",
            success:function (result) {
                if ("0" == result.code) {
                	var data = result.data.merchandiseList;
                	$("#payMerchandiseId").append("<option value=''>请选择</option>");
                	if(data&&data.length > 0){
                		$(data).each(function(){
                			
                			$("#payMerchandiseId").append("<option value="+this.merId+">"+this.merName + "</option>");
                		});
                	}
                } else {
                    _alert("错误提示",result.desc);
                }
            },
            error:function () {
                alert("通讯错误");
            }
        });
    }
    
</script>
<div id="id_list_config" delUrl="${baseURL}/pay/payAction!batchDeleteTbCzMerchandisePO.action"
     editUrl="${baseURL}/jsp/component/merchandise/merchandise_edit.jsp" viewUrl="${baseURL}/jsp/component/pay/pay_view.jsp">
</div>

<!--表格 end-->

<div class="toolBar" permission="">
    <!--筛选 end-->
    <div class="pageDown" id="idPage">

    </div>
    <!--翻页 end-->
    <div class="operation">
    	<input class="btnS4" type="button" value="导出" permission="payExport"  onclick="javascript:ajaxExport();"/>
       <!--  <input class="btnS4" type="button" value="新增商品" onclick="javascript:window.location.href='merchandise_add.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token"/>
        <input class="btnS4" type="button" value="删除商品" onclick="javascript:_doDel('id_list_config')"/> -->
    </div>
    <!--对表格数据的操作 end-->
</div>
<div>&nbsp;
</div>

</body>

</html>
