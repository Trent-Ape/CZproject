<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value="merchandiseStatus"></jsp:param>
    <jsp:param name="permission" value=""></jsp:param>
    <jsp:param name="mustPermission" value="merchandiseView"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>充值套餐-编辑</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form   method="post" id="id_form_add">
    <table class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0"
           dataSrc="${baseURL}/merchandise/merchandiseAction!ajaxView.action?id=${param.id}">
        <thead>
            <tr>
                <th colspan="4">充值套餐-编辑</th>
            </tr>
        </thead>
        <tbody>
            <input type="hidden" name="tbCzMerchandisePO.merId"/>
            <input type="hidden" name="modelCode"/>
            <tr>
                <td width="120" class="tdBlue">套餐名称：</td>
                <td width="85%" name="tbCzMerchandisePO.merName"></td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">套餐价格(单元 元)：</td>
                <td width="85%" name="tbCzMerchandisePO.merMoney"></td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">套餐状态：</td>
                <td width="85%" name="tbCzMerchandisePO.merStatusDesc"></td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">套餐排序：</td>
                <td width="85%" name="tbCzMerchandisePO.merOrder" ></td>
            </tr>
             <tr>
                <td width="120" class="tdBlue">新增时间：</td>
                <td width="85%" name="tbCzMerchandisePO.merInsert" ></td>
            </tr>
             <tr>
                <td width="120" class="tdBlue">更新时间：</td>
                <td width="85%" name="tbCzMerchandisePO.merUpdate" ></td>
            </tr>
             <tr>
                <td width="120" class="tdBlue">操作人：</td>
                <td width="85%" name="tbCzMerchandisePO.merOper" ></td>
            </tr>
        </tbody>
    </table>
</form>

<div class="toolBar">
    <div align="center">
        <input class="btn4" type="button" onclick="javascript:history.back();" value="返 回"/>
    </div>
</div>

<!--工具栏 end-->
<script type="text/javascript">
</script>
</body>
</html>
