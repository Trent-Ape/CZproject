<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value="merchandiseStatus"></jsp:param>
    <jsp:param name="permission" value=""></jsp:param>
    <jsp:param name="mustPermission" value="merchandiseAdd"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>套餐新增</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <link href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form action="${baseURL}/merchandise/merchandiseAction!addTbCzMerchandisePO.action" method="post" id="id_form_add">
    <table class="tableCommon mt5" width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead>
            <tr>
                <th colspan="4">新增套餐</th>
            </tr>
        </thead>
        <tbody>
            <input type="hidden" name="tbCzMerchandisePO.merId"/>
            <input type="hidden" name="modelCode"/>
            <tr>
                <td width="120" class="tdBlue">套餐名称：</td>
                <td width="85%"><input style="width: 500px" type="text" name="tbCzMerchandisePO.merName" valid="{must:true,length:20,tip:'套餐名称'}" />
                	<span  style="color: red;margin-left: 10px;">*最大20位字母组合</span>
                </td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">套餐价格：</td>
                <td width="85%"><input style="width: 500px" class="form120px" name="tbCzMerchandisePO.merMoney" type="text" valid="{must:true,tip:'套餐价格',fieldType:'pattern',regex:'^\\d+(.?\\d{1,2})?$'}" />
                	<span  style="color: red;margin-left: 10px;">*小数点2位有效数字</span>
                </td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">套餐状态：</td>
                <td width="85%"><select name="tbCzMerchandisePO.merStatus" defaultValue="1" dictType="merchandiseStatus"  valid="{must:true,length:100,tip:'套餐状态'}"/></td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">套餐排序：</td>
                <td width="85%"><input  style="width: 500px" type="text" name="tbCzMerchandisePO.merOrder" valid="{must:true, fieldType:'pattern', regex:'^\\d*$', tip:'套餐排序'}"/>
               		<span  style="color: red;margin-left: 10px;">*越小，排序越前</span>
                </td>
            </tr>
        </tbody>
    </table>
</form>

<div class="toolBar">
    <div align="center">
        <input class="btn4" type="button" onclick="doSave();" value="保存"/>
        <input class="btn4" type="button" onclick="javascript:history.back();" value="返 回"/>
    </div>
</div>

<!--工具栏 end-->
<script type="text/javascript">
    function doSave() {
        _doCommonSubmit("id_form_add",null,{ok:function(){history.back();},fail:function(){}});
    }
</script>
</body>
</html>
