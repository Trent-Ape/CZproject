<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="dict" value="merchandiseStatus"></jsp:param>
    <jsp:param name="permission" value="merchandiseExport"></jsp:param>
    <jsp:param name="mustPermission" value="merchandiseList"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>充值套餐-列表</title>
    <link type="text/css" href="${baseURL}/themes/do1/jquery-ui/jquery.ui.all.css" rel="stylesheet"/>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/do1/common/error1.0.js"></script>
    <script src="${baseURL}/js/do1/common/pop_up1.0.js"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
    <script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jquery-ui-1.8.custom.min.js"></script>

</head>

<body>
<!--头部 end-->


<!--公告 end-->
<div class="searchWrap">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="searchLeft"></td>
            <div class="title">
                <h2 class="icon1">充值套餐管理</h2>
            </div>
            <!--标题 end-->
        </tr>
    </table>
</div>

<!--标签选项卡 end-->
<form action="${baseURL}/merchandise/merchandiseAction!ajaxSearch.action" method="post" id="id_form_search">
    <div class="searchWrap">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="searchLeft"></td>
                <td class="searchBg">
                    <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>
                                        <td width="100" height="30">充值套餐名称：</td>
                                        <td width="140"><input class="form120px" keySearch="doSearch('1')" name="searchValue.merName" type="text" value=""/></td>
                                        <td width="100" height="30">充值套餐价格：</td>
                                        <td width="140"><input class="form120px" keySearch="doSearch('1')"  name="searchValue.merMoney" type="text" value=""/></td>
                                        <td width="100" height="30">充值套餐状态：</td>
                                        <td width="140"><select class="form120px" name="searchValue.merStatus"  dictType="merchandiseStatus"    /></td>
                                        <td><input class="btnQuery" name="input" type="button" value="查询" onclick="doSearch(1)"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="searchRight"></td>
            </tr>
            <tr>
                <td class="searchButtomLeft"></td>
                <td class="searchButtom"></td>
                <td class="searchButtoRight"></td>
            </tr>
        </table>
    </div>
</form>
<!--标题 end-->
<div class="pageDown" id="pageTop"></div>
<!--翻页 end-->

<div class="operation">
	<input class="btnS4" type="button" value="导出" permission="merchandiseExport"  onclick="javascript:ajaxExport();"/>
    <input class="btnS4" type="button" value="新增充值套餐" onclick="javascript:window.location.href='merchandise_add.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token"/>
    <input class="btnS4" type="button" value="删除充值套餐" onclick="javascript:_doDel('id_list_config')"/>
</div>

<!--工具栏 end-->
<script type="text/javascript">

    $(document).ready(function () {
        doSearch(1);
//        _showTip("hahaha1");
    });
    function doSearch($pageIndex) {
    	$("#id_form_search").attr("action","${baseURL}/merchandise/merchandiseAction!ajaxSearch.action");
        $('#id_form_search').ajaxSubmit({
            dataType:'json',
            data:{page:$pageIndex},
            success:function (result) {
                if ("0" == result.code) {
                    var list1 = new ListTable(
                            {
                                checkableColumn:"merId",
                                title:[
                                    {width:"5%", name:"merId", showName:"充值套餐ID",isCheckColunm:true},
                                    {width:"5%", name:"merName", showName:"充值套餐名称",length:20},
                                    {width:"5%", name:"merMoney", showName:"充值套餐价格",length:70},
                                    {width:"5%", name:"merOrder", showName:"充值套餐排序"},
                                    {width:"5%", name:"merStatusDesc", showName:"充值套餐状态"},
                                    {width:"10%", name:"merInsert", showName:"新增时间"},
                                    {width:"10%", name:"merUpdate", showName:"更新时间"},
                                    {width:"5%", name:"merOper", showName:"操作人"},
                                    {width:"15%", name:"operationDesc", showName:"结果描述", isOperationColumn:true}
                                ],
                                data:result.data.pageData,
                                operations:[
									{name:"查看", permission:"", event:function (index, content) {
										_doSignlView("id_list_config", content.merId);
									}},       
                                    {name:"编辑", permission:"", event:function (index, content) {
                                        _doSignlEdit("id_list_config", content.merId);
                                    }},
                                    {name:"上架", permission:"", condition:function (index, content) {
                                        return content.merStatus != "1";
                                    },event:function (index, content) {
                                    	updateStatus(content.merId,"1");
                                    }},
                                    {name:"下架", permission:"", condition:function (index, content) {
                                        return content.merStatus == "1";
                                    },event:function (index, content) {
                                    	updateStatus(content.merId,"2");
                                    }},
                                    {name:"删除", permission:"", event:function (index, content) {
                                        _doSignlDel("id_list_config", content.merId);
                                    }}
                                ],
                                trStyle:["trWhite"]
                            });
                    list1.createList("id_list_config");
                    var pager = new Pager({totalPages:result.data.maxPage, currentPage:result.data.currPage, funcName:"doSearch"});
                    pager.createPageBar("idPage");
                    pager.createPageBar("pageTop");
                } else {
                    $("#tip").error({title:"信息提示层", content:result.desc, button:[
                        {text:"确定", events:"test"},
                        {text:"取消", events:"test"}
                    ]});
                }
            },
            error:function () {
            }
        });
    }
    
    /**
     * 启用或禁用用户
     * @param userId
     * @param disabled
     */
    function updateStatus(id, status) {
        $.ajax({
            type:'post',
            url:'${baseURL}/merchandise/merchandiseAction!updateStatus.action',
            data:{
                'id':id,
                'status':status,
                'dqdp_csrf_token': dqdp_csrf_token
            },
            dataType:'json',
            success:function (result) {
                if (result.code == '0') {
                    alert("操作成功");
                    window.location.href = 'merchandise_list.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token;
                } else {
                    alert(result.desc);
                }
            },
            error:function () {
                alert('操作失败，请稍候再试');
            }
        });
    }
    
    function ajaxExport(){
    	if($("#id_pageTop_page").size()!=0) {
    		 $("#id_form_search").attr("action","${baseURL}/merchandise/merchandiseAction!export.action");
 	        $("#id_form_search").submit();
    	}else{
    		_alert("提示","没有数据可导出");
    	}
	       
	 }
    
</script>
<div id="id_list_config" delUrl="${baseURL}/merchandise/merchandiseAction!batchDeleteTbCzMerchandisePO.action"
     editUrl="${baseURL}/jsp/component/merchandise/merchandise_edit.jsp" viewUrl="${baseURL}/jsp/component/merchandise/merchandise_view.jsp">
</div>

<!--表格 end-->

<div class="toolBar" permission="">
    <!--筛选 end-->
    <div class="pageDown" id="idPage">

    </div>
    <!--翻页 end-->
    <div class="operation">
    	<input class="btnS4" type="button" value="导出" permission="merchandiseExport"  onclick="javascript:ajaxExport();"/>
        <input class="btnS4" type="button" value="新增充值套餐" onclick="javascript:window.location.href='merchandise_add.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token"/>
        <input class="btnS4" type="button" value="删除充值套餐" onclick="javascript:_doDel('id_list_config')"/>
    </div>
    <!--对表格数据的操作 end-->
</div>
<div>&nbsp;
</div>

</body>

</html>
