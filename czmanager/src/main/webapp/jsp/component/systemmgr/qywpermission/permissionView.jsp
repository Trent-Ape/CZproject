<%@ page import="cn.com.do1.common.util.web.WebUtil" %>
<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../../common/dqdp_common.jsp" %>
<jsp:include page="../../../common/dqdp_vars.jsp">
    <jsp:param name="permission" value=""></jsp:param>
    <jsp:param name="mustPermission" value="permissionView"></jsp:param>
    <jsp:param name="dict" value=""></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>查看权限信息</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
    <style type="text/css">
    </style>
</head>

<body>
<table class="tableCommon mt5" dataSrc='${baseURL}/permissionmgr/permissionmgr!viewPermission.action?perId=<%=WebUtil.getParm(request, "perId", "")%>'
       width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th colspan="4">权限信息</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="120" class="tdBlue">权限名称：</td>
            <td width="280" name="permissionMgrVO.permissionName"></td>
            <td width="120" class="tdBlue">权限代码：</td>
            <td width="280" name="permissionMgrVO.permissionCode"></td>
        </tr>
        <tr>
            <td width="120" class="tdBlue">组件名称：</td>
            <td width="280" name="permissionMgrVO.component"></td>
            <td width="120" class="tdBlue">模块名称：</td>
            <td width="280" name="permissionMgrVO.modelName"></td>
        </tr>
        <tr>
            <td width="120" height="70" class="tdBlue">
                备注：
            </td>
            <td colspan="3">
                <div style="overflow-y: scroll;" name="permissionMgrVO.memo"></div>
            </td>
        </tr>
        <tr>
            <td width="120" height="70" class="tdBlue">
                角色：
            </td>
            <td colspan="3">
                <div id="role_list"></div>
            </td>
        </tr>
    </tbody>
</table>

<div class="toolBar">
    <div align="center">
        <input class="btn4" type="button" onclick="javascript:history.back();" value="返 回"/>
    </div>
</div>

<!--工具栏 end-->

<script type="text/javascript">

    $(document).ready(function () {
        listRole(1);
    });

    function listRole($pageIndx) {
        $.ajax({
            type:'post',
            url:'${baseURL}/role/role!listRoleByPermissionId.action',
            dataType:'json',
            data:{'perId':'${param.perId}'},
            success:function (result) {
                if (result.code == '0') {
                    var roleArray = result.data.roleList;
                    var roleNameStr = '';
                    for (var i = 0; i < roleArray.length; i++) {
                        roleNameStr += roleArray[i].roleName + ",";
                    }
                    $('#role_list').html(roleNameStr);
                }
            }
        });
    }

</script>

</body>
</html>
