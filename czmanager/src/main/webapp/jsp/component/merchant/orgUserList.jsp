<%@page import="cn.com.do1.dqdp.core.ConfigMgr"%>
<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="permission" value="userAdd,userEdit,userDel,userView,userChangePassword,userList,userExport,merchantpayAdd"></jsp:param>
    <jsp:param name="mustPermission" value="userList"></jsp:param>
    <jsp:param name="dict" value="userStatus,personSex,discount"></jsp:param>
</jsp:include>  
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>代理商管理</title>

    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <link href="${baseURL}/js/3rd-plug/jquery-ui-1.8/css/smoothness/jquery-ui-1.8.custom.css" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script type="text/javascript" src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
    <script type="text/javascript" src="${baseURL}/js/do1/common/common.js"></script>
    <script src="${baseURL}/js/DatePicker/WdatePicker.js"></script>
    <script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jquery-ui-1.8.custom.min.js"></script>
    <style type="text/css">
    </style>
    <script type="text/javascript">
    </script>
</head>

<body>
<!--头部 end-->

<div>
    <form action="" method="post" id="user_search">
    	<input type="hidden" name="orgId"  value="<%=ConfigMgr.get("systemmgr", "org_merchant", "deaf8bfe-c403-4b83-84c0-ca1e8db80a05")%>"/>
        <div class="searchWrap">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="searchLeft"></td>
                    <td class="searchBg">
                        <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table class="search" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="80" height="30">代理商账号：</td>
                                            <td width="140"><input class="form120px" name="searchValue.userName" id="searchUserName" type="text" keySearch="keySearchFunc()"/></td>
                                            <td width="60" height="30">手机号码：</td>
                                            <td width="140"><input class="form120px" name="searchValue.moblie" id="searchMobile" type="text" keySearch="keySearchFunc()"/></td>
                                            <td width="60" height="30">状态：</td>
                                            <td width="140"><select width="100" id="searchStatus" name="searchValue.status" dictType="userStatus" /></td>
                                            <td width="60" height="30">享受折扣：</td>
                                            <td width="140"><input width=100 id="searchDiscount" name="searchValue.discount" /></td>
                                            <td>%</td>
                                        </tr>
                                        <tr>
                                            
                                            <td width="60" height="30">账户余额：</td>  
                                            <td width="280" colspan="3"><input class="form120px" width="100" name="searchValue.startMoney"  id="searchStartMoney" type="text" keySearch="keySearchFunc()"/>至<input class="form120px" width="100" id="searchEndMoney" type="text" name="searchValue.endMoney" keySearch="keySearchFunc()"/></td>
                                        	<td width="60" height="30">注册时间：</td>  
                                            <td width="280" colspan="3">
                                            	<input  class="form120px" width="100" name="searchValue.startDate"  id="searchStartDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true,startDate:'%y-%M-%d 00:00:00'});"  type="text" keySearch="keySearchFunc()"/>
                                                                                                                                           至<input   class="form120px" width="100" id="searchEndDate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true,startDate:'%y-%M-%d 23:59:59'});"  name="searchValue.endDate" keySearch="keySearchFunc()"/></td>
                                        	<td colspan="2">
                                        	<input  class="btnQuery" type="button" value="查询" permission="userList"
                                                                     onclick="javascript:listUser(1, currOrgId);"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="searchRight"></td>
                </tr>
                <tr>
                    <td class="searchButtomLeft"></td>
                    <td class="searchButtom"></td>
                    <td class="searchButtoRight"></td>
                </tr>
            </table>
        </div>
    </form>
    <div class="pageDown" id="topIdPage"></div>
    <div class="operation" id="id_user_toolbar">
     	<input class="btnS4" type="button" value="导出" permission="userExport" onclick="javascript:ajaxExport();"/>
        <input class="btnS4" type="button" value="新增代理商" permission="userAdd" onclick="javascript:doPersonAdd()"/>
        <input class="btnS4" type="button" value="删除" onclick="javascript:_doDel('user_list');"/>
        
    </div>
    <div id="user_list" delUrl="${baseURL}/merchant/merchantAction!delBaseUser.action"></div>
    <div class="pageDown" id="downIdPage"></div>
    <div class="operation" id="id_user_toolbar">
     	<input class="btnS4" type="button" value="导出" permission="userExport" onclick="javascript:ajaxExport();"/>
        <input class="btnS4" type="button" value="新增代理商" permission="userAdd" onclick="javascript:doPersonAdd()"/>
        <input class="btnS4" type="button" value="删除" onclick="javascript:_doDel('user_list');"/>
        
    </div>
</div>
<script src="${baseURL}/js/3rd-plug/jquery-ui-1.8/js/jgxLoader.js"></script>
<script type="text/javascript">
    function doSearch() {
        window.location.href = 'orgUserList.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token;
    }

    var currOrgId;

    $(document).ready(function () {
        currOrgId = '<%=ConfigMgr.get("systemmgr", "org_merchant", "deaf8bfe-c403-4b83-84c0-ca1e8db80a05")%>';
        if (!_isNullValue(currOrgId)) {
            listUser(1, currOrgId);
        }
    });

    function doPersonAdd() {
    	currOrgId = '<%=ConfigMgr.get("systemmgr", "org_merchant", "deaf8bfe-c403-4b83-84c0-ca1e8db80a05")%>';
        window.location.href = 'userAdd.jsp?orgId=' + currOrgId+ '&dqdp_csrf_token='+dqdp_csrf_token;
    }

    function keySearchFunc() {
        listUser(1, currOrgId);
    }

    function cleanForm() {
        $("#user_search :input").not(":button,:reset,:submit,:hidden").val("");
    }
    
  	 function ajaxExport(){
  		 
	 	if($("#id_topIdPage_page").size()!=0){
	 		
	    	$("#user_search").attr("action","${baseURL}/merchant/merchantAction!export.action");
	    	$("#user_search").submit();
	 		  
	 	} else{
	 		_alert("提示","没有数据可导出");
	 	}
  	 }

    function listUser(pageIndex, orgId) {
        // 设置当前机构id
        if (orgId != undefined) {
            currOrgId = orgId;
        }
        $("#id_user_toolbar").css("display", "block");
        loading();
        $.ajax({
            url:'${baseURL}/merchant/merchantAction!listPersonByOrgId.action',
            type:'post',
            data:{
                'page':pageIndex,
                'orgId':currOrgId,
                'searchValue.userName':$('#searchUserName').val(),
                'searchValue.mobile':$('#searchMobile').val(),
                'searchValue.discount':$('#searchDiscount').val(),
                'searchValue.startMoney':$('#searchStartMoney').val(),
                'searchValue.endMoney':$('#searchEndMoney').val(),
                'searchValue.startDate':$('#searchStartDate').val(),
                'searchValue.endDate':$('#searchEndDate').val(),
                'searchValue.status':$('#searchStatus').val(),
                'dqdp_csrf_token': dqdp_csrf_token
            },
            dataType:'json',
            success:function (result) {
                if ('0' == result.code) {
                    var userList = new ListTable({
                        checkableColumn:"personId",
                        title:[
                            {width:"5%", name:"", showName:"", isCheckColunm:true},
                            {width:"7%", name:"userName", showName:"代理商账号"},
                            {width:"6%", name:"mobile", showName:"手机号码"},
                            {width:"4%", name:"statusDesc", showName:"状态"},
                            {width:"4%", name:"discount", showName:"享受折扣"},
                            {width:"6%", name:"money", showName:"账户余额"},
                            {width:"10%", name:"insertDate", showName:"注册时间"},
                            {width:"30%", name:"operation", showName:"操作", isOperationColumn:true}
                        ],
                        data:result.data.userPager,
                        operations:[
							{name:"查看", permission:"userView", event:function (index, content) {
							    window.location.href = 'userView.jsp?personId=' + content.personId + '&userId=' + content.userId+ '&dqdp_csrf_token='+dqdp_csrf_token;
							}},
                            {name:"修改", permission:"userEdit", event:function (index, content) {
                                window.location.href = 'userEdit.jsp?personId=' + content.personId + '&userId=' + content.userId+ '&dqdp_csrf_token='+dqdp_csrf_token;
                            }},
                            {name:"充值余额", permission:"merchantpayAdd", event:function (index, content) {
                                window.location.href = '${baseURL}/jsp/component/merchantpay/merchantpay_autoadd.jsp?personId=' + content.personId + '&userId=' + content.userId+ '&dqdp_csrf_token='+dqdp_csrf_token;
                            }},
                            {name:"启用", permission:"userEdit", condition:function (index, content) {
                                return content.status != "0"
                            }, event:function (index, content) {
                                disableUser(content.userId, 'false');
                            }},
                            {name:"禁用", permission:"userEdit", condition:function (index, content) {
                                return content.status == "0"
                            }, event:function (index, content) {
                                disableUser(content.userId, 'true');
                            }},
                            {name:"禁用接口充值", permission:"userEdit", condition:function (index, content) {
                                return content.isClient == "1"
                            }, event:function (index, content) {
                                disableUser1(content.userId, "0");
                            }},
                            {name:"启用接口充值", permission:"userEdit", condition:function (index, content) {
                                return content.isClient == "0" || content.isClient == ""
                            }, event:function (index, content) {
                                disableUser1(content.userId, "1");
                            }},
                            {name:"修改密码", permission:"userChangePassword", event:function (index, content) {
                                window.location.href = 'changePassword.jsp?userId=' + content.userId+ '&dqdp_csrf_token='+dqdp_csrf_token;
                            }},
                            {name:"修改支付密码", permission:"", event:function (index, content) {
                            	window.location.href = 'changePayPassword.jsp?userId=' + content.userId+ '&dqdp_csrf_token='+dqdp_csrf_token;
                            }}
                            
                        ],
                        trStyle:["trWhite"]
                    });
                    userList.createList("user_list");
                    var pager = new Pager({totalPages:result.data.maxPage, currentPage:result.data.currPage, funcName:"listUser"});
                    pager.createPageBar("topIdPage");
                    pager.createPageBar("downIdPage");
                    window.parent.parent._resetFrameHeight(1);
                }
            },
            complete:function(){
            	loaded();
            },
            error:function(){
            	loaded();
            }
        });
    }

    /**
     * 启用或禁用用户
     * @param userId
     * @param disabled
     */
    function disableUser(userId, disabled) {
        $.ajax({
            type:'post',
            url:'${baseURL}/user/user!disableUser.action',
            data:{
                'userId':userId,
                'disabled':disabled,
                'dqdp_csrf_token': dqdp_csrf_token
            },
            dataType:'json',
            success:function (result) {
                if (result.code == '0') {
                    alert("操作成功");
                } else {
                    alert(result.desc);
                }
                window.location.href = 'orgUserList.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token;
            }, 
            complete:function(){
            },
            error:function () {
                _alert("提示",'操作失败，请稍候再试');
            }
        });
    }
    
    /**
     * 启用或禁用用户
     * @param userId
     * @param disabled
     */
    function disableUser1(userId, disabled) {
        $.ajax({
            type:'post',
            url:'${baseURL}/merchant/merchantAction!changeClient.action',
            data:{
                'userId':userId,
                'status':disabled,
                'dqdp_csrf_token': dqdp_csrf_token
            },
            dataType:'json',
            success:function (result) {
                if (result.code == '0') {
                    alert("操作成功");
                } else {
                    alert(result.desc);
                }
                window.location.href = 'orgUserList.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token;
            }, 
            complete:function(){
            },
            error:function () {
                _alert("提示",'操作失败，请稍候再试');
            }
        });
    }


</script>
</body>

</html>
