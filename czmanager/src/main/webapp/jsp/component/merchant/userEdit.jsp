<%@ page import="cn.com.do1.common.util.web.WebUtil" %>
<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="../../common/dqdp_common.jsp" %>
<jsp:include page="../../common/dqdp_vars.jsp">
    <jsp:param name="permission" value="roleList"></jsp:param>
    <jsp:param name="mustPermission" value="userEdit"></jsp:param>
    <jsp:param name="dict" value="userStatus,personSex,discount"></jsp:param>
</jsp:include>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>修改代理商</title>
    <link href="${baseURL}/themes/${style}/css/common.css" rel="stylesheet" type="text/css"/>
    <script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
    <script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
    <script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
    <script src="${baseURL}/js/do1/common/HashMap.js"></script>
    <script src="${baseURL}/jsp/component/systemmgr/js/CheckboxTable.js"></script>
    <style type="text/css">
    </style>
</head>

<body>
<form action="${baseURL}/merchant/merchantAction!updateBaseUser.action" method="post" id="user_edit">
    <table class="tableCommon mt5" dataSrc="${baseURL}/merchant/merchantAction!viewBaseUserVO.action?personId=${param.personId}" width="100%" border="0" cellspacing="0" cellpadding="0">
        <input type="hidden" name="baseUserVO.userId" value=""/>
        <input type="hidden" name="baseUserVO.personId" value=""/>
        <input type="hidden" name="merchantVO.id" value="" />
        <thead>
            <tr>
                <th colspan="4">修改代理商信息</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="120" class="tdBlue">代理商称呼：</td>
                <td width="280"><input class="form120px" name="baseUserVO.personName" type="text"
                                       valid="{must:true,tip:'用户名称',fieldType:'pattern',length:20}"/>
                <span  style="color: red;margin-left: 10px;">1-20位数字、字母、汉字组合</span>                       
                                       </td>
                <td width="120" class="tdBlue">用户状态：</td>
                <td width="280"><select name="baseUserVO.status" dictType="userStatus" defaultValue=""/></td>
            </tr>
            <tr>
                <td width="120" class="tdBlue">年龄：</td>
                <td width="280"><input class="form120px" name="baseUserVO.age" type="text"
                                       valid="{must:false, fieldType:'pattern', regex:'^\\d{1,3}$', tip:'年龄'}"/>
                                       <span  style="color: red;margin-left: 10px;">1-3位数字</span>
                                       </td>
                <td width="120" class="tdBlue">性别：</td>
                <td width="280"><select name="baseUserVO.sex" dictType="personSex" defaultValue=""/></td>
            </tr>
            
            <tr>
            <td width="120" class="tdBlue">折扣(单位 %)：</td>
            <td width="280" ><input name="merchantVO.discount" valid="{must:true, fieldType:'pattern', regex:'^\\d{1,3}$', tip:'折扣'}" />
            <span  style="color: red;margin-left: 10px;">小数点后2位</span></td>
            <td width="120" class="tdBlue">账户余额(单位 元)：</td>
            <td width="280" ><input class="form120px" name="merchantVO.money" type="text" value="0"
                           valid="{must:true, fieldType:'pattern', regex:'^\\d+(.?\\d{1,2})?$', tip:'账户余额'}"/><span  style="color: red;margin-left: 10px;">小数点后2位</span></td>
        </tr>
        
        <tr>
            <td width="120" class="tdBlue">手机号码：</td>
            <td width="280" ><input class="form120px" name="merchantVO.mobile" type="text"
                           valid="{must:false, fieldType:'pattern', regex:'^\\d{11}$', tip:'手机号码'}"/>
                           <span  style="color: red;margin-left: 10px;">11位数字</span>
                           </td>
            <td width="120" class="tdBlue">QQ：</td>
            <td width="280" ><input class="form120px" name="merchantVO.qq" type="text"
                           valid="{must:false, fieldType:'pattern', regex:'^\\d{6,15}$', tip:'QQ'}"/>
                           <span  style="color: red;margin-left: 10px;">6-15位</span>
                           </td>
        </tr>
        <tr>
            <td width="120" class="tdBlue">修改商户信息支付密码：</td>
            <td width="280" colspan="3" ><input class="form120px" name="newPassword" type="password"
                           valid="{must:true, fieldType:'pattern', regex:'^\\w{6,20}$', tip:'支付密码'}"/>
                           <span  style="color: red;margin-left: 10px;">6-11位数字或字母组合，由于修改商户信息数据敏感，请输入正确支付密码才允许修改！</span>
                           </td>
        </tr>
        
        </tbody>
    </table>
</form>

<div class="toolBar">
    <div align="center">
        <input class="btn4" id="save" type="button" value="保存"/>
        <input class="btn4" type="button" onclick="javascript:history.back();" value="返 回"/>
    </div>
</div>

<!--工具栏 end-->
<script type="text/javascript">
    $(document).ready(function () {
    });

    $('#save').click(function () {
        var dqdp = new Dqdp();
        if (dqdp.submitCheck("user_edit")) {
            // 提交数据
            $('#user_edit').ajaxSubmit({
                dataType:'json',
                success:function (result) {
                    if ("0" == result.code) {
                        alert(result.desc);
                        window.location.href=("orgUserList.jsp"+ '?dqdp_csrf_token='+dqdp_csrf_token);
                    } else {
                    	_alert("提示",result.desc);
                    }
                },
                error:function () {
                    alert('修改商户信息失败，可能是由网络原因引起的，请稍候再试');
                }
            });
        }
    });

</script>

</body>
</html>
