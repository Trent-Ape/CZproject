<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<jsp:include page="common/dqdp_vars.jsp">
	<jsp:param name="dict" value=""></jsp:param>
</jsp:include>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>申请代理商</title>
	<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
	<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
	<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
	<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
	<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body style="background: #fff;">
	<header>
		<div class="head headList">
			<h2>进驻平台流程：</h2>
		</div>
	</header>
	
	<section class="pb70">
		<div class="faqBox lcBox">
			<h3>1、注册账号</h3>
			<h3>2、目前平台是什么价格呢？</h3>
			<div class="cz">
				<p>充值越多折扣越大（暂定）：充值100元到400元的1.6元50M原价</p>
				<p>充值400元到1000元的1.5元50M（0.93折）</p>
				<p>充值1000元以上的1.46元50M（0.91折）</p>
			</div>			
			<h3>一、代理如何充值?</h3>
			<p>答：请和客服联系，目前充值方式暂时为支付宝、微信红包。</p>
		</div>
	</section>
	
	<!--
    	底部悬浮
    -->
	<%@include file="common/foot.jsp" %>
	
</body>

<script src="${baseURL}/jsp/wap/js/main.js" type="text/javascript"></script>
</html>