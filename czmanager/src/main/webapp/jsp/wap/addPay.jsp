<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<jsp:include page="common/dqdp_vars.jsp">
	<jsp:param name="dict" value=""></jsp:param>
</jsp:include>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>充流量</title>
<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form action="${baseURL}/merchant/myInfoAction!addPay.action" method="post" id="id_form_add">
	<header>
		<div class="head head_item">
			广东移动手机4G、3G、2G通用网络流量充值
		</div>
	</header>
	
	<section class="pb70">
		<div class="itemBox">
			<div class="itemInfo">
				<dl>
					<dt>请输入充值号码：</dt>
					<dd>
						<input type="text"  class="putStyle" id="payMoblie" placeholder="请输入11位联系手机"  type="text" name="tbCzPayPO.payMoblie" valid="{must:true,tip:'冲流量手机',fieldType:'pattern',regex:'^\\d{11}$'}" />
						<a href="javascript:;" class="closeData8"></a>
					</dd>
				</dl>
			</div>
			<div class="itemInfo">
				<dl>
					<dt>请选择套餐：</dt>
					<dd>
						<select   class="iSelect" id="payMerchandiseId" name="tbCzPayPO.payMerchandiseId" valid="{must:true,length:100,tip:'流量套餐'}">
						</select>
						<i class="icoDown"></i>
					</dd>
				</dl>
			</div>	
		</div>
		
		<div class="czInfoPacel">
			<div class="czInfoBox">
				<h3 id="money" >
					<div id=""><span>折扣价 <s class="zkPrice"></s> 元</span><span class="prevPrice">原价<s></s> 元</span></div>
				</h3>
				<%--<h3>--%>
					<%--<span>当前余额 <s class="bluePrice">80.0</s>元</span>--%>
					<%--<span class="prevPrice">可充值<s class="bluePrice">500M</s></span>--%>
				<%--</h3>--%>
			</div>
		</div>
		
		<div class="boxBtn">
			<a href="#" class="btnmode btnPrice">立即充值</a>
			<a href="${baseURL}/jsp/wap/addBatchPay.jsp" class="btnmode btnPriceAll">我要批量充 ></a>
		</div>
		
	</section>	 
	
	<!--
    	底部悬浮
    -->
	<%@include file="common/foot.jsp" %>

	<!--loading-->
	<div class="layerFixPAY layerLoading" style="display: none;"><!--需隐藏的话将layerLoading设置为diaplsy：none-->
		<div class="layerPop layerPopPwd layerPopPay layerPopLoading">
			<p><img src="images/loading.gif" width="43"/></p>
			<div class="payPut loadNote">
				<i class="loadText"></i>努力加载中...
			</div>
			<%--<div class="loadboxPacel">--%>
				<%--<div class="loadbox">--%>
					<%--<div class="loadingCur"></div>--%>
				<%--</div>--%>
			<%--</div>--%>
		</div>
	</div>

	
	<!--支付弹窗-->
    <div class="layerFixPAY hideObj">
    	<div class="layerPop layerPopPwd layerPopPay">
    		<p>输入支付密码</p>
    		<div class="payPut">
    			<input type="password" name="newPassword" class="payPutItem" style="letter-spacing:0.6em;"/>
    		</div>
    		<div class="payBox">
    			<a href="javascript:doSave();" class="btnmode btnSure">确认</a>
    			<a href="javascript:;" class="btnmode btnCancel">取消</a>
    		</div>
    	</div>
    </div>
</form>
</body>

<script src="${baseURL}/jsp/wap/js/main.js" type="text/javascript"></script>
<script type="text/javascript">
	document.querySelector('.payPutItem').oninput = function(){
	   if(this.value.length >=5){
	     this.value = this.value.substr(0,6)
	      return;
	   }
	 }

	function doSave() {
		var dqdp = new Dqdp();
		if (dqdp.submitCheck("id_form_add")) {
			var message="确定为手机:"+$("#payMoblie").val()+" 充值套餐：" +$("#payMerchandiseId").find("option:selected").text();
                $.ajax({
                    url: $("#id_form_add").attr("action"),
                    type:"post",
                    data:$("#id_form_add").serialize(),
                    dataType:"json",
					beforeSend:function(){
						$(".layerLoading").show();
					},
					complete:function(){
						$(".layerLoading").hide();
					},
                    success:function (result) {
                        if ("0" == result.code) {
                            alert(result.desc);
                            //window.location.href="${baseURL}/jsp/wap/payQuery.jsp";
                            window.location.reload();
                        } else{
                            alert(result.desc);
                        }
                    },
                    error:function () {
                        alert("通讯错误");
                    }
                });
		}
	}

	$(document).ready(function() {
		initPay();

		$("#payMerchandiseId").change(function(){
			$("#money").children().hide();
			$("#money [id='"+this.value+"']").show();
		});

        //弹出支付层
        $('.btnPrice').on('click',function(){
            var dqdp = new Dqdp();
            if (dqdp.submitCheck("id_form_add")) {
                $('.layerFixPAY').show();
            }
        });
	});

	function initPay() {
		$.ajax({
			url:"${baseURL}/merchant/myInfoAction!initPay.action",
			type:"post",
			dataType:"json",
			beforeSend:function(){
				$(".layerLoading").show();
			},
			complete:function(){
				$(".layerLoading").hide();
			},
			success:function (result) {
				if ("0" == result.code) {
					var data = result.data.merchandiseList;
					$("#payMerchandiseId").append("<option value=''>请选择套餐类型</option>");
					if(data&&data.length > 0){
						$(data).each(function(){

							$("#payMerchandiseId").append("<option value="+this.merId+">"+this.merName + "</option>");
							var priceHtml='<div style="display:none;" id="@merId"><span>折扣价 <s class="zkPrice">@merDiscountMoney</s> 元</span><span class="prevPrice">原价<s>@merMoney</s> 元</span></div>';
							priceHtml =priceHtml.replace("@merId",this.merId);
							priceHtml =priceHtml.replace("@merDiscountMoney",this.merDiscountMoney);
							priceHtml =priceHtml.replace("@merMoney",this.merMoney);
							$("#money").append(priceHtml);
						});
					}
				} else {
					_alert("错误提示",result.desc);
				}
			},
			error:function () {
				alert("通讯错误");
			}
		});
	}
</script>
</html>