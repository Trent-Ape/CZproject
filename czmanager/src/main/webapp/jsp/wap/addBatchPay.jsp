<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<jsp:include page="common/dqdp_vars.jsp">
	<jsp:param name="dict" value=""></jsp:param>
</jsp:include>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>充流量</title>
<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form action="${baseURL}/merchant/myInfoAction!addBatchPay.action" method="post" id="id_form_add">
	<header>
		<div class="head head_item head_item_l">
			批量充流量
		</div>
	</header>
	
	<section class="pb70">
		<div class="itemBox itemBoxPay">			
			<div class="allPayBox">
				<textarea id="payMoblie"   name="tbCzPayPO.payMoblie" valid="{must:true,tip:'充流量内容'}" placeholder="在这里输入要充值的号码 格式[手机号码 套餐]" class="allPayPhone"></textarea>
			</div>
		</div>
		
		<%--<div class="czInfoPacel">--%>
			<%--<div class="czInfoBox">--%>
				<%--<h3>--%>
					<%--<span>折扣价 <s class="zkPrice">8.0</s> 元</span>--%>
					<%--<span class="prevPrice">原价<s>10.0</s> 元</span>--%>
				<%--</h3>--%>
				<%--<h3>--%>
					<%--<span>当前余额 <s class="bluePrice">80.0</s>元</span>--%>
					<%--<span class="prevPrice">可充值<s class="bluePrice">500M</s></span>--%>
				<%--</h3>--%>
			<%--</div>--%>
		<%--</div>--%>
		
		<div class="boxBtn">
			<a href="javascript:;" class="btnmode btnPrice"  id="btnPrice">批量充</a>
			<a href="${baseURL}/jsp/wap/addPay.jsp" class="btnmode btnPricePerson">< 单独充</a>
		</div>
		<div id="result" style="display: display;">
			<div>充值结果说明：</div>
		</div>
		<div id="resultMoney" style="display: none;">

		</div>
	</section>	 
	
	<!--
    	底部悬浮
    -->
	<%@include file="common/foot.jsp" %>
	
	<!--支付弹窗-->
    <!--<div class="layerFixPAY hideObj">
    	<div class="layerPop layerPopPwd layerPopPay">
    		<p>输入支付密码</p>
    		<div class="payPut">
    			<input type="password" name="payPutItem" class="payPutItem"/>
    		</div>
    		<div class="payBox">
    			<a href="javascript:;" class="btnmode btnSure">确认</a>
    			<a href="javascript:;" class="btnmode btnCancel">取消</a>
    		</div>
    	</div>
    </div>-->

	<!--loading-->
	<div class="layerFixPAY layerLoading" style="display: none;"><!--需隐藏的话将layerLoading设置为diaplsy：none-->
		<div class="layerPop layerPopPwd layerPopPay layerPopLoading">
			<p><img src="images/loading.gif" width="43"/></p>
			<div class="payPut loadNote">
				<i class="loadText"></i>努力加载中...
			</div>
			<%--<div class="loadboxPacel">--%>
			<%--<div class="loadbox">--%>
			<%--<div class="loadingCur"></div>--%>
			<%--</div>--%>
			<%--</div>--%>
		</div>
	</div>

	<!--支付弹窗-->
	<div class="layerFixPAY hideObj">
		<div class="layerPop layerPopPwd layerPopPay">
			<p>输入支付密码</p>
			<div class="payPut">
				<input type="password" name="newPassword" class="payPutItem"/>
			</div>
			<div class="payBox">
				<a href="javascript:doSave();" class="btnmode btnSure">确认</a>
				<a href="javascript:;" class="btnmode btnCancel">取消</a>
			</div>
		</div>
	</div>
	</form>

</body>

<script src="${baseURL}/jsp/wap/js/main.js" type="text/javascript"></script>
<script type="text/javascript">

	$(document).ready(function() {

		//弹出支付层
		$('#btnPrice').on('click',function(){
			var dqdp = new Dqdp();
			if (dqdp.submitCheck("id_form_add")) {
				$('.layerFixPAY').show();
			}
		});
	});
	function doSaveInit() {
		var dqdp = new Dqdp();
		if (dqdp.submitCheck("id_form_add")) {
			//var message="确定为手机:"+$("#payMoblie").val()+" 充值套餐：" +$("#payMerchandiseId").find("option:selected").text();
			$('#id_form_add').attr("action","${baseURL}/merchant/myInfoAction!addInitBatchPay.action");
			$('#id_form_add').ajaxSubmit({
				dataType:'json',
				beforeSend:function(){
					$(".layerLoading").show();
				},
				complete:function(){
					$(".layerLoading").hide();
				},
				success:function (result) {
					if("0" == result.code){
						$("#result").html("<div>充值初始化说明：</div>" + result.data.result);
						$("#result").show();
						$("#resultMoney").html("批量充值总价格："+result.data.sumPayMoney+"元");
						$("#resultMoney").show();
					} else{
						_alert("提示",result.desc);
					}

				},
				error:function () {
					_alert("提示",'批量充值初始失败，请稍候再试');
				},complete:function(){
					loadIframe();
				}
			});
		}
	}
	function doSave() {
		var dqdp = new Dqdp();
		if (dqdp.submitCheck("id_form_add")) {
			//var message="确定为手机:"+$("#payMoblie").val()+" 充值套餐：" +$("#payMerchandiseId").find("option:selected").text();
				$('#id_form_add').attr("action","${baseURL}/merchant/myInfoAction!addBatchPay.action");
				$('#id_form_add').ajaxSubmit({
					dataType:'json',
					beforeSend:function(){
						$(".layerLoading").show();
					},
					complete:function(){
						$(".layerLoading").hide();
					},
					success:function (result) {
						if("0" == result.code){
							alert("批量充值成功");
							$("#result").html("<div>充值结果说明：</div>" + result.data.result);
							$("#result").show();
							$("#resultMoney").html("批量充值总价格："+result.data.sumPayMoney+"元");
							$("#resultMoney").show();
							$('.layerFixPAY').hide();
							$(".payPutItem").val("");
						} else{
							alert(result.desc);
						}

					},
					error:function () {
						alert('批量充值初始失败，请稍候再试');
					}
				});
		}
	}

</script>
</html>