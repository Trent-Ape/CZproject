<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<jsp:include page="common/dqdp_vars.jsp">
	<jsp:param name="dict" value=""></jsp:param>
</jsp:include>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>我的信息</title>
	<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
	<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
	<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
	<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
	<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form dataSrc='${baseURL}/merchant/myInfoAction!viewBaseUserDetailVO.action?personId=${param.personId}' method="post" id="id_form_search">
	<article>
		<section>
			<div class="myBox">
				<div class="myBoxPacel">
					<h3>
						<span>账户：<s name="baseUserVO.userName"></s></span>
					</h3>
					<h3>
						<span>账户余额：<s>¥</s> <s name="merchantVO.money"></s></span>
						<span class="prevPrice">享受折扣： <s name="merchantVO.discount"></s></span>
					</h3>
					<h3 class="state">
						<span>状态：<span name="baseUserVO.statusDesc"></span> </span>
						<i class="line"></i>
						<span>注册时间：<span name="merchantVO.insertDate"></span></span>
					</h3>
				</div>
			</div>
		</section>
	</article>
	
	<section class="pb70">
		<div class="itemBox">
			<div class="itemInfo personInfo">
				<a href="${baseURL}/jsp/wap/myInfo.jsp?personId=${currUser.personId}">
					个人信息
				</a>
			</div>
			<div class="itemInfo personInfo">
			    <a href="${baseURL}/jsp/wap/passwordSet.jsp">
					安全设置
				</a>
			</div>
			<div class="itemInfo personInfo">
				<a href="javascript:loginOut();">
					安全退出
				</a>
			</div>
		</div>
	
	</section>	 
	
	<!--
    	底部悬浮
    -->
	<%@include file="common/foot.jsp" %>
</form>
</body>

<script src="${baseURL}/jsp/wap/js/main.js" type="text/javascript"></script>
<script type="text/javascript">
	function loginOut(){
		if(confirm("确定要退出吗？")){
			window.location.href="${baseURL}/j_spring_security_logout?dqdp_csrf_token=${sessionScope.dqdp_csrf_token}";
		}
	}
</script>
</html>