<%@page language="java" contentType="text/html; charset=UTF-8" %>
<%@page import="cn.com.do1.dqdp.core.ConfigMgr"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<%--<jsp:include page="common/dqdp_vars.jsp">--%>
	<%--<jsp:param name="dict" value=""></jsp:param>--%>
<%--</jsp:include>--%>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>深讯领航代理商充值平台</title>
<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<script type="text/javascript">
	var lastError = "${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}";
	if (lastError)alert("${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}");
</script>
<form id="id_form_login" action="${baseURL}/j_spring_security_check" method="post" autocomplete="off">
	<input type='hidden' name='_spring_security_remember_me' value="true" />
	<input type='hidden' name='dqdp_csrf_token' value="${sessionScope.dqdp_csrf_token}" />
	<header>
		<div class="head">深讯领航代理商充值平台</div>
	</header>
	
	<section>
		<div class="itemBox">
			<div class="itemInfo">
				<input type="text"   class="putStyle" name="j_username" id="j_username" placeholder="请输入账号" valid="{must:true,tip:'登录账号',fieldType:'pattern',regex:'^\\w{6,20}$'}" />
				<a href="javascript:;" class="closeData"></a>
			</div>
			<div class="itemInfo">
				<input type="password"   class="putStyle" name="j_password" id="j_password" placeholder="请输入密码" valid="{must:true,tip:'登录密码',fieldType:'pattern',regex:'^\\w{6,20}$'}"/>
				<a href="javascript:;" class="closeDataPwd"></a>
			</div>
			<div class="itemInfo">
				<input type="text"   class="putStyle putStyleW50" name="j_code" id="j_code" placeholder="请输入验证码" valid="{must:true,tip:'验证码'}"/>
				<a href="javascript:;" class="closeDataW"></a>
				<div class="item_r">
					<img src="${baseURL}/PictureCheckCode" width="100" onclick="changeRegisterCode();" id="img_codeRegister"/>
					<a href="javascript:changeRegisterCode();" class="changeImg">换一张</a>
				</div>
			</div>
		</div>
		
		<div class="boxBtn">
			<p class="erro hideObj">帐号/密码错误，请重试</p>
			<a href="javascript:doLogin();" class="btnmode btnLogin">登录</a>
		</div>
		
		<div class="linkBox">
			<a href="${baseURL}/jsp/wap/addMerchat.jsp">申请代理商</a>
			<a href="javascript:;" class="faq">登录遇到问题 ></a>
		</div>
		
		<div class="cb contactUsBox">
			<h2>联系方式</h2>
			<p>电<s>话</s>：<span><%=ConfigMgr.get("systemmgr", "top_moblie","139XXXXXXX") %></span></p>
			<p>微<s>信</s>：<span><%=ConfigMgr.get("systemmgr", "top_weixin","139XXXXXXX") %></span></p>
			<p>Q<s class="qq">Q</s>：<span><%=ConfigMgr.get("systemmgr", "top_qq","139XXXXXXX") %></span></p>
		</div>
	</section>	 
	
	<!--遇到问题弹窗-->
    <div class="layerFix hideObj">
    	<div class="layerPop">
    		<div class="layerPop_top">
    			<a href="javascript:;" class="forget">忘记密码</a>
    		   <a href="${baseURL}/jsp/wap/question.jsp">常见问题</a>
    		</div>   		
    		<div class="layerPop_bottom">
    			<a href="javascript:;" class="cancelPop">取消</a>
    		</div>	
    	</div>
    </div>
    
    <!--忘记密码弹出-->
    <div class="layerFix_list hideObj">
    	<div class="layerPop layerPopPwd" style="">
    		<a href="javascript:;" class="closePop"><img src="images/closePop.png" width="18" /></a>
			<%=ConfigMgr.get("systemmgr", "forgetpasswordTips","忘记密码，请与客服联系13528823999，微信同号！QQ：2335064335") %>
    	</div>
    </div>

 </form>

</body>

<script src="${baseURL}/jsp/wap/js/main.js" type="text/javascript"></script>
<script type="text/javascript">
	//注册验证码
	function changeRegisterCode() {

		$("#img_codeRegister").attr("src","${baseURL}/PictureCheckCode?code=" + Math.random());
	}

	function doLogin() {
		var dqdp = new Dqdp();
		if (dqdp.submitCheck("id_form_login"))
			document.getElementById("id_form_login").submit();
	}
	function doLoginTest() {
		$("#j_username").val("");
		$("#j_password").val("");
	}

</script>
</html>