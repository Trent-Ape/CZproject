<%@page language="java" contentType="text/html; charset=UTF-8" %>
<%@page import="cn.com.do1.dqdp.core.ConfigMgr"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<jsp:include page="common/dqdp_vars.jsp">
	<jsp:param name="dict" value=""></jsp:param>
</jsp:include>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>我要申诉</title>
	<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
	<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
	<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
	<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
	<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form   method="post" id="id_form_add">
	<section class="pb70">		
		<div class="tsPacel" dataSrc="${baseURL}/pay/payAction!ajaxView.action?id=${param.id}">
			<!--充值失败-->	
			<div class="searchM_infoPacel">
				<div class="searchM_info">
					<div class="searchM_info_item">
						 <div class="searchM_info_l" style="width: 100%;">
						 	订单号：<span name="tbCzPayPO.payId" id="payId"> </span>
						 </div>
				         <%--<div class="searchM_info_r">--%>
				         	<%--充值时间：<span name="tbCzPayPO.payEndDate"></span>--%>
				         <%--</div>--%>
					</div>
					<div class="searchM_info_item">
						<%--<div class="searchM_info_l">--%>
							<%--订单号：<span name="tbCzPayPO.payId"> </span>--%>
						<%--</div>--%>
						<div class="searchM_info_r" style="width: 100%;">
							充值时间：<span name="tbCzPayPO.payEndDate"></span>
						</div>
					</div>
				    <div class="searchM_info_item">
						 <div class="searchM_info_l">
						 	号码：<span name="tbCzPayPO.payMoblie"></span>
						 </div>
				         <div class="searchM_info_r">
				         	充值套餐：<span name="tbCzPayPO.merName"></span>
				         </div>
					</div>
					<div class="searchM_info_item">
						 <div class="searchM_info_l">
						 	套餐原价：<span name="tbCzPayPO.payOriginalMoney"> </span>
						 </div>
				         <div class="searchM_info_r">
				         	折扣价格：<span name="tbCzPayPO.payDiscountMoney"> </span>
				         </div>
					</div>
					<div class="m_stateBox">
						 <span class="fl m_state m_fail">状态：<span name="tbCzPayPO.payStatusDesc" id="payDesc"></span></span>
					</div>
			    </div>
			</div>
			
			<div class="tsTeraPacel">
				<textarea name="Ts" class="tsTeraBox" placeholder="请输入您的申诉理由..."></textarea>
			</div>
			
			<div class="tsTeraPacel">
			   <div class="dormPancel">		 
			         <form method="post" name="form" class="formCel">
					    <input type="file" class="file" name="upFile" id="upFile" multiple="multiple"/>
					    <a href="javascript:;" class="addPic">
					    	<img class="showFileImg" src="images/fileImg.png" width="64"/>
					    </a>
					    <p class="fileTip">请上传你的流量查询记录图片</p>
					</form>						
			    </div>
			    
			</div>
		</div>
		
		<div class="boxBtn boxBtnlist">
			<a href="javascript:updateStatus();" class="btnmode btnInmidet">立即申诉 </a>
		</div>
		
	</section>	 
	
	<!--
    	底部悬浮
    -->
	<%@include file="common/foot.jsp" %>
	</form>
</body>

<script src="${baseURL}/jsp/wap/js/main.js" type="text/javascript"></script>
<script type="text/javascript">
	/**
	 * 启用或禁用用户
	 * @param userId
	 * @param disabled
	 */
	function updateStatus() {
		$("#id_form_add").attr("action","${baseURL}/payInfo/payInfoAction!payWapUpdateStatusAppeal.action");
		$("#id_form_add").ajaxSubmit({
			type:'post',
			data:{
				'id':$("#payId").html(),
				'status':$("#status").val(),
				'payDesc':$("#payDesc").html(),
				'reFilePath':$("#reFilePath").val(),
				'dqdp_csrf_token': dqdp_csrf_token
			},
			dataType:'json',
			success:function (result) {
				if (result.code == '0') {
					alert("申述成功");
					window.location.href = 'payQuery.jsp'+ '?dqdp_csrf_token='+dqdp_csrf_token;
				} else {
					alert(result.desc);
				}
			},
			error:function () {
				alert('操作失败，请稍候再试');
			}
		});
	}
</script>
</html>