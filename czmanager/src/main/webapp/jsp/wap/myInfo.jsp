<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<jsp:include page="common/dqdp_vars.jsp">
	<jsp:param name="dict" value="personSex"></jsp:param>
</jsp:include>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>个人信息</title>
	<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
	<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
	<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
	<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
	<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form action="${baseURL}/merchant/myInfoAction!updateBaseUser.action" method="post" id="user_edit">
	<section class="pb70" dataSrc="${baseURL}/merchant/myInfoAction!viewBaseUserVO.action?personId=${param.personId}">
		<input type="hidden" id="userId" name="baseUserVO.userId" value=""/>
		<input type="hidden" name="baseUserVO.personId" value=""/>
		<input type="hidden" name="merchantVO.id" value="" />
		<input type="hidden" name="baseUserVO.status" value="" />
		<div class="itemBox">
			<div class="itemInfo itemInfoUser">
				<dl>
					<dt>手机号码：</dt>
					<dd>
						<div class="editDiv" data-type ="phone" id="editPhone" name="merchantVO.mobile" ></div>
						<a href="javascript:;" class="closeDataEdit"></a>
					</dd>
				</dl>
			</div>
			<div class="itemInfo itemInfoUser">
				<dl>
					<dt>QQ：</dt>
					<dd>
						<div class="editDiv" data-type ="qq" id="editQQ" name="merchantVO.qq" ></div>
						<a href="javascript:;" class="closeDataEditQQ"></a>
					</dd>
				</dl>
			</div>
			<div class="itemInfo itemInfoUser">
				<dl>
					<dt>年龄：</dt>
					<dd>
						<div class="editDiv" data-type ="age" id="editAge" name="baseUserVO.age" style="height: 21px;" ></div>
						<a href="javascript:;" class="closeDataEditAge"></a>
					</dd>
				</dl>
			</div>
			<div class="itemInfo itemInfoUser">
				<dl>
					<dt>性别：</dt>
					<dd>
						<div class="editSexBox">
							<select class="iSelect" data-type ="sex" id="editSex" disabled="disabled" name="baseUserVO.sex" dictType="personSex">
							</select>
							<i class="icoDown hideObj"></i>	
						</div>											
					</dd>
				</dl>
			</div>
		</div>
		
		<div class="boxBtn">			
			<a href="#" class="btnmode btnEdit">编辑</a>
		</div>
	</section>	 
	
	<!--
    	底部悬浮
    -->
	<%@include file="common/foot.jsp" %>

	<!--loading-->
	<div class="layerFixPAY layerLoading" style="display: none;"><!--需隐藏的话将layerLoading设置为diaplsy：none-->
		<div class="layerPop layerPopPwd layerPopPay layerPopLoading">
			<p><img src="images/loading.gif" width="43"/></p>
			<div class="payPut loadNote">
				<i class="loadText"></i>努力加载中...
			</div>
			<%--<div class="loadboxPacel">--%>
			<%--<div class="loadbox">--%>
			<%--<div class="loadingCur"></div>--%>
			<%--</div>--%>
			<%--</div>--%>
		</div>
	</div>

</form>
</body>

<script src="${baseURL}/jsp/wap/js/main.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {

		$('.btnEdit').click(function () {
			if ($(".btnEdit").text() == "保存") {

				var dqdp = new Dqdp();
				if (dqdp.submitCheck("user_edit")) {
					// 提交数据
					$('#user_edit').ajaxSubmit({
						dataType: 'json',
						data:{
							"merchantVO.mobile":$("#editPhone").text(),
							"merchantVO.qq":$("#editQQ").text(),
							"baseUserVO.age":$("#editAge").text()
						},
						beforeSend:function(){
							$(".layerLoading").show();
						},
						complete:function(){
							$(".layerLoading").hide();
						},
						success: function (result) {
							if ("0" == result.code) {
								alert(result.desc);
								window.location.href = ("myUser.jsp" + '?dqdp_csrf_token=' + dqdp_csrf_token);
							} else {
								alert(result.desc);
							}
						},
						error: function () {
							alert('修改用户信息失败，可能是由网络原因引起的，请稍候再试');
						}
					});
				}
			}
	});
});
</script>

</html>