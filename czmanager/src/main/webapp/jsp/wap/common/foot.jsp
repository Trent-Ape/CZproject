<%@page language="java" contentType="text/html; charset=UTF-8" %>

<footer>
  <div class="foot">
    <ul>
      <li>
        <a href="${baseURL}/jsp/wap/addPay.jsp" class="ico_c">
          <i></i>
          <p>充流量</p>
        </a>
      </li>
      <li>
        <a href="${baseURL}/jsp/wap/payQuery.jsp" class="ico_j">
          <i></i>
          <p>交易记录</p>
        </a>
      </li>
      <li>
        <a href="${baseURL}/jsp/wap/payQuestion.jsp" class="ico_l">
          <i></i>
          <p>代理流程</p>
        </a>
      </li>
      <li>
        <a href="${baseURL}/jsp/wap/myUser.jsp" class="ico_i">
          <i></i>
          <p>我的信息</p>
        </a>
      </li>
    </ul>
  </div>
</footer>
<script  type="text/javascript">
  var  url =[["addPay.jsp"],["payQuery.jsp","appeal.jsp"],["payQuestion.jsp"],["myUser.jsp","myInfo.jsp","password.jsp","passwordSet.jsp","payPassword.jsp"]];
  var index = 0;
  var currentUrl = window.location.href;
  for(var k=0;k<url.length;k++){
    for(var i=0;i<url[k].length;i++){
        if(currentUrl.indexOf(url[k][i])>0){
           index = k;
           break;
        }
    }
  }
  $(".foot ul li").removeClass("f_cur");
  $(".foot ul li").eq(index).addClass("f_cur");

</script>