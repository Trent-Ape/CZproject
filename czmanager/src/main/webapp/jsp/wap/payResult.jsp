<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<jsp:include page="common/dqdp_vars.jsp">
	<jsp:param name="dict" value=""></jsp:param>
</jsp:include>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>充流量</title>
<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
	<header>
		<div class="head head_item head_item_l head_item_l_gray">
			批量充结果：
		</div>
	</header>
	
	<section class="pb70">
		<div class="payStatePacel">
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
			<div class="payState">
				<div class="fl payState_l">
					<p>13567898989<s>成功</s></p>				
				</div>
				<div class="fr payState_r">
					<p class="payFail">13567898989<s>失败</s></p>	<!--失败状态+payFail-->	
				</div>
			</div>
		</div>
		
	</section>	 
	
	<!--
    	底部悬浮
    -->
	<%@include file="common/foot.jsp" %>
</body>

<script type="text/javascript" src="js/jquery.min.js"></script>

</html>