<%@page language="java" contentType="text/html; charset=UTF-8" %>
<%@page import="cn.com.do1.dqdp.core.ConfigMgr"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<%--<jsp:include page="common/dqdp_vars.jsp">--%>
	<%--<jsp:param name="dict" value=""></jsp:param>--%>
<%--</jsp:include>--%>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>申请代理商</title>
<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form id="id_form_register" action="${baseURL}/merchant/merchantAction!addMerchant.action" method="post" autocomplete="off">
	<input type="hidden" name="userIds" id="userIds" value=""/>
	<input type="hidden" name="orgId" value="<%=ConfigMgr.get("systemmgr", "org_merchant", "deaf8bfe-c403-4b83-84c0-ca1e8db80a05")%>"/>
	<input type="hidden" name="roleIds" id="roleIds" value="<%=ConfigMgr.get("systemmgr", "merchantRole", "78e033cb-b9e8-48a6-9a8e-5890069bc529")%>"/>
	<input type="hidden" name="isInternal" value="1"/>
	<header>
		<div class="head headList">
			<h2>申请代理商</h2>
		</div>
	</header>
	
	<section>
		<div class="itemBox">
			<div class="itemInfo">
				<input type="text" name="j_reg_username" class="putStyle" id="j_reg_username" placeholder="请输入6-20位登录账号" valid="{must:true,tip:'登录账号',fieldType:'pattern',regex:'^\\w{6,20}$'}"/>
				<a href="javascript:;" class="closeData1"></a>
			</div>
			<div class="itemInfo">
				<input type="password" name="j_reg_password" class="putStyle" id="j_reg_password" placeholder="请输入6-20位数字密码" valid="{must:true,tip:'登录密码',fieldType:'pattern',regex:'^\\w{6,20}$'}" />
				<span class="erro erroPosition">登录密码只支持6位数字</span>
				<a href="javascript:;" class="closeData2"></a>
			</div>
			<div class="itemInfo">
				<input type="password" name="j_reg_confirmPassword" class="putStyle" id="j_reg_confirmPassword" placeholder="确定登陆密码"  valid="{must:true,tip:'确定登陆密码',fieldType:'pattern',regex:'^\\w{6,20}$'}"/>
				<span class="erro erroPosition">登录密码不一致</span>
				<a href="javascript:;" class="closeData3"></a>
			</div>
			<div class="itemInfo">
				<input type="password" name="j_reg_payPassword" class="putStyle" id="j_reg_payPassword" placeholder="请输入6-20位数字交易密码" valid="{must:true,tip:'交易密码',fieldType:'pattern',regex:'^\\w{6,20}$'}" />
				<span class="erro erroPosition">交易密码只支持6位数字</span>
				<a href="javascript:;" class="closeData4"></a>
			</div>
			<div class="itemInfo">
				<input type="password" name="j_reg_confirmPayPassword" class="putStyle" id="j_reg_confirmPayPassword" placeholder="确定交易密码" valid="{must:true,tip:'确定交易密码',fieldType:'pattern',regex:'^\\w{6,20}$'}" />
				<span class="erro erroPosition">交易密码不一致</span>
				<a href="javascript:;" class="closeData5"></a>
			</div>
			<div class="itemInfo">
				<input type="text" name="j_reg_mobile" class="putStyle" id="j_reg_mobile" placeholder="请输入11位联系手机" valid="{must:true, fieldType:'pattern', regex:'^\\d{11}$', tip:'联系手机'}" />
				<span class="erro erroPosition">请输入正确的手机号码</span>
				<a href="javascript:;" class="closeData6"></a>
			</div>
			<div class="itemInfo">
				<input type="text" name="j_reg_qq" class="putStyle" id="j_reg_qq" placeholder="请输入6-12位联系QQ" valid="{must:true, fieldType:'pattern', regex:'^\\d{6,12}$', tip:'联系QQ'}" />
				<a href="javascript:;" class="closeData7"></a>
			</div>
			<div class="itemInfo">
				<input type="text" name="j_code" class="putStyle putStyleW50" id="j_code" placeholder="请输入验证码" valid="{must:true,tip:'验证码'}" />
				<a href="javascript:;" class="closeDataW"></a>
				<div class="item_r">
					<img src="${baseURL}/PictureCheckCode" width="100" onclick="changeRegisterCode();" id="img_codeRegister"/>
					<a href="javascript:changeRegisterCode();" class="changeImg">换一张</a>
				</div>
			</div>
		</div>
		
		<div class="boxBtn">
			<p class="erro hideObj">验证码错误，请重试</p>
			<a href="javascript:doRegister();" class="btnmode btnApply">申请</a>
		</div>
		
	</section>
</form>
</body>

<script src="${baseURL}/jsp/wap/js/main.js" type="text/javascript"></script>
<script type="text/javascript">
	//注册验证码
	function changeRegisterCode() {

		$("#img_codeRegister").attr("src","${baseURL}/PictureCheckCode?code=" + Math.random());
	}

	function doRegister() {
		var dqdp = new Dqdp();
		if (dqdp.submitCheck("id_form_register")) {
			if(!checkPassword()){
				alert('两次输入的密码不一致！');
				return;
			}
			if(!checkPayPassword()){
				alert('两次输入的支付密码不一致！');
				return;
			}
			// 提交数据
			$('#id_form_register').ajaxSubmit({
				dataType:'json',
				success:function (result) {
					if ("0" == result.code) {
						$("#register_dialog").dialog("close");
						$("input[type='text']").val("");
						$("input[type='password']").val("");
						alert("注册成功！");
						window.history.back();
					} else {
						alert(result.desc);
					}
				},
				error:function () {
					alert('注册用户失败，请稍候再试');
				}
			});
		}
	}

	function checkPassword() {
		return $('#j_reg_password').val() == $('#j_reg_confirmPassword').val();
	}
	function checkPayPassword() {
		return $('#j_reg_payPassword').val() == $('#j_reg_confirmPayPassword').val();
	}

</script>

</html>