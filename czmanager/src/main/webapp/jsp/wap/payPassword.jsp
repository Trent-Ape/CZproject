<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<jsp:include page="common/dqdp_vars.jsp">
	<jsp:param name="dict" value=""></jsp:param>
</jsp:include>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>修改登录密码</title>
	<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
	<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
	<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
	<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
	<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
</head>

<body>
<form action="${baseURL}/merchant/myInfoAction!mychangePayPassword.action" method="post" id="user_edit_paypassword">
	<section>
		<div class="itemBox">
			<div class="itemInfo itemInfoModify">
				<dl>
					<dt style=" width: 150px; ">当前支付密码：</dt>
					<dd>
						<input type="password" name="password" type="password" class="putStyle" id="oldPassword" placeholder="输入6-20位数字密码"  valid="{must:true, fieldType:'pattern', regex:'^\\w{6,20}$', tip:'原支付密码'}"/>
						<span class="erro erroPosition">原密码错误</span>
						<a href="javascript:;" class="closeData_Pwd1"></a>
					</dd>
				</dl>			
			</div>
			<div class="itemInfo itemInfoModify">
				<dl>
					<dt style=" width: 150px; ">输入支付新密码：</dt>
					<dd>
						<input type="password"   class="putStyle" id="newPassword" name="newPassword" placeholder="输入6-20位数字密码"   valid="{must:true, fieldType:'pattern', regex:'^\\w{6,20}$', tip:'新支付密码'}"/>
						<span class="erro erroPosition">只支持6位数字</span>
						<a href="javascript:;" class="closeData_Pwd2"></a>
					</dd>
				</dl>			
			</div>
			<div class="itemInfo itemInfoModify">
				<dl>
					<dt style=" width: 150px; ">确认支付新密码：</dt>
					<dd>
						<input type="password"   class="putStyle" id="confirmNewPassword" name="confirmNewPassword" placeholder="输入6-20位数字密码" valid="{must:true, fieldType:'pattern', regex:'^\\w{6,20}$', tip:'确认新支付密码'}"/>
						<span class="erro erroPosition">新密码不一致</span>
						<a href="javascript:;" class="closeData_Pwd3"></a>
					</dd>
				</dl>			
			</div>
		</div>
		
		<div class="boxBtn">
			<a href="#" class="btnmode btnSureTo">确定</a>
		</div>
		
	</section>	
	
	<!--
    	底部悬浮
    -->
	<%@include file="common/foot.jsp" %>
			
	<!--loading-->
    <div class="layerFixPAY layerLoading" style="display: none;"><!--需隐藏的话将layerLoading设置为diaplsy：none-->
    	<div class="layerPop layerPopPwd layerPopPay layerPopLoading">
    		<p><img src="images/loading.gif" width="43"/></p>
    		<div class="payPut loadNote">
    			<i class="loadText"></i>努力加载中...
    		</div>
    		<%--<div class="loadboxPacel">--%>
			    <%--<div class="loadbox">	--%>
			        <%--<div class="loadingCur"></div>--%>
			    <%--</div>--%>
			<%--</div>--%>
    	</div>
    </div>
</form>
</body>

<script src="${baseURL}/jsp/wap/js/main.js" type="text/javascript"></script>
<script type="text/javascript">//进度条加载
	var i=0;
    function add(i){
		    var tbox =$(".loadbox");
            var loadingCur =$(".loadingCur");		
			loadingCur.css("width",i+"%");
			$('.loadText').html(i+"%");
		}

    /*创建方法（i++循环起来）*/
	function loading(){
		if(i>100){	
			$('.loadNote').html('加载完成！');
			alert("加载完成,您要跳首页呢？还是干嘛！");//加载完成需执行的步骤!			
			return;}

		if(i<=100){
			setTimeout("loading()",50)//这里调loading速度
				add(i);
			    i++;		
			}
		}
    /*调用loading()函数*/
    $(document).ready(function(){	
		//loading();

		$('.btnSureTo').click(function () {
			if(checkPayPassword()){
				var dqdp = new Dqdp();
				if (dqdp.submitCheck("user_edit_paypassword")) {
					// 提交数据
					$('#user_edit_paypassword').ajaxSubmit({
						dataType:'json',
						beforeSend:function(){
							$(".layerLoading").show();
						},
						complete:function(){
							$(".layerLoading").hide();
						},
						success:function (result) {
							if ("0" == result.code) {
								alert(result.desc);
								window.location.href=("myUser.jsp"+ '?dqdp_csrf_token='+dqdp_csrf_token);
							} else {
								alert(result.desc);
							}
						},
						error:function () {
							alert('修改用户信息失败，可能是由网络原因引起的，请稍候再试');
						}
					});
				}

			} else{
				alert("两次支付密码不正确");
			}
		});
	});

function checkPayPassword() {
	return $('#newPassword').val() == $('#confirmNewPassword').val();
}
</script>
</html>