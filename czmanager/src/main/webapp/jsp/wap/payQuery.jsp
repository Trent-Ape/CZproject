<%@page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@include file="common/dqdp_common.jsp" %>
<jsp:include page="common/dqdp_vars.jsp">
	<jsp:param name="dict" value="payStatus"></jsp:param>
</jsp:include>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="telephone=no" name="format-detection">
<meta content="email=no" name="format-detection">
<title>充流量</title>
<link rel="stylesheet" href="${baseURL}/jsp/wap/css/base.css">
<link rel="stylesheet" type="text/css" href="${baseURL}/jsp/wap/css/main.css"/>
<script src="${baseURL}/js/do1/common/jquery-1.6.3.min.js"></script>
<script src="${baseURL}/js/do1/common/common.js?ver=<%=jsVer%>"></script>
<script src="${baseURL}/js/3rd-plug/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${baseURL }/jsp/wap/js/jsrender.min.js"></script>
	<script type="text/javascript" src="${baseURL }/jsp/wap/js/birthday.js"></script>
</head>

<body>
<form action="${baseURL}/pay/payAction!ajaxMySearch.action" method="post" id="id_form_search">
	<input type="hidden" name="page" id="page" value="1"/>
	<header>
		<div class="searchPacel">
			<input type="search" name="searchValue.payMoblie" class="searchText" placeholder="手机号搜索交易记录 "/>
			<a href="javascript:$('#page').val(1);doSearch();" class="btnSearch">搜索</a>
		</div>
	</header>
	
	<section class="pb70">
		<div class="itemBox">
			<div class="itemInfo itemInfoM">
				<dl>
					<dt>充值状态：</dt>
					<dd>
						<select class="iSelect" name="searchValue.payStatus"  dictType="payStatus">
						</select>
						<i class="icoDown"></i>
					</dd>
				</dl>
			</div>
			<div class="itemInfo itemInfoM">
				<dl>
					<dt>开始时间：</dt>
					<dd>
						<div class="dateState">
							<div class="dateState_l" style="width: 100%;">
								<select class="iSelectW"   id="s_sel_year"></select>年
								<select  class="iSelectW" id="s_sel_month" ></select>月
								<select  class="iSelectW"  id="s_s_sel_month"></select>日
								<%--<i class="icoDown"></i>--%>
							</div>
							<%--<div class="fl dateState_m">至</div>--%>
							<%--<div class="dateState_r">--%>
								<%--<select class="sel_year" rel="2000"></select>年--%>
								<%--<select class="sel_month" rel="2"></select>月--%>
								<%--<select class="sel_day" rel="14"></select>日--%>
								<%--<i class="icoDown"></i>--%>
							<%--</div>--%>
						</div>
					</dd>
				</dl>
					<dl><dt>结束时间：</dt>
					<dd>
						<div class="dateState">
							<div class="dateState_l" style="width: 100%;;">
								<select class="sel_year iSelectW" id="e_sel_year"  ></select>年
								<select class="sel_month iSelectW" id="e_sel_month"  ></select>月
								<select class="sel_day iSelectW" id="e_sel_day"  ></select>日
								<%--<i class="icoDown"></i>--%>
							</div>
							<%--<div class="fl dateState_m">至</div>--%>
							<%--<div class="dateState_r">--%>
								<%--<select class="sel_year" rel="2000"></select>年--%>
								<%--<select class="sel_month" rel="2"></select>月--%>
								<%--<select class="sel_day" rel="14"></select>日--%>
								<%--<i class="icoDown"></i>--%>
							<%--</div>--%>
						</div>
					</dd>
				</dl>
			</div>	
		</div>

		<div id="lsit">
			<!--查询状态失败-->
			<%--<div class="searchM_infoPacel">--%>
				<%--<div class="searchM_info">--%>
					<%--<div class="searchM_info_item">--%>
						 <%--<div class="searchM_info_l">--%>
							<%--订单号：<span>100000001</span>--%>
						 <%--</div>--%>
						 <%--<div class="searchM_info_r">--%>
							<%--充值时间：<span>2015-5-6 23:00</span>--%>
						 <%--</div>--%>
					<%--</div>--%>
					<%--<div class="searchM_info_item">--%>
						 <%--<div class="searchM_info_l">--%>
							<%--号码：<span>13925150888</span>--%>
						 <%--</div>--%>
						 <%--<div class="searchM_info_r">--%>
							<%--充值套餐：<span>50M</span>--%>
						 <%--</div>--%>
					<%--</div>--%>
					<%--<div class="searchM_info_item">--%>
						 <%--<div class="searchM_info_l">--%>
							<%--套餐原价：<span>88</span>--%>
						 <%--</div>--%>
						 <%--<div class="searchM_info_r">--%>
							<%--折扣价格：<span>58</span>--%>
						 <%--</div>--%>
					<%--</div>--%>
					<%--<div class="m_stateBox">--%>
						<%--<span class="fl m_state m_fail">状态：充值失败</span>--%>
						<%--<a href="appeal.jsp" class="fr btnmode btnTs">立即申诉</a>--%>
					<%--</div>--%>
				<%--</div>--%>
			<%--</div>--%>
			<%--<!--查询状态成功-->--%>
			<%--<div class="searchM_infoPacel">--%>
				<%--<div class="searchM_info">--%>
					<%--<div class="searchM_info_item">--%>
						 <%--<div class="searchM_info_l">--%>
							<%--订单号：<span>100000001</span>--%>
						 <%--</div>--%>
						 <%--<div class="searchM_info_r">--%>
							<%--充值时间：<span>2015-5-6 23:00</span>--%>
						 <%--</div>--%>
					<%--</div>--%>
					<%--<div class="searchM_info_item">--%>
						 <%--<div class="searchM_info_l">--%>
							<%--号码：<span>13925150888</span>--%>
						 <%--</div>--%>
						 <%--<div class="searchM_info_r">--%>
							<%--充值套餐：<span>50M</span>--%>
						 <%--</div>--%>
					<%--</div>--%>
					<%--<div class="searchM_info_item">--%>
						 <%--<div class="searchM_info_l">--%>
							<%--套餐原价：<span>88</span>--%>
						 <%--</div>--%>
						 <%--<div class="searchM_info_r">--%>
							<%--折扣价格：<span>58</span>--%>
						 <%--</div>--%>
					<%--</div>--%>
					<%--<div class="m_stateBox" >--%>
						<%--<span class="fl m_state m_suc">状态：充值成功</span>--%>
					<%--</div>--%>
				<%--</div>--%>
			<%--</div>--%>
			<div id="more" class="searchM_infoPacel" style="text-align: center;">
				<div class="searchM_info" onclick="doSearch();">加载更多</div>
			</div>
		</div>
	</section>	 
	
	<!--
    	底部悬浮
    -->
	<%@include file="common/foot.jsp" %>

	<!--loading-->
	<div class="layerFixPAY layerLoading" style="display: none;"><!--需隐藏的话将layerLoading设置为diaplsy：none-->
		<div class="layerPop layerPopPwd layerPopPay layerPopLoading">
			<p><img src="images/loading.gif" width="43"/></p>
			<div class="payPut loadNote">
				<i class="loadText"></i>努力加载中...
			</div>
			<%--<div class="loadboxPacel">--%>
			<%--<div class="loadbox">--%>
			<%--<div class="loadingCur"></div>--%>
			<%--</div>--%>
			<%--</div>--%>
		</div>
	</div>
</form>
</body>

<script src="${baseURL}/jsp/wap/js/main.js" type="text/javascript"></script>
<script id="marketList" type="text/x-jsrender">
  {{for pageData}}
    <div class="searchM_infoPacel">
			<div class="searchM_info">
				<div class="searchM_info_item">
					 <div class="searchM_info_l" style="width:100%;">
					 	订单号：<span>{{:payId}}</span>
					 </div>
			         <%--<div class="searchM_info_r">--%>
			         	<%--充值时间：<span>{{:payStartDate}}</span>--%>
			         <%--</div>--%>
				</div>
				<div class="searchM_info_item">
					 <%--<div class="searchM_info_l" width="100%">--%>
					 	<%--订单号：<span>{{:payId}}</span>--%>
					 <%--</div>--%>
			         <div class="searchM_info_r" style="width:100%;">
			         	充值时间：<span>{{:payStartDate}}</span>
			         </div>
				</div>
			    <div class="searchM_info_item">
					 <div class="searchM_info_l">
					 	号码：<span>{{:payMoblie}}</span>
					 </div>
			         <div class="searchM_info_r">
			         	充值套餐：<span>{{:merName}}</span>
			         </div>
				</div>
				<div class="searchM_info_item">
					 <div class="searchM_info_l">
					 	套餐原价：<span>{{:payOriginalMoney}}</span>
					 </div>
			         <div class="searchM_info_r">
			         	折扣价格：<span>{{:payDiscountMoney}}</span>
			         </div>
				</div>
				<div class="m_stateBox">
					{{if payStatus==2 || payStatus==7 }}
						<span class="fl m_state m_fail">状态：{{:payStatusDesc}}</span>
						<a href="${baseURL}/jsp/wap/appeal.jsp?id={{:payId}}" class="fr btnmode btnTs">立即申诉</a>
					{{else}}<span class="fl m_state m_suc">状态：{{:payStatusDesc}}</span>
					{{/if}}
				</div>
		    </div>
		</div>
  {{/for}}
</script>
<script type="text/javascript">

	$(document).ready(function () {

//		var today=new Date();
//		$(".sel_year").attr("rel",today.getFullYear());
//		$(".sel_month").attr("rel",(today.getMonth()+1));
//		$(".sel_day").attr("rel",today.getDate());

		$.ms_DatePicker({
			YearSelector: ".sel_year",
			MonthSelector: ".sel_month",
			DaySelector: ".sel_day"
		});
		$.ms_DatePicker();

		doSearch(1);
	});
	function doSearch() {
		var s_sel_year=$("#s_sel_year").val();
		var s_sel_month=$("#s_sel_month").val();
		var s_s_sel_month=$("#s_s_sel_month").val();

		var e_sel_year=$("#e_sel_year").val();
		var e_sel_month=$("#e_sel_month").val();
		var e_sel_day=$("#e_sel_day").val();

		var startDate="";
		var endDate="";
		if(s_sel_year!="0"||e_sel_year!="0"){

			  startDate=s_sel_year+"-"+s_sel_month+"-" +s_s_sel_month+ " 00:00:00";
			  endDate=e_sel_year+"-"+e_sel_month+"-" +e_sel_day+" 23:59:59";
		}



		var data ={};

		if(startDate==""){
			startDate ="1900-12-12 00:00:00";
		}
		if(endDate==""){
			endDate="2999-12-12 23:59:59";
		}

		$("#id_form_search").attr("action","${baseURL}/pay/payAction!ajaxMySearch.action");
		$("#id_form_search").ajaxSubmit({
			type:"post",
			data:{"searchValue.startDate":startDate,
				"searchValue.endDate":endDate},
			dataType:"json",
			beforeSend:function(){
				$(".layerLoading").show();
				$("#more div").html("加载中...")
			},
			complete:function(){
				$(".layerLoading").hide();

			},
			success:function (result) {
				if ("0" == result.code) {
					var template = $.templates("#marketList");
					var htmlOutput = template.render(result.data);
					if(result.data.currPage ==1){
						$("#lsit").html(htmlOutput);
						$("#lsit").append('<div id="more" class="searchM_infoPacel" style="text-align: center;"> <div class="searchM_info" onclick="doSearch();">加载更多</div> </div>');
					}else{
						$("#more").before(htmlOutput);
					}

					if(result.data.currPage == result.data.maxPage||result.data.maxPage==0){
						$("#more div").html("没有更多数据");
						$("#more div").attr("onclick","");
					} else {
						$("#page").val(result.data.currPage + 1);
						$("#more div").html("加载更多");
					}
				} else {
					alert(result.desc);
				}
			},
			error:function () {
				alert("通讯错误");
			}
		});
	}

</script>
</html>