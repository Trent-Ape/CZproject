/*
Navicat MySQL Data Transfer

Source Server         : localhost-mysql
Source Server Version : 50519
Source Host           : localhost:3306
Source Database       : czmanager

Target Server Type    : MYSQL
Target Server Version : 50519
File Encoding         : 65001

Date: 2016-01-18 14:18:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dictionary
-- ----------------------------
DROP TABLE IF EXISTS `dictionary`;
CREATE TABLE `dictionary` (
  `FS_DICT_ITEM_ID` varchar(36) NOT NULL,
  `FS_TYPE` varchar(18) DEFAULT NULL,
  `FS_TYPE_DESC` varchar(200) DEFAULT NULL,
  `FS_ITEM_CODE` varchar(50) DEFAULT NULL,
  `FS_ITEM_DESC` varchar(200) DEFAULT NULL,
  `FS_PARENT_TYPE` varchar(18) DEFAULT NULL,
  `FS_CLASS` varchar(18) DEFAULT NULL,
  `FI_LEVEL` int(11) DEFAULT NULL,
  `FS_STATUS` char(1) DEFAULT NULL,
  PRIMARY KEY (`FS_DICT_ITEM_ID`),
  KEY `IX_DICT_TID` (`FS_TYPE`),
  KEY `IX_DICT_IID` (`FS_ITEM_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dictionary
-- ----------------------------
INSERT INTO `dictionary` VALUES ('0190b969-fc63-4ad3-a8c5-12d6b8398019', 'discount', '折扣', '1.00', '100%', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('121e18fa-eaa6-40a5-8c76-9e26ec919903', 'discount', '折扣', '0.50', '50%', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('12aa7adb-cdc3-4dbc-b610-d86f3c951eba', 'payStatus', '充值状态', '4', '已退款', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('2a1ff109-43b9-438d-994d-3517f0add669', 'payStatus', '充值状态', '3', '充值失败', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('309a40f5-3ce8-4ef6-acf7-32095532bd18', 'payStatus', '充值状态', '6', '申诉成功', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('4f18194d-d48d-49df-b50a-32b9de42f976', 'merchandiseStatus', '商品状态', '2', '下架', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('50d7fa4e-0235-4329-9154-77ad5b36bf6a', 'discount', '折扣', '0.60', '60%', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('6296eedd-37c8-40a2-8ef9-a7978e45e164', 'payStatus', '充值状态', '2', '充值完成', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('62c2b72d-e9ec-4ae5-ac58-c1142c147f1b', 'merchandiseStatus', '商品状态', '1', '上架', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('6681ba7c-f7f1-44f4-8f8e-e354bd9aad33', 'discount', '折扣', '0.70', '70%', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('81068c49-3e8d-4d43-bfbd-f147e2ea8f8d', 'discount', '折扣', '0.90', '90%', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('8a6953bf-10bc-4e32-b15e-616f08549902', 'payStatus', '充值状态', '7', '申诉失败', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('8ae4253c-70df-4897-a4fb-a965fc4e367f', 'discount', '折扣', '0.30', '30%', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('922b13b3-55fa-4117-86c8-85692ed3630f', 'payStatus', '充值状态', '5', '申诉中', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('935b57b6-ffc8-42c2-ba2a-420287ad5c0f', 'discount', '折扣', '0.10', '10%', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('d537034b-390f-425b-85c8-065d1745c703', 'payStatus', '充值状态', '1', '充值中', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('e5f4086c-1455-4a26-ac66-d30a21a50e4c', 'discount', '折扣', '0.40', '40%', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('f613eec4-0327-4e20-a47c-2eb75f0bdd34', 'discount', '折扣', '0.20', '20%', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('f82641a4-6288-4b2c-8d06-1705058a5c5b', 'discount', '折扣', '0.80', '80%', null, '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('keyword_status_0', 'keywordStatus', '关键字状态', '0', '启用', '', 'keyword', '0', '0');
INSERT INTO `dictionary` VALUES ('keyword_status_1', 'keywordStatus', '关键字状态', '1', '禁用', '', 'keyword', '0', '0');
INSERT INTO `dictionary` VALUES ('logOperationType_1', 'logOperationType', '日志操作类型', 'add', '新增', '', '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('logOperationType_2', 'logOperationType', '日志操作类型', 'update', '更新', '', '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('logOperationType_3', 'logOperationType', '日志操作类型', 'del', '删除', '', '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('logOperationType_4', 'logOperationType', '日志操作类型', 'get', '获取', '', '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('logOperationType_5', 'logOperationType', '日志操作类型', 'login', '登陆', '', '通用分类', '0', '0');
INSERT INTO `dictionary` VALUES ('person_sex_0', 'personSex', '人员性别', '0', '未知', '', 'person', '0', '0');
INSERT INTO `dictionary` VALUES ('person_sex_1', 'personSex', '人员性别', '1', '男', '', 'person', '0', '0');
INSERT INTO `dictionary` VALUES ('person_sex_2', 'personSex', '人员性别', '2', '女', '', 'person', '0', '0');
INSERT INTO `dictionary` VALUES ('task_result_0', 'jobLogResult', '任务执行结果', '0', '执行正常', '', 'schedule', '0', '0');
INSERT INTO `dictionary` VALUES ('task_result_1', 'jobLogResult', '任务执行结果', '1', '发生异常', '', 'schedule', '0', '0');
INSERT INTO `dictionary` VALUES ('task_status_0', 'taskStatus', '任务状态', '0', '正常', '', 'schedule', '0', '0');
INSERT INTO `dictionary` VALUES ('task_status_1', 'taskStatus', '任务状态', '1', '停止', '', 'schedule', '0', '0');
INSERT INTO `dictionary` VALUES ('task_status_2', 'taskStatus', '任务状态', '2', '正在运行', '', 'schedule', '0', '0');
INSERT INTO `dictionary` VALUES ('user_status_0', 'userStatus', '用户状态', '0', '正常', '', 'user', '0', '0');
INSERT INTO `dictionary` VALUES ('user_status_1', 'userStatus', '用户状态', '1', '禁用', '', 'user', '0', '0');

-- ----------------------------
-- Table structure for tb_cz_merchandise
-- ----------------------------
DROP TABLE IF EXISTS `tb_cz_merchandise`;
CREATE TABLE `tb_cz_merchandise` (
  `MER_ID` varchar(36) NOT NULL,
  `MER_NAME` varchar(200) DEFAULT NULL COMMENT '商品名称',
  `MER_MONEY` float(11,2) DEFAULT NULL COMMENT '商品金额',
  `MER_ORDER` int(1) DEFAULT NULL,
  `MER_STATUS` int(1) DEFAULT NULL,
  `MER_INSERT` datetime DEFAULT NULL,
  `MER_UPDATE` datetime DEFAULT NULL,
  `MER_OPER` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`MER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_cz_merchandise
-- ----------------------------
INSERT INTO `tb_cz_merchandise` VALUES ('5d997d11-749e-4016-883f-35c12c9ad596', '30M', '30.00', '3', '1', '2015-07-25 12:52:42', '2015-07-26 20:01:43', 'lixiong');
INSERT INTO `tb_cz_merchandise` VALUES ('7927be89-8971-4d7b-abde-b1ea8d5c77fb', '20M', '20.00', '2', '1', '2015-07-25 12:52:29', '2015-07-26 20:01:41', 'lixiong');
INSERT INTO `tb_cz_merchandise` VALUES ('d85d50e3-b90e-4a2a-8368-07a8836907d7', '40M', '40.00', '4', '1', '2015-07-25 12:52:52', '2015-07-26 20:01:46', 'lixiong');
INSERT INTO `tb_cz_merchandise` VALUES ('e2585e78-6938-49bf-a266-46be69e58e62', '50M', '50.00', '5', '1', '2015-07-25 12:53:49', '2015-07-26 20:01:37', 'lixiong');
INSERT INTO `tb_cz_merchandise` VALUES ('e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '10M', '10.00', '1', '1', '2015-07-25 12:50:43', '2015-08-06 11:25:11', 'lixiong');

-- ----------------------------
-- Table structure for tb_cz_merchant
-- ----------------------------
DROP TABLE IF EXISTS `tb_cz_merchant`;
CREATE TABLE `tb_cz_merchant` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(200) DEFAULT NULL COMMENT '商家名称',
  `PAY_PASSWORD` varchar(200) DEFAULT NULL COMMENT '地址',
  `MONEY` float(11,2) unsigned DEFAULT '0.00' COMMENT '充值金额',
  `DISCOUNT` float(3,2) unsigned DEFAULT '1.00' COMMENT '折扣',
  `MOBILE` varchar(11) DEFAULT NULL COMMENT '手机',
  `QQ` varchar(15) DEFAULT NULL,
  `USER_ID` varchar(36) DEFAULT NULL COMMENT '关联用户',
  `OPER` varchar(36) DEFAULT NULL COMMENT '操作人',
  `INSERT_DATE` datetime DEFAULT NULL COMMENT '新增日期',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新日期',
  `is_client` int(11) DEFAULT '0' COMMENT '是否可接口调用',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`) USING BTREE,
  KEY `USER_ID` (`USER_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_cz_merchant
-- ----------------------------
INSERT INTO `tb_cz_merchant` VALUES ('101577d2-e726-4ece-975b-d1d97f4bac38', '333333', '96e79218965eb72c92a549dd5a330112', '0.00', '1.00', '11111111111', '111111', '7761d4a2-1685-495a-b410-59e74a3be9bd', '333333', '2015-08-19 12:02:29', '2015-08-19 12:03:34', '0');
INSERT INTO `tb_cz_merchant` VALUES ('1e6f26da-21f5-45d4-a1bc-dfeed4603d58', '111111a', '96e79218965eb72c92a549dd5a330112', '0.00', '1.00', '11111111111', '111111', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6', '111111a', '2015-09-01 16:40:01', null, '0');
INSERT INTO `tb_cz_merchant` VALUES ('770c71c5-93e8-453d-b8a0-d38a502afa80', '222222', 'e3ceb5881a0a1fdaad01296d7554868d', '1000.00', '1.00', '11111111111', '111111', '4c46f4e8-36db-411c-902c-94ff3668b259', 'lixiong', '2015-08-06 10:59:43', '2015-08-06 11:19:21', '0');
INSERT INTO `tb_cz_merchant` VALUES ('92dba4f0-2c0f-4d2a-854f-b71dce801c5f', 'lixiong', '59cc3f857e5963cf09e5899cfe0f1a74', '0.00', '1.00', '', '', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3', 'admin', '2015-08-05 17:38:23', null, '1');
INSERT INTO `tb_cz_merchant` VALUES ('f7387a21-4106-4333-9c6c-9f5cce1f3a1b', '111111', '96e79218965eb72c92a549dd5a330112', '261.00', '1.00', '11111111111', '111111', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111', '2015-08-05 17:26:53', '2015-08-19 17:10:35', '1');

-- ----------------------------
-- Table structure for tb_cz_merchant_pay
-- ----------------------------
DROP TABLE IF EXISTS `tb_cz_merchant_pay`;
CREATE TABLE `tb_cz_merchant_pay` (
  `TB_MERCHAT_PAY_ID` varchar(36) NOT NULL,
  `TB_MERCHAT_PAY_USERID` varchar(36) DEFAULT NULL COMMENT '商户ID',
  `TB_MERCHAT_PAY_MONEY` float(11,2) DEFAULT NULL COMMENT '充值金额',
  `TB_MERCHAT_PAY_STATUS` int(1) DEFAULT NULL COMMENT '充值状态',
  `TB_MERCHAT_PAY_INSERT` datetime DEFAULT NULL COMMENT '新增时间',
  `TB_MERCHAT_PAY_UPDATE` datetime DEFAULT NULL COMMENT '更新时间',
  `TB_MERCHAT_PAY_OPER` varchar(36) DEFAULT NULL COMMENT '操作人',
  PRIMARY KEY (`TB_MERCHAT_PAY_ID`),
  KEY `TB_MERCHAT_PAY_ID` (`TB_MERCHAT_PAY_ID`) USING BTREE,
  KEY `TB_MERCHAT_PAY_USERID` (`TB_MERCHAT_PAY_USERID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_cz_merchant_pay
-- ----------------------------
INSERT INTO `tb_cz_merchant_pay` VALUES ('2015072500000001', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '10000.00', '2', '2015-07-25 13:08:55', null, 'lixiong');
INSERT INTO `tb_cz_merchant_pay` VALUES ('2015072600000001', '77b14151-922a-489f-b34c-a1b00111828f', '100.00', '2', '2015-07-26 23:31:45', null, 'lixiong');
INSERT INTO `tb_cz_merchant_pay` VALUES ('2015080600000001', '4c46f4e8-36db-411c-902c-94ff3668b259', '1000.00', '2', '2015-08-06 16:45:58', null, 'lixiong');

-- ----------------------------
-- Table structure for tb_cz_pay
-- ----------------------------
DROP TABLE IF EXISTS `tb_cz_pay`;
CREATE TABLE `tb_cz_pay` (
  `PAY_ID` varchar(36) NOT NULL,
  `PAY_MERCHANDISE_ID` varchar(36) DEFAULT NULL COMMENT '商品ID',
  `PAY_MERCHAT_ID` varchar(36) DEFAULT NULL COMMENT '商户ID',
  `PAY_MOBLIE` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `PAY_ORIGINAL_MONEY` float(11,2) DEFAULT NULL COMMENT '原始价',
  `PAY_DISCOUNT_MONEY` float(11,2) DEFAULT NULL COMMENT '折扣价',
  `PAY_STATUS` varchar(1) DEFAULT NULL COMMENT '订单状态',
  `PAY_START_DATE` datetime DEFAULT NULL COMMENT '开始时间',
  `PAY_END_DATE` datetime DEFAULT NULL COMMENT '结束时间',
  `PAY_UPDATE` datetime DEFAULT NULL COMMENT '更新时间',
  `PAY_OPER` varchar(36) DEFAULT NULL COMMENT '操作人',
  `PAY_OPER_DESC` varchar(36) DEFAULT NULL COMMENT '操作描述',
  PRIMARY KEY (`PAY_ID`),
  KEY `PAY_ID` (`PAY_ID`) USING BTREE,
  KEY `PAY_MERCHANDISE_ID` (`PAY_MERCHANDISE_ID`) USING BTREE,
  KEY `PAY_MERCHAT_ID` (`PAY_MERCHAT_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_cz_pay
-- ----------------------------
INSERT INTO `tb_cz_pay` VALUES ('2015072500000001', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '10.00', '2', '2015-07-25 13:10:52', '2015-08-06 11:25:25', '2015-08-06 11:25:25', 'lixiong', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072500000002', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '20.00', '20.00', '5', '2015-07-25 13:13:00', null, '2015-07-26 23:38:41', '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000001', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 17:17:16', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000002', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '1', '2015-07-26 17:17:16', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000003', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '33333333333', '30.00', '15.00', '1', '2015-07-26 17:17:16', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000004', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 17:17:16', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000005', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '1', '2015-07-26 17:17:16', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000006', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 17:22:05', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000007', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '1', '2015-07-26 17:22:05', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000008', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 17:28:17', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000009', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '1', '2015-07-26 17:28:17', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000010', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '33333333333', '30.00', '15.00', '1', '2015-07-26 17:28:17', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000011', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000012', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000013', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '33333333333', '30.00', '15.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000014', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000015', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000016', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '33333333333', '30.00', '15.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000017', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000018', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000019', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '33333333333', '30.00', '15.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000020', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000021', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000022', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '33333333333', '30.00', '15.00', '1', '2015-07-26 17:29:29', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000023', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 17:32:39', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000024', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '1', '2015-07-26 17:32:39', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000025', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '33333333333', '30.00', '15.00', '1', '2015-07-26 17:32:39', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000026', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 17:38:15', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000027', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '6', '2015-07-26 17:39:26', '2015-07-26 20:23:47', '2015-07-27 15:33:48', 'lixiong', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000028', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '5', '2015-07-26 17:39:26', '2015-07-26 20:23:45', '2015-07-26 22:44:43', '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000029', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '33333333333', '30.00', '15.00', '4', '2015-07-26 17:39:26', '2015-07-26 20:23:43', '2015-07-26 20:42:35', 'lixiong', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000030', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '1', '2015-07-26 21:04:50', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000031', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '11111111111', '10.00', '5.00', '3', '2015-07-26 21:05:17', null, '2015-07-26 23:37:34', 'lixiong', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000032', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '22222222222', '20.00', '10.00', '3', '2015-07-26 21:05:17', null, '2015-07-26 23:32:37', 'lixiong', null);
INSERT INTO `tb_cz_pay` VALUES ('2015072600000033', '5d997d11-749e-4016-883f-35c12c9ad596', 'df0e4ea7-6f25-498a-92df-e1b0678db216', '33333333333', '30.00', '15.00', '2', '2015-07-26 21:05:17', '2015-07-26 23:32:33', '2015-07-26 23:32:33', 'lixiong', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000001', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-08-05 17:42:45', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000002', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-08-05 17:46:48', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000003', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-08-05 17:48:51', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000004', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-08-05 17:49:49', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000005', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-08-05 17:50:52', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000006', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-08-05 17:51:45', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000007', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-08-05 18:38:32', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000008', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-08-05 18:45:24', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000009', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-05 19:56:52', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000010', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '22222222222', '20.00', '20.00', '1', '2015-08-05 19:56:52', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000011', '5d997d11-749e-4016-883f-35c12c9ad596', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '33333333333', '30.00', '30.00', '1', '2015-08-05 19:56:52', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000012', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-05 19:56:52', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000013', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '22222222222', '20.00', '20.00', '1', '2015-08-05 19:56:52', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000014', '5d997d11-749e-4016-883f-35c12c9ad596', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '33333333333', '30.00', '30.00', '1', '2015-08-05 19:56:52', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000015', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-05 19:56:52', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000016', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '22222222222', '20.00', '20.00', '1', '2015-08-05 19:56:52', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000017', '5d997d11-749e-4016-883f-35c12c9ad596', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '33333333333', '30.00', '30.00', '1', '2015-08-05 19:56:52', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000018', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-05 20:01:33', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000019', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '22222222222', '20.00', '20.00', '1', '2015-08-05 20:01:33', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000020', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '22222222222', '20.00', '20.00', '1', '2015-08-05 20:01:33', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000021', '5d997d11-749e-4016-883f-35c12c9ad596', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '33333333333', '30.00', '30.00', '1', '2015-08-05 20:01:33', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000022', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '22222222222', '20.00', '20.00', '1', '2015-08-05 20:01:33', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080500000023', '5d997d11-749e-4016-883f-35c12c9ad596', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '33333333333', '30.00', '30.00', '1', '2015-08-05 20:01:33', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015080600000001', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '2', '2015-08-06 10:58:24', '2015-08-06 11:25:38', '2015-08-06 11:25:38', 'lixiong', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000001', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-19 13:06:22', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000002', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '1111111', '10.00', '10.00', '1', '2015-08-19 13:18:32', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000003', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '1111111', '10.00', '10.00', '1', '2015-08-19 13:18:32', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000004', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '1111111', '10.00', '10.00', '1', '2015-08-19 13:18:43', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000005', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '1111111', '10.00', '10.00', '1', '2015-08-19 13:18:43', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000006', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-19 13:22:17', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000007', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-19 13:22:17', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000008', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-19 13:22:17', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000009', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '1111111111', '10.00', '10.00', '1', '2015-08-19 13:23:59', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000010', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '1111111111', '10.00', '10.00', '1', '2015-08-19 13:23:59', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000011', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '1111111111', '10.00', '10.00', '1', '2015-08-19 13:23:59', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000012', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-19 13:32:30', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000013', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-19 13:33:15', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000014', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '20.00', '20.00', '1', '2015-08-19 13:33:38', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000015', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '5', '2015-08-19 13:34:14', '2015-08-19 13:35:12', '2015-08-24 22:23:47', '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000016', '5d997d11-749e-4016-883f-35c12c9ad596', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '30.00', '30.00', '5', '2015-08-19 13:34:24', '2015-08-19 13:35:08', '2015-08-24 22:22:03', '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000017', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '20.00', '20.00', '1', '2015-08-19 16:53:07', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000018', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '20.00', '20.00', '1', '2015-08-19 17:05:30', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000019', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-19 17:05:50', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000020', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-19 17:12:07', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015081900000021', 'e7c58f18-a5b4-42ac-b32e-8c57e0ebc66a', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '11111111111', '10.00', '10.00', '1', '2015-08-19 17:12:15', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015091400000001', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-09-14 21:02:54', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015091400000002', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-09-14 21:03:59', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015091400000003', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-09-14 21:05:28', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015091400000004', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-09-14 21:05:31', null, null, '111111', null);
INSERT INTO `tb_cz_pay` VALUES ('2015091500000001', '7927be89-8971-4d7b-abde-b1ea8d5c77fb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111111', '20.00', '20.00', '1', '2015-09-15 19:25:55', null, null, '111111', null);

-- ----------------------------
-- Table structure for tb_cz_pay_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_cz_pay_info`;
CREATE TABLE `tb_cz_pay_info` (
  `ID` varchar(36) NOT NULL,
  `PAY_ID` varchar(36) DEFAULT NULL,
  `STATUS` int(1) DEFAULT NULL,
  `INFO` varchar(200) DEFAULT NULL,
  `PATH` varchar(200) DEFAULT NULL,
  `OPER` varchar(200) DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`) USING BTREE,
  KEY `PAY_ID` (`PAY_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_cz_pay_info
-- ----------------------------
INSERT INTO `tb_cz_pay_info` VALUES ('02d2fb93-290a-4421-aad4-9f16434deb83', '2015072600000029', '2', '充值完成', null, 'lixiong', '2015-07-26 20:21:42');
INSERT INTO `tb_cz_pay_info` VALUES ('0e16f56a-fd90-49ed-8bfe-c87d85f9dd91', '2015072600000029', '2', '充值完成', null, 'lixiong', '2015-07-26 20:23:43');
INSERT INTO `tb_cz_pay_info` VALUES ('11f24aa6-6154-4fb4-8b23-8b68ced9c0a7', '2015080600000001', '2', '充值完成', null, 'lixiong', '2015-08-06 11:25:38');
INSERT INTO `tb_cz_pay_info` VALUES ('14107fa7-b622-41d9-9cf8-be8809e28e27', '2015072600000027', '6', '申诉成功', null, 'lixiong', '2015-07-27 15:33:48');
INSERT INTO `tb_cz_pay_info` VALUES ('27a24554-a797-4b11-8869-8fc5dee6bff4', '2015072600000027', '5', '', '', '111111', '2015-07-26 22:52:30');
INSERT INTO `tb_cz_pay_info` VALUES ('37261e05-a2b3-47d7-b44d-031be918107d', '2015072600000029', '3', '充值失败', null, 'lixiong', '2015-07-26 20:42:07');
INSERT INTO `tb_cz_pay_info` VALUES ('391f3fcb-e999-457a-869f-b319c50f265d', '2015072600000028', '5', '1111111111', 'uploadfile/logo/2015-07-26/386f013d-8073-4b6b-8f45-1fd23d77a8f5.png', '111111', '2015-07-26 22:30:04');
INSERT INTO `tb_cz_pay_info` VALUES ('41bf507e-6dca-4234-99c3-c095eca6ba39', '2015072600000029', '4', '退款', null, 'lixiong', '2015-07-26 20:42:35');
INSERT INTO `tb_cz_pay_info` VALUES ('458980b5-5a5e-450c-ae31-0c4c2da82225', '2015072600000028', '5', '', '', '111111', '2015-07-26 22:44:43');
INSERT INTO `tb_cz_pay_info` VALUES ('4b2a6543-6a94-4665-9168-e04843b90203', '2015081900000015', '5', '充值完成', 'uploadfile/logo/2015-08-24/7d4518bd-6231-4f8a-871e-28c61dc18dd7.png', '111111', '2015-08-24 22:23:47');
INSERT INTO `tb_cz_pay_info` VALUES ('508b3a11-5269-4c72-9127-69ea04bae37e', '2015072600000027', '2', '充值完成', null, 'lixiong', '2015-07-26 20:23:47');
INSERT INTO `tb_cz_pay_info` VALUES ('513c15a2-2ebf-4bab-a9e3-383670364d66', '2015072600000032', '3', '充值失败', null, 'lixiong', '2015-07-26 23:32:37');
INSERT INTO `tb_cz_pay_info` VALUES ('5a47772b-261e-48fa-ab26-2076fd2cda04', '2015081900000016', '2', '充值完成', null, 'lixiong', '2015-08-19 13:35:08');
INSERT INTO `tb_cz_pay_info` VALUES ('6f1d9d89-d9d9-4c38-ac3c-ebef432ec1a1', '2015072600000029', '2', '充值完成', null, 'lixiong', '2015-07-26 20:21:24');
INSERT INTO `tb_cz_pay_info` VALUES ('7876188d-7cbd-475b-b56d-d8e26da2b5fa', '2015081900000015', '2', '充值完成', null, 'lixiong', '2015-08-19 13:35:12');
INSERT INTO `tb_cz_pay_info` VALUES ('8eb9b389-52f9-421f-a740-ab32806d8110', '2015072600000031', '3', '充值失败', null, 'lixiong', '2015-07-26 23:37:34');
INSERT INTO `tb_cz_pay_info` VALUES ('9407dca8-d21d-43fc-8709-2ae89ad3a9df', '2015081900000016', '5', '充值完成', 'uploadfile/logo/2015-08-24/8be2cd5c-eb5c-4801-b968-cf3864df96a8.png', '111111', '2015-08-24 22:22:10');
INSERT INTO `tb_cz_pay_info` VALUES ('a4b196c9-0484-41f6-892c-33522b908d37', '2015072600000028', '7', '申诉失败', null, 'lixiong', '2015-07-26 22:37:54');
INSERT INTO `tb_cz_pay_info` VALUES ('b89b3a8a-16ab-4443-96e2-9655d7bef5f5', '2015072600000033', '2', '充值完成', null, 'lixiong', '2015-07-26 23:32:33');
INSERT INTO `tb_cz_pay_info` VALUES ('bde6dc5a-89da-4908-8bb8-736e4d19da61', '2015072600000028', '2', '充值完成', null, 'lixiong', '2015-07-26 20:21:27');
INSERT INTO `tb_cz_pay_info` VALUES ('d79244a8-01f4-4ab4-a39d-9777ecaa3c26', '2015072500000001', '2', '充值完成', null, 'lixiong', '2015-08-06 11:25:25');
INSERT INTO `tb_cz_pay_info` VALUES ('d7f1f0eb-2a79-4d22-b856-d5268378616c', '2015072600000027', '2', '充值完成', null, 'lixiong', '2015-07-26 20:21:29');
INSERT INTO `tb_cz_pay_info` VALUES ('dfeb6009-bf3f-4a6d-a27a-c213774275d5', '2015072600000028', '2', '充值完成', null, 'lixiong', '2015-07-26 20:23:46');
INSERT INTO `tb_cz_pay_info` VALUES ('ff7d884f-d622-4b72-bafe-a44fa57f5f0f', '2015072500000002', '5', '没有充值进去', 'uploadfile/logo/2015-07-26/4185c555-3abe-44a3-8fc3-ac1fe08c081c.png', '111111', '2015-07-26 23:38:41');

-- ----------------------------
-- Table structure for tb_dqdp_config
-- ----------------------------
DROP TABLE IF EXISTS `tb_dqdp_config`;
CREATE TABLE `tb_dqdp_config` (
  `CONFIG_ID` varchar(36) NOT NULL,
  `COMPONENT_NAME` varchar(100) DEFAULT NULL,
  `MODEL_CODE` varchar(100) DEFAULT NULL,
  `CONFIG_NAME` varchar(50) DEFAULT NULL,
  `CONFIG_VALUE` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`CONFIG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dqdp_config
-- ----------------------------
INSERT INTO `tb_dqdp_config` VALUES ('0426a01e-02dd-469c-ad16-bb63abd5942c', 'systemmgr', null, 'adminRole', '0c7f6536-156d-468b-b81a-cfb5fd9918bc');
INSERT INTO `tb_dqdp_config` VALUES ('09bfd3a3-4f07-4056-b949-b7cb2e230efe', 'systemmgr', null, 'discount', '50');
INSERT INTO `tb_dqdp_config` VALUES ('27b3f18e-e410-4f0d-bffe-7815b5b7cb9d', 'gmcc', null, 'gmcc_biap_asmx_url', 'http://iams.gz.gmcc.net/WS/SSO.asmx?wsdl');
INSERT INTO `tb_dqdp_config` VALUES ('3e6126bf-e870-46b2-bc15-cb331e36edb9', 'dqdp', '', 'id', 'f912016b-ffb7-4376-b11b-a1b8e07056ef');
INSERT INTO `tb_dqdp_config` VALUES ('43d2c302-399e-44c0-9b1f-bad5c18c7fa7', 'systemmgr', null, 'weakPwd', 'false');
INSERT INTO `tb_dqdp_config` VALUES ('4609db44-4c20-457e-90e1-0007e0f29d0e', 'systemmgr', null, 'forgetpasswordTips', '忘记密码，请拨打139XXXXXXXX');
INSERT INTO `tb_dqdp_config` VALUES ('50e2fd8c-37d8-4dce-b579-f7e586d33cec', 'systemmgr', null, 'top_weixin', 'weixin88888');
INSERT INTO `tb_dqdp_config` VALUES ('51817a27-0726-4c70-933d-0bdd67876579', 'gmcc', null, 'gmcc_data_asmx_url', 'http://iams.gz.gmcc.net/WS/GZHRManager.asmx?wsdl');
INSERT INTO `tb_dqdp_config` VALUES ('54020fd7-7a00-44fd-a6e2-800e5ec5275e', 'systemmgr', null, 'org_top', 'org00000000');
INSERT INTO `tb_dqdp_config` VALUES ('5a108aca-f7ad-486d-a637-e5f1ecc8dc12', 'gmcc', null, 'GmccSsoFilterCookieName', '__GZIAMS-Passport__,iPlanetDirectoryPro');
INSERT INTO `tb_dqdp_config` VALUES ('67fee6a5-53bf-41e0-8190-106fb7a7467d', 'systemmgr', null, 'weakPwd', 'true');
INSERT INTO `tb_dqdp_config` VALUES ('6acd2fb7-545f-496a-af50-fc96c328b077', 'systemmgr', '', '_lastUpdateVersion', '1.03');
INSERT INTO `tb_dqdp_config` VALUES ('6e5d24ac-80c8-42d2-a9b0-8fb8cc70f944', 'gmcc', null, 'useGmccSsoFilter', 'false');
INSERT INTO `tb_dqdp_config` VALUES ('71219913-057d-4402-b3e3-2054127a5d2b', 'gmcc', null, 'useGmccCookieSsoFilter', 'true');
INSERT INTO `tb_dqdp_config` VALUES ('71590e3b-5c88-4147-92e6-f799d8a78131', 'systemmgr', null, 'httpEncode', 'UTF-8');
INSERT INTO `tb_dqdp_config` VALUES ('81fe5e39-ed25-4abf-92ef-be2570aec03d', 'systemmgr', null, 'top_moblie', '139XXXXXXX');
INSERT INTO `tb_dqdp_config` VALUES ('83220187-3380-4171-89a2-7335545b22e9', 'systemmgr', null, 'merchantRole', '78e033cb-b9e8-48a6-9a8e-5890069bc529');
INSERT INTO `tb_dqdp_config` VALUES ('8678bfd1-e628-4134-8834-176ad4237fc8', 'systemmgr', null, 'top_advert', '1、动感地带，我的地盘我做主。\n2、移动改变生活，中国移动通信，移动信息专家');
INSERT INTO `tb_dqdp_config` VALUES ('91a0d712-f9e5-411a-abdc-b49ae55442fe', 'systemmgr', null, 'top_tips', '测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测测试测试测试测测试测试测试测测试测试测试测');
INSERT INTO `tb_dqdp_config` VALUES ('92a580c3-dce9-47b7-ac03-68ef5fa9a7fd', 'dqdp', '', 'appRootPath', '');
INSERT INTO `tb_dqdp_config` VALUES ('9b316228-6e02-4407-acd3-0c9ac036e2e9', 'systemmgr', null, 'pageSize', '10');
INSERT INTO `tb_dqdp_config` VALUES ('a5f995ff-cce0-43bd-a3d0-ab332adc13f4', 'schedule', '', '_lastUpdateVersion', '1.04');
INSERT INTO `tb_dqdp_config` VALUES ('a975aeb2-17ca-4275-97ab-d76b3b9d5221', 'gmcc', null, 'gmcc_auto_login_password_plaintext', '3e4r5t6y@!');
INSERT INTO `tb_dqdp_config` VALUES ('af5873ff-8ac1-4387-9860-738358a1508f', 'gmcc', null, 'gmcc_notify_asmx_url', 'http://tiams.gz.gmcc.net/WS/Notify.asmx?wsdl');
INSERT INTO `tb_dqdp_config` VALUES ('d663459f-440f-4e95-a9e3-c509b661554a', 'systemmgr', null, 'top_qq', '1234567');
INSERT INTO `tb_dqdp_config` VALUES ('db7d3885-11de-4fcb-a669-6ad94d24ac47', 'dqdploger', null, 'logSearch', 'false');
INSERT INTO `tb_dqdp_config` VALUES ('dd500b7c-6d00-4ccd-82b4-064d9220ab5c', 'dqdp', '', 'logEventObserver', 'f912016b-ffb7-4376-b11b-a1b8e07056ef');
INSERT INTO `tb_dqdp_config` VALUES ('e41422d6-ee51-4e17-af61-d4ec8b973066', 'gmcc', null, 'sysAccount', 'wghzc');
INSERT INTO `tb_dqdp_config` VALUES ('ef072862-867e-4097-a2f6-b18819a995a0', 'systemmgr', null, 'org_merchant', 'deaf8bfe-c403-4b83-84c0-ca1e8db80a05');
INSERT INTO `tb_dqdp_config` VALUES ('ef42885c-7dd5-481a-8d7a-8f3ef5bcc443', 'systemmgr', null, 'copyright', '深圳深讯领航科技有限公司');
INSERT INTO `tb_dqdp_config` VALUES ('f22485f4-6eec-4bc0-800f-00d7dbab0956', 'gmcc', null, 'sysPassword', 'wghzc)!(*%267');
INSERT INTO `tb_dqdp_config` VALUES ('f4452e21-48a6-4eaf-8b0e-ee44f2f6ba54', 'dqdploger', '', '_lastUpdateVersion', '1.0');
INSERT INTO `tb_dqdp_config` VALUES ('f75c424b-7366-47e6-a1bd-26b93876bec4', 'gmcc', null, 'systemId', '__GZMobile-wghzc__');

-- ----------------------------
-- Table structure for tb_dqdp_job
-- ----------------------------
DROP TABLE IF EXISTS `tb_dqdp_job`;
CREATE TABLE `tb_dqdp_job` (
  `JOB_ID` varchar(36) NOT NULL,
  `JOB_NAME` varchar(100) DEFAULT NULL,
  `JOB_DESC` varchar(300) DEFAULT NULL,
  `IP` varchar(100) DEFAULT NULL,
  `CLASS_NAME` varchar(100) DEFAULT NULL,
  `JOB_GROUP` varchar(100) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL,
  `CRON_EXPRESSION` varchar(100) DEFAULT NULL,
  `START_TIME` datetime DEFAULT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `NEXT_RUN_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`JOB_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dqdp_job
-- ----------------------------

-- ----------------------------
-- Table structure for tb_dqdp_job_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_dqdp_job_log`;
CREATE TABLE `tb_dqdp_job_log` (
  `LOG_ID` varchar(36) NOT NULL,
  `JOB_NAME` varchar(100) DEFAULT NULL,
  `EXECUTE_RESULT` int(11) DEFAULT NULL,
  `EXECUTE_EXCEPTION` varchar(1000) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `COMPLETE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dqdp_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for tb_dqdp_keyword
-- ----------------------------
DROP TABLE IF EXISTS `tb_dqdp_keyword`;
CREATE TABLE `tb_dqdp_keyword` (
  `KEYWORD_ID` varchar(36) NOT NULL,
  `KEYWORD` varchar(36) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL COMMENT '0 : 正常，1 : 禁用',
  PRIMARY KEY (`KEYWORD_ID`),
  UNIQUE KEY `KEYWORD` (`KEYWORD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dqdp_keyword
-- ----------------------------

-- ----------------------------
-- Table structure for tb_dqdp_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_dqdp_log`;
CREATE TABLE `tb_dqdp_log` (
  `LOG_ID` varchar(36) NOT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `OPERATION_ID` varchar(36) DEFAULT NULL,
  `OPERATION_NAME` varchar(150) DEFAULT NULL,
  `MODEL_NAME` varchar(100) DEFAULT NULL,
  `OPERATION_TYPE` varchar(36) DEFAULT NULL,
  `OPERATION_RESULT` varchar(20) DEFAULT NULL,
  `OPERATION_DESC` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dqdp_log
-- ----------------------------
INSERT INTO `tb_dqdp_log` VALUES ('00278f23-5a2a-40ad-9eef-fe14e5f21d1c', '2015-07-25 23:52:57', 'lixiong', null, 'merchant_merchant', 'add', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('0075a8ed-488b-4079-971d-693e061f8fc9', '2015-07-26 18:48:32', 'admin', null, 'merchant_merchant', 'update', '1001', '修改商户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('00942e4b-1ba1-4215-8e95-7bfd560eaaa5', '2015-07-26 21:35:50', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('01bc6d4b-1162-4341-904b-5d89848db71b', '2015-07-25 13:07:40', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('01f0c78d-cf95-4e06-87d1-73a3007a5eaf', '2015-07-26 18:29:29', 'lixiong', null, 'merchant_merchant', 'update', '1001', '支付密码不正确，无法修改商户信息');
INSERT INTO `tb_dqdp_log` VALUES ('026b05f9-3cd9-4bcb-b244-a5d6b803b4a7', '2015-07-26 16:14:11', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('02770f4e-a963-49fe-bb35-8a95c76016cf', '2015-07-26 16:43:36', '222222', null, 'merchant_merchant', 'add', '1001', '对不起，你账户余额已不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('0292ec9a-612c-4aed-88f6-62201adeecb6', '2015-07-25 23:52:04', 'lixiong', null, 'merchant_merchant', 'add', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('02d493e6-15ae-4d8b-8324-af04a94c2611', '2015-07-26 20:28:58', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('02f3d410-dbea-4924-93f0-ffb5b039a317', '2015-07-26 20:01:26', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('030edd6c-1787-4b76-a214-837d704b9076', '2015-07-26 16:32:31', '111111', null, 'merchant_merchant', 'add', '1001', '充值格式不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('03198a13-a521-41d0-a682-7f36737471c1', '2015-08-05 20:01:33', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('033d4940-f975-4132-a601-99ec3ea98444', '2015-07-25 23:18:04', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('039eae00-6411-4447-99e5-2c7fc63578ad', '2015-07-27 14:24:48', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('040169d5-d071-4d07-837f-8da659ffb394', '2015-08-05 18:21:20', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0440c9a7-2b8a-4981-8a12-b7eb8320653b', '2015-07-26 16:00:28', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('05979fcb-ed94-49a4-82e9-82fc2ac6bf3b', '2015-08-19 12:03:04', '3333333', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('06c6b421-c54f-492a-8259-53325a9c147a', '2015-08-19 17:18:46', '333333', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('070bfd48-58c7-4d8b-998c-fc716c34f49a', '2015-07-26 23:06:02', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('07a249dc-0cad-4928-afb7-dc4e9b9e33d1', '2015-07-25 23:55:07', 'admin', null, 'systemmgr_config', 'add', '0', '新增配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('07fc2409-d8c5-4c51-8b09-703caff54701', '2015-07-25 23:52:06', 'lixiong', null, 'merchant_merchant', 'add', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('083279f5-89c0-4e99-824e-1547e57e977d', '2015-07-25 13:18:01', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0967ab11-b28b-4a74-96e5-0a4f22198e8a', '2015-07-26 15:17:45', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0a362d27-2686-4887-8351-02057d65cdfe', '2015-07-26 02:22:33', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('0aa7dcdb-9324-4de5-8ff1-b75134b81cd8', '2015-08-24 22:19:50', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0ad5fb48-6725-45b3-b785-f32ce635efad', '2015-09-14 21:05:28', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('0ae246ea-3c10-46ce-b7d8-b430b42d41b3', '2015-08-05 20:01:07', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0af227db-7454-4b89-bd38-77fa81066e55', '2015-07-26 13:01:09', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0b0caa67-c817-4f0b-8462-1a04d31a461d', '2015-07-26 17:11:29', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('0b1a969e-f939-4187-a5d0-942f9c200c9e', '2015-07-26 20:00:26', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('0b9fe607-b2c9-4f58-8b37-4835f0db969c', '2015-07-25 12:55:37', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0c4fe9c8-560d-495e-963e-1a950af87c44', '2015-08-05 17:19:22', 'anonymous', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误');
INSERT INTO `tb_dqdp_log` VALUES ('0c5212ce-d82f-4919-9a6f-65831210303f', '2015-08-06 11:20:45', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0c76f470-41a4-41dd-8072-5d672b1af4b0', '2015-08-19 14:18:40', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0ce6ff19-d2b8-4a6c-b47e-6fdfa63dea8d', '2015-07-25 14:15:21', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('0e0981c5-8049-4557-a684-d1b0632cff67', '2015-07-25 23:54:38', 'lixiong', null, 'merchant_merchant', 'update', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('0ea2ea48-f848-452d-8a17-cc39c52f9c5c', '2015-07-26 20:22:33', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0eb95795-254a-46fc-8052-fe1e83df91d5', '2015-08-05 18:38:32', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('0f0f52d7-c33d-4bf6-a900-472ccbbf0568', '2015-07-26 20:29:41', 'admin', null, 'dqdpdictionary_dictmgr', 'add', '0', '字典新增成功');
INSERT INTO `tb_dqdp_log` VALUES ('0f65bf67-f5d0-4175-9275-c552bfb8ddd9', '2015-08-19 16:04:47', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('0ffefca8-98c0-40de-ae38-74012be4465a', '2015-08-05 18:05:45', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('102a0e22-159b-4aeb-877a-a93f9a99038a', '2015-08-19 13:03:17', '333333', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('10667d7f-3697-4816-adda-5186c9f7a63b', '2015-09-14 21:02:54', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('11d5e283-7bab-444f-bc88-86ad532f961e', '2015-07-26 22:30:44', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('1275294e-1061-4076-89e6-53f950f50030', '2015-07-26 20:30:01', 'admin', null, 'dqdpdictionary_dictmgr', 'update', '0', '字典更新成功');
INSERT INTO `tb_dqdp_log` VALUES ('132b299c-7983-48ac-b368-27e6183df435', '2015-07-27 15:38:15', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('1417ad86-3a8e-4551-bcda-fcb6fd026edd', '2015-07-26 20:01:46', 'lixiong', null, 'merchandise_merchandise', 'update', '0', '更新成功');
INSERT INTO `tb_dqdp_log` VALUES ('149de886-8e7f-431d-bc3c-10f51f717f4a', '2015-07-26 15:59:54', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('173e55f4-b245-4efa-862f-9f18a1479e80', '2015-07-27 15:42:25', 'admin', null, 'merchant_merchant', 'del', '0', '删除用户成功');
INSERT INTO `tb_dqdp_log` VALUES ('1759f801-600c-4359-9a53-08402da529bc', '2015-08-05 17:24:14', 'anonymous', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误');
INSERT INTO `tb_dqdp_log` VALUES ('17d21bf6-5ca6-4ca2-ae74-33feea8c8c50', '2015-08-19 13:24:10', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('18bf8fdb-8836-4aea-a687-6f9b525b444e', '2015-08-19 10:58:17', '111111', null, 'merchant_merchant', 'update', '1001', '修改用户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('1908858f-e181-4c70-ac1b-c1d98d2e5edf', '2015-07-25 12:47:07', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('199d491b-d40b-4b12-8c2c-06f63bff8ad1', '2015-08-06 11:23:34', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('19c55b53-772b-41d1-8bd5-7fe0c9a2c09e', '2015-08-19 17:12:07', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('1a3ba6e7-bf32-4c62-9e0d-4e7cb10ce964', '2015-07-26 19:11:34', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('1a94770c-c3f9-47b3-93d0-3f54e79345e1', '2015-08-19 16:04:43', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('1b208e92-a256-49c6-8fd3-c8a6202447e7', '2015-08-19 17:12:15', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('1c0eb3da-a9c3-435d-bec1-0d5bf3bddf3c', '2015-08-19 13:29:45', '111111', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误');
INSERT INTO `tb_dqdp_log` VALUES ('1c22c8e5-6f4e-4e5c-974f-9a85f7bd6bb7', '2015-07-25 23:40:04', 'lixiong', null, 'merchant_merchant', 'update', '1001', '修改商户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('1c30cb6e-bbc2-4375-aff1-bb764553de72', '2015-07-26 20:30:13', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('1ccdb140-407e-412c-87df-e2d8be79fdc4', '2015-07-26 16:00:03', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('1cda40c4-a0db-4f44-88a8-d4e963af8e74', '2015-07-26 16:14:14', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('1d6afd97-a251-43ca-b300-8f823dfbd62b', '2015-07-26 16:42:18', '222222', null, 'merchant_merchant', 'add', '1001', '对不起，你账户余额已不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('1e1ffde7-3aea-4502-b30a-cc18dacb9b32', '2015-07-25 23:55:41', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('1e9e41cd-b895-405b-b620-51d5bf5a4345', '2015-07-26 21:40:13', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('1f10df62-0cee-4d59-bad0-8337fc93ffc4', '2015-08-06 11:22:57', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('1f26833b-8afd-4424-9092-be547cf8b04f', '2015-07-26 17:15:39', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('1f84f8a5-813b-4de4-84f3-57d098ead3ac', '2015-07-26 20:01:43', 'lixiong', null, 'merchandise_merchandise', 'update', '0', '更新成功');
INSERT INTO `tb_dqdp_log` VALUES ('1fbcffd5-5480-435b-a9d1-2169a34f2a11', '2015-07-25 13:12:58', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('1fd8cade-acc5-48c6-a685-c529a7e5d4d8', '2015-08-19 13:06:17', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('20308d67-3a4f-4a54-a5ba-a063be5be6a3', '2015-07-26 21:32:54', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('2123a210-a03f-4bcb-ab14-b8e25e35b4ad', '2015-07-25 21:24:15', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('2237e20f-0b35-490b-b861-d264f016035b', '2015-08-19 12:06:58', '333333', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('2246e4d2-db55-4d15-9988-f161f1fff8d4', '2015-07-26 02:22:39', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('229cc2f4-ffa0-46a2-9878-bb1611a1ff28', '2015-07-26 17:13:06', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('22c3af9d-38cd-451f-bc4a-dc48e2a387bf', '2015-08-19 16:24:29', '333333', null, 'merchant_merchant', 'add', '1001', '您账户余额:0.0 本次批量充值需要金额:10.0，余额不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('242ee303-9f19-4c67-837a-65a130998bd9', '2015-07-25 12:48:48', 'admin', null, 'merchant_merchant', 'add', '0', '新增商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('246af398-bdda-4eff-ab2c-d1dd165a7570', '2015-07-26 20:59:28', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('25898d04-e91a-4b8d-ac4a-d592f705df1f', '2015-07-26 01:27:43', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('258a741c-00d0-4f8a-b1d3-1caf262db4d7', '2015-08-19 11:02:34', '111111', null, 'merchant_merchant', 'update', '1001', '修改用户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('259c658e-81fc-4c84-8b0a-01984dc50096', '2015-08-06 10:59:43', 'anonymous', null, 'merchant_merchant', 'add', '1001', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('25c90665-a640-4e60-ac7f-d723b4585797', '2015-07-27 14:29:43', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('26258580-db32-42c2-9371-8b5811a0d6d9', '2015-08-19 13:29:22', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('26b4a8ce-cebb-4200-a0a5-a11ce938c5fb', '2015-08-19 11:16:09', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('2747dfde-5f24-4e5e-adcf-a7572c408286', '2015-07-25 14:16:44', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('27c0e070-39c8-4365-b724-d9be94fe1983', '2015-07-26 14:57:01', 'lixiong', null, 'systemmgr_config', 'update', '0', '更新配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('27f583dd-eb4e-43d1-b926-49e0d6774059', '2015-08-06 11:24:59', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('29ef92db-0427-4b0e-a82d-8c3e66a918ea', '2015-09-15 19:12:27', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('2a7bfeb3-07ac-418a-98c7-642849bb0c67', '2015-08-19 13:34:43', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('2c615f0d-28d6-40d9-b542-b5ea918053ce', '2015-08-06 11:01:18', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('2d28daa6-caa5-4b04-8282-b14083669f8a', '2015-07-26 16:41:49', '222222', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('2d917f6e-0a67-41db-848e-bf15a94e99c4', '2015-08-19 10:56:56', '111111', null, 'merchant_merchant', 'update', '1001', '修改用户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('2d955f37-50e3-431e-bd03-459d899ca4e0', '2015-07-27 14:27:11', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('2dc1cdf4-39a0-4411-9a54-e6126b939bd7', '2015-08-19 12:03:14', '333333', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('2f2aa768-a3d3-4225-a995-2cbb365112cf', '2015-07-25 13:54:35', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('2f5e5172-5edd-4faf-b4ce-aed57359036a', '2015-08-19 14:37:29', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('2f68f0a9-1496-4f96-b15d-6a095d8f7236', '2015-08-06 11:18:27', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('2f7965a0-ef59-4ce6-866d-2af758d3a571', '2015-07-26 20:29:26', 'admin', null, 'dqdpdictionary_dictmgr', 'add', '0', '字典新增成功');
INSERT INTO `tb_dqdp_log` VALUES ('2f9f5a47-9a10-45c3-920a-5542d6c3f3af', '2015-07-27 15:42:14', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('2fa242a6-8743-451a-80a0-c56a35202471', '2015-07-26 16:04:33', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('2fc5e05b-528c-4b4c-aa20-328eff0dba17', '2015-08-06 11:26:17', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('30eb16f8-a544-469f-b77c-629b048debfc', '2015-07-26 17:17:06', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('314d5669-0293-4507-be9f-111b99aac75d', '2015-07-26 13:06:07', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('31eb3f4e-0902-42a6-86b8-bb3e9d75a3c3', '2015-07-26 17:08:15', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('33995c0e-6bc7-43b1-879d-c99c705e16bd', '2015-08-19 15:31:18', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('346c603e-a40d-48d2-b430-e4293bd816c6', '2015-08-19 13:34:51', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('34a984b0-5a39-4f96-ab74-dbb1cdc548de', '2015-07-26 21:04:11', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('364bf62b-33e2-4cbc-8f95-fc3495aa9bc1', '2015-07-27 15:40:14', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('36f7535a-169d-4a91-9ee7-e2cf450c31ec', '2015-07-26 17:14:19', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('375b2c83-de78-48b5-97ad-4846d26b4053', '2015-07-26 20:12:30', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('37c1045f-c552-4e93-a06c-f39e6490bd9c', '2015-07-26 17:16:54', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('37e0074e-1cef-4aa4-bd05-afd5f8a54c52', '2015-07-25 14:16:25', 'w123456', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('37f6da99-a6b3-4caf-9578-9d8af03fb5d5', '2015-07-25 22:49:00', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('37f9b320-d16f-4445-8184-56b5beac3415', '2015-08-19 12:58:25', '333333', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('3add9360-5965-4236-bfef-757c46d5eb87', '2015-07-26 18:49:30', 'admin', null, 'merchant_merchant', 'update', '0', '修改商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('3b218905-46dc-46c1-b151-eafe4bd1aa5d', '2015-08-06 11:17:13', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('3b789f06-06ab-4688-b591-e419a2738a24', '2015-07-27 15:40:31', 'lixiong', null, 'merchandise_merchandise', 'update', '0', '更新成功');
INSERT INTO `tb_dqdp_log` VALUES ('3c4ff149-29a7-4078-999f-47968c5eabe2', '2015-07-25 13:00:57', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('3d840751-a329-434d-85bd-930399427197', '2015-07-26 13:57:25', '11111', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('3dd98fcc-bedc-4fa1-978d-3b14feb4db26', '2015-07-26 16:45:49', '222222', null, 'merchant_merchant', 'add', '1001', '您账户余额:10.0 本次批量充值需要金额:30.0，余额不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('3e07269f-7052-41f3-85dd-1af1f406efe4', '2015-07-25 12:52:42', 'lixiong', null, 'merchandise_merchandise', 'add', '0', '新增套餐成功');
INSERT INTO `tb_dqdp_log` VALUES ('3eb9d5a4-f251-476a-bb68-e029adb9edb8', '2015-07-26 23:37:51', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('3ef151ee-66e0-43f1-8c96-79e1e07249c3', '2015-09-01 16:39:52', 'anonymous', null, 'merchant_merchant', 'add', '1001', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('3f06f71b-b4ad-4391-8711-f992886b0f70', '2015-07-26 13:40:17', 'admin', null, 'systemmgr_config', 'update', '0', '更新配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('3f0acb09-16d4-419d-ae25-1677c197e073', '2015-07-25 23:37:39', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('3f2066fb-835c-45a9-aed0-9960e112bd01', '2015-07-25 23:54:16', 'lixiong', null, 'merchant_merchant', 'update', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('3f54443c-c5a0-434b-b5a4-ef1e4aab7580', '2015-07-26 17:18:35', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('3fef29d1-552f-4878-a902-e8c1ee506386', '2015-07-26 19:58:26', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('3ff51f94-ae3f-4ddc-a942-0f082736ed47', '2015-07-26 13:43:25', 'admin', null, 'systemmgr_config', 'update', '0', '更新配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('41c03750-d360-4d9f-8c47-3b48a85fc3e7', '2015-07-26 02:35:00', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('41dfb1ee-26ce-477a-b931-3a1666f4312f', '2015-08-19 11:09:22', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('41f3e42f-4b7d-4720-9f3e-daa185da40cd', '2015-07-26 20:21:20', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('42a7c9e4-997f-42f7-9555-6579e9f56bec', '2015-07-26 23:28:32', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('441f184e-6081-497d-b758-1c682e7454d2', '2015-08-19 12:04:34', 'admin', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('44378ab1-067f-41d0-aa10-4fcceb43964a', '2015-07-26 03:08:12', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('444b4547-8953-4cb5-9156-543f5a2d97f8', '2015-07-27 15:42:22', 'admin', null, 'merchant_merchant', 'del', '0', '删除用户成功');
INSERT INTO `tb_dqdp_log` VALUES ('44e1fd0f-a381-4674-9bee-24ba50e49b7c', '2015-07-26 17:39:14', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('44ed00a1-9b77-48fc-b0cb-13ba18a32c06', '2015-07-25 13:16:09', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('44fd2043-1daf-4ec9-9b15-7e37a1a584cc', '2015-08-19 10:52:25', '111111', null, 'merchant_merchant', 'update', '1001', '修改用户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('45491ba3-eff6-4639-88a8-347fc605fba9', '2015-07-26 19:04:06', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('456281ff-e1c3-4903-a11d-7aed53fa3d8e', '2015-07-26 18:42:31', 'lixiong', null, 'merchant_merchantpay', 'add', '1002', '充值支付密码不正确，充值失败');
INSERT INTO `tb_dqdp_log` VALUES ('45a307a8-e3af-49d1-b67f-554366475bf7', '2015-07-26 17:15:52', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('45fe5e37-89a4-4bbb-9f42-6a4f2f8b67fc', '2015-07-25 14:16:19', 'w12345', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('467c7cd9-f477-48d0-97a8-13d56ee094e0', '2015-07-25 23:54:40', 'lixiong', null, 'merchant_merchant', 'update', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('46a93b02-c684-4533-8c2a-af4f1290d2b1', '2015-08-19 13:59:15', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('46bf243c-5277-4da6-8592-61f0481f260f', '2015-07-26 17:07:51', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('4751cb54-3bdc-41c7-9a6c-fb2139ce05ee', '2015-08-05 17:37:47', 'lixiong', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('47908832-9473-4bef-a841-cd99fa073c0a', '2015-08-19 11:03:46', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('47ded2ba-96b1-4c24-9f76-601ea8ada126', '2015-07-26 16:00:40', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('48180470-9169-4b9d-8c87-18b4d62a3468', '2015-08-19 13:22:13', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('48512dc9-b085-415f-bbb1-195d82a4add3', '2015-08-19 10:58:10', '111111', null, 'merchant_merchant', 'update', '1001', '修改用户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('4860e021-b5e5-435b-a495-a3d48b081e50', '2015-07-26 17:13:05', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('4879fd93-35c7-4981-b81b-2d0bad1b06a0', '2015-08-19 11:03:30', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('4abd3f7b-a9c8-4181-a4e6-9fca05503264', '2015-08-05 19:56:45', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('4baebdc7-70c3-4458-ba51-c5f30db91bfe', '2015-08-19 17:01:16', '333333', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('4bc26250-b493-4e70-92ef-adaed892cdbd', '2015-08-06 11:01:11', 'admi', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('4be59843-d9a1-4b22-836b-3c185d9c607d', '2015-08-06 10:58:24', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('4c1305ce-75ff-48e3-b563-1f7d5ea55f07', '2015-07-26 16:37:14', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('4dcbed3d-1248-4185-b7eb-6f9412b680f8', '2015-07-26 17:00:19', '222222', null, 'merchant_merchant', 'add', '1001', '您账户余额:10.0 本次批量充值需要金额:30.0，余额不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('4e694daf-8177-4c54-be94-dcfe9c276092', '2015-08-19 10:57:10', '111111', null, 'merchant_merchant', 'update', '1001', '修改用户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('4fc038b5-a6b7-4253-9550-8cc530aa09a0', '2015-07-26 17:38:33', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('50154b21-19ce-4918-902b-2162bdde1c07', '2015-07-26 14:08:19', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('51bee5dd-879a-4d2f-966f-081601712ab1', '2015-08-19 13:18:44', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('526681f6-86c1-47f5-aeee-9de81eba395d', '2015-08-19 13:05:59', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('535cb9e8-93f6-4a8a-9212-a99b24aaeff5', '2015-07-26 23:39:08', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('5392c1a0-872b-4ae4-b76a-d82df11e046a', '2015-08-19 15:24:49', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('55409028-12d4-4809-9371-328b8422344c', '2015-07-25 21:24:21', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('565d04c9-78f3-4eeb-aaf2-391ac039750b', '2015-08-19 10:15:02', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('571f3c73-b0f2-432d-81fc-54d89e16fcac', '2015-07-25 23:53:01', 'lixiong', null, 'merchant_merchant', 'add', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('5812ee5a-729f-49b8-abe2-2eed673a936e', '2015-08-05 17:37:53', 'lixiong', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('582790b9-8e32-4134-b46c-8a0da90c6994', '2015-07-25 23:53:02', 'lixiong', null, 'merchant_merchant', 'add', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('583f0051-85b0-4370-a697-019c368a359c', '2015-07-25 12:55:11', 'anonymous', null, 'merchant_merchant', 'add', '1001', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('58fd6f50-b8de-44fd-b466-e122b81485f4', '2015-07-26 17:29:30', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('592a01fd-de7d-4f0d-9cb5-33c4475b6485', '2015-08-05 19:57:24', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('593b101b-b03d-4d08-9732-1b064984ca5d', '2015-07-26 02:38:21', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('593defdd-57b2-4805-9bc4-ad681ac4a2e1', '2015-08-19 17:00:54', '333333', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('59aaa7af-d737-423c-aa86-5e906a7c3e47', '2015-07-26 18:49:22', 'admin', null, 'merchant_merchant', 'update', '0', '修改商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('59cdf1c2-dd32-425a-8266-5ab3f4f6d5c3', '2015-08-19 10:34:33', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('5ab10256-2a6d-4e8e-bffd-c65528b853c8', '2015-07-26 17:16:07', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('5b77e5d5-673b-423e-82f4-d3bac4692305', '2015-08-04 10:44:11', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('5bd081f4-9794-457d-b874-ee3017307124', '2015-07-26 21:48:48', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('5c388288-1de2-4c58-86d3-45837bbaa173', '2015-08-05 17:41:37', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('5c895303-1b63-49e1-a51c-eeddc26960a6', '2015-07-26 23:31:25', 'lixiong', null, 'merchant_merchant', 'update', '0', '修改商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('5ca9fe12-f417-49a7-b41d-ceb1be1cdebd', '2015-08-05 17:40:00', 'admin', null, 'merchant_merchant', 'update', '1001', '修改商户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('5d4309a3-9aa5-4ab8-8518-3ca5bfcd010f', '2015-08-19 13:18:32', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('5d546014-5ad2-4edb-9fb6-1bc511977c7f', '2015-08-19 10:15:28', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('5d6e0261-3162-4ac9-afa1-0f958308af2d', '2015-07-26 17:18:31', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('5e607fa6-6c85-4782-be82-1951e64be496', '2015-08-05 19:56:52', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('5f4d3d43-2760-442c-9a54-4fefffa4a880', '2015-08-19 13:32:30', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('5fd20c7f-8332-434f-9a3c-46711b4e88de', '2015-07-26 23:39:02', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('60728bbc-e6cb-4b0a-85ae-d027b20b3b3c', '2015-08-05 17:50:52', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('60ce36fa-5b5e-43b0-8c1d-dcfbdc54425e', '2015-08-19 17:05:50', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('60d5379c-4c93-4588-b261-deb0da0c0bb9', '2015-07-26 17:28:11', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('60fd7f59-9f36-493a-931b-f128520adf18', '2015-07-25 13:04:47', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('610d2d9a-c138-4826-97ac-9807a48b463f', '2015-07-26 23:28:49', 'admin', null, 'merchant_merchant', 'update', '0', '修改商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('6165ab58-fcc8-4af1-8143-b36761c783ff', '2015-08-05 18:05:40', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('61a309eb-b246-421a-b1ef-8ee361bbfe32', '2015-08-05 16:24:56', 'anonymous', null, 'merchant_merchant', 'add', '1001', 'json格式错误');
INSERT INTO `tb_dqdp_log` VALUES ('61dcbb33-0f01-4d93-83fb-3a283e937506', '2015-07-26 22:52:46', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('6202a59c-849f-48df-99e2-f890f1a760c4', '2015-08-05 17:38:59', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('625def63-81f7-4cd9-b18c-c4ed7f259790', '2015-07-26 20:00:16', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('629d3276-4cee-4ce5-84f8-56e1aa631fed', '2015-08-19 12:02:30', 'anonymous', null, 'merchant_merchant', 'add', '0', '新增人员信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('6331f6c1-2b51-4b1f-8956-b26c1d78d53e', '2015-09-14 21:05:31', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('63533565-6137-432e-bc77-cd8d53923f38', '2015-08-05 17:49:49', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('6419981a-713b-4bb6-99aa-1988487404c1', '2015-07-26 16:29:46', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('653b65a5-838b-4569-a7ed-10640bf11471', '2015-07-26 17:17:16', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('6558314a-44de-408b-98e2-631c20909956', '2015-07-26 13:39:09', 'admin', null, 'systemmgr_config', 'update', '0', '更新配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('67e9d710-b5d9-4b33-bbb7-42ee025fff42', '2015-07-25 12:52:30', 'lixiong', null, 'merchandise_merchandise', 'add', '0', '新增套餐成功');
INSERT INTO `tb_dqdp_log` VALUES ('68772680-a57b-4259-90cd-7378084d33b3', '2015-07-26 14:55:46', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('689759bb-ab29-455d-9658-66b138877b86', '2015-07-25 12:55:19', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('68a40c44-f8e1-4c99-b40c-8b33e5ba0cff', '2015-07-25 12:44:59', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('69985e7d-e3d2-436b-a9bf-a28541c9dd41', '2015-07-26 23:39:45', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('6a1f6b27-cbcd-4ba6-9060-875aeb4ddd0a', '2015-07-26 18:40:09', 'lixiong', null, 'merchant_merchant', 'update', '1001', '支付密码不正确，无法修改商户信息');
INSERT INTO `tb_dqdp_log` VALUES ('6abac8d3-3813-4f88-ae61-e77b1c9e7cba', '2015-07-26 16:41:02', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('6ac0fe4b-041e-40e3-beac-d241497c5d46', '2015-08-24 22:09:18', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('6b63ab0e-127b-4ec7-b0da-c2e589db6bad', '2015-07-26 17:20:40', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('6bd65f25-43ba-4120-ba1e-958bf51c1c09', '2015-07-26 16:29:02', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('6c0c4918-15ad-43e1-ba36-0a720e752227', '2015-07-25 13:07:16', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('6d10bebe-843e-4282-9834-c5cb9151c96a', '2015-07-26 16:45:17', '222222', null, 'merchant_merchant', 'add', '1001', '账户余额:10.0本次充值需要金额:30.0，余额不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('6db13104-c7b0-4d7c-8f96-3104a2b1b3e4', '2015-08-06 11:19:21', 'lixiong', null, 'merchant_merchant', 'update', '0', '修改商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('6e0b6c91-11e7-422e-ac08-80d34ae21306', '2015-07-26 13:49:57', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('6f4d2617-6177-43cd-a0e2-d85d494bea4d', '2015-07-26 16:46:36', '222222', null, 'merchant_merchant', 'add', '1001', '您账户余额:10.0 本次充值需要金额:25.0，余额不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('701d1a0d-bc1f-4f70-9076-83397834b2b5', '2015-07-26 16:25:55', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('719cbde2-e9ba-4949-9379-2b9fc9a5e014', '2015-07-26 16:21:55', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('729b031b-7acd-4a14-8b1c-e1b0b9f5a617', '2015-07-26 17:17:03', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('72d725fa-736d-4f31-9c7f-7eda5569d1de', '2015-07-25 13:00:22', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('7334295c-a089-4f98-9615-35d8be936789', '2015-08-05 17:41:09', 'admin', null, 'merchant_merchant', 'update', '1001', '修改商户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('73f0f9e8-7254-4812-b081-ef75f0c20ca6', '2015-07-25 22:39:35', 'admin', null, 'systemmgr_config', 'add', '0', '新增配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('74716383-ccb3-4ddf-8ed5-4be3ea42cdcd', '2015-08-19 15:47:58', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('74c3fc1a-d30e-4d98-88a5-751885dc1db9', '2015-07-26 17:13:07', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('74c6720d-9b5f-4022-ac7c-28c43abdf7a2', '2015-07-26 17:01:42', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('76116988-cc39-4246-bca4-716931365ee7', '2015-07-25 14:17:04', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('76734f9d-b176-4eb4-a04c-f961e481ec02', '2015-08-05 17:42:01', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('77068892-fbe6-49c6-bdae-99a5d72feeb9', '2015-08-19 10:59:24', '111111', null, 'merchant_merchant', 'update', '1001', '修改用户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('7757c499-de43-44bb-890f-2c473eb86da7', '2015-08-19 13:06:22', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('7777b840-497d-4892-ab61-f6cf3f8abd0b', '2015-08-05 17:26:53', 'anonymous', null, 'merchant_merchant', 'add', '1001', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('7834ffc0-210b-4c76-a3c6-f66cb38efac3', '2015-08-05 17:51:45', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('787ceda3-abb9-4225-b5f6-2967fa99d243', '2015-07-26 14:57:05', 'lixiong', null, 'systemmgr_config', 'update', '0', '更新配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('78e18f2f-cb2d-4990-a0fa-0bd2fdb32b75', '2015-08-06 16:45:30', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('795443a5-4597-41a6-8862-31ba057365f1', '2015-08-19 16:24:24', '333333', null, 'merchant_merchant', 'add', '1001', '您账户余额:0.0 本次批量充值需要金额:10.0，余额不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('799e01be-e96f-4f39-974e-9ca4fe29ee72', '2015-07-27 15:42:07', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('7a219435-7e4c-406d-8f78-87e410da5063', '2015-08-19 17:29:35', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('7ac6ce1a-8d9f-4bf2-9f4c-e23720ec66fd', '2015-07-26 13:07:56', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('7aff4fc1-14bc-4bff-851e-62e816758483', '2015-08-19 12:03:34', '333333', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('7bcb4bed-f7f1-46e6-83b5-9cf9619e73b8', '2015-07-26 17:13:06', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('7c69bfec-27b1-4dbc-919d-73180e121ebd', '2015-09-15 19:25:55', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('7cd367f8-8073-47b4-8310-0a19de6d018d', '2015-07-25 23:53:00', 'lixiong', null, 'merchant_merchant', 'add', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('7d2adc6c-ea99-4138-964b-a34663f09124', '2015-08-19 10:53:15', '111111', null, 'merchant_merchant', 'update', '1001', '修改用户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('7d8fd938-4c15-45ae-bb14-bb6c21a3c3d0', '2015-08-19 13:05:52', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('7e74edce-bd09-4789-8a6f-17cbeb4c2696', '2015-08-05 17:23:05', 'anonymous', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误');
INSERT INTO `tb_dqdp_log` VALUES ('7e7a0cff-2deb-4bc9-8358-5900018f1f6d', '2015-08-19 12:19:13', '333333', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('7ecbce5e-7d88-4f15-b419-e05bf3c26ec9', '2015-07-26 21:04:21', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('7f0cf3d7-2b27-49f2-938b-08673f4fea56', '2015-07-26 13:52:22', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('7f1cc466-0e13-449f-8819-9e5f04122ce0', '2015-07-26 16:45:25', '222222', null, 'merchant_merchant', 'add', '1001', '账户余额:10.0本次充值需要金额:30.0，余额不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('7f4a50de-1d8a-4a4e-bb2b-50aad62a85e0', '2015-07-25 23:41:52', 'lixiong', null, 'merchant_merchant', 'add', '0', '新增商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('7f9c9639-7555-478e-ae30-9348a096a711', '2015-08-19 11:10:52', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('7fa608ce-dc31-43ee-9ea0-3a25f898a0ae', '2015-07-26 14:55:51', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('7faabab7-bec9-4536-80bd-f864e0c38990', '2015-07-26 18:50:06', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('7fc364ab-854a-4981-80a4-b10492f8a87c', '2015-07-27 14:27:07', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('7fe846a3-f66f-43f5-b4c5-18e9f2913bda', '2015-08-19 10:34:06', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('8055fcdd-607c-4ea3-a2de-1d778dec5221', '2015-08-19 13:02:00', '333333', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('806621a9-c110-44ed-aa61-e7a5958c7633', '2015-08-19 15:25:07', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('80cac7c3-0ab4-4890-b0e7-fd62534bc526', '2015-07-26 14:51:34', '111111', null, 'merchant_merchant', 'update', '1001', '修改用户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('80ecfd5f-6c9a-419e-b33e-fe1a1d2b23b3', '2015-07-25 21:32:15', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('8131d385-6f9a-4dc3-9195-0a5888fe5364', '2015-07-26 20:01:41', 'lixiong', null, 'merchandise_merchandise', 'update', '0', '更新成功');
INSERT INTO `tb_dqdp_log` VALUES ('81c02542-2d6e-4405-9ac6-5980abb96cb8', '2015-08-19 12:11:03', '333333', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('83491db8-a0c2-4464-946c-0a9e339dd350', '2015-07-26 20:45:15', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('840a132a-edbd-44ed-a223-eef2f28ff469', '2015-07-26 13:20:15', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('84a48be0-ea4a-45e7-92bf-18328dc400ad', '2015-07-26 14:08:06', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('84f4e1a8-4ec0-4606-a45c-283dfad446ce', '2015-07-26 13:27:20', 'admin', null, 'systemmgr_config', 'update', '0', '更新配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('84f6beea-2e4e-45ed-a556-53a9abeb1481', '2015-07-26 17:32:18', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('852e84d8-1aeb-4d9f-ad8d-807e4ad79d4d', '2015-07-25 13:09:22', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('863de018-ea7d-4c30-bfe3-3399c7c3c145', '2015-07-25 12:49:42', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('868c334c-9f62-4936-b9ba-8a93c98a8843', '2015-07-25 23:24:20', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('86e2d3d1-01cf-4c4e-9fa5-7855ae23eac3', '2015-07-26 13:55:04', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('872af1c7-788b-4faf-9c13-4dabd1153d3e', '2015-07-25 12:49:54', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('87458134-52f3-4214-9f4d-aecdcbfaaaec', '2015-07-26 20:29:49', 'admin', null, 'dqdpdictionary_dictmgr', 'add', '0', '字典新增成功');
INSERT INTO `tb_dqdp_log` VALUES ('8820686a-dd18-4df5-804f-ab03da66e36e', '2015-07-26 14:55:39', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('889b541c-2c85-49f9-be53-b5d6d624fb4f', '2015-08-05 17:38:04', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('895664f5-2629-420e-bc2c-09a7267a77f4', '2015-08-05 17:41:21', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('89a2c96f-eedb-45ab-9add-f9b18939d571', '2015-07-26 22:09:21', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('8a05e1a6-f74e-4a55-830e-5349e99dfb49', '2015-08-19 17:18:04', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('8b253f48-5b35-466a-9672-d7319e6221ab', '2015-07-26 14:08:27', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('8bd62e66-0135-4f9d-ad3d-87942033d42e', '2015-08-19 13:59:01', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('8c0d4e53-86f9-4e5d-99d4-179671350e1d', '2015-07-26 20:01:34', 'lixiong', null, 'merchandise_merchandise', 'update', '0', '更新成功');
INSERT INTO `tb_dqdp_log` VALUES ('8c565835-7ad1-44bc-a5fd-67b41d0ea274', '2015-07-26 17:38:16', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('8e9b8cea-c583-495b-9ebd-d2fc5e44f933', '2015-08-19 12:05:06', '333333', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('8f1e048f-ea0d-4565-855e-cff415df8339', '2015-07-27 14:20:11', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('90321811-9940-4324-be1d-c02b292a0f3e', '2015-07-26 17:17:11', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('903310eb-0863-4f26-ac5f-21a7fc5bfa43', '2015-07-26 22:37:47', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('9057357e-df25-410e-b241-f4845ed000f8', '2015-07-26 17:13:07', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('9086954b-0fbb-4ba2-8488-da1a84527cd6', '2015-08-05 17:48:51', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('90cb2dba-7e3f-43b1-89a1-24b7037bd471', '2015-07-26 17:29:13', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('90db60f1-5df7-4416-8619-0fdbb565c5cf', '2015-07-26 16:27:11', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('90e41f09-b242-4f4e-9c23-167ed287c7c8', '2015-07-26 20:42:00', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('9107c495-e247-4e0b-86f4-358f88bbf67c', '2015-08-19 11:05:24', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('91e403f2-9895-4b37-a4e8-37ee036fcc38', '2015-07-26 16:37:12', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('92ce4afd-4b8e-47b6-bacf-68a7b4026545', '2015-07-26 16:26:36', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('9303e8ca-ef3f-4b1b-abf3-cb23c50b1aed', '2015-07-25 12:52:52', 'lixiong', null, 'merchandise_merchandise', 'add', '0', '新增套餐成功');
INSERT INTO `tb_dqdp_log` VALUES ('9399b2f2-afb5-4c05-8e75-41c8db2bc8f3', '2015-08-05 17:42:45', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('9439db7a-4f11-4794-9432-ad62fc2a05c3', '2015-09-01 16:39:56', 'anonymous', null, 'merchant_merchant', 'add', '1001', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('954d7659-7fd5-4b15-94e0-5ec4c6756be3', '2015-07-25 23:54:51', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('9558fe32-a829-4f30-a5c1-6e494ea7be08', '2015-07-26 17:37:16', '111111', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误，请输入正确的格式');
INSERT INTO `tb_dqdp_log` VALUES ('971ec7bd-b76b-4d10-9d7d-341cf891d83b', '2015-07-27 00:08:40', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('98b262b6-b210-4d9f-99f3-bf473b714562', '2015-07-26 17:25:51', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('990b2fe8-ea76-43e8-8729-69ba8f397f72', '2015-07-25 12:56:34', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('9a0870f0-7d93-4c8d-8791-15a1bea0e5ab', '2015-07-25 13:09:30', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('9a5b459b-b9cb-498b-be82-fc9042389526', '2015-07-26 20:09:21', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('9a8e32c1-530e-4d8a-ab24-5cb647fed2c7', '2015-08-05 17:42:13', 'lixiong', null, 'merchant_merchant', 'update', '1001', '修改商户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('9ab9d502-e8e7-4ad6-baff-deb9d7cdd9be', '2015-07-26 17:22:05', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('9ac1c5ab-f9d0-406c-9a29-7b8473af1315', '2015-07-26 17:09:13', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('9aee1e2d-6fc1-44e8-9635-b8baee9467d4', '2015-07-25 13:03:45', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('9b032a09-9129-4a20-a63a-3829a48479f6', '2015-07-26 14:04:07', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('9b139f86-5413-4d68-8f88-b36394f3e22f', '2015-07-26 16:10:17', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('9bb0f2f9-c9a9-4e36-b646-66acd4f9e677', '2015-07-26 18:41:43', 'lixiong', null, 'merchant_merchantpay', 'add', '1002', '充值密码凭证不正确，充值失败');
INSERT INTO `tb_dqdp_log` VALUES ('9c88a904-c4f4-457f-8cce-d9efb49672b9', '2015-07-26 22:38:08', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('9d686ccd-88ad-4eb6-a05e-42bdaaa5f5b7', '2015-08-19 12:07:13', '333333', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('9da8a849-e6ea-4fb5-9a8c-3b49e12eff59', '2015-07-25 13:13:20', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('9e2ca2e1-095a-45dd-9580-72448cedc5d0', '2015-07-26 17:41:11', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('9ecf7a3a-a02f-4ddb-bc8f-a774fae6a8cb', '2015-07-25 13:21:19', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('9f3a7b31-422c-4b30-8c41-7d9abd6dd3a5', '2015-07-26 21:05:17', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('a01718fe-fa43-4c2c-9853-f11756f7a06f', '2015-07-26 21:51:32', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('a02b411e-10f1-428a-a2b5-75abd7e881ac', '2015-07-25 13:54:41', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('a0806cfa-ca24-4f09-af4f-4dc1d8345c70', '2015-08-06 11:01:24', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('a09f0390-6511-49c4-bb9e-70d1686f1495', '2015-07-25 23:53:07', 'lixiong', null, 'merchant_merchant', 'add', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('a0e4e0b4-01ae-47cb-aef5-ecdd0f89ab52', '2015-07-25 21:27:49', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('a1bad4d2-b595-4e98-b360-7fadbc11d76d', '2015-07-25 12:50:23', 'lixiong', null, 'merchant_merchant', 'update', '0', '修改信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('a1e2bfff-dffc-46c4-9161-623182660f1e', '2015-07-25 13:07:06', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('a1e84d7e-5f99-453b-b492-47abba5c2af6', '2015-08-19 10:12:50', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('a20f5d3c-e74a-4769-b177-f7cbf4a1e68a', '2015-09-01 16:40:01', 'anonymous', null, 'merchant_merchant', 'add', '0', '新增人员信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('a23accad-897c-4b10-9731-cf545cc5bcd7', '2015-07-27 15:33:31', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('a2576380-e2ff-47d2-869e-5f20d6d4873e', '2015-08-06 11:25:11', 'lixiong', null, 'merchandise_merchandise', 'update', '0', '更新成功');
INSERT INTO `tb_dqdp_log` VALUES ('a2a4f97c-e512-4ee7-8255-33df1d51a69b', '2015-07-26 16:32:22', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('a3250437-da7b-45f9-bd60-0a69c8a5855d', '2015-07-26 16:28:58', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('a51811ac-99b6-496a-9331-4c9044988d59', '2015-08-19 13:17:31', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('a6637c82-4df2-4ac3-bee0-171af6a5790b', '2015-07-25 12:55:11', 'anonymous', null, 'merchant_merchant', 'add', '0', '新增人员信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('a669977d-ece2-4b03-a6b5-52fba897ceef', '2015-08-19 12:02:50', '333333', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('a7120011-150f-4e87-9bd3-03d215c6b3ae', '2015-07-25 13:00:48', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('a7800561-490c-431f-b99f-d6f6c901b68c', '2015-07-27 14:24:35', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('a946d0be-fed1-4c3c-a380-b1121ca884ee', '2015-07-27 14:24:22', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('a9a20245-04ff-4834-9af8-ce042c1d7530', '2015-07-25 23:41:27', 'lixiong', null, 'merchant_merchant', 'update', '0', '修改商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('a9d94506-79df-4a00-b4dd-2aa3ed69b606', '2015-07-25 23:52:59', 'lixiong', null, 'merchant_merchant', 'add', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('aa671a51-c6f8-4f57-8a46-ac0323ac9750', '2015-07-26 16:28:09', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('abdbacb4-ee1a-4f4e-8351-4ce5d722d2fa', '2015-08-07 18:54:03', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('ac195f2e-82c6-4a2f-a25f-39c22202622f', '2015-07-26 13:53:01', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('acb967b2-0f54-4885-b47a-d17911080b46', '2015-07-26 00:05:59', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('ace56910-2484-4dc6-bbcf-8478b72d1b71', '2015-07-28 12:11:56', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('ad107efa-7d80-44ec-9bc0-6d9eb20274bf', '2015-09-01 16:39:55', 'anonymous', null, 'merchant_merchant', 'add', '1001', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('ad51426f-5988-4ab3-939d-766a3cc49105', '2015-07-26 17:21:04', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('af0d558f-7d51-4ddc-83fd-680562b8d1ac', '2015-07-25 13:08:15', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('b01dd3e0-675e-44e4-bdd0-310176488e97', '2015-08-19 12:19:08', '333333', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('b0e23da3-2f82-480b-b280-ce78b6fc6d58', '2015-07-26 16:32:28', '111111', null, 'merchant_merchant', 'add', '1001', '充值格式不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('b1233879-040c-4e4c-a655-4fadcdf2a782', '2015-07-26 21:05:15', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('b141ca8f-144a-4c91-87dd-736b5ea9bb08', '2015-08-19 15:24:25', 'w12345', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('b266235e-a4ad-46b3-bd2f-4437da7fbf0f', '2015-07-26 17:13:06', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('b2d8c5d1-07fe-453b-8aaf-2cbdfd3636eb', '2015-07-26 17:41:02', 'lixiong', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('b3815d02-a036-4c2c-8401-f4ac42770215', '2015-08-19 10:08:25', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('b3ac21fa-b46a-4fa7-8aa9-a085db0ddcfe', '2015-07-26 17:34:52', '111111', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误');
INSERT INTO `tb_dqdp_log` VALUES ('b550deb4-6067-4c48-8c66-5830f7892b4a', '2015-07-26 13:53:10', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('b5a3684b-204b-4acb-acb2-47014052612f', '2015-07-25 22:40:44', 'admin', null, 'systemmgr_config', 'update', '0', '更新配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('b6493277-1f1d-4d4b-aabe-417c2dc3e85a', '2015-07-26 13:06:27', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('b6b3a6b9-da5b-4116-abdc-e808eb8404f6', '2015-08-19 11:10:04', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('b6d56a9f-9738-4643-bab8-cdef72c623ef', '2015-08-05 17:41:53', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('b6da7295-0e75-466b-8e92-8227d523675b', '2015-08-05 17:24:12', 'anonymous', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误');
INSERT INTO `tb_dqdp_log` VALUES ('b733aa02-a775-45c3-bbc2-8ba305a72eee', '2015-08-05 17:24:20', 'anonymous', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误');
INSERT INTO `tb_dqdp_log` VALUES ('b7382cdc-6b74-4241-86e8-f7622b45301b', '2015-07-25 21:35:12', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('b7c16a52-644d-4489-93e2-179fff75988e', '2015-07-26 17:28:17', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('b83d7114-e29e-4ef4-9084-0b1d7c094ac0', '2015-07-25 13:10:52', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('b85c8256-5b2a-451d-98ad-c1e168c7ac6f', '2015-07-26 17:18:23', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('b8ccd67b-3dbf-41c4-bba7-06d683af5319', '2015-08-19 12:19:02', '333333', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('b8cfedec-6768-44ce-b9bd-73628c50c27d', '2015-08-19 13:04:45', '111111', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('b9210daa-f7f3-4758-a2b3-a8227237ee14', '2015-07-25 13:10:48', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('b95cc5f7-ac84-4ba0-bfdd-f9de14b416ce', '2015-07-26 18:29:32', 'lixiong', null, 'merchant_merchant', 'update', '1001', '支付密码不正确，无法修改商户信息');
INSERT INTO `tb_dqdp_log` VALUES ('b994faca-f87e-475f-ae69-2027d6bac069', '2015-07-26 22:41:41', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('b9a79d65-e2e0-462f-a12f-3ab0952df898', '2015-07-26 13:06:18', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('ba07e707-580e-435d-a9d0-62a87411d576', '2015-07-26 13:57:39', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('ba531157-5ebd-46a1-9382-56e54a12e00c', '2015-07-26 23:31:12', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('ba54617a-9a19-418c-8a3e-013def83810e', '2015-09-01 16:39:01', 'anonymous', null, 'merchant_merchant', 'add', '1001', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('bb4309ff-a6f4-418c-867a-3f4d5f7b87ec', '2015-08-19 17:07:37', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('bbb5f558-69b3-4046-a1c5-64e4012fb4c0', '2015-07-25 12:55:25', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('bc447372-2988-4afe-967f-3e3170c830ff', '2015-08-05 17:22:25', 'anonymous', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误');
INSERT INTO `tb_dqdp_log` VALUES ('bc65d663-8957-4990-b0ad-95e607bd585c', '2015-07-25 14:16:39', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('bc6a8a1f-c189-4360-88e1-260729aae01b', '2015-08-19 11:05:51', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('bd2bcb92-40e9-42ea-993a-34fff55f8cae', '2015-07-26 21:40:20', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('bd3ed5e7-7e54-4b6a-8c09-e1acdca13a2c', '2015-08-05 17:38:24', 'admin', null, 'merchant_merchant', 'add', '0', '新增商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('bda05396-279c-41c3-8334-6d920ae32058', '2015-07-26 20:00:31', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('bdb2f242-a0d4-47ab-aaf2-005145e2f220', '2015-08-19 13:34:14', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('be5a5126-72b3-4b25-bfcc-244c46cba742', '2015-07-26 20:45:09', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('bf164f3a-94c0-465d-a8ba-72f87a52dce7', '2015-07-26 22:24:12', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c0ecc83b-35f8-4130-94cf-ab32973a5418', '2015-07-26 13:30:17', 'admin', null, 'systemmgr_config', 'update', '0', '更新配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('c1a9a2ce-b415-4d8f-9054-fed5c3da303e', '2015-07-25 14:15:29', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c2d4262d-1bc9-446b-9579-52cf598b63ef', '2015-07-25 13:13:00', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('c34dde6e-85bc-4fe8-b45d-6361655af438', '2015-07-26 20:01:37', 'lixiong', null, 'merchandise_merchandise', 'update', '0', '更新成功');
INSERT INTO `tb_dqdp_log` VALUES ('c35436d4-326d-4505-aaae-261b5e01eccd', '2015-07-26 14:47:56', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c356a7ec-df35-4b6f-b148-1a12313126df', '2015-07-26 00:15:58', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c3d65803-44a2-4909-b560-3f920741338c', '2015-08-19 12:08:50', '333333', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c461580b-f434-4386-b496-17d8b51c5c40', '2015-08-06 10:59:43', 'anonymous', null, 'merchant_merchant', 'add', '0', '新增人员信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('c49631c5-4907-494b-a5ed-f8afb9c67206', '2015-07-26 14:52:48', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('c4d9663e-3faa-46bc-8877-caf12fac305e', '2015-08-24 20:58:18', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c4f63232-05d8-449f-81e3-55277daee3ec', '2015-07-25 13:06:07', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c50c15f9-8bd8-4f71-a558-ece2ff43a74a', '2015-08-13 15:12:00', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c5974adb-e969-4a32-9958-803eb6cd0898', '2015-07-26 00:15:09', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c5bd6aa1-d8bd-49b9-9536-b973ef4ce9ca', '2015-09-15 19:20:17', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c5d62ccd-07e9-44e0-b088-3af42098284e', '2015-07-26 20:29:03', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c6253831-a70e-4a4b-8d6b-a2851f1ecb17', '2015-07-26 18:40:10', 'lixiong', null, 'merchant_merchant', 'update', '1001', '支付密码不正确，无法修改商户信息');
INSERT INTO `tb_dqdp_log` VALUES ('c64de9d0-0c08-47a5-aad4-6eb4796fead1', '2015-08-19 13:29:18', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('c6dd1641-8ebf-410c-8aff-15705083ddcd', '2015-07-25 13:00:42', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('c7135740-9599-427c-ab51-8ce9d52740b4', '2015-07-25 21:43:01', 'admin', null, 'systemmgr_config', 'add', '0', '新增配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('c79e6b35-ece2-48ed-a6ff-4dad8e2163be', '2015-08-05 17:22:02', 'anonymous', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误');
INSERT INTO `tb_dqdp_log` VALUES ('c7cc5953-40e0-4446-b379-e7175c39a345', '2015-07-26 16:29:32', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('c818b4ee-b794-49ea-8d91-fa2917041c80', '2015-07-25 23:54:37', 'lixiong', null, 'merchant_merchant', 'update', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('c8d32171-6fb4-4c72-8437-5797d01401be', '2015-07-26 02:01:01', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c90828b6-e96a-4ffa-8574-62cdd8832234', '2015-08-19 13:33:15', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('c921622a-6c3c-46f8-8869-bac464c026b8', '2015-07-25 23:21:41', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c95c8875-b76e-4c45-98d3-77585368322a', '2015-07-26 14:58:16', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('c9dd5b74-649c-4d4f-b023-538900d3fff4', '2015-07-26 23:37:45', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('ca66c67f-99e9-45f4-a7ce-5a87cdab49c6', '2015-08-19 13:03:10', '333333', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('cb89cc61-b37d-4612-9be0-82a218a1c90c', '2015-08-19 15:24:37', 'w12345', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('cbcc6517-edf5-4939-9837-3eb25ab6465e', '2015-08-06 11:17:19', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('cbf88502-0152-4829-9aaa-930923833a6e', '2015-07-26 17:11:39', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('cc816650-06d0-4682-a10c-8887b5d96c3a', '2015-08-05 17:26:54', 'anonymous', null, 'merchant_merchant', 'add', '0', '新增人员信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('cce2f96b-d4fe-40f3-ae9d-f07532f9687d', '2015-07-26 17:21:55', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('ccf8298a-43f7-4382-a7ee-cb473751a2be', '2015-07-26 00:05:52', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('cd94727d-fd71-423e-8121-d1a62ec9fb77', '2015-08-05 17:38:31', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('ce1d765f-02ea-431f-a180-dceccfe670b1', '2015-07-26 17:37:19', '111111', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误，请输入正确的格式');
INSERT INTO `tb_dqdp_log` VALUES ('ceac30b5-28e7-400c-b8dd-4de067866255', '2015-07-26 20:22:07', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('cec3cbba-6e21-4fd5-9571-38269289fd9a', '2015-07-26 14:04:00', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('cf5e680e-2adf-4466-a4ea-28b64993b50c', '2015-08-19 13:22:17', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('d10ff866-2753-49b3-92c5-d1712eb2036b', '2015-07-25 12:53:49', 'lixiong', null, 'merchandise_merchandise', 'add', '0', '新增套餐成功');
INSERT INTO `tb_dqdp_log` VALUES ('d169b400-fc14-44e8-b073-ac4e13b7adcc', '2015-08-05 17:42:38', 'lixiong', null, 'merchant_merchant', 'update', '0', '修改商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('d182a335-d9d8-4197-8b9e-c0fa1f7e5508', '2015-07-26 16:14:07', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('d1ab5889-c52f-44f7-a66d-6aef29bbedb9', '2015-07-26 21:44:36', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('d1e6be6c-d56a-4bb8-aaeb-8fd32d85e33c', '2015-07-25 23:03:58', 'admin', null, 'systemmgr_config', 'add', '0', '新增配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('d211d7f4-0096-4d00-a975-006693f57401', '2015-08-05 17:23:31', 'anonymous', null, 'merchant_merchant', 'add', '1913', '代理商充值数据错误');
INSERT INTO `tb_dqdp_log` VALUES ('d40effda-96f6-465a-b35c-70804007a1c8', '2015-08-19 12:06:49', '333333', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('d41f9b02-8306-4ec9-8148-4dc0298103fa', '2015-08-19 13:04:18', '333333', null, 'merchant_merchant', 'add', '1001', '您账户余额:0.0 本次充值需要金额:10.0，余额不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('d447ee6a-9334-45b2-bd1f-8b2982bb4ea6', '2015-07-25 21:23:49', 'w12345', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('d4c5f2fd-bd1e-4b5e-a672-c8e94aa534e3', '2015-07-26 16:32:35', '111111', null, 'merchant_merchant', 'add', '1001', '充值格式不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('d503b402-94ff-469a-9658-749af5e98d39', '2015-08-06 10:59:58', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('d53198a7-2213-4f16-985b-67b1596709c8', '2015-08-06 11:19:00', 'lixiong', null, 'merchant_merchant', 'update', '0', '修改商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('d64bc2c0-4dee-4228-9922-ce8091a5102f', '2015-07-26 17:04:43', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('d6c5b64b-1acc-4799-837c-7847c07766df', '2015-07-26 21:04:50', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('d6e46180-deb1-4556-a21f-fb0c6237af94', '2015-07-26 21:04:27', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('d71b80e5-e15e-4022-8b80-eb373009e3f4', '2015-07-25 13:08:49', 'lixiong', null, 'merchant_merchantpay', 'add', '1002', '充值密码凭证不正确，充值失败');
INSERT INTO `tb_dqdp_log` VALUES ('d7a3e00d-b705-4635-99f3-fc97f5b899af', '2015-07-26 16:20:16', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('d840105c-8ba5-4488-8260-1c1aaed43445', '2015-08-19 13:28:38', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('d8ab798c-556e-47e2-8aa1-2c7a103ef75f', '2015-07-26 18:41:42', 'lixiong', null, 'merchant_merchantpay', 'add', '1002', '充值密码凭证不正确，充值失败');
INSERT INTO `tb_dqdp_log` VALUES ('d90d8253-ee06-4b6c-b2aa-4802fe22782e', '2015-07-27 15:42:23', 'admin', null, 'merchant_merchant', 'del', '0', '删除用户成功');
INSERT INTO `tb_dqdp_log` VALUES ('d92bf508-db7a-47a6-9f86-eed30487e5ec', '2015-07-25 12:47:14', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('d946be86-5ced-4b89-8e68-fdbce3b16d4d', '2015-08-19 16:42:08', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('d9508e39-7223-4efe-af4b-7f23ffbec5ba', '2015-08-19 12:04:41', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('d95dac87-37e7-464e-9789-40a8506e65a4', '2015-07-25 23:52:02', 'lixiong', null, 'merchant_merchant', 'add', '1913', '折扣率不能低于50%');
INSERT INTO `tb_dqdp_log` VALUES ('d9b7ee03-3e69-4e2b-b597-10ec3d6dfdcf', '2015-07-26 17:36:00', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('d9c37b88-0a02-41fb-98e2-d81dd9d8defd', '2015-08-19 13:34:25', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('da14b964-c86b-4459-a7d2-fcc8ad047e1e', '2015-09-14 21:05:28', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('da68ae80-c5b6-4674-8d5f-d3adaa83d683', '2015-07-26 16:05:16', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('dad32047-1452-427d-bd3b-2a8539fae29c', '2015-07-25 21:24:10', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('db29d17f-c76e-4a4a-829a-39921d983d65', '2015-07-25 12:56:41', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('db421061-1dfa-4c10-a08a-5cd260da369f', '2015-08-19 16:53:07', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('db4a0f46-78ec-4089-af4b-550d82aedce9', '2015-07-25 14:16:33', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('db8d2f54-556f-4866-8c60-76f1dc5b6526', '2015-07-26 13:05:47', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('dcb36185-e5a7-425d-a769-c118479f46fc', '2015-08-19 13:04:53', '111111', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('dd5c5884-ab4c-4ff8-b3ef-a044a0808769', '2015-07-26 16:42:10', '222222', null, 'merchant_merchant', 'add', '1001', '对不起，你账户余额已不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('dde1d085-d8fc-427d-beb0-86e917d9e597', '2015-07-26 17:10:21', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('dee5029e-2580-49e7-924a-3b2349089492', '2015-07-26 23:31:17', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('defd6e9d-f9f6-4961-9465-3dea0c6cd3c2', '2015-07-26 16:37:10', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('df7c3599-5518-4d8d-9fd4-a21a41d20759', '2015-07-26 13:51:54', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('e04df35a-1570-479b-b011-ba8ee007c3b2', '2015-07-25 22:48:49', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('e05151c4-f7f4-4b6a-af29-37677401eb88', '2015-08-06 11:21:43', 'admin', null, 'systemmgr_permission', 'del', '0', '删除权限成功');
INSERT INTO `tb_dqdp_log` VALUES ('e1b0a311-3480-449f-814c-e8fec9432333', '2015-07-25 23:37:51', 'lixiong', null, 'merchant_merchant', 'update', '1001', '修改商户信息失败');
INSERT INTO `tb_dqdp_log` VALUES ('e265d198-8bdc-4506-9d73-5b8ac93eaccc', '2015-07-26 20:12:35', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('e294b82e-ca20-467a-805c-3abf1738ad29', '2015-08-19 17:05:31', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('e381a8ed-bfc7-40f6-a1c2-a0e344de2db3', '2015-07-26 17:26:15', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('e4994f6a-8b37-4e7d-b284-9f532a78b760', '2015-08-05 17:29:19', 'anonymous', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('e61a3f3d-1322-4e21-a58f-5e58ab94eb62', '2015-08-19 10:11:37', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('e61ab3da-485b-468b-9aba-ba6c12986e64', '2015-08-19 11:09:52', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('e7657786-d079-4460-afbc-aaaa2d0d771f', '2015-07-25 21:42:49', 'admin', null, 'systemmgr_config', 'add', '0', '新增配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('e7e24c8c-f525-4fe2-81e3-7ef2b4fb5f75', '2015-07-25 13:08:25', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('e83d3e60-0c99-4a1d-a2b3-89ad68a5a3cd', '2015-08-19 17:05:44', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不正确');
INSERT INTO `tb_dqdp_log` VALUES ('e9b3de51-7df1-4690-89d5-56047cdf92c7', '2015-08-19 10:34:26', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('e9d29648-dfe7-4e38-949a-cd30e0f253a4', '2015-07-26 02:41:17', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('e9d90b82-ccea-4bca-96e3-b76a42db0391', '2015-07-26 16:41:34', 'lixiong', null, 'merchant_merchant', 'update', '0', '修改商户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('ea17d2d2-8c9f-4c26-b764-92797f237c1b', '2015-08-19 13:33:38', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('eb117c1f-2825-46c9-ba04-40a6a574e9f5', '2015-08-19 13:05:03', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('eb23f80d-9d1e-40aa-bee4-d2f252151e19', '2015-08-19 11:16:17', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('eb52faf5-8df4-4cd8-b1a2-5a5116d9a9ca', '2015-08-19 16:23:25', '333333', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('eba05758-2de0-4225-82f8-4fd0155eaeca', '2015-08-19 13:23:59', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('ebcf3bee-8433-4ab8-9941-fca1cc5ad5d4', '2015-07-26 17:25:00', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('ec3e8246-ffe3-4623-b516-b5d573fa4c08', '2015-07-26 02:38:16', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('ec6005bd-872c-450b-b1e4-3b9b31869e35', '2015-07-26 13:22:23', 'admin', null, 'systemmgr_config', 'update', '0', '更新配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('ecf8c061-0884-4bb6-8c9d-5ddaba87ff27', '2015-07-27 14:24:29', '111111', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('ed17e01f-c382-4fca-a3a7-47b4a14c4bf5', '2015-08-05 20:01:19', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('edb644dc-1563-49b2-8b53-a2ef57042382', '2015-07-25 14:17:08', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('ee6fcbbd-1736-41b5-ba49-39e08453a924', '2015-09-01 16:40:01', 'anonymous', null, 'merchant_merchant', 'add', '1001', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('eec84617-3ccc-4d4b-b6c3-51151b0d9718', '2015-07-26 17:32:39', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('f03746b3-7b7a-450d-9a32-186413d8f828', '2015-08-19 17:10:35', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('f056f91e-b2c2-4d06-bad5-2e72a0f0ee74', '2015-09-14 21:03:17', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('f09629c1-36b6-44b3-8fee-81bdcaaaac0d', '2015-07-26 17:12:55', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('f0a12581-9fbb-4c16-9d27-aaf14ac8e96a', '2015-07-25 12:50:43', 'lixiong', null, 'merchandise_merchandise', 'add', '0', '新增成功');
INSERT INTO `tb_dqdp_log` VALUES ('f0b47494-a3d9-4004-b256-f6c9c7b02a81', '2015-07-26 02:22:13', 'admin', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('f0f0dca8-d1bd-4c47-bd05-adf4bdb5658f', '2015-08-19 11:12:43', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('f186c653-f542-4402-99f9-1090ab3c84ac', '2015-08-19 13:33:11', '111111', null, 'merchant_merchant', 'add', '1001', '支付密码不能为空');
INSERT INTO `tb_dqdp_log` VALUES ('f1b9d253-1f64-4013-a907-315b7bd4b702', '2015-07-26 20:59:32', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('f27d6832-5088-485b-8c02-0431673c87a2', '2015-08-05 17:39:06', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('f3ea6dcb-b7e3-47ba-89ad-0f60fd132f26', '2015-08-24 23:35:33', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('f4db2a68-834d-4a74-baef-5f00d5a0c224', '2015-07-26 20:01:15', 'admin', null, 'systemmgr_role', 'update', '0', '更新角色成功');
INSERT INTO `tb_dqdp_log` VALUES ('f4f798e2-96f8-4472-952d-f118e5fcbf5d', '2015-07-26 02:22:24', 'lixiong', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('f5211fcb-7c8a-43ce-9c6f-3c7095e23f15', '2015-07-26 17:28:25', '111111', null, 'merchant_merchant', 'add', '0', '初始化充值数据成功');
INSERT INTO `tb_dqdp_log` VALUES ('f54d2573-ebbd-41f5-b8b6-a604378da759', '2015-08-05 18:45:24', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('f5b5863f-5cf5-476f-a36d-884ad810e95a', '2015-07-25 12:56:57', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('f6c2af5f-75b9-4498-b4e6-7179c6a182b5', '2015-07-25 13:03:49', '111111', null, 'merchant_merchant', 'update', '0', '修改用户信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('f6ea68a7-5272-43ef-a8a0-039c24ab791a', '2015-08-06 11:24:23', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('f70607a4-caf8-46ef-acce-559bdfd4be11', '2015-07-26 18:43:22', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('f9bb628d-3c54-4f67-ae35-e1e48fbcff0e', '2015-07-25 13:24:28', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('fa22d448-30d5-4037-81af-bee19bb063c9', '2015-08-05 17:39:32', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('fa2eefd4-433c-4d5d-9569-8075a883ec56', '2015-08-06 11:18:35', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('fa68886a-059d-4ad6-8f2b-609a1b30a26b', '2015-08-19 12:08:33', '333333', null, 'login', 'login', '800000002', '验证码错误');
INSERT INTO `tb_dqdp_log` VALUES ('fa980b68-0d97-457d-bbac-1a678e9a6a86', '2015-07-25 23:17:21', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('facf0377-c1de-46f3-bef0-5126a4ba0dd9', '2015-08-05 17:35:13', 'anonymous', null, 'merchant_merchant', 'add', '1001', '您账户余额:0.0 本次批量充值需要金额:20.0，余额不足，请充值再操作');
INSERT INTO `tb_dqdp_log` VALUES ('fb6f2019-7565-40c3-b67a-1a3fc9f37a46', '2015-07-26 17:00:28', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('fccf09a9-b7d0-4e17-8018-2c0849a2a4e2', '2015-08-05 18:20:16', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('fcd0daf8-bf06-4268-83af-f64cce098cc7', '2015-07-25 13:07:20', 'admin', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('fd3820e9-3345-4ccd-a615-ce6cadceba94', '2015-08-05 19:56:25', '111111', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('fd5d617b-9785-44d7-9b29-bd6583af9736', '2015-07-26 17:39:26', '111111', null, 'merchant_merchant', 'add', '0', '充流量提交成功');
INSERT INTO `tb_dqdp_log` VALUES ('fd7784fa-180c-4629-849e-ad8575a27be4', '2015-07-26 13:37:38', 'admin', null, 'systemmgr_config', 'add', '0', '新增配置信息成功');
INSERT INTO `tb_dqdp_log` VALUES ('feef43c5-6383-4791-aa82-2d944d1eb407', '2015-07-25 21:23:59', 'w12345', null, 'login', 'login', '800000002', '用户名或密码错误');
INSERT INTO `tb_dqdp_log` VALUES ('ff55e07b-7d33-4f82-87df-6214e5cbeb0c', '2015-08-05 17:42:57', 'lixiong', null, 'login', 'login', '0', '登录成功');
INSERT INTO `tb_dqdp_log` VALUES ('ffea5905-9bf9-48b7-850a-52ec147b266c', '2015-08-05 17:46:48', 'anonymous', null, 'merchant_merchant', 'add', '0', '充流量提交成功');

-- ----------------------------
-- Table structure for tb_dqdp_organization
-- ----------------------------
DROP TABLE IF EXISTS `tb_dqdp_organization`;
CREATE TABLE `tb_dqdp_organization` (
  `ORGANIZATION_ID` varchar(36) NOT NULL,
  `ORGANIZATION_NAME` varchar(100) DEFAULT NULL,
  `LEFTVALUE` decimal(8,0) DEFAULT NULL,
  `RIGHTVALUE` decimal(8,0) DEFAULT NULL,
  `ORGANIZATION_DESCRIPTION` varchar(300) DEFAULT NULL,
  `PHONE` varchar(20) DEFAULT NULL,
  `ADDRESS` varchar(300) DEFAULT NULL,
  `LAYER` decimal(8,0) DEFAULT NULL,
  `PARENT_ID` varchar(36) DEFAULT NULL,
  `IS_PARENT` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ORGANIZATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dqdp_organization
-- ----------------------------
INSERT INTO `tb_dqdp_organization` VALUES ('1def6d3f-ddcb-4ff4-bcda-0538252ff4ad', '后台管理', '1', '2', '', '', '', '1', 'org00000000', '1');
INSERT INTO `tb_dqdp_organization` VALUES ('deaf8bfe-c403-4b83-84c0-ca1e8db80a05', '注册商家', '3', '4', '', '', '', '1', 'org00000000', '1');
INSERT INTO `tb_dqdp_organization` VALUES ('org00000000', '充值管理中心', '0', '5', '', '', '', '0', null, '0');

-- ----------------------------
-- Table structure for tb_dqdp_permission
-- ----------------------------
DROP TABLE IF EXISTS `tb_dqdp_permission`;
CREATE TABLE `tb_dqdp_permission` (
  `PERMISSION_ID` varchar(36) NOT NULL,
  `PERMISSION_CODE` varchar(100) DEFAULT NULL,
  `PERMISSION_NAME` varchar(100) DEFAULT NULL,
  `TARGET_TYPE` decimal(2,0) DEFAULT NULL COMMENT '1:url',
  `MEMO` varchar(1024) DEFAULT NULL,
  `MODEL_NAME` varchar(100) DEFAULT NULL,
  `COMPONENT` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PERMISSION_ID`),
  KEY `IX_DQDP_PERMISSION_CODE` (`PERMISSION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dqdp_permission
-- ----------------------------
INSERT INTO `tb_dqdp_permission` VALUES ('00acc8c7-07b0-4ec8-b9ae-fe52552d485e', 'payDelete', '手机充值删除', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('02713736-ffc3-4c8e-a57d-2d8d0feb70e3', 'userPayPassword', '用户--修改支付密码', null, '', '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('0294b07d-1358-449b-92f3-5ffbed8c73c0', 'personManage', '人员管理', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('04203ca1-0a84-4c31-914d-3a58a986b826', 'roleManage', '角色管理', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('08839a83-359a-4955-bf4d-20d6a3f0b4c1', 'personEdit', '人员--编辑人员', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('0d6fca0f-7dd0-4bea-b1ad-220b0968de9c', 'payRefund', '手机充值充值退款', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('17db3c51-016a-428b-b2ee-bde45e565370', 'myPayList', '我的手机充值', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('1aa0da20-8831-4443-91b1-223fdfc3d79f', 'payUpdateStatusAppeal', '手机充值申诉操作', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('1cb9b83e-2203-472b-9d1c-026d5a1d5c9c', 'permissionEdit', '权限--编辑权限', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('1fb864a0-899a-4656-8d3c-eba634a435ed', 'payView', '手机充值查看', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('2568b1e8-c75a-4388-830e-86a36f767a83', 'permissionDel', '权限--删除权限', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('275bae6c-43c4-4b60-8e61-fbb874602cdd', 'permissionView', '权限--查看权限', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('278379cc-ea48-4237-b82a-4d90995a4195', 'myUserInfoAdd', '我要充流量', '0', null, '我的管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('286de048-c1aa-44ae-bd16-0e162ef03d33', 'roleDel', '角色--删除角色', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('2be093e5-a2e0-4a6e-bab0-26d144cdc07c', 'userManage', '用户管理', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('2c1733f2-2b5a-41b2-8c4d-2af8ea9615da', 'orgAdd', '组织机构--新增机构', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('2e3bb8c6-b5e1-4577-9f18-d11260c6eca3', 'myUserInfoBatchAdd', '我要批量充流量', '0', null, '我的管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('30e8a0d1-106f-49af-9f63-69ae015d1868', 'personView', '人员--查看人员', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('34588c54-4f56-44c5-b04b-8a7f3e5d3b32', 'merchantList', '商家列表', '0', null, '商家管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('37fd325a-b79f-4593-83c6-323c0efe72b4', 'payUpdateStatus', '手机充值状态操作', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('3bedc2e2-903f-4329-94fe-50058c46ff47', 'payFail', '手机充值充值失败', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('40b927cd-f6ec-4579-81a4-4d3e96ac7a70', 'payIng', '手机充值充值中', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('412e0b7c-26dc-4b71-b8d5-5b634264a818', 'userEdit', '用户--编辑用户', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('482b1d34-7b3f-4894-aa50-e27486686435', 'merchandiseExport', '套餐导出', '0', null, '套餐管理', 'merchandise');
INSERT INTO `tb_dqdp_permission` VALUES ('49e32b01-96df-4bb3-9ceb-68de1ae5fa43', 'merchantpayExport', '商家平台充值导出', '0', null, '商家平台充值管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('4a349c99-7f3b-406d-a083-e51d05060148', 'merchandiseList', '套餐列表', '0', null, '套餐管理', 'merchandise');
INSERT INTO `tb_dqdp_permission` VALUES ('4a9514f4-8535-4ae0-847f-8f9c80326c1d', 'myAdminUserInfoEdit', '管理员修改信息', '0', null, '我的管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('57218057-4d7f-465d-8217-a5436380070d', 'userList', '用户--查看用户列表', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('5e0271da-bbeb-4a00-9865-d1248fa057e4', 'userDel', '用户--删除用户', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('5e209090-3731-40c9-b45c-4b5beedeb040', 'scheduleManage', '任务管理', '0', '', '任务管理', 'schedule');
INSERT INTO `tb_dqdp_permission` VALUES ('607fd863-a9f8-4e67-8089-6825db39a724', 'payEdit', '手机充值编辑', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('64792613-f691-4e68-88dc-9fa97a91b323', 'orgEdit', '组织机构--编辑机构', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('6870af6b-af75-4789-bc2d-786fc3e90404', 'myUserInfoView', '我的信息', '0', null, '我的管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('6affa08a-ecfb-4660-847a-d4adb438dfa7', 'personAdd', '人员--新增人员', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('6f09932c-4fb7-45e9-a51e-9d5e84e6c8cb', 'adminConfig', '管理员配置修改', '0', null, '我的管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('74013e39-7c33-4ba7-a512-8e5eba447fa1', 'userChangePassword', '用户--修改密码', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('7819ef4c-3083-43ba-8910-c0bfb1b9e728', 'changePayPassword', '支付密码', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('78ec8593-a6dd-41e1-afcf-16f3da70f7c8', 'permissionAdd', '权限--新增权限', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('7ff5f7d6-7ee0-4326-a0c1-c55e11386fa5', 'systemManage', '系统管理', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('8e875d16-fb3b-4390-8c98-3942e5e3fd8e', 'myUserInfoManage', '我的管理', '0', null, '我的管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('8f946ada-a880-4786-aecb-59ceedc5e27d', 'roleEdit', '角色--编辑角色', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('90a7de09-d874-43c1-a615-b163b93d91f7', 'merchantpayManage', '商家平台充值管理', '0', null, '商家平台充值管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('9125f42f-61d5-406f-8979-861a016e267f', 'myUserInfoEdit', '修改信息', '0', null, '我的管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('a2e6d135-4ab8-4627-9eb9-54deb03dee6d', 'roleList', '角色--查看列表', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('a6f86c55-aa02-4a6c-9f83-079671368538', 'userExport', '用户--导出', null, '', '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('a9a8b803-2747-4ddd-b58f-e13d0d841512', 'roleAdd', '角色--新增角色', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('a9e5a215-d9f4-4f97-9aec-1bf26a0b69a5', 'permissionList', '权限--查看权限列表', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('aa3d1cff-0f04-40a0-99f1-8f42e1edc92e', 'orgList', '组织机构--查看机构列表', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('ab089884-5bfa-4a39-94b2-57ea2d5b1750', 'merchantpayAdd', '商家平台充值新增', '0', null, '商家平台充值管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('ac189a33-df40-469c-a2bf-6f68b13feb66', 'orgManage', '组织机构管理', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('b37a4653-aef5-4f3b-b026-a528023eb5ee', 'merchandiseManage', '套餐管理', '0', null, '套餐管理', 'merchandise');
INSERT INTO `tb_dqdp_permission` VALUES ('b52729a1-c38a-4640-b553-302db2e4a789', 'orgDel', '组织机构--删除机构', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('b64082bc-620a-4928-abdf-c01fe444a49d', 'merchandiseAdd', '套餐新增', '0', null, '套餐管理', 'merchandise');
INSERT INTO `tb_dqdp_permission` VALUES ('b648c1ec-a4d5-4df2-8e2f-8c06b650e655', 'payBatchAdd', '手机充值批量新增', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('b9511817-16bc-4624-8e67-eff31cc99efb', 'merchantpayList', '商家平台充值列表', '0', null, '商家平台充值管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('bbaad3be-acc8-4bc0-a9e9-364b355a2346', 'merchandiseEdit', '套餐编辑', '0', null, '套餐管理', 'merchandise');
INSERT INTO `tb_dqdp_permission` VALUES ('bc340d15-9d7c-4c94-b445-85cd0bb829fb', 'payUpdateStatusAppealSuccess', '手机充值申诉成功操作', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('c4101de0-5299-41b9-bf0a-f778326a1b32', 'payExport', '手机充值管理充值导出', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('c9b056f8-0126-45bf-87bb-53da568b438b', 'payManage', '手机充值管理', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('ca0985e0-d0fc-459d-bc67-6d6f80a0e960', 'permissionManage', '权限管理', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('cec46422-5df9-40e2-a383-77642e4b5338', 'orgView', '组织机构--查看机构', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('d08328e7-7bf2-43c0-ac69-9d04f5d55577', 'myPayExport', '我的手机充值管理充值导出', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('d6d73029-6dfd-4caa-85a4-7be750f68eb4', 'personList', '人员--查看人员列表', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('d8c469cf-42bc-4a07-b7ad-aee4fd856288', 'merchantpayView', '商家平台充值查看', '0', null, '商家平台充值管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('d95b9ea3-b53a-4df5-bf65-a924403225c2', 'userAdd', '用户--新增用户', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('d9e1aa02-1dfa-4e13-ab84-e13683e943c8', 'payAdd', '手机充值新增', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('df5bfc07-b62a-4002-91e5-7fb363d65507', 'merchantManage', '商家管理', '0', null, '商家管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('dfca6c89-297d-472f-a9f8-bfbba2ff98b0', 'merchandiseView', '套餐查看', '0', null, '套餐管理', 'merchandise');
INSERT INTO `tb_dqdp_permission` VALUES ('e59514a8-f672-441a-8d8d-55a98f0fbe98', 'payUpdateStatusFail', '手机充值失败操作', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('e6b2264c-de71-4035-9182-3a9e62919e2d', 'payUpdateStatusCompile', '手机充值完成操作', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('e962498c-37b9-47ed-8cb3-2b6ce350bcca', 'merchantpayEdit', '商家平台充值编辑', '0', null, '商家平台充值管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('ef6fe087-6fcf-4294-9021-2e115f05f27a', 'flowEdit', '代理商流程修改', '0', null, '我的管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('efbf8123-8ee2-4b72-96e2-407c630f1b54', 'merchantpayDelete', '商家平台充值删除', '0', null, '商家平台充值管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('f2a7887e-a0ef-4593-852f-62a936b96ab5', 'personDel', '人员--删除人员', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('f6757baa-0257-43c9-ab07-7d36555cea05', 'userView', '用户--查看用户', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('f73cab4b-02c5-4a8c-aaf8-cc1fff9c5009', 'payUpdateStatusAppealFail', '手机充值申诉失败操作', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('f7cb4758-8101-4bbf-8aa1-5f3c6e9609fb', 'payList', '手机充值列表', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('f8abbb63-2e15-406a-80d4-9028eaa923df', 'payCompile', '手机充值充值完成', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('f9021c4d-8d57-47e1-801d-417decc8f08e', 'roleView', '角色--查看角色', '0', null, '系统管理', 'systemmgr');
INSERT INTO `tb_dqdp_permission` VALUES ('f9ca8365-7ac1-4dd1-b4a4-10d8b4851243', 'myAdminUserInfoView', '管理员的信息', '0', null, '我的管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('fb26a961-2919-4a6f-9895-08e0a3662506', 'payUpdateStatusMoney', '手机充值退款操作', '0', null, '手机充值管理', 'pay');
INSERT INTO `tb_dqdp_permission` VALUES ('fbb47357-9f3e-45c6-be02-da4976ec1e69', 'flowView', '代理商流程查看', '0', null, '我的管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('fc49ad54-4e34-42c0-8778-daba858c99d8', 'merchantpayMyList', '我的商家平台充值列表', '0', null, '商家平台充值管理', 'merchant');
INSERT INTO `tb_dqdp_permission` VALUES ('user', 'ROLE_user', 'user', '0', null, null, null);

-- ----------------------------
-- Table structure for tb_dqdp_person
-- ----------------------------
DROP TABLE IF EXISTS `tb_dqdp_person`;
CREATE TABLE `tb_dqdp_person` (
  `PERSON_ID` varchar(36) NOT NULL,
  `PERSON_NAME` varchar(100) DEFAULT NULL,
  `AGE` decimal(3,0) DEFAULT NULL,
  `SEX` decimal(1,0) DEFAULT '0',
  PRIMARY KEY (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dqdp_person
-- ----------------------------
INSERT INTO `tb_dqdp_person` VALUES ('0a30a164-79d2-448d-88cc-64f88359b12b', '222222', null, null);
INSERT INTO `tb_dqdp_person` VALUES ('1267755d-9929-4c1f-8caa-07ed967dae1c', '111111', '12', '1');
INSERT INTO `tb_dqdp_person` VALUES ('377f7e86-e56a-42ee-bd83-51ec4459c82c', '111111a', null, null);
INSERT INTO `tb_dqdp_person` VALUES ('53970958-b8c8-423c-8ac2-13f4df2868f0', 'lixiong', null, null);
INSERT INTO `tb_dqdp_person` VALUES ('ad2726d7-3188-4085-a406-36a1d17b6ce0', '333333', '12', '1');

-- ----------------------------
-- Table structure for tb_dqdp_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_dqdp_role`;
CREATE TABLE `tb_dqdp_role` (
  `ROLE_ID` varchar(36) NOT NULL,
  `ROLE_NAME` varchar(100) DEFAULT NULL,
  `ROLE_DESCRIPTION` varchar(300) DEFAULT NULL,
  `STATUS` decimal(2,0) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dqdp_role
-- ----------------------------
INSERT INTO `tb_dqdp_role` VALUES ('0c7f6536-156d-468b-b81a-cfb5fd9918bc', '管理员', '后台管理员', '0');
INSERT INTO `tb_dqdp_role` VALUES ('78e033cb-b9e8-48a6-9a8e-5890069bc529', '代理商', '代理商用户管理', '0');

-- ----------------------------
-- Table structure for tb_dqdp_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_dqdp_user`;
CREATE TABLE `tb_dqdp_user` (
  `USER_ID` varchar(36) NOT NULL,
  `USER_NAME` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(60) DEFAULT NULL,
  `STATUS` decimal(2,0) DEFAULT NULL COMMENT '0：正常',
  `LAST_LOGIN_TIME` datetime DEFAULT NULL,
  `IS_INTERNAL` decimal(2,0) DEFAULT NULL COMMENT '0 : 内部用户，1 : 外部用户',
  PRIMARY KEY (`USER_ID`),
  KEY `USER_ID` (`USER_ID`) USING BTREE,
  KEY `USER_NAME` (`USER_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dqdp_user
-- ----------------------------
INSERT INTO `tb_dqdp_user` VALUES ('2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3', 'lixiong', '59cc3f857e5963cf09e5899cfe0f1a74', '0', null, '1');
INSERT INTO `tb_dqdp_user` VALUES ('4c46f4e8-36db-411c-902c-94ff3668b259', '222222', 'e3ceb5881a0a1fdaad01296d7554868d', '0', null, '1');
INSERT INTO `tb_dqdp_user` VALUES ('509b438c-d4d5-4c2c-8346-1521f5f0b7e6', '111111a', '96e79218965eb72c92a549dd5a330112', '0', null, '1');
INSERT INTO `tb_dqdp_user` VALUES ('7761d4a2-1685-495a-b410-59e74a3be9bd', '333333', '1a100d2c0dab19c4430e7d73762b3423', '0', null, '1');
INSERT INTO `tb_dqdp_user` VALUES ('9c8f7426-c913-4b64-b4b3-645587f8dee7', '111111', '96e79218965eb72c92a549dd5a330112', '0', null, '1');
INSERT INTO `tb_dqdp_user` VALUES ('admin', 'admin', '098f6bcd4621d373cade4e832627b4f6', '0', null, null);

-- ----------------------------
-- Table structure for tb_method_dict
-- ----------------------------
DROP TABLE IF EXISTS `tb_method_dict`;
CREATE TABLE `tb_method_dict` (
  `DICT_ID` varchar(36) NOT NULL,
  `DICT_TYPE` varchar(100) NOT NULL,
  `CLZ` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`DICT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_method_dict
-- ----------------------------

-- ----------------------------
-- Table structure for tb_person_organization_ref
-- ----------------------------
DROP TABLE IF EXISTS `tb_person_organization_ref`;
CREATE TABLE `tb_person_organization_ref` (
  `REF_ID` varchar(36) NOT NULL,
  `PERSON_ID` varchar(36) DEFAULT NULL,
  `ORG_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`REF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_person_organization_ref
-- ----------------------------
INSERT INTO `tb_person_organization_ref` VALUES ('1df8e203-87ed-4d65-9914-d6254e9bd8f7', '0a30a164-79d2-448d-88cc-64f88359b12b', 'deaf8bfe-c403-4b83-84c0-ca1e8db80a05');
INSERT INTO `tb_person_organization_ref` VALUES ('383fcd81-1e99-4d3d-8162-70acbac87d95', 'ad2726d7-3188-4085-a406-36a1d17b6ce0', 'deaf8bfe-c403-4b83-84c0-ca1e8db80a05');
INSERT INTO `tb_person_organization_ref` VALUES ('483de745-4fa9-4c45-9f61-830af4c27964', '377f7e86-e56a-42ee-bd83-51ec4459c82c', 'deaf8bfe-c403-4b83-84c0-ca1e8db80a05');
INSERT INTO `tb_person_organization_ref` VALUES ('db7da780-9efa-438c-8ec2-cf2099a57da5', '1267755d-9929-4c1f-8caa-07ed967dae1c', 'deaf8bfe-c403-4b83-84c0-ca1e8db80a05');
INSERT INTO `tb_person_organization_ref` VALUES ('e24b6b71-d808-4a80-b199-d6def5d61c18', '53970958-b8c8-423c-8ac2-13f4df2868f0', 'org00000000');

-- ----------------------------
-- Table structure for tb_person_user_ref
-- ----------------------------
DROP TABLE IF EXISTS `tb_person_user_ref`;
CREATE TABLE `tb_person_user_ref` (
  `REF_ID` varchar(36) NOT NULL,
  `PERSON_ID` varchar(36) DEFAULT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `REF_TYPE` decimal(2,0) DEFAULT NULL,
  `REF_AURTH_CODE` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`REF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_person_user_ref
-- ----------------------------
INSERT INTO `tb_person_user_ref` VALUES ('02825f7c-b9f8-4e7c-9946-502eaa34971d', '377f7e86-e56a-42ee-bd83-51ec4459c82c', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6', null, null);
INSERT INTO `tb_person_user_ref` VALUES ('55c48411-df58-46c3-afbe-7833a2f0efc5', '0a30a164-79d2-448d-88cc-64f88359b12b', '4c46f4e8-36db-411c-902c-94ff3668b259', null, null);
INSERT INTO `tb_person_user_ref` VALUES ('97b521b9-36a0-40a0-8291-423791c343d3', '53970958-b8c8-423c-8ac2-13f4df2868f0', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3', null, null);
INSERT INTO `tb_person_user_ref` VALUES ('ca31634b-23fb-4806-8560-585a4636199e', 'ad2726d7-3188-4085-a406-36a1d17b6ce0', '7761d4a2-1685-495a-b410-59e74a3be9bd', null, null);
INSERT INTO `tb_person_user_ref` VALUES ('cb91d373-f2f2-48af-bd7d-d5e58704b16a', '1267755d-9929-4c1f-8caa-07ed967dae1c', '9c8f7426-c913-4b64-b4b3-645587f8dee7', null, null);

-- ----------------------------
-- Table structure for tb_role_permission_ref
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_permission_ref`;
CREATE TABLE `tb_role_permission_ref` (
  `REF_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) DEFAULT NULL,
  `PERMISSION_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`REF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_role_permission_ref
-- ----------------------------
INSERT INTO `tb_role_permission_ref` VALUES ('076974ad-eb96-4612-8b5a-c5ea34f9076a', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '6f09932c-4fb7-45e9-a51e-9d5e84e6c8cb');
INSERT INTO `tb_role_permission_ref` VALUES ('14168d38-0cfa-4e07-b9af-c24b62ce724c', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '04203ca1-0a84-4c31-914d-3a58a986b826');
INSERT INTO `tb_role_permission_ref` VALUES ('156ac6ae-1ac7-4317-b2a7-abdd5cd68f62', '78e033cb-b9e8-48a6-9a8e-5890069bc529', 'd9e1aa02-1dfa-4e13-ab84-e13683e943c8');
INSERT INTO `tb_role_permission_ref` VALUES ('196900e1-7dba-4ddd-a14b-49177b318b38', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'df5bfc07-b62a-4002-91e5-7fb363d65507');
INSERT INTO `tb_role_permission_ref` VALUES ('1aafdfd1-8c88-4061-8bd7-e779caa45cf4', '78e033cb-b9e8-48a6-9a8e-5890069bc529', '9125f42f-61d5-406f-8979-861a016e267f');
INSERT INTO `tb_role_permission_ref` VALUES ('1b95524a-d1c1-47ff-b631-1778f169124b', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '1cb9b83e-2203-472b-9d1c-026d5a1d5c9c');
INSERT INTO `tb_role_permission_ref` VALUES ('265791bf-5939-4439-86e7-ca699b07de71', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'e6b2264c-de71-4035-9182-3a9e62919e2d');
INSERT INTO `tb_role_permission_ref` VALUES ('26dc80fd-51c5-48d0-b55a-058b84199def', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'f73cab4b-02c5-4a8c-aaf8-cc1fff9c5009');
INSERT INTO `tb_role_permission_ref` VALUES ('2d800bcd-2c16-4417-ba14-30ab6ddc050a', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '30e8a0d1-106f-49af-9f63-69ae015d1868');
INSERT INTO `tb_role_permission_ref` VALUES ('2db9565f-05d1-4571-9fde-b8bbb3fcffc4', '78e033cb-b9e8-48a6-9a8e-5890069bc529', 'fc49ad54-4e34-42c0-8778-daba858c99d8');
INSERT INTO `tb_role_permission_ref` VALUES ('2ee78d70-dd41-471f-9a6c-162185d5c6c1', '78e033cb-b9e8-48a6-9a8e-5890069bc529', '8e875d16-fb3b-4390-8c98-3942e5e3fd8e');
INSERT INTO `tb_role_permission_ref` VALUES ('2f6e36f3-4a97-4515-8bdc-96f05b67a9c4', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '8f946ada-a880-4786-aecb-59ceedc5e27d');
INSERT INTO `tb_role_permission_ref` VALUES ('3210d0e5-9574-4fe9-94b7-ed171372be7c', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'efbf8123-8ee2-4b72-96e2-407c630f1b54');
INSERT INTO `tb_role_permission_ref` VALUES ('35267eed-7f48-489d-8c95-a97ee29db205', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '37fd325a-b79f-4593-83c6-323c0efe72b4');
INSERT INTO `tb_role_permission_ref` VALUES ('35e009ed-a28c-4869-86d6-11835102268b', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'b37a4653-aef5-4f3b-b026-a528023eb5ee');
INSERT INTO `tb_role_permission_ref` VALUES ('3c144191-ca1c-4427-bfbd-6a2a69705174', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'f8abbb63-2e15-406a-80d4-9028eaa923df');
INSERT INTO `tb_role_permission_ref` VALUES ('3d7a177a-5449-401c-95b8-e575db354497', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'bbaad3be-acc8-4bc0-a9e9-364b355a2346');
INSERT INTO `tb_role_permission_ref` VALUES ('41eb2cb5-acfa-4bfe-b916-20412ef434be', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'a2e6d135-4ab8-4627-9eb9-54deb03dee6d');
INSERT INTO `tb_role_permission_ref` VALUES ('4e47b699-0a30-4bff-b733-4f0001894f63', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'd95b9ea3-b53a-4df5-bf65-a924403225c2');
INSERT INTO `tb_role_permission_ref` VALUES ('4fd23def-47e8-4708-b14f-95888a570c47', '78e033cb-b9e8-48a6-9a8e-5890069bc529', 'fbb47357-9f3e-45c6-be02-da4976ec1e69');
INSERT INTO `tb_role_permission_ref` VALUES ('4fe58cc4-723e-4dc4-b208-2c3c82ae7029', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '40b927cd-f6ec-4579-81a4-4d3e96ac7a70');
INSERT INTO `tb_role_permission_ref` VALUES ('5036f1a9-2afc-4968-aa88-044aa7fdf11d', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '2568b1e8-c75a-4388-830e-86a36f767a83');
INSERT INTO `tb_role_permission_ref` VALUES ('5096894a-aef7-4365-930f-7245f96a1111', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '4a9514f4-8535-4ae0-847f-8f9c80326c1d');
INSERT INTO `tb_role_permission_ref` VALUES ('52ea58f5-7872-45dd-8638-40104bf62fbf', '78e033cb-b9e8-48a6-9a8e-5890069bc529', 'b648c1ec-a4d5-4df2-8e2f-8c06b650e655');
INSERT INTO `tb_role_permission_ref` VALUES ('537ecc79-8a45-48a5-bc11-726e57c3e85a', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'dfca6c89-297d-472f-a9f8-bfbba2ff98b0');
INSERT INTO `tb_role_permission_ref` VALUES ('55de7079-fb1c-43ad-bdba-88af3de370ad', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'fb26a961-2919-4a6f-9895-08e0a3662506');
INSERT INTO `tb_role_permission_ref` VALUES ('56a36cdd-f5ae-498f-b5cd-b8dd02d9771f', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '78ec8593-a6dd-41e1-afcf-16f3da70f7c8');
INSERT INTO `tb_role_permission_ref` VALUES ('57e2fd85-12d8-46cc-9b70-84301449b3b4', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'cec46422-5df9-40e2-a383-77642e4b5338');
INSERT INTO `tb_role_permission_ref` VALUES ('5896813d-b4c9-493e-a5d1-ebfc280979e1', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '00acc8c7-07b0-4ec8-b9ae-fe52552d485e');
INSERT INTO `tb_role_permission_ref` VALUES ('5bf9a272-f5a5-4545-a29d-731a9a184ad4', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'fc49ad54-4e34-42c0-8778-daba858c99d8');
INSERT INTO `tb_role_permission_ref` VALUES ('5e7b43f3-a01d-4c2a-86f0-31df86c2f962', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '49e32b01-96df-4bb3-9ceb-68de1ae5fa43');
INSERT INTO `tb_role_permission_ref` VALUES ('5eecc932-b443-4da7-b96e-2d5e80fac1b2', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'ac189a33-df40-469c-a2bf-6f68b13feb66');
INSERT INTO `tb_role_permission_ref` VALUES ('62613efd-f816-4e4c-a876-b285f65e97cb', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'ab089884-5bfa-4a39-94b2-57ea2d5b1750');
INSERT INTO `tb_role_permission_ref` VALUES ('63a9c95c-3339-4057-bef3-a811245bb43a', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'ef6fe087-6fcf-4294-9021-2e115f05f27a');
INSERT INTO `tb_role_permission_ref` VALUES ('66d33436-01ad-47a3-9a2a-33d75ced8a0f', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '412e0b7c-26dc-4b71-b8d5-5b634264a818');
INSERT INTO `tb_role_permission_ref` VALUES ('6ea5c56e-c10b-4443-a872-baff15c1037e', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '2c1733f2-2b5a-41b2-8c4d-2af8ea9615da');
INSERT INTO `tb_role_permission_ref` VALUES ('6f7b4a56-b086-4660-83ca-d3a032d38f5b', '78e033cb-b9e8-48a6-9a8e-5890069bc529', '17db3c51-016a-428b-b2ee-bde45e565370');
INSERT INTO `tb_role_permission_ref` VALUES ('6fb2333e-16fd-487b-a9a2-9afdfdca6ee3', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '275bae6c-43c4-4b60-8e61-fbb874602cdd');
INSERT INTO `tb_role_permission_ref` VALUES ('70a3461f-7b99-493b-be32-0293910fd4c2', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '74013e39-7c33-4ba7-a512-8e5eba447fa1');
INSERT INTO `tb_role_permission_ref` VALUES ('70d9a0c6-2089-4600-92df-3ac4814ef634', '78e033cb-b9e8-48a6-9a8e-5890069bc529', 'd08328e7-7bf2-43c0-ac69-9d04f5d55577');
INSERT INTO `tb_role_permission_ref` VALUES ('726479e1-0788-4fe7-9927-229089e9b08c', '78e033cb-b9e8-48a6-9a8e-5890069bc529', '7819ef4c-3083-43ba-8910-c0bfb1b9e728');
INSERT INTO `tb_role_permission_ref` VALUES ('770a3c7e-79f8-40e7-8ba8-2c7ac371cee8', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '34588c54-4f56-44c5-b04b-8a7f3e5d3b32');
INSERT INTO `tb_role_permission_ref` VALUES ('7b24694f-c30e-4c9c-a034-1ff298c4bb32', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'a9a8b803-2747-4ddd-b58f-e13d0d841512');
INSERT INTO `tb_role_permission_ref` VALUES ('7fef73f2-501a-4645-8515-618b3adfadb2', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '5e0271da-bbeb-4a00-9865-d1248fa057e4');
INSERT INTO `tb_role_permission_ref` VALUES ('817be4f2-5093-4a6b-ab0b-da41527eeb4a', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'e962498c-37b9-47ed-8cb3-2b6ce350bcca');
INSERT INTO `tb_role_permission_ref` VALUES ('82cb31bd-42bf-43d8-81dd-23911df29f1d', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'ca0985e0-d0fc-459d-bc67-6d6f80a0e960');
INSERT INTO `tb_role_permission_ref` VALUES ('85fc2612-c2e1-4218-8a00-a3f872b26bc1', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'user');
INSERT INTO `tb_role_permission_ref` VALUES ('8816a978-034f-4574-85d6-70ee5d0583a9', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '64792613-f691-4e68-88dc-9fa97a91b323');
INSERT INTO `tb_role_permission_ref` VALUES ('88b7a2a6-491d-4829-8133-8291674ef703', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '482b1d34-7b3f-4894-aa50-e27486686435');
INSERT INTO `tb_role_permission_ref` VALUES ('8b9717a4-2aad-45b1-b050-1ca9e58faf56', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '4a349c99-7f3b-406d-a083-e51d05060148');
INSERT INTO `tb_role_permission_ref` VALUES ('8e34bc35-ebee-4455-801b-f185ac5a7db6', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '90a7de09-d874-43c1-a615-b163b93d91f7');
INSERT INTO `tb_role_permission_ref` VALUES ('93478c27-8a08-4ef5-89a3-26d8d40c6b40', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'd6d73029-6dfd-4caa-85a4-7be750f68eb4');
INSERT INTO `tb_role_permission_ref` VALUES ('97dd4a58-eaaf-4421-b731-ec8576fe0114', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '1fb864a0-899a-4656-8d3c-eba634a435ed');
INSERT INTO `tb_role_permission_ref` VALUES ('9a96576d-5f0e-4e90-9ec6-1ac897109ea0', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'b64082bc-620a-4928-abdf-c01fe444a49d');
INSERT INTO `tb_role_permission_ref` VALUES ('9f0dbd52-6771-4bfd-a42b-6d53073f1660', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'aa3d1cff-0f04-40a0-99f1-8f42e1edc92e');
INSERT INTO `tb_role_permission_ref` VALUES ('a0281641-afdd-4367-930b-c27efb5b0443', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '2be093e5-a2e0-4a6e-bab0-26d144cdc07c');
INSERT INTO `tb_role_permission_ref` VALUES ('a0411936-747a-4a91-8cca-d3cc0e63c784', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'b9511817-16bc-4624-8e67-eff31cc99efb');
INSERT INTO `tb_role_permission_ref` VALUES ('a3907fc1-b039-40ff-880a-811379d77d58', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'f7cb4758-8101-4bbf-8aa1-5f3c6e9609fb');
INSERT INTO `tb_role_permission_ref` VALUES ('a46969a6-b085-4fb5-8c78-6c3c85275532', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '6affa08a-ecfb-4660-847a-d4adb438dfa7');
INSERT INTO `tb_role_permission_ref` VALUES ('a5f5804c-23ca-4325-b7ef-81b0b6b43a18', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'a9e5a215-d9f4-4f97-9aec-1bf26a0b69a5');
INSERT INTO `tb_role_permission_ref` VALUES ('a7e03538-7233-46c7-83a6-fa5ffa778fa8', '78e033cb-b9e8-48a6-9a8e-5890069bc529', '1fb864a0-899a-4656-8d3c-eba634a435ed');
INSERT INTO `tb_role_permission_ref` VALUES ('ac3b43ae-89f8-422b-bf0d-736186bba255', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '08839a83-359a-4955-bf4d-20d6a3f0b4c1');
INSERT INTO `tb_role_permission_ref` VALUES ('b09bb466-1e7f-4234-83c5-6722f7bebec5', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '286de048-c1aa-44ae-bd16-0e162ef03d33');
INSERT INTO `tb_role_permission_ref` VALUES ('b1c7eba1-21e5-4be1-826d-1594ee47f3ab', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'f6757baa-0257-43c9-ab07-7d36555cea05');
INSERT INTO `tb_role_permission_ref` VALUES ('b63ece50-c89a-465c-8bc7-bf0b7fdefe83', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '607fd863-a9f8-4e67-8089-6825db39a724');
INSERT INTO `tb_role_permission_ref` VALUES ('b6fb86b6-f827-473e-8d61-34b2a3223682', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '0294b07d-1358-449b-92f3-5ffbed8c73c0');
INSERT INTO `tb_role_permission_ref` VALUES ('bb81620f-c3e7-43fc-a4c6-8ad9f77a0598', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'b52729a1-c38a-4640-b553-302db2e4a789');
INSERT INTO `tb_role_permission_ref` VALUES ('bfe125dd-98b7-473c-bba5-fbf9e2683881', '78e033cb-b9e8-48a6-9a8e-5890069bc529', 'user');
INSERT INTO `tb_role_permission_ref` VALUES ('c0008318-b31e-4387-b799-4712053d5f13', '78e033cb-b9e8-48a6-9a8e-5890069bc529', '2e3bb8c6-b5e1-4577-9f18-d11260c6eca3');
INSERT INTO `tb_role_permission_ref` VALUES ('c11399c1-fff1-49b8-b660-03547ba94f47', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'd8c469cf-42bc-4a07-b7ad-aee4fd856288');
INSERT INTO `tb_role_permission_ref` VALUES ('cb70f376-d6a4-46ae-bec8-ab5be478a917', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'c9b056f8-0126-45bf-87bb-53da568b438b');
INSERT INTO `tb_role_permission_ref` VALUES ('cc0623cc-fe75-4818-9f55-c554d0124f0a', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '57218057-4d7f-465d-8217-a5436380070d');
INSERT INTO `tb_role_permission_ref` VALUES ('cf9af475-e989-4c67-bd40-8916666a3cf4', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'bc340d15-9d7c-4c94-b445-85cd0bb829fb');
INSERT INTO `tb_role_permission_ref` VALUES ('cfe2cc7b-a145-48e4-bbf2-733b0ef223ff', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '3bedc2e2-903f-4329-94fe-50058c46ff47');
INSERT INTO `tb_role_permission_ref` VALUES ('d4bdfb6c-44af-4d1d-acae-f012eeb77cd6', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '0d6fca0f-7dd0-4bea-b1ad-220b0968de9c');
INSERT INTO `tb_role_permission_ref` VALUES ('dc17fc91-68c5-40e4-b101-93f6c1c9b864', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '7819ef4c-3083-43ba-8910-c0bfb1b9e728');
INSERT INTO `tb_role_permission_ref` VALUES ('de254267-091b-4529-ac28-557f231b9b35', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'f9ca8365-7ac1-4dd1-b4a4-10d8b4851243');
INSERT INTO `tb_role_permission_ref` VALUES ('e0f29817-80e5-436c-bd70-d5a96ddd0f03', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '8e875d16-fb3b-4390-8c98-3942e5e3fd8e');
INSERT INTO `tb_role_permission_ref` VALUES ('e192a72f-b7dd-407a-b5c0-fbbe6c69f572', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', '02713736-ffc3-4c8e-a57d-2d8d0feb70e3');
INSERT INTO `tb_role_permission_ref` VALUES ('e1d351b3-0b77-4797-9097-97a73cb56d26', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'f9021c4d-8d57-47e1-801d-417decc8f08e');
INSERT INTO `tb_role_permission_ref` VALUES ('e3066008-8839-4ba5-8bc8-99728a996448', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'f2a7887e-a0ef-4593-852f-62a936b96ab5');
INSERT INTO `tb_role_permission_ref` VALUES ('e6ca3a44-ee6e-44b9-8089-1c6b0d02b82b', '78e033cb-b9e8-48a6-9a8e-5890069bc529', '1aa0da20-8831-4443-91b1-223fdfc3d79f');
INSERT INTO `tb_role_permission_ref` VALUES ('e944418a-2f2e-4e1a-b49c-cf950743baaa', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'fbb47357-9f3e-45c6-be02-da4976ec1e69');
INSERT INTO `tb_role_permission_ref` VALUES ('ed94cd00-bf48-4b5a-8b7b-b82146e8e2a1', '78e033cb-b9e8-48a6-9a8e-5890069bc529', '278379cc-ea48-4237-b82a-4d90995a4195');
INSERT INTO `tb_role_permission_ref` VALUES ('f674f80d-f432-4d75-8e6f-9da5c3da07fa', '78e033cb-b9e8-48a6-9a8e-5890069bc529', '6870af6b-af75-4789-bc2d-786fc3e90404');
INSERT INTO `tb_role_permission_ref` VALUES ('f7ef02b2-0570-432e-9323-078f2a9f0756', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'c4101de0-5299-41b9-bf0a-f778326a1b32');
INSERT INTO `tb_role_permission_ref` VALUES ('f87b2baa-e3e4-4e27-a12b-a1d5e76a228f', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'd9e1aa02-1dfa-4e13-ab84-e13683e943c8');
INSERT INTO `tb_role_permission_ref` VALUES ('f8c7206a-9aaa-4c62-9367-88c7e36170d9', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'a6f86c55-aa02-4a6c-9f83-079671368538');
INSERT INTO `tb_role_permission_ref` VALUES ('ff307205-d115-4fa6-bdb5-3a52b296c0a2', '0c7f6536-156d-468b-b81a-cfb5fd9918bc', 'e59514a8-f672-441a-8d8d-55a98f0fbe98');

-- ----------------------------
-- Table structure for tb_user_permission_ref
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_permission_ref`;
CREATE TABLE `tb_user_permission_ref` (
  `REF_ID` varchar(36) NOT NULL,
  `PERMISSION_ID` varchar(36) DEFAULT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`REF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_user_permission_ref
-- ----------------------------
INSERT INTO `tb_user_permission_ref` VALUES ('03e15791-889c-4197-9d84-cb37655c8682', '1fb864a0-899a-4656-8d3c-eba634a435ed', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('040f8e1b-d29b-43f4-96af-841dfd635753', '8e875d16-fb3b-4390-8c98-3942e5e3fd8e', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('042d9790-79d9-4519-887e-3d764e8981e4', 'fbb47357-9f3e-45c6-be02-da4976ec1e69', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('0aa7c354-4132-4a65-a819-114806fa8e1d', '4a9514f4-8535-4ae0-847f-8f9c80326c1d', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('0d5621d5-3ed0-44db-a1f6-dd2c26c3af02', 'e962498c-37b9-47ed-8cb3-2b6ce350bcca', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('0dd8dfea-0041-4b38-9e74-c859f82b49a4', '2e3bb8c6-b5e1-4577-9f18-d11260c6eca3', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('0ed10cc6-03ff-4a1b-afce-c447f0cfb10b', '1fb864a0-899a-4656-8d3c-eba634a435ed', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('12634e02-f12f-41ce-9561-550473f86813', '7819ef4c-3083-43ba-8910-c0bfb1b9e728', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('140b3a06-607e-4015-a3c1-1765b220428d', '49e32b01-96df-4bb3-9ceb-68de1ae5fa43', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('144feccf-a523-4a92-9be3-e4bc369e775c', 'a6f86c55-aa02-4a6c-9f83-079671368538', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('17b215a0-0880-4c44-b3bb-e8b70420a0cf', '7819ef4c-3083-43ba-8910-c0bfb1b9e728', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('181e37b3-02e9-4ece-bda1-069a587152c6', '1fb864a0-899a-4656-8d3c-eba634a435ed', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('19bbb68a-1dd3-429d-94a0-ed3bfc8cf4ac', 'ab089884-5bfa-4a39-94b2-57ea2d5b1750', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('1b6d77c0-7951-452d-b5d1-691539b328d6', 'd9e1aa02-1dfa-4e13-ab84-e13683e943c8', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('1c3eff0a-a079-4e69-8595-63422c8082ad', 'ef6fe087-6fcf-4294-9021-2e115f05f27a', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('1f40f384-350a-464c-85d6-2103fa69e542', 'efbf8123-8ee2-4b72-96e2-407c630f1b54', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('1fda7a5e-a67c-4e4e-b492-f14373fa17aa', 'b648c1ec-a4d5-4df2-8e2f-8c06b650e655', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('21a069d4-4887-4160-becc-73842ea180da', 'f6757baa-0257-43c9-ab07-7d36555cea05', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('21af9395-2a35-46cb-bbb0-0f6235a1d120', 'f73cab4b-02c5-4a8c-aaf8-cc1fff9c5009', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('23722cdd-bc3b-4933-8a93-b065f49716cf', '6affa08a-ecfb-4660-847a-d4adb438dfa7', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('23cad2e9-4b60-4580-867e-46f55d2f4b9d', '04203ca1-0a84-4c31-914d-3a58a986b826', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('24091af8-48fb-484b-b112-c92a72dab859', '6f09932c-4fb7-45e9-a51e-9d5e84e6c8cb', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('26b6b39a-3e9f-4cce-9a35-53471d40b44b', '08839a83-359a-4955-bf4d-20d6a3f0b4c1', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('26c4f414-b204-4bb1-b869-5d482317f090', '2e3bb8c6-b5e1-4577-9f18-d11260c6eca3', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('2d3d1db7-c077-4c5a-9645-720caeca4774', 'user', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('2f7a6ef1-2e4a-4ed7-b822-8f1245837b6f', '2e3bb8c6-b5e1-4577-9f18-d11260c6eca3', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('2fa93731-a89a-424d-8780-f000fea25306', '2568b1e8-c75a-4388-830e-86a36f767a83', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('30e28c32-c302-447c-b423-1454ba0d9e81', 'd08328e7-7bf2-43c0-ac69-9d04f5d55577', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('31606ffe-9d73-4aec-b75b-d3c1b4bd7c0c', 'd95b9ea3-b53a-4df5-bf65-a924403225c2', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('370ac68b-f133-4b36-9955-6f4c419f188e', '8e875d16-fb3b-4390-8c98-3942e5e3fd8e', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('3867579b-0d98-411e-b582-113779c8d354', '7819ef4c-3083-43ba-8910-c0bfb1b9e728', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('38b75b29-a348-4074-a502-a7b2facbdffa', 'fc49ad54-4e34-42c0-8778-daba858c99d8', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('3e7e6f23-d51f-47ed-9820-18b9f2abb3cd', 'b64082bc-620a-4928-abdf-c01fe444a49d', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('3fab4082-0d94-40b5-99eb-0fefdbfa57a5', 'd08328e7-7bf2-43c0-ac69-9d04f5d55577', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('3fcd6976-263d-4410-ba1e-fc4002012ccf', 'c4101de0-5299-41b9-bf0a-f778326a1b32', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('43f4d296-87e4-49ef-8324-b0019f8aa221', 'a9a8b803-2747-4ddd-b58f-e13d0d841512', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('4538c317-4d18-4eb4-80ed-f9d49f7aa757', 'bbaad3be-acc8-4bc0-a9e9-364b355a2346', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('46ca677c-289b-4927-8e96-d7bce00a662e', '6870af6b-af75-4789-bc2d-786fc3e90404', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('47c0cb58-6db2-4554-8f35-ed86a322549c', 'e59514a8-f672-441a-8d8d-55a98f0fbe98', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('48ed825e-b201-438a-9642-89238dcfac89', '17db3c51-016a-428b-b2ee-bde45e565370', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('5069e454-af9d-4e39-a59b-f2b409c63234', '7819ef4c-3083-43ba-8910-c0bfb1b9e728', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('51da098d-38f2-49a3-bea5-686519ce88e8', '9125f42f-61d5-406f-8979-861a016e267f', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('54768248-d1ba-4669-a7bb-b2209c9755af', '17db3c51-016a-428b-b2ee-bde45e565370', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('56c1803a-a1fb-4613-a081-4828856f4b6a', 'd9e1aa02-1dfa-4e13-ab84-e13683e943c8', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('58aa6f33-61c9-49d6-b3c3-25077d167554', '0d6fca0f-7dd0-4bea-b1ad-220b0968de9c', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('59d7c126-6dc2-4598-88f8-587d8dbead70', 'f9021c4d-8d57-47e1-801d-417decc8f08e', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('61730f8a-40d1-4625-9881-e0a4022610a0', 'd9e1aa02-1dfa-4e13-ab84-e13683e943c8', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('655cc2c7-ab5e-45af-884c-307588c98009', '7819ef4c-3083-43ba-8910-c0bfb1b9e728', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('67682a5c-82e9-4d30-a099-e7772f9382f7', '4a349c99-7f3b-406d-a083-e51d05060148', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('6abe980e-bbe6-4870-a8a3-176ffc0c0206', '6870af6b-af75-4789-bc2d-786fc3e90404', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('6eef064e-8eed-4765-8c7e-308d3afafb3c', 'df5bfc07-b62a-4002-91e5-7fb363d65507', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('70c042de-eb12-487e-8e6b-22f854cf86d7', 'aa3d1cff-0f04-40a0-99f1-8f42e1edc92e', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('722fcc87-9ffd-4786-9169-be2121454d62', '8e875d16-fb3b-4390-8c98-3942e5e3fd8e', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('75053a3d-9530-44dc-9073-827e41807438', '02713736-ffc3-4c8e-a57d-2d8d0feb70e3', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('77708a4d-d403-47b1-a09c-cf42d84ee87a', 'd08328e7-7bf2-43c0-ac69-9d04f5d55577', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('77f3f9aa-c78f-4276-8e1c-55ec9afc75f0', '6870af6b-af75-4789-bc2d-786fc3e90404', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('78c41981-df19-40b6-84ae-3d0957405a3b', 'a2e6d135-4ab8-4627-9eb9-54deb03dee6d', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('7b583165-4244-42df-a453-7fb65172beeb', '00acc8c7-07b0-4ec8-b9ae-fe52552d485e', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('7ceeb36b-690b-48ce-a936-36c9c7a40384', 'd9e1aa02-1dfa-4e13-ab84-e13683e943c8', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('7eff41c6-88af-4565-8e24-44e43b454f39', '2e3bb8c6-b5e1-4577-9f18-d11260c6eca3', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('816ae46b-5f3c-4596-b601-ee44efa54e49', 'f9ca8365-7ac1-4dd1-b4a4-10d8b4851243', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('81c50da0-a024-47c2-ab1e-ed83e297d9de', '412e0b7c-26dc-4b71-b8d5-5b634264a818', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('854ae947-e896-4aa7-aa20-11e9046e930f', 'dfca6c89-297d-472f-a9f8-bfbba2ff98b0', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('858c19d3-95f4-4e89-be47-a28dc41db1ac', '1aa0da20-8831-4443-91b1-223fdfc3d79f', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('86a5a458-ef4b-4eb4-b8fb-13935e9d36f6', 'fb26a961-2919-4a6f-9895-08e0a3662506', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('88ecd6f4-952b-4829-aa6e-49eb63612602', 'd08328e7-7bf2-43c0-ac69-9d04f5d55577', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('8987de7b-4348-4496-8d7a-1460d9152e14', 'cec46422-5df9-40e2-a383-77642e4b5338', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('8ce49ee8-edf0-4c13-9126-eb5816d436c0', '2be093e5-a2e0-4a6e-bab0-26d144cdc07c', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('8dffe663-f34e-402f-bd4d-288ed9deeaf1', 'fc49ad54-4e34-42c0-8778-daba858c99d8', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('8e0641df-282d-4191-be58-224f38317b11', '9125f42f-61d5-406f-8979-861a016e267f', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('8ee20807-c3fe-4100-b7fb-57eb0c48dd8b', '1cb9b83e-2203-472b-9d1c-026d5a1d5c9c', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('94037566-340e-4f68-bd85-3614763935ce', 'user', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('9699f882-3aec-4e86-bff5-d35afb12e588', 'b52729a1-c38a-4640-b553-302db2e4a789', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('96e99a2c-4c17-4be3-bb1c-e20d93ee3f44', '5e0271da-bbeb-4a00-9865-d1248fa057e4', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('99b8874c-a32d-4afb-872c-a99348899403', '2c1733f2-2b5a-41b2-8c4d-2af8ea9615da', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('9cc57521-cf5d-499e-ab4c-886d37482fba', '17db3c51-016a-428b-b2ee-bde45e565370', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('9d2adb55-90c3-4e59-a63b-95a6b4b0e15a', '8e875d16-fb3b-4390-8c98-3942e5e3fd8e', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('9d8f6702-49fb-43e1-a094-7baaf2934edd', '90a7de09-d874-43c1-a615-b163b93d91f7', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('a052640c-70e5-481e-8452-0f6edf1b2774', 'c9b056f8-0126-45bf-87bb-53da568b438b', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('a052cdeb-0d77-4d91-9c2a-59bd4262d88e', 'ca0985e0-d0fc-459d-bc67-6d6f80a0e960', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('a1484a4f-5ab1-4993-892d-4abe805149de', 'ac189a33-df40-469c-a2bf-6f68b13feb66', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('a18fe4ec-2f18-4997-95eb-db71915228d7', 'f2a7887e-a0ef-4593-852f-62a936b96ab5', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('a499558f-594d-4bd1-99ef-0fb03a54259b', 'fc49ad54-4e34-42c0-8778-daba858c99d8', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('a684508b-012d-4243-a553-62f921356e4d', 'b648c1ec-a4d5-4df2-8e2f-8c06b650e655', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('a6e6aa41-759e-49f3-ac8f-6cd84b1fd86d', 'e6b2264c-de71-4035-9182-3a9e62919e2d', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('a9f816d4-cd72-4835-8a9a-329d5b50fa49', '278379cc-ea48-4237-b82a-4d90995a4195', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('admin_user', 'user', 'admin');
INSERT INTO `tb_user_permission_ref` VALUES ('b03493ff-4c1d-483e-8d2f-cf4a9618f687', '9125f42f-61d5-406f-8979-861a016e267f', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('b0795983-e194-4bc7-aa0c-d5253813432c', 'b648c1ec-a4d5-4df2-8e2f-8c06b650e655', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('b59ea55c-64a7-4b20-ac39-a8fe31aa9e24', '78ec8593-a6dd-41e1-afcf-16f3da70f7c8', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('b8293134-280f-4768-8eaa-664702945f6b', 'b9511817-16bc-4624-8e67-eff31cc99efb', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('be6d666a-fb68-43fb-b2b7-e03373e2bae3', 'd6d73029-6dfd-4caa-85a4-7be750f68eb4', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('bfe3ea9c-b29c-4eaa-8383-b82387fe07a3', '9125f42f-61d5-406f-8979-861a016e267f', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('c00fae9d-499d-4bb0-8a6d-d6da189400ed', '482b1d34-7b3f-4894-aa50-e27486686435', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('c02f62c0-887c-4e3a-8cae-05eb3ddacd49', 'fbb47357-9f3e-45c6-be02-da4976ec1e69', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('c1c2492c-f328-4b17-a086-ae4a5b886668', 'fc49ad54-4e34-42c0-8778-daba858c99d8', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('c4938fd8-5305-42cc-b979-ad4a5d2c9eb3', '278379cc-ea48-4237-b82a-4d90995a4195', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('c4a579a2-5ce9-4c14-b8c3-637a0353975a', '74013e39-7c33-4ba7-a512-8e5eba447fa1', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('c551d95b-42a1-4eb9-98fd-ad4d85b4d0eb', '57218057-4d7f-465d-8217-a5436380070d', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('c71f4bc6-020e-46d3-b79b-f567358aba7c', '3bedc2e2-903f-4329-94fe-50058c46ff47', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('c9424474-2317-4c3b-81a9-f2304cf729c0', '1fb864a0-899a-4656-8d3c-eba634a435ed', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('c98b1a65-71b2-41ab-a1f1-2d6c1ca74ea7', 'fbb47357-9f3e-45c6-be02-da4976ec1e69', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('c9f224a0-59da-47e0-8a1c-ccd5d5a758e0', 'd8c469cf-42bc-4a07-b7ad-aee4fd856288', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('cc5d935c-9462-4e03-92f7-52c5c1be811a', '34588c54-4f56-44c5-b04b-8a7f3e5d3b32', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('cceaf865-f837-4db4-9b0a-bdcb5ad4582d', 'bc340d15-9d7c-4c94-b445-85cd0bb829fb', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('d221bd7a-6120-4eed-b04a-8d14eb6c7752', 'b37a4653-aef5-4f3b-b026-a528023eb5ee', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('d27e6ca1-237d-4ad7-aba5-f2a6d275afec', '1aa0da20-8831-4443-91b1-223fdfc3d79f', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('d3b231dd-0497-42ec-8973-fe12366de6f2', '37fd325a-b79f-4593-83c6-323c0efe72b4', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('d47028c0-5b18-411d-928c-31de90eaf9bf', '1fb864a0-899a-4656-8d3c-eba634a435ed', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('d4a54e64-ae23-48a3-bff6-d5aebb9228fa', '275bae6c-43c4-4b60-8e61-fbb874602cdd', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('d4def6d5-dc05-48a7-945f-7674d7d075d0', '607fd863-a9f8-4e67-8089-6825db39a724', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('d7e2fcad-04b2-4739-b629-9e94d6dc7f9b', 'user', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('d88fe5c5-382e-4033-b937-eb5589ba4116', '0294b07d-1358-449b-92f3-5ffbed8c73c0', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('d9395374-cba7-4f4f-92e2-af560ab84c43', 'fbb47357-9f3e-45c6-be02-da4976ec1e69', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('d985f5b4-33eb-41a3-b04c-99dd51329fc5', '286de048-c1aa-44ae-bd16-0e162ef03d33', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('db995dc7-3d92-4443-ac5f-ab80680c61fe', '6870af6b-af75-4789-bc2d-786fc3e90404', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('dd6f16ca-460c-4c3d-919d-8ab061c3989c', 'fc49ad54-4e34-42c0-8778-daba858c99d8', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('de907f67-1813-4758-a70d-ada66c51f7dc', '1aa0da20-8831-4443-91b1-223fdfc3d79f', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6');
INSERT INTO `tb_user_permission_ref` VALUES ('e034a7c4-8aaf-489a-a5da-8a5fae9c12cc', 'user', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('e0c3e516-6164-4d97-82b6-e89483e66bee', '64792613-f691-4e68-88dc-9fa97a91b323', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('e198f451-8d90-41bc-9cfa-0f14b5cb8281', 'f7cb4758-8101-4bbf-8aa1-5f3c6e9609fb', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('e4cebb7f-2d61-4a42-9256-26810072be5a', 'b648c1ec-a4d5-4df2-8e2f-8c06b650e655', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('e5adaf61-d8e4-4a06-83d4-67d6f0433b59', '278379cc-ea48-4237-b82a-4d90995a4195', '4c46f4e8-36db-411c-902c-94ff3668b259');
INSERT INTO `tb_user_permission_ref` VALUES ('e5fa1d15-a5e7-466c-a319-7fc20a9ad6c3', 'f8abbb63-2e15-406a-80d4-9028eaa923df', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('e633e202-52ed-4ac5-b0b4-982fd8c4b88b', 'd9e1aa02-1dfa-4e13-ab84-e13683e943c8', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('e63d2d1d-3072-4361-9081-345d9b0d32ca', '8f946ada-a880-4786-aecb-59ceedc5e27d', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('e6ea5cb3-5868-4826-9383-87e12e34d95d', 'a9e5a215-d9f4-4f97-9aec-1bf26a0b69a5', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('ea00dbe3-4d03-4f98-b92f-6e979196daa7', '30e8a0d1-106f-49af-9f63-69ae015d1868', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('eb49145f-fff2-4ae2-9ab4-7a65c01affe0', '278379cc-ea48-4237-b82a-4d90995a4195', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('eb5156ca-3fb3-4c3b-b07e-755ffe6ac6f9', '17db3c51-016a-428b-b2ee-bde45e565370', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('edebc7cd-4fd7-446f-918e-65e9b00afb4c', '8e875d16-fb3b-4390-8c98-3942e5e3fd8e', '9c8f7426-c913-4b64-b4b3-645587f8dee7');
INSERT INTO `tb_user_permission_ref` VALUES ('ee1f44b0-de49-4b95-b248-3754c10685f9', '1aa0da20-8831-4443-91b1-223fdfc3d79f', '7761d4a2-1685-495a-b410-59e74a3be9bd');
INSERT INTO `tb_user_permission_ref` VALUES ('f0d2757f-46c8-4b1a-9a70-60010cd431e3', '40b927cd-f6ec-4579-81a4-4d3e96ac7a70', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('f489bfd0-542f-4616-a2c3-38da6091f9ac', 'fbb47357-9f3e-45c6-be02-da4976ec1e69', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3');
INSERT INTO `tb_user_permission_ref` VALUES ('f9f6bba3-2ebe-4938-9b0e-ebb38741388a', 'user', '9c8f7426-c913-4b64-b4b3-645587f8dee7');

-- ----------------------------
-- Table structure for tb_user_role_ref
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role_ref`;
CREATE TABLE `tb_user_role_ref` (
  `REF_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `ROLE_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`REF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_user_role_ref
-- ----------------------------
INSERT INTO `tb_user_role_ref` VALUES ('60d0ebe6-b571-499b-8bfe-bbae5b0d7712', '4c46f4e8-36db-411c-902c-94ff3668b259', '78e033cb-b9e8-48a6-9a8e-5890069bc529');
INSERT INTO `tb_user_role_ref` VALUES ('64f233fa-9770-40b5-b1b7-ff5879358abb', '9c8f7426-c913-4b64-b4b3-645587f8dee7', '78e033cb-b9e8-48a6-9a8e-5890069bc529');
INSERT INTO `tb_user_role_ref` VALUES ('9f1453d2-fb77-4789-9d8e-907cf96a5d32', '7761d4a2-1685-495a-b410-59e74a3be9bd', '78e033cb-b9e8-48a6-9a8e-5890069bc529');
INSERT INTO `tb_user_role_ref` VALUES ('b469f308-d6d3-4486-a6b8-335c9cc0d79b', '2a7a45b6-d11d-4b3b-9a5d-0babcc881cf3', '0c7f6536-156d-468b-b81a-cfb5fd9918bc');
INSERT INTO `tb_user_role_ref` VALUES ('f12bf824-66b3-4924-b500-1853c49bbf12', '509b438c-d4d5-4c2c-8346-1521f5f0b7e6', '78e033cb-b9e8-48a6-9a8e-5890069bc529');
