package cn.com.do1.component.merchant.merchant.vo;

import cn.com.do1.common.annotation.bean.FormatMask;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzMerchantDetailVO {

    private java.lang.String id;
    private java.lang.String name;
    private java.lang.String payPassword;
    private java.lang.String money;
    @FormatMask(type = "number", value = "#0%")
    private java.lang.String discount;
    private java.lang.String mobile;
    private java.lang.String qq;
    private java.lang.String userId;
    private java.lang.String oper;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    private java.lang.String insertDate;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    private java.lang.String updateDate;
    private java.lang.String isClient;
    
    

    public java.lang.String getId() {
        return id;
    }



    public void setId(java.lang.String id) {
        this.id = id;
    }



    public java.lang.String getName() {
        return name;
    }



    public void setName(java.lang.String name) {
        this.name = name;
    }



    public java.lang.String getPayPassword() {
        return payPassword;
    }



    public void setPayPassword(java.lang.String payPassword) {
        this.payPassword = payPassword;
    }



    public java.lang.String getMoney() {
        return money;
    }



    public void setMoney(java.lang.String money) {
        this.money = money;
    }



    public java.lang.String getDiscount() {
        return discount;
    }



    public void setDiscount(java.lang.String discount) {
        this.discount = discount;
    }



    public java.lang.String getMobile() {
        return mobile;
    }



    public void setMobile(java.lang.String mobile) {
        this.mobile = mobile;
    }



    public java.lang.String getQq() {
        return qq;
    }



    public void setQq(java.lang.String qq) {
        this.qq = qq;
    }



    public java.lang.String getUserId() {
        return userId;
    }



    public void setUserId(java.lang.String userId) {
        this.userId = userId;
    }

    public java.lang.String getOper() {
        return oper;
    }



    public void setOper(java.lang.String oper) {
        this.oper = oper;
    }



    public java.lang.String getInsertDate() {
        return insertDate;
    }



    public void setInsertDate(java.lang.String insertDate) {
        this.insertDate = insertDate;
    }



    public java.lang.String getUpdateDate() {
        return updateDate;
    }



    public void setUpdateDate(java.lang.String updateDate) {
        this.updateDate = updateDate;
    }

    public java.lang.String getIsClient() {
        return isClient;
    }



    public void setIsClient(java.lang.String isClient) {
        this.isClient = isClient;
    }



    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
