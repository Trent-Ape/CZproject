package cn.com.do1.component.pay.payinfo.service.impl;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.BaseService;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.component.pay.payinfo.dao.*;
import cn.com.do1.component.pay.payinfo.service.IPayInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
@Service("payInfoService")
public class PayInfoServiceImpl extends BaseService implements IPayInfoService {
    private final static transient Logger logger = LoggerFactory.getLogger(PayInfoServiceImpl.class);

    private IPayInfoDAO payInfoDAO;

    @Resource
    public void setPayInfoDAO(IPayInfoDAO payInfoDAO) {
        this.payInfoDAO = payInfoDAO;
        setDAO(payInfoDAO);
    }

    public Pager searchPayInfo(Map searchMap, Pager pager) throws Exception, BaseException {
        return payInfoDAO.searchPayInfo(searchMap, pager);
    }
}
