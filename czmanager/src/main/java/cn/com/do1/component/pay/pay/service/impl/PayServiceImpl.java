package cn.com.do1.component.pay.pay.service.impl;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.BaseService;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.common.util.DateUtil;
import cn.com.do1.component.merchant.merchant.dao.IMerchantDAO;
import cn.com.do1.component.pay.pay.dao.*;
import cn.com.do1.component.pay.pay.model.TbCzPayPO;
import cn.com.do1.component.pay.pay.service.IPayService;
import cn.com.do1.component.pay.pay.vo.TbCzPayClientVO;
import cn.com.do1.component.pay.pay.vo.TbCzPayVO;
import cn.com.do1.component.pay.payinfo.model.TbCzPayInfoPO;
import cn.com.do1.component.util.DecimalFormatUtil;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
@Service("payService")
public class PayServiceImpl extends BaseService implements IPayService {
    private final static transient Logger logger = LoggerFactory.getLogger(PayServiceImpl.class);

    private IPayDAO payDAO;

    @Resource
    private IMerchantDAO chantDAO;

    @Resource
    public void setPayDAO(IPayDAO payDAO) {
        this.payDAO = payDAO;
        setDAO(payDAO);
    }

    public Pager searchPay(Map searchMap, Pager pager) throws Exception, BaseException {
        return payDAO.searchPay(searchMap, pager);
    }

    public void addPay(TbCzPayPO payPo) throws Exception, BaseException {

        chantDAO.minMoney(payPo.getPayMerchatId(), payPo.getPayDiscountMoney());

//        int maxOrder = this.payDAO.getMaxOrder();
//        maxOrder = maxOrder + 1;
//        payPo.setPayId(new SimpleDateFormat("yyyyMMdd").format(new Date())
//                + DecimalFormatUtil.getFormatNumber(maxOrder));

        String maxOrder = DateUtil.format(new Date(), "yyyyMMddHHmmssSS");
        payPo.setPayId(maxOrder);

        payDAO.insert(payPo);
    }

    public String addPayExt(TbCzPayPO payPo) throws Exception, BaseException {

        chantDAO.minMoney(payPo.getPayMerchatId(), payPo.getPayDiscountMoney());
//        int maxOrder = this.payDAO.getMaxOrder();
//        maxOrder = maxOrder + 1;
//        payPo.setPayId(new SimpleDateFormat("yyyyMMdd").format(new Date())
//                + DecimalFormatUtil.getFormatNumber(maxOrder));

        String maxOrder = DateUtil.format(new Date(), "yyyyMMddHHmmssSS");
        payPo.setPayId(maxOrder);

        payDAO.insert(payPo);

        return payPo.getPayId();
    }

    @Override
    public TbCzPayVO getTbCzPayByPayID(String id) throws Exception, BaseException {
        return payDAO.getTbCzPayByPayID(id);
    }

    @Override
    public TbCzPayClientVO getTbCzPayClientByPayID(String id) throws Exception, BaseException {
        return payDAO.getTbCzPayClientByPayID(id);
    }

    @Override
    public void payUpdateStatusCompile(String id, String oper) throws Exception, BaseException {

        payDAO.updatePayStatus(id, "2");
        TbCzPayInfoPO info = new TbCzPayInfoPO();
        info.setPayId(id);
        info.setStatus(2);
//        info.setUpdate(new Date());
        info.setOper(oper);
        info.setInfo("充值完成");

        super.insertPO(info, true);
    }

    @Override
    public void payUpdateStatusFail(String id) throws Exception, BaseException {
    }

    @Override
    public void payUpdateStatusMoney(String id) throws Exception, BaseException {
    }

    @Override
    public void payUpdateStatusAppeal(String id, String desc, String path) throws Exception, BaseException {
    }

    @Override
    public void payUpdateStatusAppealSuccess(String id) throws Exception, BaseException {
    }

    @Override
    public void payUpdateStatusAppealFail(String id) throws Exception, BaseException {
    }
}
