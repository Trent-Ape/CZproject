package cn.com.do1.component.baseweb.ui;

import cn.com.do1.common.framebase.dqdp.ErrorCode;
import cn.com.do1.common.framebase.struts.BaseAction;
import cn.com.do1.common.util.TreeObject;
import cn.com.do1.dqdp.core.menu.MenuItem;
import cn.com.do1.dqdp.core.menu.MenuMgr;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright © 2012 广州市道一信息技术有限公司
 * All rights reserved.
 * User: saron
 * Date: 12-3-6
 * Time: 下午5:15
 * ★★★★★★★★★★★★★★★★★★★★★★★★★★
 * ★                         Saron出品
 * ★★★★★★★★★★★★★★★★★★★★★★★★★★
 */
public class MainPageAction extends BaseAction {
    static Logger logger = Logger.getLogger(MainPageAction.class);
    public void getMainPageInfo() {
        try {
            List<MenuItem> currUserMenu = MenuMgr.getCurrUserMenu();
            List<TreeObject> treeList = new ArrayList<TreeObject>();
            for (MenuItem menuItem : currUserMenu) {
                TreeObject treeObject = menuItem.converToTreeObject();
                treeList.add(treeObject);
            }
            setActionResult("0", "首页信息获取成功");
            addJsonArray("tree", treeList);
        } catch (Exception e) {
            e.printStackTrace();
            setActionResult(ErrorCode.UNKOWN_EXCEPTION, "首页信息获取失败");
        }
        doJsonOut();
    }
}
