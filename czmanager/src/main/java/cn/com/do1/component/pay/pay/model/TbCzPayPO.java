package cn.com.do1.component.pay.pay.model;

import cn.com.do1.common.annotation.bean.PageView;
import cn.com.do1.common.annotation.bean.Validation;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.common.util.reflation.ConvertUtil;
import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;
import java.util.Date;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzPayPO implements IBaseDBVO {
    private java.lang.String payId;
    @Validation(must = false, length = 36, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "payMerchandiseId", showType = "input", showOrder = 1, showLength = 36)
    private java.lang.String payMerchandiseId;
    @Validation(must = false, length = 36, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "payMerchatId", showType = "input", showOrder = 2, showLength = 36)
    private java.lang.String payMerchatId;
    @Validation(must = false, length = 11, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "payMoblie", showType = "input", showOrder = 3, showLength = 11)
    private java.lang.String payMoblie;
    @Validation(must = false, length = 12, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "payOriginalMoney", showType = "input", showOrder = 4, showLength = 12)
    private java.lang.Float payOriginalMoney;
    @Validation(must = false, length = 12, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "payDiscountMoney", showType = "input", showOrder = 5, showLength = 12)
    private java.lang.Float payDiscountMoney;
    @Validation(must = false, length = 255, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "payStatus", showType = "input", showOrder = 6, showLength = 255)
    private java.lang.String payStatus;
    @Validation(must = false, length = 19, fieldType = "datetime", regex = "")
    @PageView(showName = "payStartDate", showType = "datetime", showOrder = 7, showLength = 19)
    private java.util.Date payStartDate;
    @Validation(must = false, length = 19, fieldType = "datetime", regex = "")
    @PageView(showName = "payEndDate", showType = "datetime", showOrder = 8, showLength = 19)
    private java.util.Date payEndDate;
    @Validation(must = false, length = 19, fieldType = "datetime", regex = "")
    @PageView(showName = "payUpdate", showType = "datetime", showOrder = 9, showLength = 19)
    private java.util.Date payUpdate;
    @Validation(must = false, length = 36, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "payOper", showType = "input", showOrder = 10, showLength = 36)
    private java.lang.String payOper;
    @Validation(must = false, length = 36, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "payOperDesc", showType = "input", showOrder = 11, showLength = 36)
    private java.lang.String payOperDesc;

    public void setPayId(java.lang.String payId) {
        this.payId = payId;
    }

    public java.lang.String getPayId() {
        return this.payId;
    }


    public void setPayMerchandiseId(java.lang.String payMerchandiseId) {
        this.payMerchandiseId = payMerchandiseId;
    }

    public java.lang.String getPayMerchandiseId() {
        return this.payMerchandiseId;
    }


    public void setPayMerchatId(java.lang.String payMerchatId) {
        this.payMerchatId = payMerchatId;
    }

    public java.lang.String getPayMerchatId() {
        return this.payMerchatId;
    }


    public void setPayMoblie(java.lang.String payMoblie) {
        this.payMoblie = payMoblie;
    }

    public java.lang.String getPayMoblie() {
        return this.payMoblie;
    }


    public void setPayOriginalMoney(java.lang.Float payOriginalMoney) {
        this.payOriginalMoney = payOriginalMoney;
    }

    public java.lang.Float getPayOriginalMoney() {
        return this.payOriginalMoney;
    }


    public void setPayDiscountMoney(java.lang.Float payDiscountMoney) {
        this.payDiscountMoney = payDiscountMoney;
    }

    public java.lang.Float getPayDiscountMoney() {
        return this.payDiscountMoney;
    }


    public void setPayStatus(java.lang.String payStatus) {
        this.payStatus = payStatus;
    }

    public java.lang.String getPayStatus() {
        return this.payStatus;
    }


    public void setPayStartDate(java.util.Date payStartDate) {
        this.payStartDate = payStartDate;
    }

    public void setPayStartDate(java.lang.String payStartDate) {
        this.payStartDate = ConvertUtil.cvStUtildate(payStartDate);
    }

    public java.util.Date getPayStartDate() {
        return this.payStartDate;
    }


    public void setPayEndDate(java.util.Date payEndDate) {
        this.payEndDate = payEndDate;
    }

    public void setPayEndDate(java.lang.String payEndDate) {
        this.payEndDate = ConvertUtil.cvStUtildate(payEndDate);
    }

    public java.util.Date getPayEndDate() {
        return this.payEndDate;
    }


    public void setPayUpdate(java.util.Date payUpdate) {
        this.payUpdate = payUpdate;
    }

    public void setPayUpdate(java.lang.String payUpdate) {
        this.payUpdate = ConvertUtil.cvStUtildate(payUpdate);
    }

    public java.util.Date getPayUpdate() {
        return this.payUpdate;
    }


    public void setPayOper(java.lang.String payOper) {
        this.payOper = payOper;
    }

    public java.lang.String getPayOper() {
        return this.payOper;
    }


    public void setPayOperDesc(java.lang.String payOperDesc) {
        this.payOperDesc = payOperDesc;
    }

    public java.lang.String getPayOperDesc() {
        return this.payOperDesc;
    }

    /**
     * 获取数据库中对应的表名
     * 
     * @return
     */
    public String _getTableName() {
        return "TB_CZ_PAY";
    }

    /**
     * 获取对应表的主键字段名称
     * 
     * @return
     */
    public String _getPKColumnName() {
        return "payId";
    }

    /**
     * 获取主键值
     * 
     * @return
     */
    public String _getPKValue() {
        return String.valueOf(payId);
    }

    /**
     * 设置主键的值
     * 
     * @return
     */
    public void _setPKValue(Object value) {
        this.payId = (java.lang.String) value;
    }

    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
