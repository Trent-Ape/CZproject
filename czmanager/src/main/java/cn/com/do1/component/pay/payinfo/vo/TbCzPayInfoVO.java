package cn.com.do1.component.pay.payinfo.vo;

import cn.com.do1.common.annotation.bean.PageView;
import cn.com.do1.common.annotation.bean.Validation;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.common.util.reflation.ConvertUtil;
import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;
import java.util.Date;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzPayInfoVO {
    private java.lang.String id;
    private java.lang.String payId;
    private java.lang.String status;
    private java.lang.String info;
    private java.lang.String path;
    private java.lang.String oper;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    private java.lang.String updateDate;

    @DictDesc(refField = "status", typeName = "payStatus")
    private java.lang.String statusDesc;

    public java.lang.String getId() {
        return id;
    }



    public void setId(java.lang.String id) {
        this.id = id;
    }



    public java.lang.String getPayId() {
        return payId;
    }



    public void setPayId(java.lang.String payId) {
        this.payId = payId;
    }



    public java.lang.String getStatus() {
        return status;
    }



    public void setStatus(java.lang.String status) {
        this.status = status;
    }



    public java.lang.String getInfo() {
        return info;
    }



    public void setInfo(java.lang.String info) {
        this.info = info;
    }



    public java.lang.String getPath() {
        return path;
    }



    public void setPath(java.lang.String path) {
        this.path = path;
    }



    public java.lang.String getOper() {
        return oper;
    }



    public void setOper(java.lang.String oper) {
        this.oper = oper;
    }



    public java.lang.String getUpdateDate() {
        return updateDate;
    }



    public void setUpdateDate(java.lang.String updateDate) {
        this.updateDate = updateDate;
    }



    public java.lang.String getStatusDesc() {
        return statusDesc;
    }



    public void setStatusDesc(java.lang.String statusDesc) {
        this.statusDesc = statusDesc;
    }



    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
