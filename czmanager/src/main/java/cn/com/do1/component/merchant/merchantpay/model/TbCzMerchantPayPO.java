package cn.com.do1.component.merchant.merchantpay.model;

import cn.com.do1.common.annotation.bean.PageView;
import cn.com.do1.common.annotation.bean.Validation;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.common.util.reflation.ConvertUtil;
import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;
import java.util.Date;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzMerchantPayPO implements IBaseDBVO {
    private java.lang.String tbMerchatPayId;
    @Validation(must = false, length = 36, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "tbMerchatPayUserid", showType = "input", showOrder = 1, showLength = 36)
    private java.lang.String tbMerchatPayUserid;
    @Validation(must = false, length = 11, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "tbMerchatPayMoney", showType = "input", showOrder = 2, showLength = 11)
    private java.lang.Float tbMerchatPayMoney;
    @Validation(must = false, length = 1, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "tbMerchatPayStatus", showType = "input", showOrder = 3, showLength = 1)
    private java.lang.Integer tbMerchatPayStatus;
    @Validation(must = false, length = 19, fieldType = "datetime", regex = "")
    @PageView(showName = "tbMerchatPayInsert", showType = "datetime", showOrder = 4, showLength = 19)
    private java.util.Date tbMerchatPayInsert;
    @Validation(must = false, length = 19, fieldType = "datetime", regex = "")
    @PageView(showName = "tbMerchatPayUpdate", showType = "datetime", showOrder = 5, showLength = 19)
    private java.util.Date tbMerchatPayUpdate;
    @Validation(must = false, length = 36, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "tbMerchatPayOper", showType = "input", showOrder = 6, showLength = 36)
    private java.lang.String tbMerchatPayOper;

    public void setTbMerchatPayId(java.lang.String tbMerchatPayId) {
        this.tbMerchatPayId = tbMerchatPayId;
    }

    public java.lang.String getTbMerchatPayId() {
        return this.tbMerchatPayId;
    }


    public void setTbMerchatPayUserid(java.lang.String tbMerchatPayUserid) {
        this.tbMerchatPayUserid = tbMerchatPayUserid;
    }

    public java.lang.String getTbMerchatPayUserid() {
        return this.tbMerchatPayUserid;
    }


    public void setTbMerchatPayMoney(java.lang.Float tbMerchatPayMoney) {
        this.tbMerchatPayMoney = tbMerchatPayMoney;
    }

    public java.lang.Float getTbMerchatPayMoney() {
        return this.tbMerchatPayMoney;
    }


    public void setTbMerchatPayStatus(java.lang.Integer tbMerchatPayStatus) {
        this.tbMerchatPayStatus = tbMerchatPayStatus;
    }

    public java.lang.Integer getTbMerchatPayStatus() {
        return this.tbMerchatPayStatus;
    }


    public void setTbMerchatPayInsert(java.util.Date tbMerchatPayInsert) {
        this.tbMerchatPayInsert = tbMerchatPayInsert;
    }

    public void setTbMerchatPayInsert(java.lang.String tbMerchatPayInsert) {
        this.tbMerchatPayInsert = ConvertUtil.cvStUtildate(tbMerchatPayInsert);
    }

    public java.util.Date getTbMerchatPayInsert() {
        return this.tbMerchatPayInsert;
    }


    public void setTbMerchatPayUpdate(java.util.Date tbMerchatPayUpdate) {
        this.tbMerchatPayUpdate = tbMerchatPayUpdate;
    }

    public void setTbMerchatPayUpdate(java.lang.String tbMerchatPayUpdate) {
        this.tbMerchatPayUpdate = ConvertUtil.cvStUtildate(tbMerchatPayUpdate);
    }

    public java.util.Date getTbMerchatPayUpdate() {
        return this.tbMerchatPayUpdate;
    }


    public void setTbMerchatPayOper(java.lang.String tbMerchatPayOper) {
        this.tbMerchatPayOper = tbMerchatPayOper;
    }

    public java.lang.String getTbMerchatPayOper() {
        return this.tbMerchatPayOper;
    }

    /**
     * 获取数据库中对应的表名
     * 
     * @return
     */
    public String _getTableName() {
        return "TB_CZ_MERCHANT_PAY";
    }

    /**
     * 获取对应表的主键字段名称
     * 
     * @return
     */
    public String _getPKColumnName() {
        return "tbMerchatPayId";
    }

    /**
     * 获取主键值
     * 
     * @return
     */
    public String _getPKValue() {
        return String.valueOf(tbMerchatPayId);
    }

    /**
     * 设置主键的值
     * 
     * @return
     */
    public void _setPKValue(Object value) {
        this.tbMerchatPayId = (java.lang.String) value;
    }

    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
