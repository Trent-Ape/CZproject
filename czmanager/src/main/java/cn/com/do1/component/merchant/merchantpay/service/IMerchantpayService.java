package cn.com.do1.component.merchant.merchantpay.service;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.framebase.dqdp.IBaseService;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.component.merchant.merchantpay.model.TbCzMerchantPayPO;

import java.util.Map;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public interface IMerchantpayService extends IBaseService {

    Pager searchMerchantpay(Map searchMap, Pager pager) throws Exception, BaseException;

    public Pager searchMyMerchantpay(Map searchMap, Pager pager) throws Exception, BaseException;
    
    void addMerchantpay(TbCzMerchantPayPO tbCzMerchantPayPO) throws Exception, BaseException;

}
