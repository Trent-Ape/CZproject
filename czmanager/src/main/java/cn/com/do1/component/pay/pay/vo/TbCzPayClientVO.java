package cn.com.do1.component.pay.pay.vo;

import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;
import cn.com.do1.component.txtUtil.annotation.TitleAnnotation;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzPayClientVO {
    @TitleAnnotation(titleName = "订单号")
    private String payId;
    private String payMerchandiseId;
    @TitleAnnotation(titleName = "套餐名称")
    private String merName;
    private String payMerchatId;
    @TitleAnnotation(titleName = "代理商名称")
    private String name;
    @TitleAnnotation(titleName = "代理商账号")
    private String userName;
    @TitleAnnotation(titleName = "充值手机")
    private String payMoblie;
    @TitleAnnotation(titleName = "套餐原价")
    private String payOriginalMoney;
    @TitleAnnotation(titleName = "折扣价格")
    private String payDiscountMoney;
    private String payStatus;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    @TitleAnnotation(titleName = "充值请求时间")
    private String payStartDate;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    @TitleAnnotation(titleName = "充值完成时间")
    private String payEndDate;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    @TitleAnnotation(titleName = "更新时间")
    private String payUpdate;

    @DictDesc(refField = "payStatus", typeName = "payStatus")
    @TitleAnnotation(titleName = "充值状态")
    private String payStatusDesc;


    public String getPayId() {
        return payId;
    }



    public void setPayId(String payId) {
        this.payId = payId;
    }



    public String getPayMerchandiseId() {
        return payMerchandiseId;
    }



    public void setPayMerchandiseId(String payMerchandiseId) {
        this.payMerchandiseId = payMerchandiseId;
    }



    public String getMerName() {
        return merName;
    }



    public void setMerName(String merName) {
        this.merName = merName;
    }



    public String getPayMerchatId() {
        return payMerchatId;
    }



    public void setPayMerchatId(String payMerchatId) {
        this.payMerchatId = payMerchatId;
    }



    public String getName() {
        return name;
    }



    public void setName(String name) {
        this.name = name;
    }



    public String getPayMoblie() {
        return payMoblie;
    }



    public void setPayMoblie(String payMoblie) {
        this.payMoblie = payMoblie;
    }



    public String getPayOriginalMoney() {
        return payOriginalMoney;
    }



    public void setPayOriginalMoney(String payOriginalMoney) {
        this.payOriginalMoney = payOriginalMoney;
    }



    public String getPayDiscountMoney() {
        return payDiscountMoney;
    }



    public void setPayDiscountMoney(String payDiscountMoney) {
        this.payDiscountMoney = payDiscountMoney;
    }



    public String getPayStatus() {
        return payStatus;
    }



    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }



    public String getPayStartDate() {
        return payStartDate;
    }



    public void setPayStartDate(String payStartDate) {
        this.payStartDate = payStartDate;
    }



    public String getPayEndDate() {
        return payEndDate;
    }



    public void setPayEndDate(String payEndDate) {
        this.payEndDate = payEndDate;
    }



    public String getPayUpdate() {
        return payUpdate;
    }



    public void setPayUpdate(String payUpdate) {
        this.payUpdate = payUpdate;
    }

    public String getPayStatusDesc() {
        return payStatusDesc;
    }



    public void setPayStatusDesc(String payStatusDesc) {
        this.payStatusDesc = payStatusDesc;
    }

    public String getUserName() {
        return userName;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }


    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
