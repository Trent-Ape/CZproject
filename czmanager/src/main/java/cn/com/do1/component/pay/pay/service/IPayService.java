package cn.com.do1.component.pay.pay.service;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.framebase.dqdp.IBaseService;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.component.pay.pay.model.TbCzPayPO;
import cn.com.do1.component.pay.pay.vo.TbCzPayClientVO;
import cn.com.do1.component.pay.pay.vo.TbCzPayVO;

import java.util.Map;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public interface IPayService extends IBaseService {
    Pager searchPay(Map searchMap, Pager pager) throws Exception, BaseException;

    TbCzPayVO getTbCzPayByPayID(String id) throws Exception, BaseException;

    TbCzPayClientVO getTbCzPayClientByPayID(String id) throws Exception, BaseException;

    void addPay(TbCzPayPO payPo) throws Exception, BaseException;

    String addPayExt(TbCzPayPO payPo) throws Exception, BaseException;

    void payUpdateStatusCompile(String id, String oper) throws Exception, BaseException;

    void payUpdateStatusFail(String id) throws Exception, BaseException;

    void payUpdateStatusMoney(String id) throws Exception, BaseException;

    void payUpdateStatusAppeal(String id, String desc, String path) throws Exception, BaseException;

    void payUpdateStatusAppealSuccess(String id) throws Exception, BaseException;

    void payUpdateStatusAppealFail(String id) throws Exception, BaseException;


}
