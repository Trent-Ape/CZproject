package cn.com.do1.component.util;

import java.text.DecimalFormat;

public class DecimalFormatUtil {
    public static String defaultPattern = "00000000";

    public static String getFormatNumber(Object obj, String pattern) {
        DecimalFormat fm = new DecimalFormat(pattern);
        return fm.format(obj);
    }

    public static String getFormatNumber(Object obj) {
        DecimalFormat fm = new DecimalFormat(defaultPattern);
        return fm.format(obj);
    }
}
