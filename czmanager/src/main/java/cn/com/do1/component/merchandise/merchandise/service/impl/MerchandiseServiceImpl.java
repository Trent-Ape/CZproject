package cn.com.do1.component.merchandise.merchandise.service.impl;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.BaseService;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.component.merchandise.merchandise.dao.*;
import cn.com.do1.component.merchandise.merchandise.model.TbCzMerchandisePO;
import cn.com.do1.component.merchandise.merchandise.service.IMerchandiseService;
import cn.com.do1.component.merchandise.merchandise.vo.TbCzMerchandiseVO;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
@Service("merchandiseService")
public class MerchandiseServiceImpl extends BaseService implements IMerchandiseService {
    private final static transient Logger logger = LoggerFactory.getLogger(MerchandiseServiceImpl.class);

    private IMerchandiseDAO merchandiseDAO;

    @Resource
    public void setMerchandiseDAO(IMerchandiseDAO merchandiseDAO) {
        this.merchandiseDAO = merchandiseDAO;
        setDAO(merchandiseDAO);
    }

    public Pager searchMerchandise(Map searchMap, Pager pager) throws Exception, BaseException {
        return merchandiseDAO.searchMerchandise(searchMap, pager);
    }

    @Override
    public void updateStatus(String id, String status) throws Exception, BaseException {
        merchandiseDAO.updateStatus(id, status);
    }

    @Override
    public List<TbCzMerchandiseVO> getMerchandiseByStatus(String status) throws Exception, BaseException {
        return merchandiseDAO.getMerchandiseByStatus(status);
    }

    @Override
    public List<TbCzMerchandiseVO> getTop10UseMerchandise() throws Exception, BaseException {
        return merchandiseDAO.getTop10UseMerchandise();
    }

    @Override
    public List<TbCzMerchandiseVO> getUseMerchandise() throws Exception, BaseException {
        return this.getMerchandiseByStatus("1");
    }
    
    @Override
    public List<TbCzMerchandiseVO> getMerchandise() throws Exception, BaseException {
        return merchandiseDAO.getMerchandise();
    }
}
