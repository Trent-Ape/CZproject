package cn.com.do1.component.merchant.merchant.vo;

import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;
import cn.com.do1.component.txtUtil.annotation.TitleAnnotation;


/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzMerchantListVO {

    private String userId;
    @TitleAnnotation(titleName = "代理商账号")
    private String userName;
    private String status;
    private String password;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    private String lastLoginTime;
    @DictDesc(refField = "status", typeName = "userStatus")
    @TitleAnnotation(titleName = "状态")
    private String statusDesc;
    private String personId;
    @TitleAnnotation(titleName = "代理商名称")
    private String personName;
    private String sex;
    @DictDesc(refField = "sex", typeName = "personSex")
    private String sexDesc;
    private String age;
    private String roleIds;
    private String isInternal;
    private String orgName;
    private String orgId;

    private java.lang.String id;
    private java.lang.String name;
    private java.lang.String payPassword;
    @TitleAnnotation(titleName = "代理商余额")
    private java.lang.String money;
    @FormatMask(type = "number", value = "#0%")
    @TitleAnnotation(titleName = "折扣")
    private java.lang.String discount;
    @TitleAnnotation(titleName = "手机号码")
    private java.lang.String mobile;
    @TitleAnnotation(titleName = "qq号码")
    private java.lang.String qq;
    @TitleAnnotation(titleName = "操作人")
    private java.lang.String oper;
    @TitleAnnotation(titleName = "注册时间")
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    private java.lang.String insertDate;
    @TitleAnnotation(titleName = "更新时间")
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    private java.lang.String updateDate;

    private java.lang.String isClient;

    public String getUserId() {
        return userId;
    }



    public void setUserId(String userId) {
        this.userId = userId;
    }



    public String getUserName() {
        return userName;
    }



    public void setUserName(String userName) {
        this.userName = userName;
    }



    public String getStatus() {
        return status;
    }



    public void setStatus(String status) {
        this.status = status;
    }



    public String getPassword() {
        return password;
    }



    public void setPassword(String password) {
        this.password = password;
    }



    public String getLastLoginTime() {
        return lastLoginTime;
    }



    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }



    public String getStatusDesc() {
        return statusDesc;
    }



    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }



    public String getPersonId() {
        return personId;
    }



    public void setPersonId(String personId) {
        this.personId = personId;
    }



    public String getPersonName() {
        return personName;
    }



    public void setPersonName(String personName) {
        this.personName = personName;
    }



    public String getSex() {
        return sex;
    }



    public void setSex(String sex) {
        this.sex = sex;
    }



    public String getSexDesc() {
        return sexDesc;
    }



    public void setSexDesc(String sexDesc) {
        this.sexDesc = sexDesc;
    }



    public String getAge() {
        return age;
    }



    public void setAge(String age) {
        this.age = age;
    }



    public String getRoleIds() {
        return roleIds;
    }



    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }



    public String getIsInternal() {
        return isInternal;
    }



    public void setIsInternal(String isInternal) {
        this.isInternal = isInternal;
    }



    public String getOrgName() {
        return orgName;
    }



    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }



    public String getOrgId() {
        return orgId;
    }



    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }



    public java.lang.String getId() {
        return id;
    }



    public void setId(java.lang.String id) {
        this.id = id;
    }



    public java.lang.String getName() {
        return name;
    }



    public void setName(java.lang.String name) {
        this.name = name;
    }



    public java.lang.String getPayPassword() {
        return payPassword;
    }



    public void setPayPassword(java.lang.String payPassword) {
        this.payPassword = payPassword;
    }



    public java.lang.String getMoney() {
        return money;
    }



    public void setMoney(java.lang.String money) {
        this.money = money;
    }



    public java.lang.String getDiscount() {
        return discount;
    }



    public void setDiscount(java.lang.String discount) {
        this.discount = discount;
    }



    public java.lang.String getMobile() {
        return mobile;
    }



    public void setMobile(java.lang.String mobile) {
        this.mobile = mobile;
    }



    public java.lang.String getQq() {
        return qq;
    }



    public void setQq(java.lang.String qq) {
        this.qq = qq;
    }



    public java.lang.String getOper() {
        return oper;
    }



    public void setOper(java.lang.String oper) {
        this.oper = oper;
    }



    public java.lang.String getInsertDate() {
        return insertDate;
    }



    public void setInsertDate(java.lang.String insertDate) {
        this.insertDate = insertDate;
    }



    public java.lang.String getUpdateDate() {
        return updateDate;
    }



    public void setUpdateDate(java.lang.String updateDate) {
        this.updateDate = updateDate;
    }


    public java.lang.String getIsClient() {
        return isClient;
    }



    public void setIsClient(java.lang.String isClient) {
        this.isClient = isClient;
    }



    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
