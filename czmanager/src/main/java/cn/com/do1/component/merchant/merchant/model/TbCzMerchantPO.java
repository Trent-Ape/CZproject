package cn.com.do1.component.merchant.merchant.model;

import cn.com.do1.common.annotation.bean.PageView;
import cn.com.do1.common.annotation.bean.Validation;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.common.util.reflation.ConvertUtil;
import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;
import java.util.Date;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzMerchantPO implements IBaseDBVO {
    private java.lang.String id;
    @Validation(must = false, length = 200, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "name", showType = "input", showOrder = 1, showLength = 200)
    private java.lang.String name;
    @Validation(must = false, length = 200, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "payPassword", showType = "input", showOrder = 2, showLength = 200)
    private java.lang.String payPassword;
    @Validation(must = false, length = 11, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "money", showType = "input", showOrder = 3, showLength = 11)
    private java.lang.Float money;
    @Validation(must = false, length = 3, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "discount", showType = "input", showOrder = 4, showLength = 3)
    private java.lang.Float discount;
    @Validation(must = false, length = 11, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "mobile", showType = "input", showOrder = 5, showLength = 11)
    private java.lang.String mobile;
    @Validation(must = false, length = 15, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "qq", showType = "input", showOrder = 6, showLength = 15)
    private java.lang.String qq;
    @Validation(must = false, length = 36, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "userId", showType = "input", showOrder = 7, showLength = 36)
    private java.lang.String userId;
    @Validation(must = false, length = 36, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "oper", showType = "input", showOrder = 8, showLength = 36)
    private java.lang.String oper;
    @Validation(must = false, length = 19, fieldType = "datetime", regex = "")
    @PageView(showName = "insertDate", showType = "datetime", showOrder = 9, showLength = 19)
    private java.util.Date insertDate;
    @Validation(must = false, length = 19, fieldType = "datetime", regex = "")
    @PageView(showName = "updateDate", showType = "datetime", showOrder = 10, showLength = 19)
    private java.util.Date updateDate;
    @Validation(must = false, length = 11, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "isClient", showType = "input", showOrder = 11, showLength = 11)
    private java.lang.Integer isClient;

    public void setId(java.lang.String id) {
        this.id = id;
    }

    public java.lang.String getId() {
        return this.id;
    }


    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getName() {
        return this.name;
    }


    public void setPayPassword(java.lang.String payPassword) {
        this.payPassword = payPassword;
    }

    public java.lang.String getPayPassword() {
        return this.payPassword;
    }


    public void setMoney(java.lang.Float money) {
        this.money = money;
    }

    public void setMoney(java.lang.String money) {
        this.money = ConvertUtil.Str2Float(money);
    }

    public java.lang.Float getMoney() {
        return this.money;
    }


    public void setDiscount(java.lang.Float discount) {
        this.discount = discount;
    }

    public void setDiscount(java.lang.String discount) {
        this.discount = ConvertUtil.Str2Float(discount);
    }

    public java.lang.Float getDiscount() {
        return this.discount;
    }


    public void setMobile(java.lang.String mobile) {
        this.mobile = mobile;
    }

    public java.lang.String getMobile() {
        return this.mobile;
    }

    public void setQq(java.lang.String qq) {
        this.qq = qq;
    }

    public java.lang.String getQq() {
        return this.qq;
    }

    public void setUserId(java.lang.String userId) {
        this.userId = userId;
    }

    public java.lang.String getUserId() {
        return this.userId;
    }

    public java.lang.String getOper() {
        return oper;
    }

    public void setOper(java.lang.String oper) {
        this.oper = oper;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public void setInsertDate(java.lang.String insertDate) {
        this.insertDate = ConvertUtil.cvStUtildate(insertDate);
    }

    public java.util.Date getInsertDate() {
        return this.insertDate;
    }


    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setUpdateDate(java.lang.String updateDate) {
        this.updateDate = ConvertUtil.cvStUtildate(updateDate);
    }

    public java.util.Date getUpdateDate() {
        return this.updateDate;
    }


    public void setIsClient(java.lang.Integer isClient) {
        this.isClient = isClient;
    }

    public void setIsClient(java.lang.String isClient) {
        this.isClient = ConvertUtil.cvStIntg(isClient);
    }

    public java.lang.Integer getIsClient() {
        return this.isClient;
    }

    /**
     * 获取数据库中对应的表名
     * 
     * @return
     */
    public String _getTableName() {
        return "TB_CZ_MERCHANT";
    }

    /**
     * 获取对应表的主键字段名称
     * 
     * @return
     */
    public String _getPKColumnName() {
        return "id";
    }

    /**
     * 获取主键值
     * 
     * @return
     */
    public String _getPKValue() {
        return String.valueOf(id);
    }

    /**
     * 设置主键的值
     * 
     * @return
     */
    public void _setPKValue(Object value) {
        this.id = (java.lang.String) value;
    }

    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
