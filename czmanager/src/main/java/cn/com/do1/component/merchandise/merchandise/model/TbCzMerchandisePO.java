package cn.com.do1.component.merchandise.merchandise.model;

import cn.com.do1.common.annotation.bean.PageView;
import cn.com.do1.common.annotation.bean.Validation;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.common.util.reflation.ConvertUtil;
import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;
import java.util.Date;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzMerchandisePO implements IBaseDBVO {
    private java.lang.String merId;
    @Validation(must = false, length = 200, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "merName", showType = "input", showOrder = 1, showLength = 200)
    private java.lang.String merName;
    @Validation(must = false, length = 11, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "merMoney", showType = "input", showOrder = 2, showLength = 11)
    private java.lang.Float merMoney;
    @Validation(must = false, length = 11, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "merOrder", showType = "input", showOrder = 3, showLength = 11)
    private java.lang.Integer merOrder;
    @Validation(must = false, length = 1, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "merStatus", showType = "input", showOrder = 4, showLength = 1)
    private java.lang.Integer merStatus;
    @Validation(must = false, length = 19, fieldType = "datetime", regex = "")
    @PageView(showName = "merInsert", showType = "datetime", showOrder = 5, showLength = 19)
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    private java.util.Date merInsert;
    @Validation(must = false, length = 19, fieldType = "datetime", regex = "")
    @PageView(showName = "merUpdate", showType = "datetime", showOrder = 6, showLength = 19)
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    private java.util.Date merUpdate;
    @Validation(must = false, length = 36, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "merOper", showType = "input", showOrder = 7, showLength = 36)
    private java.lang.String merOper;
    

    public void setMerId(java.lang.String merId) {
        this.merId = merId;
    }

    public java.lang.String getMerId() {
        return this.merId;
    }


    public void setMerName(java.lang.String merName) {
        this.merName = merName;
    }

    public java.lang.String getMerName() {
        return this.merName;
    }


    public void setMerMoney(java.lang.Float merMoney) {
        this.merMoney = merMoney;
    }

    public java.lang.Float getMerMoney() {
        return this.merMoney;
    }


    public void setMerOrder(java.lang.Integer merOrder) {
        this.merOrder = merOrder;
    }

    public java.lang.Integer getMerOrder() {
        return this.merOrder;
    }


    public void setMerStatus(java.lang.Integer merStatus) {
        this.merStatus = merStatus;
    }

    public java.lang.Integer getMerStatus() {
        return this.merStatus;
    }


    public void setMerInsert(java.util.Date merInsert) {
        this.merInsert = merInsert;
    }

    public void setMerInsert(java.lang.String merInsert) {
        this.merInsert = ConvertUtil.cvStUtildate(merInsert);
    }

    public java.util.Date getMerInsert() {
        return this.merInsert;
    }


    public void setMerUpdate(java.util.Date merUpdate) {
        this.merUpdate = merUpdate;
    }

    public void setMerUpdate(java.lang.String merUpdate) {
        this.merUpdate = ConvertUtil.cvStUtildate(merUpdate);
    }

    public java.util.Date getMerUpdate() {
        return this.merUpdate;
    }


    public void setMerOper(java.lang.String merOper) {
        this.merOper = merOper;
    }

    public java.lang.String getMerOper() {
        return this.merOper;
    }

    /**
     * 获取数据库中对应的表名
     * 
     * @return
     */
    public String _getTableName() {
        return "TB_CZ_MERCHANDISE";
    }

    /**
     * 获取对应表的主键字段名称
     * 
     * @return
     */
    public String _getPKColumnName() {
        return "merId";
    }

    /**
     * 获取主键值
     * 
     * @return
     */
    public String _getPKValue() {
        return String.valueOf(merId);
    }

    /**
     * 设置主键的值
     * 
     * @return
     */
    public void _setPKValue(Object value) {
        this.merId = (java.lang.String) value;
    }

    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
