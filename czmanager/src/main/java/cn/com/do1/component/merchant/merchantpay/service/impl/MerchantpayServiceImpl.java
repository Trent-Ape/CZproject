package cn.com.do1.component.merchant.merchantpay.service.impl;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.BaseService;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.common.util.AssertUtil;
import cn.com.do1.common.util.DateUtil;
import cn.com.do1.component.merchant.merchant.dao.IMerchantDAO;
import cn.com.do1.component.merchant.merchantpay.dao.*;
import cn.com.do1.component.merchant.merchantpay.model.TbCzMerchantPayPO;
import cn.com.do1.component.merchant.merchantpay.service.IMerchantpayService;
import cn.com.do1.component.util.DecimalFormatUtil;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
@Service("merchantpayService")
public class MerchantpayServiceImpl extends BaseService implements IMerchantpayService {
    private final static transient Logger logger = LoggerFactory.getLogger(MerchantpayServiceImpl.class);

    private IMerchantpayDAO merchantpayDAO;

    @Resource
    private IMerchantDAO merchantDAO;

    @Resource
    public void setMerchantpayDAO(IMerchantpayDAO merchantpayDAO) {
        this.merchantpayDAO = merchantpayDAO;
        setDAO(merchantpayDAO);
    }

    public Pager searchMerchantpay(Map searchMap, Pager pager) throws Exception, BaseException {
        return merchantpayDAO.searchMerchantpay(searchMap, pager);
    }

    public Pager searchMyMerchantpay(Map searchMap, Pager pager) throws Exception, BaseException {
        return merchantpayDAO.searchMyMerchantpay(searchMap, pager);
    }

    @Override
    public void addMerchantpay(TbCzMerchantPayPO tbCzMerchantPayPO) throws Exception, BaseException {
//        int maxOrder = this.merchantpayDAO.getMaxOrder();
//        maxOrder = maxOrder + 1;
//        tbCzMerchantPayPO.setTbMerchatPayId(new SimpleDateFormat("yyyyMMdd").format(new Date())
//                + DecimalFormatUtil.getFormatNumber(maxOrder));

        String maxOrder = DateUtil.format(new Date(), "yyyyMMddHHmmssSS");
        tbCzMerchantPayPO.setTbMerchatPayId(maxOrder);

        tbCzMerchantPayPO.setTbMerchatPayStatus(2);
        this.merchantpayDAO.insert(tbCzMerchantPayPO);

        this.merchantDAO.addMoney(tbCzMerchantPayPO.getTbMerchatPayUserid(), tbCzMerchantPayPO.getTbMerchatPayMoney().floatValue());

    }
}
