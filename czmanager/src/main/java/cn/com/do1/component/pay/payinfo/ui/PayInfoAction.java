package cn.com.do1.component.pay.payinfo.ui;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.annotation.struts.*;
import cn.com.do1.common.exception.IllegalParameterException;
import cn.com.do1.common.framebase.struts.BaseAction;
import cn.com.do1.component.fileupload.util.ConstConfig;
import cn.com.do1.component.fileupload.util.FileUpload;
import cn.com.do1.component.fileupload.util.ResultTip;
import cn.com.do1.component.fileupload.util.StringUtil;
import cn.com.do1.component.pay.pay.model.TbCzPayPO;
import cn.com.do1.component.pay.pay.service.IPayService;
import cn.com.do1.component.pay.payinfo.model.*;
import cn.com.do1.component.pay.payinfo.service.IPayInfoService;
import cn.com.do1.component.systemmgr.user.model.User;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import cn.com.do1.common.exception.BaseException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.sql.SQLException;

import cn.com.do1.common.util.AssertUtil;
import cn.com.do1.common.util.DateUtil;
import cn.com.do1.dqdp.core.DqdpAppContext;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class PayInfoAction extends BaseAction {
    private final static transient Logger logger = LoggerFactory.getLogger(PayInfoAction.class);
    private IPayInfoService payInfoService;
    private TbCzPayInfoPO tbCzPayInfoPO;
    private TbCzPayPO tbCzPayPO;
    private String ids[];
    private String id;

    private File upFile;
    private String upFileFileName;

    private String status;
    private String reFilePath;
    private String payDesc;


    public TbCzPayPO getTbCzPayPO() {
        return tbCzPayPO;
    }

    public void setTbCzPayPO(TbCzPayPO tbCzPayPO) {
        this.tbCzPayPO = tbCzPayPO;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReFilePath() {
        return reFilePath;
    }

    public void setReFilePath(String reFilePath) {
        this.reFilePath = reFilePath;
    }

    public String getPayDesc() {
        return payDesc;
    }

    public void setPayDesc(String payDesc) {
        this.payDesc = payDesc;
    }

    public IPayService getPayService() {
        return payService;
    }

    public void setPayService(IPayService payService) {
        this.payService = payService;
    }

    @Resource
    private IPayService payService;

    public File getUpFile() {
        return upFile;
    }

    public void setUpFile(File upFile) {
        this.upFile = upFile;
    }


    public String getUpFileFileName() {
        return upFileFileName;
    }

    public void setUpFileFileName(String upFileFileName) {
        this.upFileFileName = upFileFileName;
    }

    public IPayInfoService getPayInfoService() {
        return payInfoService;
    }

    @Resource
    public void setPayInfoService(IPayInfoService payInfoService) {
        this.payInfoService = payInfoService;
    }

    public String[] getIds() {
        return ids;
    }

    public void setIds(String[] ids) {
        this.ids = ids;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * 列表查询时，页面要传递的参数
     */
    @SearchValueTypes(nameFormat = "false", value = {
            @SearchValueType(name = "testDate", type = "date", format = "yyyy-MM-dd HH:mm:ss"),
            @SearchValueType(name = "testNumber", type = "number"),
            @SearchValueType(name = "testString", type = "string", format = "%%%s%%")})
    @JSONOut(catchException = @CatchException(errCode = "1001", successMsg = "查询成功", faileMsg = "查询失败"))
    public void ajaxSearch() throws Exception, BaseException {
        Pager pager = new Pager(ServletActionContext.getRequest(), getPageSize());
        pager = payInfoService.searchPayInfo(getSearchValue(), pager);
        addJsonPager("pageData", pager);
    }

    @JSONOut(catchException = @CatchException(errCode = "1002", successMsg = "新增成功", faileMsg = "新增失败"))
    public void ajaxAdd() throws Exception, BaseException {
        // todo:完成新增的代码;
    }

    @JSONOut(catchException = @CatchException(errCode = "1003", successMsg = "更新成功", faileMsg = "更新失败"))
    public void ajaxUpdate() throws Exception, BaseException {
        // todo:完成更新的代码;
    }

    @JSONOut(catchException = @CatchException(errCode = "1004", successMsg = "删除成功", faileMsg = "删除失败"))
    public void ajaxBatchDelete() throws Exception, BaseException {
        // 完成批量更新的代码
    }

    public void setTbCzPayInfoPO(TbCzPayInfoPO tbCzPayInfoPO) {
        this.tbCzPayInfoPO = tbCzPayInfoPO;
    }

    public TbCzPayInfoPO setTbCzPayInfoPO() {
        return this.tbCzPayInfoPO;
    }

    public void addTbCzPayInfoPO() {
        super.ajaxAdd(tbCzPayInfoPO);
    }

    public void updateTbCzPayInfoPO() {
        super.ajaxUpdate(tbCzPayInfoPO);
    }

    public void deleteTbCzPayInfoPO() {
        if (AssertUtil.isEmpty(id))
            id = ids[0];
        tbCzPayInfoPO._setPKValue(id);
        super.ajaxDelete(tbCzPayInfoPO);
    }

    public void batchDeleteTbCzPayInfoPO() {
        super.ajaxBatchDelete(TbCzPayInfoPO.class, ids);
    }

    @JSONOut(catchException = @CatchException(errCode = "1005", successMsg = "查询成功", faileMsg = "查询失败"))
    public void ajaxView() throws Exception, BaseException {
        TbCzPayInfoPO xxPO = payInfoService.searchByPk(TbCzPayInfoPO.class, id);
        addJsonFormateObj("tbCzPayInfoPO", xxPO);// 注意，PO才用addJsonFormateObj，如果是VO，要采用addJsonObj
    }

    public TbCzPayInfoPO getTbCzPayInfoPO() {
        return this.tbCzPayInfoPO;
    }


    @JSONOut(catchException = @CatchException(errCode = "1004", successMsg = "上传成功", faileMsg = "上传失败"))
    public void uploadFile() {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("text/html;charset=UTF-8");
        String reFilePath = "";
        long fileSize = 2 * 1024 * 1024;
        try {
            String fileType = upFileFileName.substring(upFileFileName.lastIndexOf(".") + 1, upFileFileName.length());
            if (!FileUpload.isImagesFile(fileType)) {

                setActionResult("1913", "您上传的文件格式不正确，请重新上传！");
            }

            else if (upFile.length() > fileSize) {

                setActionResult("1913", "您上传的文件太大，请重新上传！");

            } else {

                reFilePath =
                        FileUpload.uploadFileUUID(upFile,
                                ConstConfig.MERCHANT_LOGO_PATH + "/" + DateUtil.formartCurrentDate(), upFileFileName);

                if (!StringUtil.isNullEmpty(reFilePath)) {
                    
                    addJsonObj("url",
                            request.getScheme() + "://" + ConstConfig.PIC_SERVER_NAME + request.getContextPath() + "/"
                                    + reFilePath);
                    addJsonObj("reFilePath", reFilePath);

                } else {

                    setActionResult("1913", "上传失败！");
                }
            }

        } catch (Exception e) {
            logger.error("上传失败", e);
            setActionResult("1913", "上传失败！");
        }
    }

    @JSONOut(catchException = @CatchException(errCode = "1004", successMsg = "操作成功", faileMsg = "操作失败"))
     @ActionRoles("payUpdateStatusAppeal")
     public void payUpdateStatusAppeal() {
        try {
            User user = (User) DqdpAppContext.getCurrentUser();
            // payService.payUpdateStatusCompile(id, user.getUsername());

            TbCzPayPO payPO = payService.searchByPk(TbCzPayPO.class, id);
            payPO.setPayUpdate(new Date());
            payPO.setPayStatus("5");
            payPO.setPayOper(user.getUsername());
            payService.updatePO(payPO, false);

            TbCzPayInfoPO info = new TbCzPayInfoPO();
            info.setPayId(id);
            info.setStatus(5);
            info.setUpdateDate(new Date());
            info.setOper(user.getUsername());
            info.setInfo(this.payDesc);
            info.setPath(this.reFilePath);
            payService.insertPO(info, true);

        } catch (Exception e) {
            logger.error("更新支付订出错", e);
            setActionResult("1913", "更新支付订出错");

        } catch (BaseException e) {
            logger.error("更新支付订出错", e);
            setActionResult("1913", "更新支付订出错");
        }
    }

    @JSONOut(catchException = @CatchException(errCode = "1004", successMsg = "操作成功", faileMsg = "操作失败"))
    @ActionRoles("payUpdateStatusAppeal")
    public void payWapUpdateStatusAppeal() {

        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("text/html;charset=UTF-8");
        String reFilePath = "";
        long fileSize = 2 * 1024 * 1024;
        try {
            String fileType = upFileFileName.substring(upFileFileName.lastIndexOf(".") + 1, upFileFileName.length());
            if (!FileUpload.isImagesFile(fileType)) {

                setActionResult("1913", "您上传的文件格式不正确，请重新上传！");
            }

            else if (upFile.length() > fileSize) {

                setActionResult("1913", "您上传的文件太大，请重新上传！");

            } else {

                reFilePath =
                        FileUpload.uploadFileUUID(upFile,
                                ConstConfig.MERCHANT_LOGO_PATH + "/" + DateUtil.formartCurrentDate(), upFileFileName);

                if (!StringUtil.isNullEmpty(reFilePath)) {

//                    addJsonObj("url",
//                            request.getScheme() + "://" + ConstConfig.PIC_SERVER_NAME + request.getContextPath() + "/"
//                                    + reFilePath);
//                    addJsonObj("reFilePath", reFilePath);

                } else {

                    setActionResult("1913", "上传失败！");
                }
            }

        } catch (Exception e) {
            logger.error("上传失败", e);
            setActionResult("1913", "上传失败！");
        }


        try {
            User user = (User) DqdpAppContext.getCurrentUser();
            // payService.payUpdateStatusCompile(id, user.getUsername());

            TbCzPayPO payPO = payService.searchByPk(TbCzPayPO.class, id);
            payPO.setPayUpdate(new Date());
            payPO.setPayStatus("5");
            payPO.setPayOper(user.getUsername());
            payService.updatePO(payPO, false);

            TbCzPayInfoPO info = new TbCzPayInfoPO();
            info.setPayId(id);
            info.setStatus(5);
            info.setUpdateDate(new Date());
            info.setOper(user.getUsername());
            info.setInfo(this.payDesc);
            info.setPath(reFilePath);
            payService.insertPO(info, true);

        } catch (Exception e) {
            logger.error("申述出错", e);
            setActionResult("1913", "申述出错");

        } catch (BaseException e) {
            logger.error("申述出错", e);
            setActionResult("1913", "申述出错");
        }
    }
}
