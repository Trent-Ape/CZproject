package cn.com.do1.component.pay.pay.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.BaseDAOImpl;
import cn.com.do1.component.merchant.merchant.vo.TbCzMerchantVO;
import cn.com.do1.component.merchant.merchantpay.vo.TbCzMerchantPayVO;
import cn.com.do1.component.pay.pay.dao.IPayDAO;
import cn.com.do1.component.pay.pay.vo.TbCzPayClientVO;
import cn.com.do1.component.pay.pay.vo.TbCzPayVO;
import cn.com.do1.component.util.DecimalFormatUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class PayDAOImpl extends BaseDAOImpl implements IPayDAO {
    private final static transient Logger logger = LoggerFactory.getLogger(PayDAOImpl.class);

    @Override
    public Pager searchPay(Map searchMap, Pager pager) throws Exception, BaseException {

        StringBuffer dataSql = new StringBuffer();
        dataSql.append("SELECT pay.*, ");
        dataSql.append("       NAME, ");
        dataSql.append("       merdise.MER_NAME, ");
        dataSql.append("       usr.USER_NAME ");
        dataSql.append("FROM   tb_cz_pay pay ");
        dataSql.append("       LEFT JOIN tb_cz_merchant mer ");
        dataSql.append("         ON mer.USER_ID = pay.PAY_MERCHAT_ID ");
        dataSql.append("       LEFT JOIN tb_cz_merchandise merdise ");
        dataSql.append("         ON merdise.MER_ID = pay.PAY_MERCHANDISE_ID ");
        dataSql.append("       LEFT JOIN tb_dqdp_user usr ");
        dataSql.append("         ON usr.USER_ID = mer.USER_ID ");
        dataSql.append("         WHERE pay_Id LIKE :payId ");
        dataSql.append("         AND pay_Moblie LIKE :payMoblie ");
        dataSql.append("         AND pay_Merchandise_Id = :payMerchandiseId ");
        dataSql.append("         AND pay_Status = :payStatus ");
        dataSql.append("         AND NAME LIKE :name ");
        dataSql.append("         AND USER_NAME LIKE :userName ");
        dataSql.append("         AND pay_Merchat_Id = :userId ");
        dataSql.append("         AND pay_Start_Date >= :startDate AND pay_Start_Date<= :endDate");

        String countSql =
                "select count(1) from ("
                        + dataSql.toString().replaceAll("(?i)order\\s+by\\s+.[^\\s,]+(,\\s+.[^\\s,]+)*", "") + "  ) a ";

        String orderBy = " ORDER BY PAY_START_DATE desc ";

        return super.pageSearchByField(TbCzPayVO.class, countSql, dataSql.toString() + orderBy, searchMap, pager);
    }

    @Override
    public int getMaxOrder() throws Exception, BaseException {

        super.preparedSql("SELECT RIGHT (max(PAY_ID), " + DecimalFormatUtil.defaultPattern.length()
                + ") FROM tb_cz_pay WHERE LEFT (PAY_ID, 8) = :date");
        super.setPreValue("date", new SimpleDateFormat("yyyyMMdd").format(new Date()));

        return super.executeScalar(Integer.class);
    }

    public TbCzPayVO getTbCzPayByPayID(String id) throws Exception, BaseException {
        StringBuffer dataSql = new StringBuffer();
        dataSql.append("SELECT pay.*, ");
        dataSql.append("       NAME, ");
        dataSql.append("       merdise.MER_NAME, ");
        dataSql.append("       usr.USER_NAME ");
        dataSql.append("FROM   tb_cz_pay pay ");
        dataSql.append("       LEFT JOIN tb_cz_merchant mer ");
        dataSql.append("         ON mer.USER_ID = pay.PAY_MERCHAT_ID ");
        dataSql.append("       LEFT JOIN tb_cz_merchandise merdise ");
        dataSql.append("         ON merdise.MER_ID = pay.PAY_MERCHANDISE_ID ");
        dataSql.append("       LEFT JOIN tb_dqdp_user usr ");
        dataSql.append("         ON usr.USER_ID = mer.USER_ID ");
        dataSql.append("         WHERE pay_Id = :payId ");

        super.preparedSql(dataSql.toString());
        super.setPreValue("payId", id);
        return super.executeQuery(TbCzPayVO.class);
    }

    public TbCzPayClientVO getTbCzPayClientByPayID(String id) throws Exception, BaseException {
        StringBuffer dataSql = new StringBuffer();
        dataSql.append("SELECT pay.*, ");
        dataSql.append("       NAME, ");
        dataSql.append("       merdise.MER_NAME, ");
        dataSql.append("       usr.USER_NAME ");
        dataSql.append("FROM   tb_cz_pay pay ");
        dataSql.append("       LEFT JOIN tb_cz_merchant mer ");
        dataSql.append("         ON mer.USER_ID = pay.PAY_MERCHAT_ID ");
        dataSql.append("       LEFT JOIN tb_cz_merchandise merdise ");
        dataSql.append("         ON merdise.MER_ID = pay.PAY_MERCHANDISE_ID ");
        dataSql.append("       LEFT JOIN tb_dqdp_user usr ");
        dataSql.append("         ON usr.USER_ID = mer.USER_ID ");
        dataSql.append("         WHERE pay_Id = :payId ");

        super.preparedSql(dataSql.toString());
        super.setPreValue("payId", id);
        return super.executeQuery(TbCzPayClientVO.class);
    }

    @Override
    public void updatePayStatus(String payId, String status) throws Exception, BaseException {
        super.preparedSql("update tb_cz_pay p set p.PAY_STATUS =:PAY_STATUS where p.PAY_ID=:PAY_ID");
        super.setPreValue("PAY_ID", payId);
        super.setPreValue("PAY_STATUS", status);
        super.executeUpdate();

    }

}
