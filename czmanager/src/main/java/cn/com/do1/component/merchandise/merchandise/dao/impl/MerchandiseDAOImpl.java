package cn.com.do1.component.merchandise.merchandise.dao.impl;

import java.util.List;
import java.util.Map;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.BaseDAOImpl;
import cn.com.do1.component.merchandise.merchandise.dao.IMerchandiseDAO;
import cn.com.do1.component.merchandise.merchandise.vo.TbCzMerchandiseVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class MerchandiseDAOImpl extends BaseDAOImpl implements IMerchandiseDAO {
    private final static transient Logger logger = LoggerFactory.getLogger(MerchandiseDAOImpl.class);

    public Pager searchMerchandise(Map searchMap, Pager pager) throws Exception, BaseException {
        String dataSql =
                "select  *  from TB_CZ_MERCHANDISE where MER_NAME LIKE :merName and MER_MONEY=:merMoney and MER_STATUS=:merStatus ORDER BY MER_ORDER ";
        String countSQL =
                "select count(1) from (" + dataSql.replaceAll("(?i)order\\s+by\\s+.[^\\s,]+(,\\s+.[^\\s,]+)*", "")
                        + "  ) a ";

        return super.pageSearchByField(TbCzMerchandiseVO.class, countSQL, dataSql, searchMap, pager);
    }

    @Override
    public void updateStatus(String id, String status) throws Exception, BaseException {
        preparedSql("UPDATE TB_CZ_MERCHANDISE SET MER_STATUS=:MER_STATUS where MER_ID=:MER_ID");
        setPreValue("MER_ID", id);
        setPreValue("MER_STATUS", status);
        executeUpdate();

    }

    @Override
    public List<TbCzMerchandiseVO> getMerchandiseByStatus(String status) throws Exception, BaseException {
        preparedSql("SELECT * FROM TB_CZ_MERCHANDISE WHERE  MER_STATUS= :MER_STATUS   ORDER BY MER_ORDER ");
        setPreValue("MER_STATUS", status);
        return getList(TbCzMerchandiseVO.class);
    }

    @Override
    public List<TbCzMerchandiseVO> getTop10UseMerchandise() throws Exception, BaseException {
        preparedSql("SELECT * FROM TB_CZ_MERCHANDISE WHERE  MER_STATUS= :MER_STATUS   ORDER BY MER_ORDER LIMIT 10");
        setPreValue("MER_STATUS", "1");
        return getList(TbCzMerchandiseVO.class);
    }

    @Override
    public List<TbCzMerchandiseVO> getMerchandise() throws Exception, BaseException {
        preparedSql("SELECT * FROM TB_CZ_MERCHANDISE ORDER BY MER_ORDER ");
        return getList(TbCzMerchandiseVO.class);
    }
}
