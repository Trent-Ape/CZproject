package cn.com.do1.component.merchandise.merchandise.service;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.framebase.dqdp.IBaseService;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.component.merchandise.merchandise.vo.TbCzMerchandiseVO;

import java.util.List;
import java.util.Map;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public interface IMerchandiseService extends IBaseService {
    
    Pager searchMerchandise(Map searchMap, Pager pager) throws Exception, BaseException;

    /**
     * 更新状态
     * 
     * @param id
     * @param status
     * @throws Exception
     * @throws BaseException
     */
    void updateStatus(String id, String status) throws Exception, BaseException;

    /**
     * 根据状态查询商品
     * 
     * @param status
     * @return
     * @throws Exception
     * @throws BaseException
     */
    List<TbCzMerchandiseVO> getMerchandiseByStatus(String status) throws Exception, BaseException;

    /**
     * 获取可用商品
     * 
     * @return
     * @throws Exception
     * @throws BaseException
     */
    List<TbCzMerchandiseVO> getUseMerchandise() throws Exception, BaseException;
    

    /**
     * 获取可用10商品
     * 
     * @return
     * @throws Exception
     * @throws BaseException
     */
    List<TbCzMerchandiseVO> getTop10UseMerchandise() throws Exception, BaseException;
    
    /**
     * 获取所有商品
     * 
     * @return
     * @throws Exception
     * @throws BaseException
     */
    List<TbCzMerchandiseVO> getMerchandise() throws Exception, BaseException;

}
