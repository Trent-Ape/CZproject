package cn.com.do1.component.merchandise.merchandise.vo;

import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;
import cn.com.do1.component.txtUtil.annotation.TitleAnnotation;


/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzMerchandiseVO {

    private java.lang.String merId;
    @TitleAnnotation(titleName = "套餐名称")
    private java.lang.String merName;
    @TitleAnnotation(titleName = "套餐价格")
    private java.lang.String merMoney;
    @TitleAnnotation(titleName = "套餐排序")
    private java.lang.String merOrder;
    private java.lang.String merStatus;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    @TitleAnnotation(titleName = "更新时间")
    private java.lang.String merUpdate;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    @TitleAnnotation(titleName = "新增时间")
    private java.lang.String merInsert;
    @TitleAnnotation(titleName = "操作人")
    private java.lang.String merOper;

    @DictDesc(refField = "merStatus", typeName = "merchandiseStatus")
    @TitleAnnotation(titleName = "商品状态")
    private java.lang.String merStatusDesc;

    private java.lang.String merDiscountMoney;//折扣价格

    public void setMerId(java.lang.String merId) {
        this.merId = merId;
    }

    public java.lang.String getMerId() {
        return this.merId;
    }


    public void setMerName(java.lang.String merName) {
        this.merName = merName;
    }

    public java.lang.String getMerName() {
        return this.merName;
    }


    public void setMerMoney(java.lang.String merMoney) {
        this.merMoney = merMoney;
    }

    public java.lang.String getMerMoney() {
        return this.merMoney;
    }


    public void setMerOrder(java.lang.String merOrder) {
        this.merOrder = merOrder;
    }

    public java.lang.String getMerOrder() {
        return this.merOrder;
    }


    public void setMerStatus(java.lang.String merStatus) {
        this.merStatus = merStatus;
    }

    public java.lang.String getMerStatus() {
        return this.merStatus;
    }


    public void setMerUpdate(java.lang.String merUpdate) {
        this.merUpdate = merUpdate;
    }

    public java.lang.String getMerUpdate() {
        return this.merUpdate;
    }

    public java.lang.String getMerStatusDesc() {
        return merStatusDesc;
    }

    public void setMerStatusDesc(java.lang.String merStatusDesc) {
        this.merStatusDesc = merStatusDesc;
    }

    public java.lang.String getMerInsert() {
        return merInsert;
    }

    public void setMerInsert(java.lang.String merInsert) {
        this.merInsert = merInsert;
    }

    public java.lang.String getMerOper() {
        return merOper;
    }

    public void setMerOper(java.lang.String merOper) {
        this.merOper = merOper;
    }

    public java.lang.String getMerDiscountMoney() {
        return merDiscountMoney;
    }

    public void setMerDiscountMoney(java.lang.String merDiscountMoney) {
        this.merDiscountMoney = merDiscountMoney;
    }

    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
