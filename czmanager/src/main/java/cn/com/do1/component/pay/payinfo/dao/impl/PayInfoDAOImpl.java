package cn.com.do1.component.pay.payinfo.dao.impl;

import java.util.Map;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.BaseDAOImpl;
import cn.com.do1.component.pay.pay.vo.TbCzPayVO;
import cn.com.do1.component.pay.payinfo.dao.IPayInfoDAO;
import cn.com.do1.component.pay.payinfo.vo.TbCzPayInfoVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class PayInfoDAOImpl extends BaseDAOImpl implements IPayInfoDAO {
    private final static transient Logger logger = LoggerFactory.getLogger(PayInfoDAOImpl.class);

    @Override
    public Pager searchPayInfo(Map searchMap, Pager pager) throws Exception, BaseException {

        StringBuffer dataSql = new StringBuffer();
        dataSql.append("SELECT *  FROM tb_cz_pay_info  WHERE PAY_ID=:payId");

        String countSql =
                "select count(1) from ("
                        + dataSql.toString().replaceAll("(?i)order\\s+by\\s+.[^\\s,]+(,\\s+.[^\\s,]+)*", "") + "  ) a ";

        String orderBy = " ORDER BY UPDATE_DATE desc ";

        return super.pageSearchByField(TbCzPayInfoVO.class, countSql, dataSql.toString() + orderBy, searchMap, pager);
    }

}
