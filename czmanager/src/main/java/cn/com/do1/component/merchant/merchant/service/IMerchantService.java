package cn.com.do1.component.merchant.merchant.service;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.framebase.dqdp.IBaseService;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.component.merchant.merchant.model.TbCzMerchantPO;
import cn.com.do1.component.merchant.merchant.vo.TbCzMerchantDetailVO;
import cn.com.do1.component.merchant.merchant.vo.TbCzMerchantVO;
import cn.com.do1.component.systemmgr.person.model.PersonVO;
import cn.com.do1.component.systemmgr.user.model.BaseUserVO;
import cn.com.do1.component.systemmgr.user.model.UserVO;

import java.sql.SQLException;
import java.util.Map;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public interface IMerchantService extends IBaseService {
    Pager searchMerchant(Map searchMap, Pager pager) throws Exception, BaseException;

    /**
     * 新增商户信息
     * 
     * @param personvo
     * @param uservo
     * @param TbCzMerchantVO
     * @param map
     * @throws NoSuchFieldException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     * @throws BaseException
     * @throws Exception
     */
    public void addPerson(PersonVO personvo, UserVO uservo, TbCzMerchantVO tbCzMerchantPO, Map map) throws NoSuchFieldException,
            InstantiationException, IllegalAccessException, SQLException, BaseException, Exception;

    /**
     * 删除用户信息
     * 
     * @param ids
     * @throws SQLException
     */
    public void delPerson(String[] ids) throws Exception, BaseException;

    /**
     * 批量删除用户信息
     * 
     * @param ids
     * @throws Exception
     * @throws BaseException
     */
    public void delBaseUserByPersonId(String[] ids) throws Exception, BaseException;

    /**
     * 根据用户名查看商家信息
     * 
     * @param PersonId
     * @return
     * @throws SQLException
     */
    public TbCzMerchantDetailVO getMerchantDetailByPersonId(String PersonId) throws SQLException;

    /**
     * 根据用户名查看商家信息
     * 
     * @param PersonId
     * @return
     * @throws SQLException
     */
    public TbCzMerchantVO getMerchantByPersonId(String PersonId) throws SQLException;

    /**
     * 根据用户名查看商家信息
     * 
     * @param PersonId
     * @return
     * @throws SQLException
     */
    public TbCzMerchantVO getMerchantByUserID(String UserID) throws SQLException;
    
    /**
     * 根据用户名查看商家信息
     * 
     * @param PersonId
     * @return
     * @throws SQLException
     */
    public TbCzMerchantVO getMerchantByUserAccount(String UserAccount) throws SQLException;

    /**
     * 删除商户信息
     * 
     * @param PersonId
     * @throws SQLException
     */
    public void delMerchantByPersonId(String PersonId) throws SQLException;

    /**
     * 更新商户信息
     * 
     * @param paramBaseUserVO
     * @param tbCzMerchantPO
     * @param paramMap
     * @throws Exception
     * @throws BaseException
     */
    public void updateBaseUser(BaseUserVO paramBaseUserVO, TbCzMerchantVO merchantVO, Map paramMap) throws Exception, BaseException;
    
    /**
     * 更新商户信息
     * 
     * @param paramBaseUserVO
     * @param tbCzMerchantPO
     * @param paramMap
     * @throws Exception
     * @throws BaseException
     */
    public void updateAdminBaseUser(BaseUserVO paramBaseUserVO, TbCzMerchantVO merchantVO, Map paramMap) throws Exception, BaseException;

    /**
     * 修改支付密码
     * 
     * @param userId
     * @param newPassword
     * @throws BaseException
     * @throws Exception
     */
    public void changePayPassword(String userId, String newPassword) throws BaseException, Exception;
    
    /**
     * 修改支付密码
     * 
     * @param userId
     * @param newPassword
     * @throws BaseException
     * @throws Exception
     */
    public void changePayClient(String userId, String newPassword) throws BaseException, Exception;

    /**
     * 根据机构查询商户信息
     * 
     * @param paramString
     * @param paramPager
     * @param paramMap
     * @return
     * @throws Exception
     * @throws BaseException
     */
    public Pager listPersonByOrg(String paramString, Pager paramPager, Map paramMap) throws Exception, BaseException;

    /**
     * 减少金额
     * 
     * @param userId
     * @param money
     * @return
     * @throws SQLException
     */
    public boolean minMoney(String userId, float money) throws SQLException;
    
    public boolean addMoney(String userId, float money) throws SQLException;
}
