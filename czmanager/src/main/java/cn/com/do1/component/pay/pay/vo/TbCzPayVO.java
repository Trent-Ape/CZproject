package cn.com.do1.component.pay.pay.vo;

import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;
import cn.com.do1.component.txtUtil.annotation.TitleAnnotation;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzPayVO {
    @TitleAnnotation(titleName = "订单号")
    private java.lang.String payId;
    private java.lang.String payMerchandiseId;
    @TitleAnnotation(titleName = "套餐名称")
    private java.lang.String merName;
    private java.lang.String payMerchatId;
    @TitleAnnotation(titleName = "代理商名称")
    private java.lang.String name;
    @TitleAnnotation(titleName = "代理商账号")
    private java.lang.String userName;
    @TitleAnnotation(titleName = "充值手机")
    private java.lang.String payMoblie;
    @TitleAnnotation(titleName = "套餐原价")
    private java.lang.String payOriginalMoney;
    @TitleAnnotation(titleName = "折扣价格")
    private java.lang.String payDiscountMoney;
    private java.lang.String payStatus;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    @TitleAnnotation(titleName = "充值请求时间")
    private java.lang.String payStartDate;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    @TitleAnnotation(titleName = "充值完成时间")
    private java.lang.String payEndDate;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    @TitleAnnotation(titleName = "更新时间")
    private java.lang.String payUpdate;
    @TitleAnnotation(titleName = "操作人")
    private java.lang.String payOper;
    @TitleAnnotation(titleName = "操作描述")
    private java.lang.String payOperDesc;

    @DictDesc(refField = "payStatus", typeName = "payStatus")
    @TitleAnnotation(titleName = "充值状态")
    private java.lang.String payStatusDesc;

    @DictDesc(refField = "payStatus", typeName = "payStatus")
    private java.lang.String payOperDesc1;


    public java.lang.String getPayId() {
        return payId;
    }



    public void setPayId(java.lang.String payId) {
        this.payId = payId;
    }



    public java.lang.String getPayMerchandiseId() {
        return payMerchandiseId;
    }



    public void setPayMerchandiseId(java.lang.String payMerchandiseId) {
        this.payMerchandiseId = payMerchandiseId;
    }



    public java.lang.String getMerName() {
        return merName;
    }



    public void setMerName(java.lang.String merName) {
        this.merName = merName;
    }



    public java.lang.String getPayMerchatId() {
        return payMerchatId;
    }



    public void setPayMerchatId(java.lang.String payMerchatId) {
        this.payMerchatId = payMerchatId;
    }



    public java.lang.String getName() {
        return name;
    }



    public void setName(java.lang.String name) {
        this.name = name;
    }



    public java.lang.String getPayMoblie() {
        return payMoblie;
    }



    public void setPayMoblie(java.lang.String payMoblie) {
        this.payMoblie = payMoblie;
    }



    public java.lang.String getPayOriginalMoney() {
        return payOriginalMoney;
    }



    public void setPayOriginalMoney(java.lang.String payOriginalMoney) {
        this.payOriginalMoney = payOriginalMoney;
    }



    public java.lang.String getPayDiscountMoney() {
        return payDiscountMoney;
    }



    public void setPayDiscountMoney(java.lang.String payDiscountMoney) {
        this.payDiscountMoney = payDiscountMoney;
    }



    public java.lang.String getPayStatus() {
        return payStatus;
    }



    public void setPayStatus(java.lang.String payStatus) {
        this.payStatus = payStatus;
    }



    public java.lang.String getPayStartDate() {
        return payStartDate;
    }



    public void setPayStartDate(java.lang.String payStartDate) {
        this.payStartDate = payStartDate;
    }



    public java.lang.String getPayEndDate() {
        return payEndDate;
    }



    public void setPayEndDate(java.lang.String payEndDate) {
        this.payEndDate = payEndDate;
    }



    public java.lang.String getPayUpdate() {
        return payUpdate;
    }



    public void setPayUpdate(java.lang.String payUpdate) {
        this.payUpdate = payUpdate;
    }



    public java.lang.String getPayOper() {
        return payOper;
    }



    public void setPayOper(java.lang.String payOper) {
        this.payOper = payOper;
    }



    public java.lang.String getPayOperDesc() {
        return payOperDesc;
    }



    public void setPayOperDesc(java.lang.String payOperDesc) {
        this.payOperDesc = payOperDesc;
    }



    public java.lang.String getPayStatusDesc() {
        return payStatusDesc;
    }



    public void setPayStatusDesc(java.lang.String payStatusDesc) {
        this.payStatusDesc = payStatusDesc;
    }



    public java.lang.String getPayOperDesc1() {
        return payOperDesc1;
    }



    public void setPayOperDesc1(java.lang.String payOperDesc1) {
        this.payOperDesc1 = payOperDesc1;
    }



    public java.lang.String getUserName() {
        return userName;
    }



    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }



    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
