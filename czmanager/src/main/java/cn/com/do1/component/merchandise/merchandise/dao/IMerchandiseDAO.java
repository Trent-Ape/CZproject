package cn.com.do1.component.merchandise.merchandise.dao;
import java.util.List;
import java.util.Map;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.IBaseDAO;
import cn.com.do1.component.merchandise.merchandise.vo.TbCzMerchandiseVO;

/**
* Copyright &copy; 2010 广州市道一信息技术有限公司
* All rights reserved.
* User: ${user}
*/
public interface IMerchandiseDAO extends IBaseDAO {
    
    public Pager searchMerchandise(Map searchMap, Pager pager) throws Exception, BaseException;
    
    /**
     * 更新状态
     * 
     * @param id
     * @param status
     * @throws Exception
     * @throws BaseException
     */
    void updateStatus(String id, String status) throws Exception, BaseException;

    /**
     * 根据状态查询商品
     * 
     * @param status
     * @return
     * @throws Exception
     * @throws BaseException
     */
    List<TbCzMerchandiseVO> getMerchandiseByStatus(String status) throws Exception, BaseException;

    /**
     * 获取可用10商品
     * 
     * @return
     * @throws Exception
     * @throws BaseException
     */
    List<TbCzMerchandiseVO> getTop10UseMerchandise() throws Exception, BaseException;
    
    public List<TbCzMerchandiseVO> getMerchandise() throws Exception, BaseException;

}
