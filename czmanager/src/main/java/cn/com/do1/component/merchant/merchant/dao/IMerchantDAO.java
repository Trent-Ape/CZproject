package cn.com.do1.component.merchant.merchant.dao;

import java.sql.SQLException;
import java.util.Map;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.IBaseDAO;
import cn.com.do1.component.merchant.merchant.vo.TbCzMerchantDetailVO;
import cn.com.do1.component.merchant.merchant.vo.TbCzMerchantVO;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public interface IMerchantDAO extends IBaseDAO {

    public Pager searchMerchant(Map searchMap, Pager pager) throws Exception, BaseException;

    public TbCzMerchantVO getMerchantByPersonId(String PersonId) throws SQLException;

    public TbCzMerchantVO getMerchantByUserID(String UserID) throws SQLException;
    
    /**
     * 根据用户名查看商家信息
     * 
     * @param PersonId
     * @return
     * @throws SQLException
     */
    public TbCzMerchantVO getMerchantByUserAccount(String UserAccount) throws SQLException;

    public TbCzMerchantDetailVO getMerchantDetailByPersonId(String PersonId) throws SQLException;

    public void delMerchantByPersonId(String PersonId) throws SQLException;

    public Pager listPersonByOrg(String orgId, Pager pager, Map searchValue) throws Exception, BaseException;

    public boolean updateMoney(String userId, float money) throws SQLException;

    public boolean addMoney(String userId, float money) throws SQLException;

    public boolean minMoney(String userId, float money) throws SQLException;

}
