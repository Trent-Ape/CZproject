package cn.com.do1.component.merchant.merchant.service.impl;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.BaseService;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.common.util.AssertUtil;
import cn.com.do1.common.util.reflation.BeanHelper;
import cn.com.do1.common.util.reflation.ClassTypeUtil;
import cn.com.do1.common.util.security.SecurityUtil;
import cn.com.do1.component.merchant.merchant.dao.*;
import cn.com.do1.component.merchant.merchant.model.TbCzMerchantPO;
import cn.com.do1.component.merchant.merchant.service.IMerchantService;
import cn.com.do1.component.merchant.merchant.vo.TbCzMerchantDetailVO;
import cn.com.do1.component.merchant.merchant.vo.TbCzMerchantVO;
import cn.com.do1.component.systemmgr.person.dao.IPersonDAO;
import cn.com.do1.component.systemmgr.person.model.PersonVO;
import cn.com.do1.component.systemmgr.person.model.TbDqdpPersonPO;
import cn.com.do1.component.systemmgr.person.service.IPersonService;
import cn.com.do1.component.systemmgr.user.model.BaseUserVO;
import cn.com.do1.component.systemmgr.user.model.TbDqdpUserPO;
import cn.com.do1.component.systemmgr.user.model.UserVO;
import cn.com.do1.component.systemmgr.user.service.IUserService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
@Service("merchantService")
public class MerchantServiceImpl extends BaseService implements IMerchantService {
    private final static transient Logger logger = LoggerFactory.getLogger(MerchantServiceImpl.class);

    private IMerchantDAO merchantDAO;

    private IPersonService myPersonService;

    @Resource
    private IUserService userService;

    @Resource
    private IPersonDAO personDAO;

    @Resource
    public void setMyPersonService(IPersonService myPersonService) {
        this.myPersonService = myPersonService;
    }

    @Resource
    public void setMerchantDAO(IMerchantDAO merchantDAO) {
        this.merchantDAO = merchantDAO;
        setDAO(merchantDAO);
    }

    public Pager searchMerchant(Map searchMap, Pager pager) throws Exception, BaseException {
        return merchantDAO.searchMerchant(searchMap, pager);
    }

    @Override
    public void addPerson(PersonVO personvo, UserVO uservo, TbCzMerchantVO merchantVO, Map map)
            throws NoSuchFieldException, InstantiationException, IllegalAccessException, SQLException, BaseException,
            Exception {

        uservo.setUserId(UUID.randomUUID().toString());
        myPersonService.addPerson(personvo, uservo, map);

        // 添加额外商家信息
        merchantVO.setUserId(uservo.getUserId());
        merchantVO.setName(personvo.getPersonName());

        if (!AssertUtil.isEmpty(merchantVO.getPayPassword())) {
            this.userService.validatePassword(merchantVO.getPayPassword());
            merchantVO.setPayPassword(SecurityUtil.getMd5Code(merchantVO.getPayPassword()));
        }

        TbCzMerchantPO tbCzMerchantPO = ClassTypeUtil.getAutoReplaceBeanInstance(TbCzMerchantPO.class);
        BeanHelper.copyFormatProperties(tbCzMerchantPO, merchantVO, false);
        if (AssertUtil.isEmpty(tbCzMerchantPO.getDiscount())) {
            tbCzMerchantPO.setDiscount(1f);
        } else {
            tbCzMerchantPO.setDiscount(tbCzMerchantPO.getDiscount() / 100);
        }
        if (AssertUtil.isEmpty(tbCzMerchantPO.getMoney())) {
            tbCzMerchantPO.setMoney(0f);
        }
        tbCzMerchantPO.setIsClient(0);
        super.insertPO(tbCzMerchantPO, true);

    }

    @Override
    public TbCzMerchantDetailVO getMerchantDetailByPersonId(String PersonId) throws SQLException {
        return merchantDAO.getMerchantDetailByPersonId(PersonId);
    }

    @Override
    public TbCzMerchantVO getMerchantByPersonId(String PersonId) throws SQLException {
        return merchantDAO.getMerchantByPersonId(PersonId);
    }

    @Override
    public TbCzMerchantVO getMerchantByUserID(String UserID) throws SQLException {
        return merchantDAO.getMerchantByUserID(UserID);
    }

    /**
     * 根据用户名查看商家信息
     * 
     * @param PersonId
     * @return
     * @throws SQLException
     */
    public TbCzMerchantVO getMerchantByUserAccount(String UserAccount) throws SQLException {
        return merchantDAO.getMerchantByUserAccount(UserAccount);
    }

    @Override
    public void delPerson(String[] ids) throws Exception, BaseException {

        this.myPersonService.delPerson(ids);

        for (String personId : ids) {
            delMerchantByPersonId(personId);
        }
    }

    @Override
    public void delMerchantByPersonId(String PersonId) throws SQLException {
        merchantDAO.delMerchantByPersonId(PersonId);

    }

    @Override
    public void delBaseUserByPersonId(String[] ids) throws Exception, BaseException {

        for (String personId : ids) {
            delMerchantByPersonId(personId);
        }

        this.myPersonService.delBaseUserByPersonId(ids);

    }

    @Override
    public void updateBaseUser(BaseUserVO paramBaseUserVO, TbCzMerchantVO merchantVO, Map paramMap) throws Exception,
            BaseException {
        // this.myPersonService.updateBaseUser(paramBaseUserVO, paramMap);

        TbDqdpPersonPO personPO = (TbDqdpPersonPO) ClassTypeUtil.getAutoReplaceBeanInstance(TbDqdpPersonPO.class);
        BeanHelper.copyFormatProperties(personPO, paramBaseUserVO, false);
        updatePO(personPO, false);

        TbCzMerchantPO tbCzMerchantPO = ClassTypeUtil.getAutoReplaceBeanInstance(TbCzMerchantPO.class);
        BeanHelper.copyFormatProperties(tbCzMerchantPO, merchantVO, false);
        tbCzMerchantPO.setName(paramBaseUserVO.getPersonName());
        if (!AssertUtil.isEmpty(tbCzMerchantPO.getDiscount())) {
            tbCzMerchantPO.setDiscount(tbCzMerchantPO.getDiscount() / 100);
        }

        super.updatePO(tbCzMerchantPO, false);

    }

    @Override
    public void updateAdminBaseUser(BaseUserVO paramBaseUserVO, TbCzMerchantVO merchantVO, Map paramMap)
            throws Exception, BaseException {
        this.myPersonService.updateBaseUser(paramBaseUserVO, paramMap);

        TbCzMerchantPO tbCzMerchantPO = ClassTypeUtil.getAutoReplaceBeanInstance(TbCzMerchantPO.class);
        BeanHelper.copyFormatProperties(tbCzMerchantPO, merchantVO, false);
        tbCzMerchantPO.setName(paramBaseUserVO.getPersonName());
        if (!AssertUtil.isEmpty(tbCzMerchantPO.getDiscount())) {
            tbCzMerchantPO.setDiscount(tbCzMerchantPO.getDiscount() / 100);
        }

        super.updatePO(tbCzMerchantPO, false);

    }

    public void changePayPassword(String userId, String newPassword) throws BaseException, Exception {
        if (AssertUtil.isEmpty(userId)) {
            throw new BaseException("用户为空");
        }
        if (AssertUtil.isEmpty(newPassword)) {
            throw new BaseException("新密码为空");
        }
        TbCzMerchantVO tbCzMerchantVO = this.getMerchantByUserID(userId);
        if (AssertUtil.isEmpty(tbCzMerchantVO)) {
            throw new BaseException("用户不存在");
        }
        this.userService.validatePassword(newPassword);

        tbCzMerchantVO.setPayPassword(SecurityUtil.getMd5Code(newPassword));

        TbCzMerchantPO tbCzMerchantPO = ClassTypeUtil.getAutoReplaceBeanInstance(TbCzMerchantPO.class);
        BeanHelper.copyFormatProperties(tbCzMerchantPO, tbCzMerchantVO, false);

        updatePO(tbCzMerchantPO, false);
    }

    /**
     * 修改支付密码
     * 
     * @param userId
     * @param newPassword
     * @throws BaseException
     * @throws Exception
     */
    public void changePayClient(String userId, String newPassword) throws BaseException, Exception {
        if (AssertUtil.isEmpty(userId)) {
            throw new BaseException("用户为空");
        }
        if (AssertUtil.isEmpty(newPassword)) {
            throw new BaseException("状态为空");
        }
        TbCzMerchantVO tbCzMerchantVO = this.getMerchantByUserID(userId);
        if (AssertUtil.isEmpty(tbCzMerchantVO)) {
            throw new BaseException("用户不存在");
        }
        tbCzMerchantVO.setIsClient(newPassword);

        TbCzMerchantPO tbCzMerchantPO = ClassTypeUtil.getAutoReplaceBeanInstance(TbCzMerchantPO.class);
        BeanHelper.copyFormatProperties(tbCzMerchantPO, tbCzMerchantVO, false);

        updatePO(tbCzMerchantPO, false);
    }

    public Pager listPersonByOrg(String orgId, Pager pager, Map searchValue) throws Exception, BaseException {
        return this.merchantDAO.listPersonByOrg(orgId, pager, searchValue);
    }

    public boolean minMoney(String userId, float money) throws SQLException {
        return this.merchantDAO.minMoney(userId, money);
    }

    public boolean addMoney(String userId, float money) throws SQLException {
        return this.merchantDAO.addMoney(userId, money);
    }
}
