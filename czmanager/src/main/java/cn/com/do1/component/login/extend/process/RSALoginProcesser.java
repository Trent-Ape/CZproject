package cn.com.do1.component.login.extend.process;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import cn.com.do1.common.annotation.reger.ProcesserUnit;
import cn.com.do1.dqdp.core.permission.ILoginProcesser;

@ProcesserUnit(name = "RSALoginProcesser", order = -1)
public class RSALoginProcesser implements ILoginProcesser {
    private transient final static Logger logger = LoggerFactory.getLogger(RSALoginProcesser.class);

    @Override
    public void afterProcess(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse,
            Authentication authentication) throws AuthenticationException {}

    @Override
    public void exceptionProcess(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse,
            Throwable throwable) {

    }

    @Override
    public void preProcess(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse)
            throws AuthenticationException {

        String randomCode = httpservletrequest.getParameter("j_code");

        String sessionCode = (String) httpservletrequest.getSession().getAttribute("RandString");

        if (randomCode == null || !randomCode.equalsIgnoreCase(sessionCode)) {
            logger.debug("------->>验证码错误");
            throw new BadCredentialsException("验证码错误");
        }

    }
}
