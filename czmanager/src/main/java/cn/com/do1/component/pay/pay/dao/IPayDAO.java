package cn.com.do1.component.pay.pay.dao;

import java.util.Map;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.IBaseDAO;
import cn.com.do1.component.pay.pay.vo.TbCzPayClientVO;
import cn.com.do1.component.pay.pay.vo.TbCzPayVO;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public interface IPayDAO extends IBaseDAO {

    public Pager searchPay(Map searchMap, Pager pager) throws Exception, BaseException;

    public int getMaxOrder() throws Exception, BaseException;

    public void updatePayStatus(String payId, String status) throws Exception, BaseException;

    public TbCzPayVO getTbCzPayByPayID(String id) throws Exception, BaseException;

    public TbCzPayClientVO getTbCzPayClientByPayID(String id) throws Exception, BaseException;

}
