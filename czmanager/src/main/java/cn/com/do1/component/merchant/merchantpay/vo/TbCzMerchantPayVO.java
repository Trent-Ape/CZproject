package cn.com.do1.component.merchant.merchantpay.vo;

import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;
import cn.com.do1.component.txtUtil.annotation.TitleAnnotation;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class TbCzMerchantPayVO {
    private java.lang.String tbMerchatPayId;
    @TitleAnnotation(titleName = "平台充值订单号")
    private java.lang.String tbMerchatPayUserid;
    @TitleAnnotation(titleName = "充值金额")
    private java.lang.String tbMerchatPayMoney;
    private java.lang.String tbMerchatPayStatus;
    @TitleAnnotation(titleName = "充值时间")
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    private java.lang.String tbMerchatPayInsert;
    @FormatMask(type = "date", value = "yyyy-MM-dd HH:mm:ss")
    private java.lang.String tbMerchatPayUpdate;
    @TitleAnnotation(titleName = "操作人")
    private java.lang.String tbMerchatPayOper;
    private java.lang.String userId;
    @TitleAnnotation(titleName = "充值账户名称")
    private java.lang.String userName;

    @DictDesc(refField = "tbMerchatPayStatus", typeName = "payStatus")
    @TitleAnnotation(titleName = "充值状态")
    private java.lang.String tbMerchatPayStatusDesc;


    public java.lang.String getTbMerchatPayId() {
        return tbMerchatPayId;
    }

    public void setTbMerchatPayId(java.lang.String tbMerchatPayId) {
        this.tbMerchatPayId = tbMerchatPayId;
    }

    public java.lang.String getTbMerchatPayUserid() {
        return tbMerchatPayUserid;
    }

    public void setTbMerchatPayUserid(java.lang.String tbMerchatPayUserid) {
        this.tbMerchatPayUserid = tbMerchatPayUserid;
    }

    public java.lang.String getTbMerchatPayMoney() {
        return tbMerchatPayMoney;
    }

    public void setTbMerchatPayMoney(java.lang.String tbMerchatPayMoney) {
        this.tbMerchatPayMoney = tbMerchatPayMoney;
    }

    public java.lang.String getTbMerchatPayStatus() {
        return tbMerchatPayStatus;
    }

    public void setTbMerchatPayStatus(java.lang.String tbMerchatPayStatus) {
        this.tbMerchatPayStatus = tbMerchatPayStatus;
    }

    public java.lang.String getTbMerchatPayInsert() {
        return tbMerchatPayInsert;
    }

    public void setTbMerchatPayInsert(java.lang.String tbMerchatPayInsert) {
        this.tbMerchatPayInsert = tbMerchatPayInsert;
    }

    public java.lang.String getTbMerchatPayUpdate() {
        return tbMerchatPayUpdate;
    }

    public void setTbMerchatPayUpdate(java.lang.String tbMerchatPayUpdate) {
        this.tbMerchatPayUpdate = tbMerchatPayUpdate;
    }

    public java.lang.String getTbMerchatPayOper() {
        return tbMerchatPayOper;
    }

    public void setTbMerchatPayOper(java.lang.String tbMerchatPayOper) {
        this.tbMerchatPayOper = tbMerchatPayOper;
    }

    public java.lang.String getUserId() {
        return userId;
    }

    public void setUserId(java.lang.String userId) {
        this.userId = userId;
    }

    public java.lang.String getUserName() {
        return userName;
    }

    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }

    public java.lang.String getTbMerchatPayStatusDesc() {
        return tbMerchatPayStatusDesc;
    }

    public void setTbMerchatPayStatusDesc(java.lang.String tbMerchatPayStatusDesc) {
        this.tbMerchatPayStatusDesc = tbMerchatPayStatusDesc;
    }

    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this,
                org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
