package cn.com.do1.component.pay.payinfo.model;

import cn.com.do1.common.annotation.bean.PageView;
import cn.com.do1.common.annotation.bean.Validation;
import cn.com.do1.common.framebase.dqdp.IBaseDBVO;
import cn.com.do1.common.util.reflation.ConvertUtil;
import cn.com.do1.common.annotation.bean.DictDesc;
import cn.com.do1.common.annotation.bean.FormatMask;

import java.util.Date;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司
 * All rights reserved.
 * User: ${user}
 */
public class TbCzPayInfoPO implements IBaseDBVO {
    private java.lang.String id;
    @Validation(must = false, length = 36, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "payId", showType = "input", showOrder = 1, showLength = 36)
    private java.lang.String payId;
    @Validation(must = false, length = 1, fieldType = "pattern", regex = "^\\d*$")
    @PageView(showName = "status", showType = "input", showOrder = 2, showLength = 1)
    private java.lang.Integer status;
    @Validation(must = false, length = 200, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "info", showType = "input", showOrder = 3, showLength = 200)
    private java.lang.String info;
    @Validation(must = false, length = 200, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "path", showType = "input", showOrder = 4, showLength = 200)
    private java.lang.String path;
    @Validation(must = false, length = 200, fieldType = "pattern", regex = "^.*$")
    @PageView(showName = "oper", showType = "input", showOrder = 5, showLength = 200)
    private java.lang.String oper;
    @Validation(must = false, length = 19, fieldType = "datetime", regex = "")
    @PageView(showName = "updateDate", showType = "datetime", showOrder = 6, showLength = 19)
    private java.util.Date updateDate;

    public void setId(java.lang.String id) {
        this.id = id;
    }

    public java.lang.String getId() {
        return this.id;
    }


    public void setPayId(java.lang.String payId) {
        this.payId = payId;
    }

    public java.lang.String getPayId() {
        return this.payId;
    }


    public void setStatus(java.lang.Integer status) {
        this.status = status;
    }

    public java.lang.Integer getStatus() {
        return this.status;
    }


    public void setInfo(java.lang.String info) {
        this.info = info;
    }

    public java.lang.String getInfo() {
        return this.info;
    }


    public void setPath(java.lang.String path) {
        this.path = path;
    }

    public java.lang.String getPath() {
        return this.path;
    }


    public void setOper(java.lang.String oper) {
        this.oper = oper;
    }

    public java.lang.String getOper() {
        return this.oper;
    }


    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setUpdateDate(java.lang.String updateDate) {
        this.updateDate = ConvertUtil.cvStUtildate(updateDate);
    }

    public java.util.Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * 获取数据库中对应的表名
     *
     * @return
     */
    public String _getTableName() {
        return "TB_CZ_PAY_INFO";
    }

    /**
     * 获取对应表的主键字段名称
     *
     * @return
     */
    public String _getPKColumnName() {
        return "id";
    }

    /**
     * 获取主键值
     *
     * @return
     */
    public String _getPKValue() {
        return String.valueOf(id);
    }

    /**
     * 设置主键的值
     *
     * @return
     */
    public void _setPKValue(Object value) {
        this.id = (java.lang.String) value;
    }

    /**
     * 重写默认的toString方法，使其调用输出的内容更有意义
     */
    public String toString() {
        return new org.apache.commons.lang3.builder.ReflectionToStringBuilder(this, org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE).toString();
    }
}
