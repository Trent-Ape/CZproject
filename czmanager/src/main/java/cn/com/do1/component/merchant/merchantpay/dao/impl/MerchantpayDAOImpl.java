package cn.com.do1.component.merchant.merchantpay.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import cn.com.do1.common.dac.Pager;
import cn.com.do1.common.exception.BaseException;
import cn.com.do1.common.framebase.dqdp.BaseDAOImpl;
import cn.com.do1.common.util.DateUtil;
import cn.com.do1.component.merchant.merchant.vo.TbCzMerchantVO;
import cn.com.do1.component.merchant.merchantpay.dao.IMerchantpayDAO;
import cn.com.do1.component.merchant.merchantpay.vo.TbCzMerchantPayVO;
import cn.com.do1.component.util.DecimalFormatUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright &copy; 2010 广州市道一信息技术有限公司 All rights reserved. User: ${user}
 */
public class MerchantpayDAOImpl extends BaseDAOImpl implements IMerchantpayDAO {
    private final static transient Logger logger = LoggerFactory.getLogger(MerchantpayDAOImpl.class);

    @Override
    public Pager searchMerchantpay(Map searchMap, Pager pager) throws Exception, BaseException {

        StringBuffer dataSql = new StringBuffer();
        dataSql.append("SELECT pay.*, ");
        dataSql.append("       u.USER_ID, ");
        dataSql.append("       u.USER_NAME ");
        dataSql.append("FROM   tb_cz_merchant_pay pay ");
        dataSql.append("       JOIN tb_dqdp_user u ");
        dataSql.append("         ON u.user_id = pay.TB_MERCHAT_PAY_USERID ");
        dataSql.append("         WHERE pay.TB_MERCHAT_PAY_STATUS = :tbMerchatPayStatus ");
        dataSql.append("         AND u.USER_NAME LIKE :userName ");

        String orderBy = "ORDER  BY TB_MERCHAT_PAY_INSERT desc ";


        String countSQL =
                "select count(1) from (" + dataSql.toString().replaceAll("(?i)order\\s+by\\s+.[^\\s,]+(,\\s+.[^\\s,]+)*", "") + "  ) a ";

        return super.pageSearchByField(TbCzMerchantPayVO.class, countSQL, dataSql.toString() + orderBy, searchMap, pager);
    }

    @Override
    public Pager searchMyMerchantpay(Map searchMap, Pager pager) throws Exception, BaseException {

        StringBuffer dataSql = new StringBuffer();
        dataSql.append("SELECT pay.*, ");
        dataSql.append("       u.USER_ID, ");
        dataSql.append("       u.USER_NAME ");
        dataSql.append("FROM   tb_cz_merchant_pay pay ");
        dataSql.append("       JOIN tb_dqdp_user u ");
        dataSql.append("         ON u.user_id = pay.TB_MERCHAT_PAY_USERID ");
        dataSql.append("       JOIN tb_person_user_ref person ");
        dataSql.append("         ON u.user_id = person.USER_ID ");
        dataSql.append("         WHERE pay.TB_MERCHAT_PAY_STATUS = :tbMerchatPayStatus ");
        dataSql.append("         AND u.USER_NAME LIKE :userName ");
        dataSql.append("         AND person.PERSON_ID = :personId ");

        String orderBy = "ORDER  BY TB_MERCHAT_PAY_INSERT desc ";

        String countSQL =
                "select count(1) from (" + dataSql.toString().replaceAll("(?i)order\\s+by\\s+.[^\\s,]+(,\\s+.[^\\s,]+)*", "") + "  ) a ";

        return super.pageSearchByField(TbCzMerchantPayVO.class, countSQL, dataSql.toString() + orderBy, searchMap, pager);
    }

    @Override
    public int getMaxOrder() throws Exception, BaseException {

        super.preparedSql("SELECT RIGHT (max(TB_MERCHAT_PAY_ID), " + DecimalFormatUtil.defaultPattern.length()
                + ") FROM tb_cz_merchant_pay WHERE LEFT (TB_MERCHAT_PAY_ID, 8) = :date");
        super.setPreValue("date", new SimpleDateFormat("yyyyMMdd").format(new Date()));

        return super.executeScalar(Integer.class);
    }

}
